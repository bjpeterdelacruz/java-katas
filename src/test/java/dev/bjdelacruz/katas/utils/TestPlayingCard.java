/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import org.junit.jupiter.api.Test;

/**
 * Unit tests for the {@link PlayingCard} class.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestPlayingCard {

  /** Tests the creation of a new playing card. */
  @Test
  public void testNewCard() {
    var card = new PlayingCard("AD", false);
    assertThat(card.getCard()).isEqualTo("AD");
    assertThat(card.getCardValue()).isEqualTo('A');
    assertThat(card.getCardSuit()).isEqualTo('D');
    assertThat(card.isCardFacingDown()).isFalse();

    card.flipCardOver();
    assertThat(card.isCardFacingDown()).isTrue();
    card.flipCardOver();
    assertThat(card.isCardFacingDown()).isFalse();

    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> new PlayingCard(null, false))
            .withMessageContainingAll("Card value is null or empty string.");

    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> new PlayingCard("", false))
            .withMessageContainingAll("Card value is null or empty string.");

    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> new PlayingCard("A", false))
            .withMessageContainingAll("Card value must be of length 2.");

    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> new PlayingCard("BD", false))
            .withMessageContainingAll("Invalid card value: BD");

    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> new PlayingCard("0D", false))
            .withMessageContainingAll("Invalid card value: 0D");

    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> new PlayingCard("5J", false))
            .withMessageContainingAll("Invalid card value: 5J");
  }

}
