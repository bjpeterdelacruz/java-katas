/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.utils;

import static org.assertj.core.api.Assertions.assertThat;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Unit tests for the {@link KataUtils} utility class.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestKataUtils {

  private Path tmp;

  /**
   * Creates a temporary file for the unit tests.
   * 
   * @throws IOException If there are problems creating the file.
   */
  @BeforeEach
  public void init() throws IOException {
    tmp = Files.createTempFile("temp", ".tmp");
  }

  /**
   * Deletes the temporary file.
   * 
   * @throws IOException If there are problems deleting the file.
   */
  @AfterEach
  public void cleanup() throws IOException {
    Files.delete(tmp);
  }

  /**
   * Tests the {@link KataUtils#readLines} method.
   * 
   * @throws IOException If there are problems writing and reading to file.
   */
  @Test
  public void testReadLines() throws IOException {
    var lines = "4\n4 2 3 1\n1 3 2 4\n3 2 1 4\n2 3 4 1\nKongou\nHiei\nHaruna\nKirishima";
    try (var out = new PrintWriter(tmp.toFile(), StandardCharsets.UTF_8)) {
      out.println(lines);
    }

    var list = KataUtils.readLines(tmp.toFile().getAbsolutePath());

    var array = lines.split("\n");

    for (var idx = 0; idx < list.size(); idx++) {
      assertThat(array[idx]).isEqualTo(list.get(idx));
    }
  }

  /** Tests the {@link KataUtils#stringsListToString(List)} method. */
  @Test
  public void testStringsListToList() {
    assertThat(KataUtils.stringsListToString(null)).isEmpty();
    assertThat(KataUtils.stringsListToString(new ArrayList<>())).isEmpty();

    var list = Arrays.asList("Kongou", "Hiei", "Haruna", "Kirishima");

    assertThat(KataUtils.stringsListToString(list)).isEqualTo("Kongou Hiei Haruna Kirishima");
  }

  /** Tests the {@link KataUtils#getBalanceAsString} method. */
  @Test
  public void testGetBalanceAsString() {
    assertThat(KataUtils.getBalanceAsString(2)).isEqualTo("0.02");
    assertThat(KataUtils.getBalanceAsString(12)).isEqualTo("0.12");
    assertThat(KataUtils.getBalanceAsString(-2)).isEqualTo("-0.02");
    assertThat(KataUtils.getBalanceAsString(-12)).isEqualTo("-.12");
    assertThat(KataUtils.getBalanceAsString(123)).isEqualTo("1.23");
    assertThat(KataUtils.getBalanceAsString(-123)).isEqualTo("-1.23");
  }

  /** Tests the {@link KataUtils#getAllPermutationsOfSubsequences} method. */
  @Test
  public void testGetAllPermutationsOfSubsequences() {
    assertThat(KataUtils.getAllPermutationsOfSubsequences(new HashSet<>())).isEmpty();

    var characters = new HashSet<>(List.of('a', 'b', 'c'));
    var permutations = KataUtils.getAllPermutationsOfSubsequences(characters);

    assertThat(permutations).containsOnly("a", "b", "c", "ab", "ac", "ba", "bc", "ca", "cb", "abc", "acb", "bac", "bca",
            "cab", "cba");
  }

  /** Tests the {@link KataUtils#makeStringsList(List)} method. */
  @Test
  public void testMakeStringsList() {
    assertThat(KataUtils.makeStringsList(new ArrayList<>())).isEmpty();

    var strings = KataUtils.makeStringsList(List.of('a', 'b', 'c'));
    assertThat(strings).containsOnly("abc", "acb", "bac", "bca", "cab", "cba");
  }

}
