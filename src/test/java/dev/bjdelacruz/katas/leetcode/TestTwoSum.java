/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.leetcode;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link TwoSum}.
 */
public class TestTwoSum {
    /**
     * Tests the twoSum method.
     */
    @Test
    public void basicTests() {
        assertThat(TwoSum.twoSum(new int[] {1, 3, 6, 4, 5}, 5), is(equalTo(new int[] {0, 3})));
        assertThat(TwoSum.twoSum(new int[] {3, 3}, 6), is(equalTo(new int[] {0, 1})));
        assertThat(TwoSum.twoSum(new int[] {1, 3, 5, 7}, 10), is(equalTo(new int[] {1, 3})));
    }
}
