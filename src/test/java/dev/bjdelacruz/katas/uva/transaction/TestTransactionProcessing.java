/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva.transaction;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import java.util.List;
import org.junit.jupiter.api.Test;
import dev.bjdelacruz.katas.uva.UvaKataTest;

/**
 * JUnit tests for the {@link TransactionProcessing} class.
 * 
 * @author BJ Peter DeLaCruz &lt;bj.peter.delacruz@gmail.com&gt;
 */
public class TestTransactionProcessing extends UvaKataTest {

  /**
   * Tests the {@link TransactionProcessing#process()} method with a valid transaction.
   */
  @Test
  public void testValidTransaction() {
    var tp = new TransactionProcessing();

    tp.setLines(List.of("111Cash", "211Accounts Payable", "000No such account", "100111 12345", "100211 -12345",
            "000000 0"));
    tp.processLines();

    assertThat(getOutputStreamContents()).isEqualTo("Transaction 100 is not out of balance.\n");
  }

  /**
   * Tests the {@link TransactionProcessing#process()} method with an invalid transaction.
   */
  @Test
  public void testInvalidTransaction() {
    var tp = new TransactionProcessing();

    tp.setLines(List.of("111Cash", "211Accounts Payable", "000No such account", "100111 12345", "100211 -12344",
            "000000 0"));
    tp.processLines();

    var expected = """
    *** Transaction 100 is out of balance ***
    111 Cash                                    123.45
    211 Accounts Payable                       -123.44
    999 Out of Balance                            0.01
    
    """;

    assertThat(getOutputStreamContents()).isEqualTo(expected);
  }

  /** Tests the {@link Transaction} class. */
  @Test
  public void testTransaction() {
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> new Transaction(-1, 1, 0))
            .withMessageContainingAll("Invalid account number: -1");

    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> new Transaction(1, -1, 0))
            .withMessageContainingAll("Invalid transaction number: -1");
  }

  /** Tests all invalid input values. */
  @Test
  public void testInvalidInput() {
    var tp = new TransactionProcessing();

    tp.setLines(List.of("211Accounts Payable", "000", "100111 12345 12345"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(tp::processLines)
            .withMessageContainingAll("Transaction is in incorrect format: 100111 12345 12345");
  }

  /** Tests the {@link Account} class. */
  @Test
  public void testAccount() {
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> new Account(-1, "test"))
            .withMessageContainingAll("Invalid account number: -1");

    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> new Account(1, null))
            .withMessageContainingAll("Account description must not be null or an empty string.");

    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> new Account(1, ""))
            .withMessageContainingAll("Account description must not be null or an empty string.");
  }

}
