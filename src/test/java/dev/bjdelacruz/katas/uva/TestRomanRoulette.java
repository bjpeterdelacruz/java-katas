/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * Unit tests for the {@link RomanRoulette} kata.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestRomanRoulette extends UvaKataTest {

  /** Tests whether the output is valid. */
  @Test
  public void testOutput() {
    var r = new RomanRoulette(1);

    r.setLines(List.of("1 1", "1 5", "5 2", "0 0"));
    r.processLines();

    var expected = """
    N: 1\tK: 1\tPosition: 1
    N: 1\tK: 5\tPosition: 1
    N: 5\tK: 2\tPosition: 4
    """;

    assertThat(getOutputStreamContents()).isEqualTo(expected);
  }

  /** Tests all invalid input values. */
  @Test
  public void testInvalidInput() {
    var r = new RomanRoulette(1);

    r.setLines(List.of("1 1 3", "1 5", "0 0"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(r::processLines)
            .withMessageContainingAll("Only need 2 parameters: N and K. Found 3.");

    r = new RomanRoulette(1);
    r.setLines(List.of("1 K", "1 5", "0 0"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(r::processLines)
            .withMessageContainingAll("Non-numeric characters found: 1 K");
  }

}
