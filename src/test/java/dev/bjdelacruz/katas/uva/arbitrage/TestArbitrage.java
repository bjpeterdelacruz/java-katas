/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva.arbitrage;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import java.util.List;

import org.junit.jupiter.api.Test;
import dev.bjdelacruz.katas.uva.UvaKataTest;

/**
 * Unit tests for the {@link Arbitrage} kata.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestArbitrage extends UvaKataTest {

  /** Tests whether the output is valid. */
  @Test
  public void testOutput1() {
    var a = new Arbitrage();

    a.setLines(List.of("3", "1.2 .89", ".88 5.1", "1.1 0.15"));
    a.processLines();

    assertThat(a.getExchanges().stream().filter(ce -> ce.exchangeSequence().equals("121")).findFirst()).isNotEmpty();
  }

  /** Tests whether the output is valid. */
  @Test
  public void testOutput2() {
    var a = new Arbitrage();

    a.setLines(List.of("4", "3.1 0.0023 0.35", "0.21 0.00353 8.13", "200 180.559 10.339", "2.11 0.089 0.06111"));
    a.processLines();

    assertThat(a.getExchanges().stream().filter(ce -> ce.exchangeSequence().equals("1241")).findFirst()).isNotEmpty();
  }

  /** Tests whether the output is valid. */
  @Test
  public void testOutput3() {
    var a = new Arbitrage();

    a.setLines(List.of("2", "2.0", "0.45"));
    a.processLines();

    var expected = "\nNo arbitrage sequence exists.\n" + System.lineSeparator();

    assertThat(getOutputStreamContents()).endsWith(expected);
  }

  /** Tests all invalid input values. */
  @Test
  public void testInvalidInput() {
    var a = new Arbitrage();

    a.setLines(List.of("1", "2.0", "0.45"));
    assertThatExceptionOfType(RuntimeException.class).isThrownBy(a::processLines)
            .withMessageContainingAll("Invalid dimension found: 1");
  }

  /** Tests the equals and hashCode methods of the {@link CurrencyExchange} class. */
  @Test
  public void testCurrencyExchange() {
    var ce = new CurrencyExchange("test", 0.2);

    assertThat(ce).isEqualTo(new CurrencyExchange("test", 0.2));

    assertThat(ce).isNotEqualTo(new CurrencyExchange("Test", 0.2));
    assertThat(ce).isNotEqualTo(new CurrencyExchange("test", 2.0));
  }

}
