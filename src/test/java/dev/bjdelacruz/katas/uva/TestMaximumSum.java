/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * Unit tests for the {@link MaximumSum} kata.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestMaximumSum extends UvaKataTest {

  /** Tests whether the output is valid. */
  @Test
  public void testOutput1() {
    var sum = new MaximumSum();

    sum.setLines(List.of("4", "0 -2 -7  0 9  2 -6  2", "-4  1 -4  1 -1", "8  0 -2"));
    sum.processLines();

    assertThat(getOutputStreamContents()).isEqualTo("15");
  }

  /** Tests whether the output is valid. */
  @Test
  public void testOutput2() {
    var sum = new MaximumSum();

    sum.setLines(List.of("2", "0 -1 -1 2"));
    sum.processLines();

    assertThat(getOutputStreamContents()).isEqualTo("2");

    setUpOutputStream();

    sum = new MaximumSum();

    sum.setLines(List.of("3", "0 0 8", "0 2 0  -1 0 0"));
    sum.processLines();

    assertThat(getOutputStreamContents()).isEqualTo("10");

    setUpOutputStream();

    sum = new MaximumSum();

    sum.setLines(List.of("3", "0 0 8", "0 2 0", "0 1 0"));
    sum.processLines();

    assertThat(getOutputStreamContents()).isEqualTo("11");
  }

  /** Tests all invalid input values. */
  @Test
  public void testInvalidInput() {
    var sum = new MaximumSum();

    sum.setLines(List.of("N", "1 2 3"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(sum::processLines)
            .withMessageContainingAll("Expected integer for size of rectangle.");

    sum = new MaximumSum();

    sum.setLines(List.of());
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(sum::processLines)
            .withMessageContainingAll("Expected integer for size of rectangle.");
  }

}
