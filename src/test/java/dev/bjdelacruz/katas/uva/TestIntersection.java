/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Test;

/**
 * Unit tests for the {@link Intersection} kata.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestIntersection extends UvaKataTest {

  /** Tests whether the output is valid. */
  @Test
  public void testOutput1() {
    var intersection = new Intersection();

    intersection.setLines(new ArrayList<>(Arrays.asList("1", "4 9 11 2 1 5 7 1")));
    intersection.processLines();

    assertThat(getOutputStreamContents()).isEqualTo("F\n");
  }

  /** Tests whether the output is valid. */
  @Test
  public void testOutput2() {
    var intersection = new Intersection();

    intersection.setLines(new ArrayList<>(Arrays.asList("1", "1 1 10 10 1 9 8 1")));
    intersection.processLines();

    assertThat(getOutputStreamContents()).isEqualTo("T\n");
  }

  /** Tests whether the output is valid. */
  @Test
  public void testOutput3() {
    var intersection = new Intersection();

    intersection.setLines(new ArrayList<>(Arrays.asList("1", "11 2 4 9 1 5 7 1")));
    intersection.processLines();

    assertThat(getOutputStreamContents()).isEqualTo("F\n");

    setUpOutputStream();

    intersection = new Intersection();

    intersection.setLines(new ArrayList<>(Arrays.asList("1", "10 10 1 1 1 9 8 1")));
    intersection.processLines();

    assertThat(getOutputStreamContents()).isEqualTo("T\n");
  }

  /** Tests whether the output is valid. */
  @Test
  public void testOutput4() {
    var intersection = new Intersection();

    intersection.setLines(new ArrayList<>(Arrays.asList("1", "6 6 4 2 1 5 7 1")));
    intersection.processLines();

    assertThat(getOutputStreamContents()).isEqualTo("T\n");
  }

  /** Tests all invalid input values. */
  @Test
  public void testInvalidInput() {
    var intersection = new Intersection();

    intersection.setLines(new ArrayList<>(Arrays.asList("X", "1 1 10 10 1 9 8 1")));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(intersection::processLines)
            .withMessageContainingAll("Non-numeric character(s) found: X");

    intersection = new Intersection();

    intersection.setLines(new ArrayList<>(Arrays.asList("2", "1 1 10 10 1 9 8 1")));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(intersection::processLines)
            .withMessageContainingAll("Expected 2 lines. Found 1.");

    intersection = new Intersection();

    intersection.setLines(new ArrayList<>(Arrays.asList("1", "1 1 10 10 1 9 8 1 2")));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(intersection::processLines)
            .withMessageContainingAll("Expected 8 integers. Found 9.");

    intersection = new Intersection();

    intersection.setLines(new ArrayList<>(Arrays.asList("1", "2 2 2 2 1 9 8 1")));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(intersection::processLines)
            .withMessageContainingAll("Length of line cannot be 0.");
  }

  /** Tests the inner Rectangle class. */
  @Test
  public void testRectangle() {
    var p = new Point(1, 1);

    assertThatExceptionOfType(NullPointerException.class).isThrownBy(() -> new Intersection.Rectangle(null, p));

    assertThatExceptionOfType(NullPointerException.class).isThrownBy(() -> new Intersection.Rectangle(p, null));

    var topLeft = new Point(2, 2);
    var bottomRight = new Point(3, 3);

    var rectangle = new Intersection.Rectangle(topLeft, bottomRight);
    assertThat(rectangle.topLeft()).isEqualTo(topLeft);
    assertThat(rectangle.bottomRight()).isEqualTo(bottomRight);
  }

}
