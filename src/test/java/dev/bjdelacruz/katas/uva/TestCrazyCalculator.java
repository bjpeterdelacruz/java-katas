/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * JUnit tests for the Crazy Calculator kata.
 * 
 * @author BJ Peter DeLaCruz &lt;bj.peter.delacruz@gmail.com&gt;
 */
public class TestCrazyCalculator {

  /**
   * Tests the Crazy Calculator kata.
   */
  @Test
  public void testCrazyCalculator() {
    var calculator = new CrazyCalculator();

    calculator.setLines(List.of("+@1L", "-+3R", "*-2R", "//2R", "1@1", "5@5+4", "2@3-12/6/5+3", "-1+3", "", "-1--3"));
    calculator.processLines();

    var results = calculator.getResults();
    assertThat(results).hasSize(5);
    assertThat(Integer.parseInt(results.getFirst())).isEqualTo(2);
    assertThat(Integer.parseInt(results.get(1))).isEqualTo(6);
    assertThat(Integer.parseInt(results.get(2))).isEqualTo(14);
    assertThat(Integer.parseInt(results.get(3))).isEqualTo(-4);
    assertThat(Integer.parseInt(results.get(4))).isEqualTo(3);
  }

  /** Tests all invalid input values. */
  @Test
  public void testInvalidInput() {
    var calculator = new CrazyCalculator();

    calculator.setLines(List.of("+@1LR"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(calculator::processLines)
            .withMessageContainingAll("Line doesn't contain just four characters.");

    calculator = new CrazyCalculator();
    calculator.setLines(List.of("+@1L", "-+3R", "+@1L"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(calculator::processLines)
            .withMessageContainingAll("Rule already defined: +@1L");

    calculator = new CrazyCalculator();
    calculator.setLines(List.of("+@1L", "-+3R", "*-2R", "//2R", "0/0"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(calculator::processLines)
            .withMessageContainingAll("Attempted to divide by 0.");
  }

  /** Tests the {@link CrazyCalculator.Rule} class. */
  @Test
  public void testRule() {
    var rule = new CrazyCalculator.Rule('+', '@', 1, 'L', 10);

    assertThat(rule).isEqualTo(new CrazyCalculator.Rule('+', '@', 1, 'L', 10));

    assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> new CrazyCalculator.Rule('+', '\'', 1, 'L', 10))
            .withMessageContainingAll("Not a valid crazy operator: \'");

    assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> new CrazyCalculator.Rule('+', '@', 1, 'K', 10))
            .withMessageContainingAll("Not a valid associativity: K");

    assertThat(rule.setLastPosition(5)).isEqualTo(new CrazyCalculator.Rule('+', '@', 1, 'L', 5));

    assertThat(rule).isNotEqualTo(new CrazyCalculator.Rule('-', '@', 1, 'L', 10));
    assertThat(rule).isNotEqualTo(new CrazyCalculator.Rule('+', '[', 1, 'L', 10));
    assertThat(rule).isNotEqualTo(new CrazyCalculator.Rule('+', '@', 2, 'L', 10));
    assertThat(rule).isNotEqualTo(new CrazyCalculator.Rule('+', '@', 1, 'R', 10));
    assertThat(rule).isNotEqualTo(new CrazyCalculator.Rule('+', '@', 1, 'L', 20));
  }

}
