/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * Unit tests for the {@link Blocks} kata.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestBlocks extends UvaKataTest {

  /** Tests whether the output is valid. */
  @Test
  public void testOutput1() {
    var b = new Blocks();

    b.setLines(List.of("10", "move 9 onto 1", "move 8 over 1", "move 7 over 1", "move 6 over 1", "pile 8 over 6",
            "pile 8 over 5", "move 2 over 1", "move 4 over 9", "quit"));
    b.processLines();

    var expected = "0: 0 \n";
    expected += "1: 1 9 2 4 \n";
    expected += "2: \n";
    expected += "3: 3 \n";
    expected += "4: \n";
    expected += "5: 5 8 7 6 \n";
    expected += "6: \n";
    expected += "7: \n";
    expected += "8: \n";
    expected += "9: \n";

    assertThat(getOutputStreamContents()).isEqualTo(expected);
  }

  /** Tests whether the output is valid. */
  @Test
  public void testOutput2() {
    var b = new Blocks();

    b.setLines(List.of("10", "move 1 over 2", "move 2 onto 3", "pile 6 over 5", "pile 5 onto 7",
            "move 8 over 8", "quit"));
    b.processLines();

    var expected = "0: 0 \n";
    expected += "1: 1 \n";
    expected += "2: \n";
    expected += "3: 3 2 \n";
    expected += "4: 4 \n";
    expected += "5: \n";
    expected += "6: \n";
    expected += "7: 7 5 6 \n";
    expected += "8: 8 \n";
    expected += "9: 9 \n";

    assertThat(getOutputStreamContents()).isEqualTo(expected);
  }

  /** Tests whether the output is valid. */
  @Test
  public void testOutput3() {
    var b = new Blocks();

    b.setLines(List.of("5", "move 1 onto 2", "move 3 onto 4", "move 2 onto 4", "quit"));
    b.processLines();

    var expected = "0: 0 \n";
    expected += "1: 1 \n";
    expected += "2: \n";
    expected += "3: 3 \n";
    expected += "4: 4 2 \n";

    assertThat(getOutputStreamContents()).isEqualTo(expected);
  }

  /** Tests whether the output is valid. */
  @Test
  public void testOutput4() {
    var b = new Blocks();

    b.setLines(List.of("5", "pile 1 onto 2", "pile 3 onto 4", "pile 2 onto 4", "quit"));
    b.processLines();

    var expected = "0: 0 \n";
    expected += "1: \n";
    expected += "2: \n";
    expected += "3: 3 \n";
    expected += "4: 4 2 1 \n";

    assertThat(getOutputStreamContents()).isEqualTo(expected);
  }

  /** Tests all invalid input values. */
  @Test
  public void testInvalidInput() {
    var b = new Blocks();
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(b::processLines)
            .withMessageContainingAll("Expected integer for number of blocks.");

    b = new Blocks();
    b.setLines(List.of("10", "moves 1 onto 8"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(b::processLines)
            .withMessageContainingAll("Invalid command: moves");

    b = new Blocks();
    b.setLines(List.of("10", "move a onto 1"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(b::processLines)
            .withMessageContainingAll("Expected integer for first block.");

    b = new Blocks();
    b.setLines(List.of("10", "move 1 onto a"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(b::processLines)
            .withMessageContainingAll("Expected integer for second block.");
  }

}
