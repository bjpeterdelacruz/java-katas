/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * Unit tests for the {@link PancakeStacks} kata.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestPancakeStacks extends UvaKataTest {

  /** Tests whether the output is valid. */
  @Test
  public void testOutput() {
    var ps = new PancakeStacks();

    ps.setLines(List.of("1 2 3 4 5", "5 4 3 2 1", "5 1 2 3 4", "5 1 4 3 2"));
    ps.processLines();

    var expected = """
    Stack:     [1, 2, 3, 4, 5]
    Positions: [0]
    
    Stack:     [5, 4, 3, 2, 1]
    Positions: [1, 0]
    
    Stack:     [5, 1, 2, 3, 4]
    Positions: [1, 2, 0]
    
    Stack:     [5, 1, 4, 3, 2]
    Positions: [1, 2, 2, 0]
    
    """;

    assertThat(getOutputStreamContents()).isEqualTo(expected);
  }

}
