package dev.bjdelacruz.katas.uva.searching;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import java.util.List;
import org.junit.jupiter.api.Test;
import dev.bjdelacruz.katas.uva.UvaKataTest;

/**
 * Unit tests for the {@link KeyWord} kata.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestKeyWord extends UvaKataTest {

  /** Tests whether the output is valid. */
  @Test
  public void testOutput() {
    var kw = new KeyWord();

    kw.setLines(List.of(
            "P: 0 rock art",
            "P: 3 concepts conceptions",
            "P: 1   art rock   metaphor concepts",
            "T: Rock Art of the Maori|",
            "T: Jazz and Rock - Art Brubeck and Elvis Presley|",
            "T: Don't Rock --- the Boat as Metaphor in 1984, Concepts",
            "   and (Mis)-Conceptions of an Art Historian.|",
            "T: Carved in Rock, The Art and Craft of making promises",
            "   believable when your `phone bills have gone",
            "   through the roof|",
            "#"));
    kw.processLines();

    assertThat(getOutputStreamContents()).isEqualTo("1: 1,2\n2:\n3: 1,2,3,4\n");
  }

  /** Tests all invalid input values. */
  @Test
  public void testInvalidInput() {
    var kw = new KeyWord();

    kw.setLines(List.of(
            "P: 0 rock art",
            "P: 3 concepts conceptions",
            "P: P   art rock   metaphor concepts",
            "T: Rock Art of the Maori|",
            "T: Jazz and Rock - Art Brubeck and Elvis Presley|",
            "T: Don't Rock --- the Boat as Metaphor in 1984, Concepts",
            "   and (Mis)-Conceptions of an Art Historian.|",
            "T: Carved in Rock, The Art and Craft of making promises",
            "   believable when your `phone bills have gone",
            "   through the roof|",
            "#"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(kw::processLines)
            .withMessageContainingAll("Expected number for threshold, was p.");
  }

}
