package dev.bjdelacruz.katas.uva.searching;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import dev.bjdelacruz.katas.uva.UvaKataTest;

/**
 * Unit tests for the {@link SearchingQuickly} kata.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestSearchingQuickly extends UvaKataTest {

  /** Tests whether the output is valid. */
  @Test
  public void testOutput() {
    var kw = new SearchingQuickly();

    var ignoreWordsList = List.of("is", "the", "of", "and", "as", "a", "but");

    var titlesList = List.of("Descent of Man", "The Ascent of Man", "The Old Man and The Sea",
            "A Portrait of The Artist As a Young Man", "A Man is a Man but Bubblesort IS A DOG");

    var lines = new ArrayList<>(ignoreWordsList);
    lines.add("::");
    lines.addAll(titlesList);

    kw.setLines(lines);
    kw.processLines();

    var keywordsList = List.of("artist", "ascent", "bubblesort", "descent", "dog", "man", "man", "man",
            "man", "man", "man", "old", "portrait", "sea", "young");

    assertThat(kw.getIgnoreWordsList()).isEqualTo(ignoreWordsList);
    assertThat(kw.getKeywordsList()).isEqualTo(keywordsList);

    var expected = """
    a portrait of the ARTIST as a young man
    the ASCENT of man
    a man is a man but BUBBLESORT is a dog
    DESCENT of man
    a man is a man but bubblesort is a DOG
    descent of MAN
    the ascent of MAN
    the old MAN and the sea
    a portrait of the artist as a young MAN
    a MAN is a man but bubblesort is a dog
    a man is a MAN but bubblesort is a dog
    the OLD man and the sea
    a PORTRAIT of the artist as a young man
    the old man and the SEA
    a portrait of the artist as a YOUNG man
    """;

    assertThat(getOutputStreamContents()).isEqualTo(expected);
  }

  /** Tests all invalid input values. */
  @Test
  public void testInvalidInput() {
    var kw = new SearchingQuickly();

    kw.setLines(List.of());
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(kw::processLines)
            .withMessageContainingAll("List is empty.");

    kw = new SearchingQuickly();

    kw.setLines(List.of("::"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(kw::processLines)
            .withMessageContainingAll("At least one title must be specified.");
  }

}
