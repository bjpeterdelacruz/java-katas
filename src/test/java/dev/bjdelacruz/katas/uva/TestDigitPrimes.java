/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import java.util.List;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for the {@link DigitPrimes} kata.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestDigitPrimes extends UvaKataTest {

  /** Tests whether the output is valid. */
  @Test
  public void basicTests() {
    var primes = new DigitPrimes();
    primes.setLines(List.of("3", "10 20", "10 100", "100 10000"));
    primes.processLines();

    assertThat(primes.getResults(), is(equalTo(List.of(1, 10, 576))));

    primes = new DigitPrimes();
    primes.setLines(List.of("1", "0 9"));
    primes.processLines();

    assertThat(primes.getResults(), is(equalTo(List.of(4))));

    primes = new DigitPrimes();
    primes.setLines(List.of("3", "10 20", "10 100"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(primes::processLines);

    primes = new DigitPrimes();
    primes.setLines(List.of("abc", "10 20", "10 100", "100 10000"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(primes::processLines);
  }

}
