package dev.bjdelacruz.katas.uva.squares;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import java.util.List;
import org.junit.jupiter.api.Test;
import dev.bjdelacruz.katas.uva.UvaKataTest;

/**
 * Unit tests for the {@link Squares} kata.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestSquares extends UvaKataTest {

  /** Tests whether the output is valid. */
  @Test
  public void testOutput1() {
    var s = new Squares();

    s.setLines(List.of("4", "16", "H 1 1", "H 1 3", "H 2 1", "H 2 2", "H 2 3", "H 3 2",
            "H 4 2", "H 4 3", "V 1 1", "V 2 1", "V 2 2", "V 2 3", "V 3 2", "V 4 1", "V 4 2", "V 4 3"));
    s.processLines();

    var expected = """
    Problem #1
    
    2 squares of size 1
    1 square of size 2
    
    **********************************
    
    """;

    assertThat(getOutputStreamContents()).isEqualTo(expected);
  }

  /** Tests the {@link Line} class. */
  @Test
  public void testLine() {
    var line = new Line(Line.Direction.HORIZONTAL, 1, 1, 2, 2);

    assertThat(line.equals(new Line(Line.Direction.HORIZONTAL, 1, 1, 2, 2))).isTrue();

    assertThat(line.equals(new Line(Line.Direction.VERTICAL, 1, 1, 2, 2))).isFalse();
    assertThat(line.equals(new Line(Line.Direction.HORIZONTAL, 2, 1, 2, 2))).isFalse();
    assertThat(line.equals(new Line(Line.Direction.HORIZONTAL, 1, 2, 2, 2))).isFalse();
    assertThat(line.equals(new Line(Line.Direction.HORIZONTAL, 1, 1, 1, 2))).isFalse();
    assertThat(line.equals(new Line(Line.Direction.HORIZONTAL, 1, 1, 2, 1))).isFalse();
  }

  /** Tests whether the output is valid. */
  @Test
  public void testOutput2() {
    var s = new Squares();

    s.setLines(List.of("2", "3", "H 1 1", "H 2 1", "V 1 2"));
    s.processLines();

    assertThat(getOutputStreamContents()).contains("No completed squares can be found.");
  }

  /** Tests all invalid input values. */
  @Test
  public void testInvalidInput() {
    var s = new Squares();
    s.setLines(List.of("2", "3", "K 1 2"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(s::processLines)
            .withMessageContainingAll("Invalid value for direction: K");

    s = new Squares();
    s.setLines(List.of("2", "3", "KC 1 2 3"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(s::processLines)
            .withMessageContainingAll("Expected direction, row index, and column index. Found [KC 1 2 3].");

    s = new Squares();
    s.setLines(List.of("2", "3", "H 1 2"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(s::processLines)
            .withMessageContainingAll("Expected 3 lines. Found 1.");

    s = new Squares();
    // Lines: (1, 1) -> (1, 2) : (1, 2) -> (1, 3) : (5, 2) -> (5, 3)
    s.setLines(List.of("2", "3", "H 1 1", "H 1 2", "H 5 2"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(s::processLines)
            .withMessageContainingAll("Invalid point for line: 5 2");

    s = new Squares();
    // Lines: (1, 1) -> (1, 2) : (1, 2) -> (1, 3) : (1, 3) -> (1, 4)
    s.setLines(List.of("2", "3", "H 1 1", "H 1 2", "H 1 3"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(s::processLines)
            .withMessageContainingAll("Invalid point for line: 1 3");

    s = new Squares();
    // Lines: (1, 1) -> (2, 1) : (2, 1) -> (3, 1) : (5, 2) -> (6, 2)
    s.setLines(List.of("2", "3", "V 1 1", "V 1 2", "V 5 2"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(s::processLines)
            .withMessageContainingAll("Invalid point for line: 5 2");

    s = new Squares();
    // Lines: (1, 1) -> (2, 1) : (2, 1) -> (3, 1) : (3, 1) -> (4, 1)
    s.setLines(List.of("2", "3", "V 1 1", "V 1 2", "V 1 3"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(s::processLines)
            .withMessageContainingAll("Invalid point for line: 1 3");

    s = new Squares();
    s.setLines(List.of("K", "3", "H 1 1", "H 1 2", "H 2 1"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(s::processLines)
            .withMessageContainingAll("Dimension is not an integer.");

    s = new Squares();
    s.setLines(List.of("2", "K", "H 1 1", "H 1 2", "H 2 1"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(s::processLines)
            .withMessageContainingAll("Number of lines is not an integer.");

    s = new Squares();
    s.setLines(List.of("1", "3", "H 1 1", "H 1 2", "H 2 1"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(s::processLines)
            .withMessageContainingAll("Dimension must be greater than 1.");

    s = new Squares();
    s.setLines(List.of("2", "0", "H 1 1", "H 1 2", "H 2 1"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(s::processLines)
            .withMessageContainingAll("Number of lines must be greater than 0.");
  }

}
