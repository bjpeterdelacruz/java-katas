/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * Unit tests for the {@link WordCrosses} kata.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestWordCrosses extends UvaKataTest {

  /** Tests whether the output is valid. */
  @Test
  public void testOutput() {
    var wc = new WordCrosses();

    wc.setLines(List.of("MATCHES CHEESECAKE PICNIC EXCUSES", "#"));
    wc.processLines();

    var expected = " C          " + System.lineSeparator();
    expected += " H          " + System.lineSeparator();
    expected += " E          " + System.lineSeparator();
    expected += " E          " + System.lineSeparator();
    expected += " S          " + System.lineSeparator();
    expected += " E          E" + System.lineSeparator();
    expected += " C          X" + System.lineSeparator();
    expected += "MATCHES   PICNIC" + System.lineSeparator();
    expected += " K          U" + System.lineSeparator();
    expected += " E          S" + System.lineSeparator();
    expected += "            E" + System.lineSeparator();
    expected += "            S" + System.lineSeparator();
    expected += System.lineSeparator();

    assertThat(getOutputStreamContents()).isEqualTo(expected);

    setUpOutputStream();

    wc = new WordCrosses();

    wc.setLines(List.of("PEANUT BANANA VACUUM GREEDY", "#"));
    wc.processLines();

    expected = "Unable to make two crosses.\n" + System.lineSeparator();

    assertThat(getOutputStreamContents()).isEqualTo(expected);

    setUpOutputStream();

    wc = new WordCrosses();

    wc.setLines(List.of());
    wc.processLines();

    assertThat(getOutputStreamContents()).isEmpty();
  }

  /**
   * Tests invalid input values.
   */
  @Test
  public void testInvalidInput() {
    var wc = new WordCrosses();

    var line = "MATCHES CHEESECAKE PICNIC EXCUSES PEANUT";
    wc.setLines(List.of(line, "#"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(wc::processLines)
            .withMessageContainingAll("Line does not contain four and only four words: " + line);
  }

}
