/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Tests the LinearEquationSolver class by solving some test equations.
 * 
 * @author BJ Peter DeLaCruz &lt;bj.peter.delacruz@gmail.com&gt;
 */
public class TestLinearEquationSolver extends UvaKataTest {

  private List<String> variablesL;
  private List<String> constantsL;
  private List<String> variablesR;
  private List<String> constantsR;

  /**
   * Sets up the lists.
   */
  @BeforeEach
  public void setUp() {
    variablesL = new ArrayList<>();
    constantsL = new ArrayList<>();
    variablesR = new ArrayList<>();
    constantsR = new ArrayList<>();
  }

  /**
   * Test equation 1. The solution should be -1/2.
   */
  @Test
  public void testEquation1() {
    processLists("10x +5 -5 + 10 - 5 = 0");
    assertThat(variablesL).hasSize(1);
    assertThat(constantsL).hasSize(4);
    assertThat(variablesR).hasSize(0);
    assertThat(constantsR).hasSize(1);
    var result = new LinearEquationSolver().solve(variablesL, constantsL, variablesR, constantsR);
    assertThat(Double.compare(-0.5, result)).isEqualTo(0);
  }

  /**
   * Test equation 2. The solution should be -1.
   */
  @Test
  public void testEquation2() {
    processLists("5 +10x -x + 4 + 0 = 0");
    assertThat(variablesL).hasSize(2);
    assertThat(constantsL).hasSize(3);
    assertThat(variablesR).hasSize(0);
    assertThat(constantsR).hasSize(1);
    var result = new LinearEquationSolver().solve(variablesL, constantsL, variablesR, constantsR);
    assertThat(Double.compare(-1, result)).isEqualTo(0);
  }

  /**
   * Test equation 3. There should be infinite solutions.
   */
  @Test
  public void testEquation3() {
    processLists("5 + x = x + 5");
    assertThat(variablesL).hasSize(1);
    assertThat(constantsL).hasSize(1);
    assertThat(variablesR).hasSize(1);
    assertThat(constantsR).hasSize(1);
    var result = new LinearEquationSolver().solve(variablesL, constantsL, variablesR, constantsR);
    assertThat(Double.compare(Double.POSITIVE_INFINITY, result)).isEqualTo(0);
  }

  /**
   * Test equation 4. There should be no solutions.
   */
  @Test
  public void testEquation4() {
    processLists("1 - 2 + 3 - 4 = 4 - 3 + 2 - 1");
    assertThat(variablesL).hasSize(0);
    assertThat(constantsL).hasSize(4);
    assertThat(variablesR).hasSize(0);
    assertThat(constantsR).hasSize(4);
    var result = new LinearEquationSolver().solve(variablesL, constantsL, variablesR, constantsR);
    assertThat(Double.compare(Double.NaN, result)).isEqualTo(0);
  }

  /**
   * Test equation 5. The solution should be 3.
   */
  @Test
  public void testEquation5() {
    processLists("1 - 2 + 3 - 4 + 5 = x");
    assertThat(variablesL).hasSize(0);
    assertThat(constantsL).hasSize(5);
    assertThat(variablesR).hasSize(1);
    assertThat(constantsR).hasSize(0);
    var result = new LinearEquationSolver().solve(variablesL, constantsL, variablesR, constantsR);
    assertThat(Double.compare(3.0, result)).isEqualTo(0);
  }

  /**
   * Test equation 6. The solution should be -1.
   */
  @Test
  public void testEquation6() {
    processLists("1 + 2 - 3 + 4 - 5 = x");
    assertThat(variablesL).hasSize(0);
    assertThat(constantsL).hasSize(5);
    assertThat(variablesR).hasSize(1);
    assertThat(constantsR).hasSize(0);
    var result = new LinearEquationSolver().solve(variablesL, constantsL, variablesR, constantsR);
    assertThat(Double.compare(-1.0, result)).isEqualTo(0);
  }

  /**
   * Processes both sides of the equation.
   * 
   * @param equation The equation to solve.
   */
  private void processLists(String equation) {
    var tokenizer = new StringTokenizer(equation);
    while (tokenizer.hasMoreTokens()) {
      if (LinearEquationSolver.processSide(tokenizer, this.variablesL, this.constantsL)) {
        break;
      }
    }
    while (tokenizer.hasMoreTokens()) {
      LinearEquationSolver.processSide(tokenizer, this.variablesR, this.constantsR);
    }
  }

  /** Tests whether the output is valid. */
  @Test
  public void testOutput() {
    var solver = new LinearEquationSolver();

    solver.setLines(List.of("4", "2x - 4 + 5x + 300 = 98x", "x + 2 = 2 + x", "x - 3x = 2 - x", "2 - 2 = x"));
    solver.processLines();

    assertThat(solver.getResults()).isEqualTo(Arrays.asList("3", "IDENTITY", "2", "0"));
  }

  /** Tests all invalid input values. */
  @Test
  public void testInvalidInput() {
    var solver = new LinearEquationSolver();
    solver.setLines(List.of("2", "2x - 4 + 5x + 300 = 98x"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(solver::processLines)
            .withMessageContainingAll("Expected 2 equations. Found 1.");

    solver = new LinearEquationSolver();
    solver.setLines(List.of("K", "2x - 4 + 5x + 300 = 98x", "x + 2 = 2 + x"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(solver::processLines)
            .withMessageContainingAll("Expected an integer for number of equations to solve.");

    solver = new LinearEquationSolver();
    solver.setLines(List.of("2", "2x - 4 + 5x + 300 = 98x", "x + 2"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(solver::processLines)
            .withMessageContainingAll("Invalid equation: x + 2 (missing right-hand side)");

    solver = new LinearEquationSolver();
    solver.setLines(List.of("2", "2x - 4 + 5x + 300 = 98x", "x + 2 = y"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(solver::processLines)
            .withMessageContainingAll("Invalid token: y");

    solver = new LinearEquationSolver();
    solver.setLines(List.of("1", "2 2 = x"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(solver::processLines)
            .withMessageContainingAll("Expected + or - between operands.");

    solver = new LinearEquationSolver();
    solver.setLines(List.of("1", "2 + 2 = -"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(solver::processLines)
            .withMessageContainingAll("Invalid equation: missing operand.");
  }
}
