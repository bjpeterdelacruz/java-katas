/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import java.util.List;

import org.junit.jupiter.api.Test;
import dev.bjdelacruz.commons.utils.StringUtils;

/**
 * Unit tests for the {@link Spreadsheet} kata.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestSpreadsheet extends UvaKataTest {

  /** Tests whether the output is valid. */
  @Test
  public void testOutput1() {
    var s = new Spreadsheet();

    s.setNumSpreadsheets(1);
    assertThat(s.getNumSpreadsheets()).isEqualTo(1);
    s.setLines(List.of("4 3", "10 34 37 =A1+B1+C1", "40 17 34 =A2+B2+C2", "=A1+A2 =B1+B2 =C1+C2 =D1+D2"));
    s.processLines();

    var array = new Integer[3][];
    array[0] = new Integer[] {10, 34, 37, 81};
    array[1] = new Integer[] {40, 17, 34, 91};
    array[2] = new Integer[] {50, 51, 71, 172};

    var expected = StringUtils.get2dArrayContents(array) + System.lineSeparator();

    assertThat(getOutputStreamContents()).isEqualTo(expected);
  }

  /** Tests whether the output is valid. */
  @Test
  public void testOutput2() {
    var s = new Spreadsheet();

    s.setNumSpreadsheets(1);
    assertThat(s.getNumSpreadsheets()).isEqualTo(1);
    s.setLines(List.of("4 3", "10 34 37 =a1+b1+c1", "40 17 34 =a2+b2+c2", "=a1+a2 =b1+b2 =c1+c2 =d1+d2"));
    s.processLines();

    var array = new Integer[3][];
    array[0] = new Integer[] {10, 34, 37, 81};
    array[1] = new Integer[] {40, 17, 34, 91};
    array[2] = new Integer[] {50, 51, 71, 172};

    var expected = StringUtils.get2dArrayContents(array) + System.lineSeparator();

    assertThat(getOutputStreamContents()).isEqualTo(expected);
  }

  /** Tests whether the output is valid. */
  @Test
  public void testOutput3() {
    var s = new Spreadsheet();

    s.setNumSpreadsheets(1);
    assertThat(s.getNumSpreadsheets()).isEqualTo(1);
    s.setLines(List.of("2 2", "10 =A1+A2", "=B2+A1 1"));
    s.processLines();

    var array = new Integer[2][];
    array[0] = new Integer[] {10, 21};
    array[1] = new Integer[] {11, 1};

    var expected = StringUtils.get2dArrayContents(array) + System.lineSeparator();

    assertThat(getOutputStreamContents()).isEqualTo(expected);
  }

  /** Tests all invalid input values. */
  @Test
  public void testInvalidInput() {
    var s = new Spreadsheet();

    s.setNumSpreadsheets(1);
    assertThat(s.getNumSpreadsheets()).isEqualTo(1);
    s.setLines(List.of("-1 3", "10 34 37 =A1+B1+C1", "40 17 34 =A2+B2+C2", "=A1+A2 =B1+B2 =C1+C2 =D1+D2"));

    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(s::processLines)
            .withMessageContainingAll("Negative values are not allowed for rows and columns.");

    setUpOutputStream();

    s = new Spreadsheet();

    s.setNumSpreadsheets(1);
    assertThat(s.getNumSpreadsheets()).isEqualTo(1);
    s.setLines(List.of("4 -1", "10 34 37 =A1+B1+C1", "40 17 34 =A2+B2+C2", "=A1+A2 =B1+B2 =C1+C2 =D1+D2"));

    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(s::processLines)
            .withMessageContainingAll("Negative values are not allowed for rows and columns.");

    setUpOutputStream();

    s = new Spreadsheet();

    s.setNumSpreadsheets(1);
    assertThat(s.getNumSpreadsheets()).isEqualTo(1);
    s.setLines(List.of("4 3 1", "10 34 37 =A1+B1+C1", "40 17 34 =A2+B2+C2", "=A1+A2 =B1+B2 =C1+C2 =D1+D2"));

    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(s::processLines)
            .withMessageContainingAll("Expected only 2 values: number of rows and columns.");

    setUpOutputStream();

    s = new Spreadsheet();

    s.setNumSpreadsheets(1);
    assertThat(s.getNumSpreadsheets()).isEqualTo(1);
    s.setLines(List.of("4 3", "10 34 37 =A1/B1+C1", "40 17 34 =A2+B2+C2", "=A1+A2 =B1+B2 =C1+C2 =D1+D2"));

    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(s::processLines)
            .withMessageContainingAll("Illegal character found: /");

    setUpOutputStream();

    s = new Spreadsheet();

    s.setNumSpreadsheets(1);
    assertThat(s.getNumSpreadsheets()).isEqualTo(1);
    s.setLines(List.of("4 3", "10 34 37 =A1+B1+Z23", "40 17 34 =A2+B2+C2", "=A1+A2 =B1+B2 =C1+C2 =D1+D2"));

    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(s::processLines)
            .withMessageContainingAll("row 23, column 26 does not exist.");

    setUpOutputStream();

    s = new Spreadsheet();

    s.setNumSpreadsheets(1);
    assertThat(s.getNumSpreadsheets()).isEqualTo(1);
    s.setLines(List.of("4 3", "10 34 37 A1+B1+C1", "40 17 34 =A2+B2+C2", "=A1+A2 =B1+B2 =C1+C2 =D1+D2"));

    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(s::processLines)
            .withMessageContainingAll("Illegal token found: A1+B1+C1");

    s = new Spreadsheet();

    s.setNumSpreadsheets(1);
    assertThat(s.getNumSpreadsheets()).isEqualTo(1);
    s.setLines(List.of("4 3", "10 34 37 =D1+B1+C1", "40 17 34 =A2+B2+C2", "=A1+A2 =B1+B2 =C1+C2 =D1+D2"));

    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(s::processLines)
            .withMessageContainingAll("Cyclic dependency detected in formula =D1+B1+C1. D1 refers to row 1, column 4.");
  }

}
