/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * Tests the {@link CycleLength} kata.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestCycleLength extends UvaKataTest {

  /** Tests whether the output is valid. */
  @Test
  public void testOutput() {
    var c = new CycleLength();

    c.setLines(List.of("10 1", "100 200", "201 210", "900 1000", "1000 900", "999999 999990"));
    c.processLines();

    var expected = """
    10 1 20
    100 200 125
    201 210 89
    900 1000 174
    1000 900 174
    999999 999990 259
    """;

    assertThat(getOutputStreamContents()).isEqualTo(expected);
  }

  /** Tests all invalid input values. */
  @Test
  public void testInvalidInput() {
    var c = new CycleLength();

    c.setLines(List.of("a 1", "100 200", "201 210"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(c::processLines)
            .withMessageContainingAll("Value for i is not an integer.");

    c = new CycleLength();
    c.setLines(List.of("10 1", "100 b", "201 210"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(c::processLines)
            .withMessageContainingAll("Value for j is not an integer.");
  }

}
