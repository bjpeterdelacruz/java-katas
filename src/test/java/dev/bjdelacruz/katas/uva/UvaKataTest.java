/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

/**
 * Base class for unit tests.
 * 
 * @author BJ Peter DeLaCruz
 */
public abstract class UvaKataTest {

  private ByteArrayOutputStream outContent;

  /** Sets up the I/O. */
  @BeforeEach
  public void init() {
    setUpOutputStream();
  }

  /** Resets the I/O. */
  @AfterEach
  public void cleanup() {
    System.setOut(System.out);
  }

  /** Sets up the output stream. */
  protected void setUpOutputStream() {
    outContent = new ByteArrayOutputStream();
    try {
      System.setOut(new PrintStream(outContent, false, StandardCharsets.UTF_8));
    }
    catch (SecurityException se) {
      throw new RuntimeException(se);
    }
  }

  /**
   * @return The contents of the byte array output stream.
   */
  protected String getOutputStreamContents() {
    return outContent.toString(StandardCharsets.UTF_8);
  }

}
