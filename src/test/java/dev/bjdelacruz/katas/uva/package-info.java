/**
 * Contains unit tests for University of Valladolid (UVa) katas.
 */
package dev.bjdelacruz.katas.uva;
