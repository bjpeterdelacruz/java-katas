/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva.bangla;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import java.util.List;

import org.junit.jupiter.api.Test;
import dev.bjdelacruz.katas.uva.UvaKataTest;

/**
 * Unit tests for the {@link BanglaNumbers} kata.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestBanglaNumbers extends UvaKataTest {

  /** Tests whether the output is valid. */
  @Test
  public void testOutput1() {
    var b = new BanglaNumbers();

    b.setLines(List.of("23764"));
    b.processLines();

    var expected = "23 hajar 7 shata 64" + System.lineSeparator();

    assertThat(getOutputStreamContents()).isEqualTo(expected);
  }

  /** Tests whether the output is valid. */
  @Test
  public void testOutput2() {
    var b = new BanglaNumbers();

    b.setLines(List.of("45897458973958"));
    b.processLines();

    var expected = "45 lakh 89 hajar 7 shata 45 kuti 89 lakh 73 hajar 9 shata 58" + System.lineSeparator();

    assertThat(getOutputStreamContents()).isEqualTo(expected);
  }

  /** Tests all invalid input values. */
  @Test
  public void testInvalidInput() {
    var b = new BanglaNumbers();

    b.setLines(List.of("9999999999999999"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(b::processLines)
            .withMessageContainingAll("9999999999999999 is greater than 999999999999999");
  }

}
