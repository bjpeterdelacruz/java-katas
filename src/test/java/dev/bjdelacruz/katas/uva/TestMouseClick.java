/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * Unit tests for the {@link MouseClick} kata.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestMouseClick extends UvaKataTest {

  /** Tests whether the output is valid. */
  @Test
  public void testOutput() {
    var mc = new MouseClick();

    mc.setLines(List.of("I 216 28", "R 22 19 170 102", "I 40 150", "I 96 138", "I 36 193",
            "R 305 13 425 103", "I 191 184", "I 387 200", "R 266 63 370 140", "I 419 134", "I 170 102", "M 50 50",
            "M 236 30", "M 403 167", "M 330 83", "#"));
    mc.processLines();

    assertThat(getOutputStreamContents()).isEqualTo("A\n   1\n   6   7\nC\n");
  }

}
