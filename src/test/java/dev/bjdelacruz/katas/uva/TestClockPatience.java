/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * Unit tests for the {@link ClockPatience} kata.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestClockPatience extends UvaKataTest {

  /** Tests whether the output is valid. */
  @Test
  public void testOutput() {
    var clock = new ClockPatience();

    clock.setLines(List.of("TS QC 8S 8D QH 2D 3H KH 9H 2H TH KS KC",
            "9D JH 7H JD 2S QS TD 2C 4H 5H AD 4D 5D", "6D 4S 9S 5S 7S JS 8H 3D 8C 3S 4C 6S 9C",
            "AS 7C AH 6H KD JC 7D AC 5C TC QD 6C 3C", "#"));
    clock.processLines();

    assertThat(getOutputStreamContents()).endsWith("44,KD" + System.lineSeparator());
  }

}
