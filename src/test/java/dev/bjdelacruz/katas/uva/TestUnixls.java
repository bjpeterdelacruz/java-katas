/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * Tests for the {@link Unixls} class.
 * 
 * @author BJ Peter DeLaCruz &lt;bj.peter.delacruz@gmail.com&gt;
 */
public class TestUnixls extends UvaKataTest {

  /**
   * Tests the methods in the {@link Unixls} class.
   * 
   * @throws Exception If a problem occurred while reading in the list of filenames from a file.
   */
  @Test
  public void basicTests() throws Exception {
    var filePath = Paths.get(Thread.currentThread().getContextClassLoader().getResource("test.txt").toURI());

    var filenames = Files.readAllLines(filePath, Charset.defaultCharset());

    var unixls = new Unixls();

    unixls.setLines(filenames.subList(0, filenames.indexOf("12")));
    unixls.processLines();

    var table = """
    ------------------------------------------------------------
    12345678.123         2short4me\040\040\040\040\040\040\040\040\040\040
    mid_size_name        much_longer_name\040\040\040
    shorter              size-1\040\040\040\040\040\040\040\040\040\040\040\040\040
    size2                size3\040\040\040\040\040\040\040\040\040\040\040\040\040\040
    tiny                 very_long_file_name
    ------------------------------------------------------------
    """;

    assertThat(unixls.getTable(), is(equalTo(table)));
    assertThat(Unixls.getNumberOfColumns(19), is(equalTo(2)));

    setUpOutputStream();

    unixls = new Unixls();

    unixls.setLines(filenames.subList(filenames.indexOf("12"), filenames.indexOf("19")));
    unixls.processLines();

    table = "------------------------------------------------------------\n";
    table += "Alfalfa        Buckwheat      Butch          Cotton       \n";
    table += "Darla          Froggy         Joe            Mrs_Crabapple\n";
    table += "P.D.           Porky          Stimey         Weaser       \n";
    table += "------------------------------------------------------------\n";

    assertThat(unixls.getTable(), is(equalTo(table)));
    assertThat(Unixls.getNumberOfColumns(13), is(equalTo(4)));

    setUpOutputStream();

    unixls = new Unixls();

    unixls.setLines(filenames.subList(filenames.indexOf("19"), filenames.size()));
    unixls.processLines();

    table = "------------------------------------------------------------\n";
    table += "Alice       Bobby       Buffy       Carol       Chris     \n";
    table += "Cindy       Danny       Greg        Jan         Jody      \n";
    table += "Keith       Lori        Marsha      Miho        Mike      \n";
    table += "Mr._French  Peter       Shirley     Sissy       \n";
    table += "------------------------------------------------------------\n";

    assertThat(unixls.getTable(), is(equalTo(table)));
    assertThat(Unixls.getNumberOfColumns(10), is(equalTo(5)));
  }

  /**
   * Tests the getNumberOfColumns and getColumn methods using invalid input, which should throw an exception.
   */
  @Test
  public void testInvalidInput() {
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> Unixls.getNumberOfColumns(0));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> Unixls.getColumn(null, 10));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> Unixls.getColumn("text.txt", 0));
  }

  /**
   * Tests the getFilesTable method.
   */
  @Test
  public void testGetFilesTable() {
    assertThat(Unixls.getFilesTable(null), is(equalTo("")));
    assertThat(Unixls.getFilesTable(List.of()), is(equalTo("")));
    assertThat(Unixls.getFilesTable(List.of("text.txt")), is(equalTo("text.txt")));
  }
}
