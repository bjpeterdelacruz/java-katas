/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva.consecutive;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * JUnit tests for the {@link ConsecutivePrimes} class.
 * 
 * @author BJ Peter DeLaCruz &lt;bj.peter.delacruz@gmail.com&gt;
 */
public class TestConsecutivePrimes {

  /**
   * Tests the {@link ConsecutivePrimes#getCount(int)} and {@link ConsecutivePrimes#getResults(int)}
   * methods.
   */
  @Test
  public void testPrimes() {
    var primes = new ConsecutivePrimes();
    assertThat(primes.getCount(2), is(equalTo(1)));
    assertThat(primes.getResults(2), is(equalTo("[2]")));

    primes = new ConsecutivePrimes();
    assertThat(primes.getCount(53), is(equalTo(2)));
    assertThat(primes.getResults(53), is(equalTo("[5, 7, 11, 13, 17] [53]")));

    primes = new ConsecutivePrimes();
    assertThat(primes.getCount(41), is(equalTo(3)));
    assertThat(primes.getResults(41), is(equalTo("[2, 3, 5, 7, 11, 13] [11, 13, 17] [41]")));
  }

  /**
   * Tests the {@link ConsecutivePrimes#getCount(int)} and {@link ConsecutivePrimes#getResults(int)}
   * methods.
   */
  @Test
  public void testNoPrimes() {
    var primes = new ConsecutivePrimes();
    assertThat(primes.getCount(20), is(equalTo(0)));
    assertThat(primes.getResults(0), is(equalTo("[]")));
  }

}
