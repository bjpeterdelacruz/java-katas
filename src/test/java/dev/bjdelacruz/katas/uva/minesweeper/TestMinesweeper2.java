/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva.minesweeper;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import java.util.List;
import org.junit.jupiter.api.Test;
import dev.bjdelacruz.katas.uva.UvaKataTest;

/**
 * Unit tests for the {@link Minesweeper} kata.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestMinesweeper2 extends UvaKataTest {

  /** Tests a valid 4x4 minefield. */
  @Test
  public void testOutput1() {
    var m = new Minesweeper();
    m.setLines(List.of("4 4", "* . . .", ". . . .", ". * . .", ". . . ."));
    m.processLines();

    var expected = """
    Field #1
    *100
    2210
    1*10
    1110
    
    """;

    assertThat(getOutputStreamContents()).isEqualTo(expected);
  }

  /** Tests the equals and hashCode methods of the {@link MinesweeperSquare} class. */
  @Test
  public void testMinesweeperSquare() {
    var square1 = new MinesweeperSquare<>(1, 1, false);
    assertThat(square1).isEqualTo(new MinesweeperSquare<>(1, 1, false));

    assertThat(square1).isNotEqualTo(new MinesweeperSquare<>(2, 1, false));
    assertThat(square1).isNotEqualTo(new MinesweeperSquare<>(2, 2, false));
    assertThat(square1).isNotEqualTo(new MinesweeperSquare<>(1, 1, true));

    square1.incrementNumSurroundingMines();

    assertThat(square1).isNotEqualTo(new MinesweeperSquare<>(1, 1, false));

    var square2 = new MinesweeperSquare<>(1, 1, true);
    assertThat(square2).isEqualTo(new MinesweeperSquare<>(1, 1, true));

    assertThat(square2).isNotEqualTo(new MinesweeperSquare<>(2, 1, true));
    assertThat(square2).isNotEqualTo(new MinesweeperSquare<>(1, 2, true));
    assertThat(square2).isNotEqualTo(new MinesweeperSquare<>(1, 1, false));

    square2.incrementNumSurroundingMines();

    assertThat(square2).isNotEqualTo(new MinesweeperSquare<>(1, 1, true));
  }

  /** Tests a valid 3x5 minefield. */
  @Test
  public void testOutput2() {
    var m = new Minesweeper();
    m.setLines(List.of("3 5", "* * . . .", ". . . . .", ". * . . ."));
    m.processLines();

    var expected = """
    Field #1
    **100
    33200
    1*100
    
    """;

    assertThat(getOutputStreamContents()).isEqualTo(expected);
  }

  /** Tests invalid input values. */
  @Test
  public void testInvalidInput() {
    var m = new Minesweeper();

    m.setLines(List.of("4 4 4"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(m::processLines)
            .withMessageContainingAll("Expected 2 arguments: number of rows and number of columns.");

    m = new Minesweeper();

    m.setLines(List.of("2 2", ". .", "."));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(m::processLines)
            .withMessageContainingAll("Expected 2 columns. Found 1.");

    m = new Minesweeper();

    m.setLines(List.of("2 2", ". .", ". . ."));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(m::processLines)
            .withMessageContainingAll("Expected 2 columns. Found 3.");

    m = new Minesweeper();

    m.setLines(List.of("2 2", ". ."));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(m::processLines)
            .withMessageContainingAll("Expected 2 rows. Found 1.");

    m = new Minesweeper();

    m.setLines(List.of("2 2", ". .", ". .", ". ."));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(m::processLines)
            .withMessageContainingAll("Expected 2 rows. Found 3.");

    m.setLines(List.of("2 2", ". *", "+ ."));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(m::processLines)
            .withMessageContainingAll("Invalid character: +");

    m = new Minesweeper();

    m.setLines(List.of("2 2", ". *", "* ."));
    m.processLines();

    assertThat(m.getSquareAt(3, 1)).isNull();
    assertThat(m.getSquareAt(1, 3)).isNull();
    assertThat(m.getSquareAt(0, 1)).isNull();
    assertThat(m.getSquareAt(1, 0)).isNull();
    assertThat(m.getSquareAt(1, 1)).isNotNull();
    assertThat(m.getSquareAt(1, 2)).isNotNull();
    assertThat(m.getSquareAt(2, 1)).isNotNull();
    assertThat(m.getSquareAt(2, 2)).isNotNull();
  }

}
