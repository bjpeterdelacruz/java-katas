/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva.minesweeper;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * JUnit tests for the {@link Minesweeper} class.
 * 
 * @author BJ Peter DeLaCruz &lt;bj.peter.delacruz@gmail.com&gt;
 */
public class TestMinesweeper {

  private Minesweeper minesweeper;
  private static final String EMPTY_ROW = ". . . .";

  /**
   * Creates an empty grid and number of rows and columns in the grid.
   */
  @BeforeEach
  public void setUp() {
    minesweeper = new Minesweeper();
    minesweeper.setNumRows(2);
    minesweeper.setNumColumns(4);
  }

  /**
   * Tests the upper left corner in the grid.
   */
  @Test
  public void testUpperLeftCorner() {
    minesweeper.addRow("* . . .", 1);
    minesweeper.addRow(EMPTY_ROW, 2);
    minesweeper.processMine(minesweeper.getSquareAt(1, 1));
    assertThat(minesweeper.getSquareAt(1, 2).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(2, 2).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(2, 1).getNumSurroundingMines(), is(equalTo(1)));
  }

  /**
   * Tests the upper right corner in the grid.
   */
  @Test
  public void testUpperRightCorner() {
    minesweeper.addRow(". . . *", 1);
    minesweeper.addRow(EMPTY_ROW, 2);
    minesweeper.processMine(minesweeper.getSquareAt(1, 4));
    assertThat(minesweeper.getSquareAt(1, 3).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(2, 3).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(2, 4).getNumSurroundingMines(), is(equalTo(1)));
  }

  /**
   * Tests the lower right corner in the grid.
   */
  @Test
  public void testLowerRightCorner() {
    minesweeper.addRow(EMPTY_ROW, 1);
    minesweeper.addRow(". . . *", 2);
    minesweeper.processMine(minesweeper.getSquareAt(2, 4));
    assertThat(minesweeper.getSquareAt(1, 4).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(1, 3).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(2, 3).getNumSurroundingMines(), is(equalTo(1)));
  }

  /**
   * Tests the lower left corner in the grid.
   */
  @Test
  public void testLowerLeftCorner() {
    minesweeper.addRow(EMPTY_ROW, 1);
    minesweeper.addRow("* . . .", 2);
    minesweeper.processMine(minesweeper.getSquareAt(2, 1));
    assertThat(minesweeper.getSquareAt(1, 1).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(1, 2).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(2, 2).getNumSurroundingMines(), is(equalTo(1)));
  }

  /**
   * Tests the first row in the grid.
   */
  @Test
  public void testFirstRow() {
    minesweeper.addRow(". * . *", 1);
    minesweeper.addRow(EMPTY_ROW, 2);
    minesweeper.processMine(minesweeper.getSquareAt(1, 2));
    minesweeper.processMine(minesweeper.getSquareAt(1, 4));
    assertThat(minesweeper.getSquareAt(1, 1).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(1, 3).getNumSurroundingMines(), is(equalTo(2)));
    assertThat(minesweeper.getSquareAt(2, 1).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(2, 2).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(2, 3).getNumSurroundingMines(), is(equalTo(2)));
    assertThat(minesweeper.getSquareAt(2, 4).getNumSurroundingMines(), is(equalTo(1)));
  }

  /**
   * Tests the last row in the grid.
   */
  @Test
  public void testLastRow() {
    minesweeper.addRow(EMPTY_ROW, 1);
    minesweeper.addRow(". * . *", 2);
    minesweeper.processMine(minesweeper.getSquareAt(2, 2));
    minesweeper.processMine(minesweeper.getSquareAt(2, 4));
    assertThat(minesweeper.getSquareAt(1, 1).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(1, 2).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(1, 3).getNumSurroundingMines(), is(equalTo(2)));
    assertThat(minesweeper.getSquareAt(1, 4).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(2, 1).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(2, 3).getNumSurroundingMines(), is(equalTo(2)));
  }

  /**
   * Tests the first column in the grid.
   */
  @Test
  public void testFirstColumn() {
    minesweeper.setNumRows(4);
    minesweeper.addRow(EMPTY_ROW, 1);
    minesweeper.addRow("* . . .", 2);
    minesweeper.addRow(EMPTY_ROW, 3);
    minesweeper.addRow(". * . .", 4);
    minesweeper.processMine(minesweeper.getSquareAt(2, 1));
    minesweeper.processMine(minesweeper.getSquareAt(4, 2));
    assertThat(minesweeper.getSquareAt(1, 1).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(1, 2).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(2, 2).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(3, 1).getNumSurroundingMines(), is(equalTo(2)));
    assertThat(minesweeper.getSquareAt(3, 2).getNumSurroundingMines(), is(equalTo(2)));
    assertThat(minesweeper.getSquareAt(3, 3).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(4, 1).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(4, 3).getNumSurroundingMines(), is(equalTo(1)));
  }

  /**
   * Tests the last column in the grid.
   */
  @Test
  public void testLastColumn() {
    minesweeper.setNumRows(4);
    minesweeper.addRow(". * . .", 1);
    minesweeper.addRow(". * . *", 2);
    minesweeper.addRow(EMPTY_ROW, 3);
    minesweeper.addRow(EMPTY_ROW, 4);
    minesweeper.processMine(minesweeper.getSquareAt(1, 2));
    minesweeper.processMine(minesweeper.getSquareAt(2, 2));
    minesweeper.processMine(minesweeper.getSquareAt(2, 4));
    assertThat(minesweeper.getSquareAt(1, 1).getNumSurroundingMines(), is(equalTo(2)));
    assertThat(minesweeper.getSquareAt(2, 1).getNumSurroundingMines(), is(equalTo(2)));
    assertThat(minesweeper.getSquareAt(3, 1).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(3, 2).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(1, 3).getNumSurroundingMines(), is(equalTo(3)));
    assertThat(minesweeper.getSquareAt(2, 3).getNumSurroundingMines(), is(equalTo(3)));
    assertThat(minesweeper.getSquareAt(3, 3).getNumSurroundingMines(), is(equalTo(2)));
    assertThat(minesweeper.getSquareAt(1, 4).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(3, 4).getNumSurroundingMines(), is(equalTo(1)));
  }

  /**
   * Tests a square inside the grid.
   */
  @Test
  public void testInsideGrid() {
    minesweeper.setNumColumns(3);
    minesweeper.setNumRows(3);
    minesweeper.addRow(". . .", 1);
    minesweeper.addRow(". * .", 2);
    minesweeper.addRow(". . .", 3);
    minesweeper.processMine(minesweeper.getSquareAt(2, 2));
    assertThat(minesweeper.getSquareAt(1, 1).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(1, 2).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(1, 3).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(2, 1).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(2, 3).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(3, 1).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(3, 2).getNumSurroundingMines(), is(equalTo(1)));
    assertThat(minesweeper.getSquareAt(3, 3).getNumSurroundingMines(), is(equalTo(1)));
  }

}
