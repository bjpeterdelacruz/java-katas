/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * Unit tests for the {@link FollowingOrders} kata.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestFollowingOrders extends UvaKataTest {

  /** Tests whether the output is valid. */
  @Test
  public void testOutput() {
    var fo = new FollowingOrders();

    fo.setLines(List.of("a b f g", "a b b f"));
    fo.processLines();

    var expected = """
    abfg
    abgf
    agbf
    gabf""";

    assertThat(fo.getSolution()).isEqualTo(expected);

    fo = new FollowingOrders();

    fo.setLines(List.of("v w x y z", "v y x v z v w v"));
    fo.processLines();

    expected = """
    wxzvy
    wzxvy
    xwzvy
    xzwvy
    zwxvy
    zxwvy""";

    assertThat(fo.getSolution()).isEqualTo(expected);

    fo = new FollowingOrders();

    fo.setLines(List.of("x y z", "x y z y y z"));
    fo.processLines();

    assertThat(fo.getSolution()).endsWith("No ordering exists.");
  }

  /** Tests all invalid input values. */
  @Test
  public void testInvalidInput() {
    var fo = new FollowingOrders();

    fo.setLines(List.of("v w x"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(fo::processLines)
            .withMessageContainingAll("Size does not equal 2.");

    fo = new FollowingOrders();

    fo.setLines(List.of("v w x z z", "v y x v z v w v"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(fo::processLines)
            .withMessageContainingAll("Duplicate letters exist: v w x z z");

    fo = new FollowingOrders();

    fo.setLines(List.of("v w x y z", "v y x v z v w"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(fo::processLines)
            .withMessageContainingAll("Expected even number of constraints but was odd.");
  }

}
