package dev.bjdelacruz.katas.uva.treelevel;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import java.util.List;
import org.junit.jupiter.api.Test;
import dev.bjdelacruz.katas.uva.UvaKataTest;

/**
 * Unit tests for the {@link TreeLevel} kata.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestTreeLevel2 extends UvaKataTest {

  /** Tests whether the output is valid. */
  @Test
  public void testOutput() {
    var tree = new TreeLevel();

    tree.setLines(List.of("(11,LL) (7,LLL) (8,R)", "(5,) (4,L) (13,RL) (2,LLR) (1,RRR) (4,RR) ()", "(3,L} (4,8) ()"));
    tree.processLines();

    assertThat(getOutputStreamContents()).isEqualTo("5 4 8 11 13 4 7 2 1\nnot complete\n");
  }

  /** Tests all invalid input values. */
  @Test
  public void testInvalidInput() {
    var tree = new TreeLevel();

    tree.setLines(List.of("(11,LL) (7,LLL) (8,R)", "(5,) (4,L) (13,RL) (2,LLR) (1,RRR) (4,K) ()", "(3,L} (4,8) ()"));
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(tree::processLines)
            .withMessageContainingAll("Invalid character found: K");
  }

  /** Tests the {@link TreeNode} class. */
  @Test
  public void testNode() {
    var root1 = new TreeNode(10, "L");
    root1.setLeft(new TreeNode(9, "LL"));
    root1.setRight(new TreeNode(11, "LR"));

    var root2 = new TreeNode(10, "L");
    root2.setLeft(new TreeNode(9, "LL"));
    root2.setRight(new TreeNode(11, "LR"));

    assertThat(root1).isEqualTo(root2);

    assertThat(new TreeNode(10, "L")).isNotEqualTo(new TreeNode(9, "L"));
    assertThat(new TreeNode(10, "R")).isNotEqualTo(new TreeNode(10, "L"));

    var node1 = new TreeNode(10, "L");
    var node2 = new TreeNode(9, "LL");
    node1.setLeft(node2);

    assertThat(node1).isNotEqualTo(node2);

    node1 = new TreeNode(10, "L");
    node2 = new TreeNode(11, "LR");
    node1.setRight(node2);

    assertThat(node1).isNotEqualTo(node2);
  }

}
