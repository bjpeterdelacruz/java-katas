/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Unit tests for the {@link HistoryGrading} kata.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestHistoryGrading extends UvaKataTest {

  private Path tmp;

  /**
   * Creates a temporary file for the unit tests.
   * 
   * @throws IOException If there are problems creating the file.
   */
  @BeforeEach
  public void setUpTmpDir() throws IOException {
    tmp = Files.createTempFile("temp", ".tmp");
  }

  /**
   * Deletes the temporary file.
   * 
   * @throws IOException If there are problems deleting the file.
   */
  @AfterEach
  public void deleteTmpDir() throws IOException {
    Files.delete(tmp);
  }

  /**
   * Tests whether the output is valid.
   * 
   * @throws IOException If there are problems during the test.
   */
  @Test
  public void testOutput1() throws IOException {
    var history = new HistoryGrading();

    var input = """
            4
            4 2 3 1
            1 3 2 4
            3 2 1 4
            2 3 4 1""";
    try (var out = new PrintWriter(tmp.toFile(), StandardCharsets.UTF_8)) {
      out.println(input);
    }

    assertThat(history.readFile(tmp.toFile().getAbsolutePath())).isTrue();
    history.printResults();

    var expected = """
            Grades:
            1
            2
            3
            """;

    assertThat(getOutputStreamContents()).endsWith(expected);
  }

  /**
   * Tests whether the output is valid.
   * 
   * @throws IOException If there are problems during the test.
   */
  @Test
  public void testOutput2() throws IOException {
    var history = new HistoryGrading();

    var input = """
            10
            3 1 2 4 9 5 10 6 8 7
            1 2 3 4 5 6 7 8 9 10
            4 7 2 3 10 6 9 1 5 8
            3 1 2 4 9 5 10 6 8 7
            2 10 1 3 8 4 9 5 7 6""";
    try (var out = new PrintWriter(tmp.toFile(), StandardCharsets.UTF_8)) {
      out.println(input);
    }

    assertThat(history.readFile(tmp.toFile().getAbsolutePath())).isTrue();
    history.printResults();

    var expected = """
    Grades:
    6
    4
    10
    5
    """;

    assertThat(getOutputStreamContents()).endsWith(expected);
  }

  /**
   * Tests all invalid input values.
   * 
   * @throws IOException If there are problems during the test.
   */
  @Test
  public void testInvalidInput1() throws IOException {
    var history = new HistoryGrading();

    var input = """
            10
            3 1 2 4 9 5 10 6 8 7
            2 3 4 5 6 7 8 9 10
            4 7 2 3 10 6 9 1 5 8
            3 1 2 4 9 5 10 6 8 7
            2 10 1 3 8 4 9 5 7 6""";
    try (var out = new PrintWriter(tmp.toFile(), StandardCharsets.UTF_8)) {
      out.println(input);
    }
    assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> history.readFile(tmp.toFile().getAbsolutePath()))
            .withMessageContainingAll("Expected 10 responses from student. Found 9.");
  }

  /**
   * Tests all invalid input values.
   * 
   * @throws IOException If there are problems during the test.
   */
  @Test
  public void testInvalidInput2() throws IOException {
    var history = new HistoryGrading();

    try (var out = new PrintWriter(tmp.toFile(), StandardCharsets.UTF_8)) {
      out.println("1\n3 1 2 4 9 5 10 6 8 7");
    }
    assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> history.readFile(tmp.toFile().getAbsolutePath()))
            .withMessageContainingAll("Length must be between 2 and 20, inclusive.");
  }

  /**
   * Tests all invalid input values.
   * 
   * @throws IOException If there are problems during the test.
   */
  @Test
  public void testInvalidInput3() throws IOException {
    var history = new HistoryGrading();

    try (var out = new PrintWriter(tmp.toFile(), StandardCharsets.UTF_8)) {
      out.println("K\n3 1 2 4 9 5 10 6 8 7");
    }
    assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> history.readFile(tmp.toFile().getAbsolutePath()))
            .withMessageContainingAll("First line is not a number.");
  }

  /**
   * Tests all invalid input values.
   */
  @Test
  public void testInvalidInput4() {
    var history = new HistoryGrading();
    assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> history.readFile(tmp.toFile().getAbsolutePath()))
            .withMessageContainingAll("Empty file.");
  }

  /**
   * Tests all invalid input values.
   * 
   * @throws IOException If there are problems during the test.
   */
  @Test
  public void testInvalidInput5() throws IOException {
    var history = new HistoryGrading();

    try (var out = new PrintWriter(tmp.toFile(), StandardCharsets.UTF_8)) {
      out.println("2");
    }
    assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> history.readFile(tmp.toFile().getAbsolutePath()))
            .withMessageContainingAll("Missing line containing correct order of historical events.");
  }

  /**
   * Tests all invalid input values.
   * 
   * @throws IOException If there are problems during the test.
   */
  @Test
  public void testInvalidInput6() throws IOException {
    var history = new HistoryGrading();

    try (var out = new PrintWriter(tmp.toFile(), StandardCharsets.UTF_8)) {
      out.println("3\n1 2 3");
    }
    assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> history.readFile(tmp.toFile().getAbsolutePath()))
            .withMessageContainingAll("Need at least one student's responses.");
  }

  /**
   * Tests all invalid input values.
   * 
   * @throws IOException If there are problems during the test.
   */
  @Test
  public void testInvalidInput7() throws IOException {
    var history = new HistoryGrading();

    try (var out = new PrintWriter(tmp.toFile(), StandardCharsets.UTF_8)) {
      out.println("3\n1 2\n1 2 3");
    }
    assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> history.readFile(tmp.toFile().getAbsolutePath()))
            .withMessageContainingAll("Expected 3 historical events. Found 2.");
  }

  /**
   * Tests all invalid input values.
   */
  @Test
  public void testInvalidInput8() {
    var history1 = new HistoryGrading();
    assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> history1.readFile(""))
            .withMessageContainingAll("Filename must not be null or empty.");

    var history2 = new HistoryGrading();
    assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> history2.readFile(""))
            .withMessageContainingAll("Filename must not be null or empty.");

    var history3 = new HistoryGrading();
    assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> history3.readFile("nonexistingfile.txt"))
            .withMessageContainingAll("A problem occurred while trying to process nonexistingfile.txt.");
  }

  /**
   * Tests all invalid input values.
   * 
   * @throws IOException If there are problems during the test.
   */
  @Test
  public void testInvalidInput9() throws IOException {
    var history = new HistoryGrading();

    try (var out = new PrintWriter(tmp.toFile(), StandardCharsets.UTF_8)) {
      out.println("10\n3 1 2 4 9 5 10 6 8 8\n1 2 3 4 5 6 7 8 9 10\n4 7 2 3 10 6 9 1 5 8");
      out.println("3 1 2 4 9 5 10 6 8 7\n2 10 1 3 8 4 9 5 7 6");
    }

    assertThat(history.readFile(tmp.toFile().getAbsolutePath())).isFalse();
  }

  /**
   * Tests all invalid input values.
   * 
   * @throws IOException If there are problems during the test.
   */
  @Test
  public void testInvalidInput10() throws IOException {
    var history = new HistoryGrading();

    try (var out = new PrintWriter(tmp.toFile(), StandardCharsets.UTF_8)) {
      out.println("10\n3 1 2 4 9 5 10 6 8 7\n1 2 3 4 5 6 7 8 9 10\n4 7 2 3 10 6 9 1 5 8");
      out.println("3 1 2 4 9 5 10 6 8 7\n2 10 1 3 8 4 9 5 6 6");
    }

    assertThat(history.readFile(tmp.toFile().getAbsolutePath())).isFalse();
  }

}
