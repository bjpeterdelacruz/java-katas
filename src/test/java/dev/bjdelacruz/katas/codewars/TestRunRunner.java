package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link RunRunner}.
 */
public class TestRunRunner {

    /**
     * Tests the runRunners method by looping many times and each time checking whether the list of thread names
     * contains two and only two thread names.
     */
    @Test
    public void testRunRunners() {
        for (var i = 0; i < 250; i++) {
            RunRunner.runRunners();
            assertThat(RunRunner.getThreadNames(), is(equalTo(new HashSet<>(List.of("logan5-1", "jessica6-2")))));
        }
    }
}
