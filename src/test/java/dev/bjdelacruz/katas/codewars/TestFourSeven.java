package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link FourSeven}.
 */
public class TestFourSeven {

    /**
     * Tests the fourSeven method.
     *
     * @throws Exception If there are any issues reading in the source code file
     */
    @Test
    public void testFourSeven() throws Exception {
        assertThat(FourSeven.fourSeven(4), is(equalTo(7)));
        assertThat(FourSeven.fourSeven(7), is(equalTo(4)));
        assertThat(FourSeven.fourSeven(-10), is(equalTo(0)));
        assertThat(FourSeven.fourSeven(0), is(equalTo(0)));
        assertThat(FourSeven.fourSeven(10), is(equalTo(0)));
        var packageName = "/src/main/java/" + FourSeven.class.getPackage().getName().replace(".", "/");

        var file = new File(System.getProperty("user.dir") + packageName + "/FourSeven.java");
        assertThat(file.exists(), is(equalTo(true)));
        var p = Pattern.compile("(if|else|switch|\\?)");
        for (var line : Files.readAllLines(file.toPath(), StandardCharsets.UTF_8)) {
            // Verify that the source code does not contain any if statements, switch statements, or ternary operators.
            // Ignore these words if they appear in JavaDoc comments.
            assertThat(p.matcher(line).find() && !line.contains("*"), is(equalTo(false)));
        }
    }
}
