package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link ChangingLetters}.
 */
public class TestChangingLetters {

    /**
     * Tests the swap method.
     */
    @Test
    public void testSwap() {
        assertThat(ChangingLetters.swap("aiea"), is(equalTo("AIEA")));
        assertThat(ChangingLetters.swap(""), is(equalTo("")));
        assertThat(ChangingLetters.swap("Hello World"), is(equalTo("HEllO WOrld")));
        assertThat(ChangingLetters.swap("cnsnnts"), is(equalTo("cnsnnts")));
    }
}
