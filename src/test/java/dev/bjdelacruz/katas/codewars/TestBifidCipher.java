package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link BifidCipher}.
 */
public class TestBifidCipher {

    /**
     * Tests the encode process.
     */
    @Test
    public void testEncode() {
        assertThat(BifidCipher.encodeBifid("", "ELVIS"), is(equalTo("CWUAS")));
        assertThat(BifidCipher.encodeBifid("KEY", ""), is(equalTo("")));
        assertThat(BifidCipher.encodeBifid("CODEWARS", "WARRIOR"), is(equalTo("ORGFOSR")));
        assertThat(BifidCipher.encodeBifid("ELVIS", "JIMMY JAMES"), is(equalTo("EKUCERRRIS")));
        assertThat(BifidCipher.encodeBifid("BIFID", "THE INVENTOR OF BIFID IS"), is(equalTo("RCPGSSBBBDTELGTHLGHG")));
    }

    /**
     * Tests the decode process.
     */
    @Test
    public void testDecode() {
        assertThat(BifidCipher.decodeBifid("", "CWUAS"), is(equalTo("ELVIS")));
        assertThat(BifidCipher.decodeBifid("", ""), is(equalTo("")));
        assertThat(BifidCipher.decodeBifid("ELVIS", "GMGIIBT"), is(equalTo("MEMPHIS")));
        assertThat(BifidCipher.decodeBifid("INVENTOR", "DLXEBIQNLVEVZLE"), is(equalTo("FELIXDELASTELLE")));
    }

    /**
     * Gets the contents of the 2-dimensional matrix in the Polybius Square and tests whether the cipher key is
     * generated correctly.
     */
    @Test
    public void testPrintMatrix() {
        assertThat(BifidCipher.printMatrix("codewars"), is(equalTo("""
         \n C O D E W\040
          A R S B F\040
          G H I K L\040
          M N P Q T\040
          U V X Y Z\040
         """)));
        assertThat(BifidCipher.printMatrix(""), is(equalTo("""
         \n A B C D E\040
          F G H I K\040
          L M N O P\040
          Q R S T U\040
          V W X Y Z\040
         """)));
    }

    /**
     * Tests the encode process using invalid input, which should throw an exception.
     */
    @Test
    public void testInvalidInput() {
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(
                () -> BifidCipher.encodeBifid("KEY", "HELLO WORLD!"));
    }
}
