package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link PrintingArrayElements}.
 */
public class TestPrintingArrayElements {
    /**
     * Tests the printArray method.
     */
    @Test
    public void testPrintArray() {
        assertThat(PrintingArrayElements.printArray(new Integer[] {1, 3, 2, 4, 6, 8}),
                is(equalTo("1,3,2,4,6,8")));
        assertThat(PrintingArrayElements.printArray(new String[] {"a", "B", "c", "D"}), is(equalTo("a,B,c,D")));
    }
}
