package dev.bjdelacruz.katas.codewars;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ThreadLocalRandom;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link ConvertTimeToString}.
 */
public class TestConvertTimeToString {
    /**
     * Tests that the conversion is correct.
     */
    @Test
    public void shouldConvertCorrectly() {
        assertThat(ConvertTimeToString.convertTime(90061), is(equalTo("1 1 1 1")));
        assertThat(ConvertTimeToString.convertTime(-90061), is(equalTo("-1 -1 -1 -1")));
    }

    /**
     * Tests that the result consists of all zeroes.
     */
    @Test
    public void shouldReturnZeroes() {
        assertThat(ConvertTimeToString.convertTime(0), is(equalTo("0 0 0 0")));
        assertThat(ConvertTimeToString.convertTime(-0), is(equalTo("0 0 0 0")));
    }

    /**
     * Tests that seconds are converted correctly.
     */
    @Test
    public void shouldConvertSec() {
        assertThat(ConvertTimeToString.convertTime(33), is(equalTo("0 0 0 33")));
        assertThat(ConvertTimeToString.convertTime(-33), is(equalTo("0 0 0 -33")));

        assertThat(ConvertTimeToString.convertTime(55), is(equalTo("0 0 0 55")));
        assertThat(ConvertTimeToString.convertTime(-55), is(equalTo("0 0 0 -55")));
    }

    /**
     * Tests that minutes are converted correctly.
     */
    @Test
    public void shouldConvertMin() {
        assertThat(ConvertTimeToString.convertTime(98), is(equalTo("0 0 1 38")));
        assertThat(ConvertTimeToString.convertTime(-98), is(equalTo("0 0 -1 -38")));

        assertThat(ConvertTimeToString.convertTime(111), is(equalTo("0 0 1 51")));
        assertThat(ConvertTimeToString.convertTime(-111), is(equalTo("0 0 -1 -51")));
    }

    /**
     * Tests that hours are converted correctly.
     */
    @Test
    public void shouldConvertHour() {
        assertThat(ConvertTimeToString.convertTime(7260), is(equalTo("0 2 1 0")));
        assertThat(ConvertTimeToString.convertTime(-7260), is(equalTo("0 -2 -1 0")));
    }

    /**
     * Tests that days are converted correctly.
     */
    @Test
    public void shouldConvertDays() {
        assertThat(ConvertTimeToString.convertTime(93660), is(equalTo("1 2 1 0")));
        assertThat(ConvertTimeToString.convertTime(-93660), is(equalTo("-1 -2 -1 0")));
    }

    /**
     * Random tests
     */
    @Test
    public void randomTests() {
        for (var i = 0; i < 100; i++) {
            var time = -100000 + ThreadLocalRandom.current().nextInt(100000);
            assertThat(ConvertTimeToString.convertTime(time), is(equalTo(convertTime(time))));
        }
    }

    /**
     * Kata author's solution.
     *
     * @param timeDiff The number of seconds
     * @return A string containing the number of days, hours, minutes, and seconds
     */
    @SuppressFBWarnings()
    private static String convertTime(int timeDiff) {
        var days = (int) Math.floor(timeDiff / 86400);
        var hour = (int) Math.floor((timeDiff % 86400) / 3600);
        var min = (int) Math.floor((timeDiff % 3600) / 60);
        var sec = timeDiff % 60;

        return days + " " + hour + " " + min + " " + sec;
    }
}
