package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link DecipherTheMessage}.
 */
public class TestDecipherTheMessage {

    /**
     * Tests the decipher method using an empty string.
     */
    @Test
    public void testEmptyNote() {
        assertThat(DecipherTheMessage.decipher(""), is(equalTo("")));
    }

    /**
     * Tests the decipher method using a string that only contains lowercase letters.
     */
    @Test
    public void testLowerCaseNote() {
        assertThat(DecipherTheMessage.decipher(
                "lvahhe bl lh uhkbgz b vtg'm uxebxox maxkx tkx lh ftgr ahnkl exym"), is(equalTo(
                "school is so boring i can't believe there are so many hours left"
        )));
    }

    /**
     * Tests the decipher method using a string that only contains uppercase letters.
     */
    @Test
    public void testUpperCaseNote() {
        assertThat(DecipherTheMessage.decipher(
                "HFZ FTR RHNK WKXLL BL LH VNMX"), is(equalTo(
                "OMG MAY YOUR DRESS IS SO CUTE"
        )));
    }

    /**
     * Tests the decipher method using a string that only contains puncuation symbols.
     */
    @Test
    public void testPunctionationNote() {
        assertThat(DecipherTheMessage.decipher(
                "Axr Ftr! Pabva mxtvaxk wh rhn mabgd bl gbvxk, Fk. Chgxl hk Fkl. Itmxe?"), is(equalTo(
                "Hey May! Which teacher do you think is nicer, Mr. Jones or Mrs. Patel?"
        )));
    }
}
