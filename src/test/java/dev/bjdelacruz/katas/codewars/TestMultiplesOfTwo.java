package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link MultiplesOfTwo}.
 */
public class TestMultiplesOfTwo {
    /**
     * Tests the findMultiples method.
     */
    @Test
    public void testFindMultiples() {
        assertThat(MultiplesOfTwo.findMultiples(2, 4, 40), is(equalTo(List.of(4, 8, 12, 16, 20, 24, 28, 32, 36))));
        assertThat(MultiplesOfTwo.findMultiples(5, 13, 800),
                is(equalTo(List.of(65, 130, 195, 260, 325, 390, 455, 520, 585, 650, 715, 780))));
        assertThat(MultiplesOfTwo.findMultiples(27, 29, 2000), is(equalTo(List.of(783, 1566))));
        assertThat(MultiplesOfTwo.findMultiples(20, 16, 1000),
                is(equalTo(List.of(80, 160, 240, 320, 400, 480, 560, 640, 720, 800, 880, 960))));
        assertThat(MultiplesOfTwo.findMultiples(7, 16, 1000),
                is(equalTo(List.of(112, 224, 336, 448, 560, 672, 784, 896))));
    }
}
