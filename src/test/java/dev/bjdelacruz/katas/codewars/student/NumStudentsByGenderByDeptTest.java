package dev.bjdelacruz.katas.codewars.student;

import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link NumStudentsByGenderByDept}.
 */
public class NumStudentsByGenderByDeptTest {
    /**
     * Tests the {@link NumStudentsByGenderByDept#getNumStudentsByGenderByDepartment(Stream)} method.
     */
    @Test
    public void basicTestGetTheNumberOfStudentsByGenderForEachDepartment() {

        var actual = NumStudentsByGenderByDept.getNumStudentsByGenderByDepartment(
                StudentTestUtils.getStudents());
        var map1 = Map.of(Student.Gender.MALE, 1L, Student.Gender.FEMALE, 1L);
        var map2 = Map.of(Student.Gender.MALE, 2L, Student.Gender.FEMALE, 1L);

        assertThat(actual, is(equalTo(Map.of("CS", map1, "Philology", map2))));

    }
}
