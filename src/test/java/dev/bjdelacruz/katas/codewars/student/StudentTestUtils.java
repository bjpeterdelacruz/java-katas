package dev.bjdelacruz.katas.codewars.student;

import java.util.stream.Stream;

/**
 * This class contains helper methods that are called by unit tests that use the Student class.
 */
public final class StudentTestUtils {
    private StudentTestUtils() {
    }

    /**
     * Returns a stream of Student objects.
     *
     * @return A stream of Student objects.
     */
    public static Stream<Student> getStudents() {
        var george = new Student("George", 95, "Philology", Student.Gender.FEMALE);
        var anton = new Student("Anton", 90, "CS", Student.Gender.MALE);
        var jack = new Student("Jack", 82, "Philology", Student.Gender.MALE);
        var mike = new Student("Mike", 60, "Philology", Student.Gender.MALE);
        var jane = new Student("Jane", 65, "CS", Student.Gender.FEMALE);

        return Stream.of(george, anton, jack, mike, jane);
    }
}
