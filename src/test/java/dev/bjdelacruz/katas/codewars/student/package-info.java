/**
 * Contains unit tests for Codewars katas that use the Student class.
 */
package dev.bjdelacruz.katas.codewars.student;
