package dev.bjdelacruz.katas.codewars.student;

import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link NumStudentsByDept}.
 */
public class NumStudentsByDeptTest {
    /**
     * Tests the {@link NumStudentsByDept#getNumberOfStudentsByDepartment(Stream)} method.
     */
    @Test
    public void basicTestGetNumberOfStudentsByDepartment() {
        var actual = NumStudentsByDept.getNumberOfStudentsByDepartment(StudentTestUtils.getStudents());
        assertThat(actual, is(equalTo(Map.of("CS", 2L, "Philology", 3L))));
    }
}
