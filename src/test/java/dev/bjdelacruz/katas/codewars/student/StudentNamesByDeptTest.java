package dev.bjdelacruz.katas.codewars.student;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link StudentNamesByDept}.
 */
public class StudentNamesByDeptTest {
    /**
     * Tests the getStudentNamesByDepartment method.
     */
    @Test
    public void basicTestGetStudentNamesByDepartment() {
        var actual = StudentNamesByDept.getStudentNamesByDepartment(StudentTestUtils.getStudents());
        assertThat(actual, is(equalTo(
                Map.of("Philology", Arrays.asList("George", "Jack", "Mike"), "CS", Arrays.asList("Anton", "Jane")))));
    }
}
