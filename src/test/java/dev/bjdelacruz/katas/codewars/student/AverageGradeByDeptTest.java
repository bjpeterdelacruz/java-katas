package dev.bjdelacruz.katas.codewars.student;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link AverageGradeByDept}.
 */
public class AverageGradeByDeptTest {
    /**
     * Tests the getAverageGradeByDepartment method.
     */
    @Test
    public void basicTestGetAverageGradeByDepartment() {
        var actual = AverageGradeByDept.getAverageGradeByDepartment(StudentTestUtils.getStudents());
        assertThat(actual, is(equalTo(Map.of("CS", 77.5, "Philology", 79.0))));
    }
}
