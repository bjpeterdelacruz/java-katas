package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link AreaOrPerimeter}.
 */
public class TestAreaOrPerimeter {
    /**
     * Tests the areaOrPerimeter method.
     */
    @Test
    public void testAreaOrPerimeter() {
        assertThat(AreaOrPerimeter.areaOrPerimeter(5, 5), is(equalTo(25)));
        assertThat(AreaOrPerimeter.areaOrPerimeter(4, 5), is(equalTo(18)));
        assertThat(AreaOrPerimeter.areaOrPerimeter(5, 4), is(equalTo(18)));
        assertThat(AreaOrPerimeter.areaOrPerimeter(10, 10), is(equalTo(100)));
    }
}
