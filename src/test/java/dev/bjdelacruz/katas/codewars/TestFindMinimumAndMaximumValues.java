package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link FindMaximumAndMinimumValues}.
 */
public class TestFindMinimumAndMaximumValues {
    /**
     * Tests the max method.
     */
    @Test
    public void testMax() {
        assertThat(FindMaximumAndMinimumValues.max(new int[]{1, 2, 5, 4, 3, 10, 12, 11}), is(equalTo(12)));
        assertThat(FindMaximumAndMinimumValues.max(new int[]{2, 2, 2, 2, 2}), is(equalTo(2)));
    }

    /**
     * Tests the min method.
     */
    @Test
    public void testMin() {
        assertThat(FindMaximumAndMinimumValues.min(new int[]{1, 2, 5, 4, 3, 10, 12, 11}), is(equalTo(1)));
        assertThat(FindMaximumAndMinimumValues.min(new int[]{2, 2, 2, 2, 2}), is(equalTo(2)));
    }
}
