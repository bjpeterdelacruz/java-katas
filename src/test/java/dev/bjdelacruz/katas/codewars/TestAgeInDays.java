package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link AgeInDays}.
 */
public class TestAgeInDays {

    /**
     * Tests the ageInDays method.
     */
    @Test
    public void testAgeInDays() {
        assertThat(AgeInDays.ageInDays(2015, 11, 1, LocalDate.of(2015, 11, 30)),
                is(equalTo("You are 29 days old")));

        assertThat(AgeInDays.ageInDays(1986, 3, 31, LocalDate.of(2021, 3, 31)),
                is(equalTo("You are 12784 days old")));
    }
}
