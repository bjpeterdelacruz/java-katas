package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link Antistring}.
 */
public class TestAntistring {

    /**
     * Tests the antiString method.
     */
    @Test
    public void testAntiString() {
        assertThat(Antistring.antiString("a0"), is(equalTo("9Z")));
        assertThat(Antistring.antiString("aBc123"), is(equalTo("678XyZ")));
        assertThat(Antistring.antiString("678bcdXYZ"), is(equalTo("abcWXY123")));
    }
}
