package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;
import java.util.HashSet;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link ColorGhost}.
 */
public class TestColorGhost {
    /**
     * Tests the getColor method in the ColorGhost class.
     */
    @Test
    public void testGetColor() {
        var ghost = new ColorGhost();
        var colors = new HashSet<String>();
        while (colors.size() != 4) {
            colors.add(ghost.getColor());
        }
        for (var color : ColorGhost.getColors()) {
            assertThat(colors.contains(color), is(equalTo(true)));
        }
    }
}
