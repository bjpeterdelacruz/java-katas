package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link SortArrays1}.
 */
public class TestSortArrays1 {
    /**
     * Tests the string version of the sortArray method.
     */
    @Test
    public void testSortArrayString() {
        assertThat(SortArrays1.sortArray(new String[] {"b", "c", "a"}), is(equalTo(new String[] {"a", "b", "c"})));
    }

    /**
     * Tests the int version of the sortArray method.
     */
    @Test
    public void testSortArrayInt() {
        assertThat(SortArrays1.sortArray(new int[] {3, 2, 1}), is(equalTo(new int[] {1, 2, 3})));
    }

    /**
     * Tests the long version of the sortArray method.
     */
    @Test
    public void testSortArrayLong() {
        assertThat(SortArrays1.sortArray(new long[] {3L, 2L, 1L}), is(equalTo(new long[] {1L, 2L, 3L})));
    }

    /**
     * Tests the double version of the sortArray method.
     */
    @Test
    public void testSortArrayDouble() {
        assertThat(SortArrays1.sortArray(new double[] {3.0, 2.5, 1.0}), is(equalTo(new double[] {1.0, 2.5, 3.0})));
    }

    /**
     * Tests the float version of the sortArray method.
     */
    @Test
    public void testSortArrayFloat() {
        assertThat(SortArrays1.sortArray(new float[] {3.0F, 2.5F, 1.0F}), is(equalTo(new float[] {1.0F, 2.5F, 3.0F})));
    }
}
