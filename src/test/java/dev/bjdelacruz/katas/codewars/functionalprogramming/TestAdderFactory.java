package dev.bjdelacruz.katas.codewars.functionalprogramming;

import org.junit.jupiter.api.Test;

import java.util.function.IntUnaryOperator;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link AdderFactory}.
 */
public class TestAdderFactory {
    /**
     * Tests whether the {@link AdderFactory#create(int)} method returns an instance of
     * {@link java.util.function.IntUnaryOperator}.
     */
    @Test
    public void testCorrectFunctionType() {
        assertThat(AdderFactory.create(1), is(instanceOf(IntUnaryOperator.class)));
    }

    /**
     * Tests the functions returned by the {@link AdderFactory#create(int)} method.
     */
    @Test
    public void testMakeFunction() {
        assertThat(AdderFactory.create(1).applyAsInt(4), is(equalTo(5)));
        assertThat(AdderFactory.create(12).applyAsInt(-40), is(equalTo(-28)));
        assertThat(AdderFactory.create(-13).applyAsInt(5), is(equalTo(-8)));
        assertThat(AdderFactory.create(13587).applyAsInt(-21654), is(equalTo(-8067)));
    }

    /**
     * Tests a composed function, which consists of two functions that are returned by the
     * {@link AdderFactory#create(int)} method.
     */
    @Test
    public void testSeparateClosures() {
        var addOne = AdderFactory.create(1);
        var subOne = AdderFactory.create(-1);
        assertThat(addOne.applyAsInt(4), is(equalTo(5)));
        assertThat(subOne.applyAsInt(4), is(equalTo(3)));
        var composedToZero = addOne.andThen(subOne);
        assertThat(composedToZero.applyAsInt(4), is(equalTo(4)));
    }
}
