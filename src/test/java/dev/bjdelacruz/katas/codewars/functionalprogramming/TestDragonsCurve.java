package dev.bjdelacruz.katas.codewars.functionalprogramming;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link DragonsCurve}.
 */
public class TestDragonsCurve {
    private static final List<Integer> NUM_TIMES = Arrays.asList(0, 1, 2, 3, 4, 5);
    private static final Map<Integer, String> TESTS = new HashMap<>();
    private static final List<String> CURVES = Arrays.asList("F", "FRFR", "FRFRRLFLFR", "FRFRRLFLFRRLFRFRLLFLFR",
            "FRFRRLFLFRRLFRFRLLFLFRRLFRFRRLFLFRLLFRFRLLFLFR",
            "FRFRRLFLFRRLFRFRLLFLFRRLFRFRRLFLFRLLFRFRLLFLFRRLFRFRRLFLFRRLFRFRLLFLFRLLFRFRRLFLFRLLFRFRLLFLFR");

    /**
     * Sets up the {@link TestDragonsCurve#TESTS} map before running the tests.
     */
    @BeforeEach
    public void setup() {
        IntStream.range(0, NUM_TIMES.size()).forEach(i -> TESTS.put(NUM_TIMES.get(i), CURVES.get(i)));
    }

    /**
     * Tests the {@link DragonsCurve#createCurve(int)} method.
     */
    @Test
    public void testCreateCurve() {
        TESTS.forEach((i, s) -> assertThat(DragonsCurve.createCurve(i), is(equalTo(s))));
    }

    /**
     * Tests the {@link DragonsCurve#howMany(char, String)} method.
     */
    @Test
    public void testHowMany() {
        final var fCounts = Arrays.asList(1L, 2L, 4L, 8L, 16L, 32L);
        final var rCounts = Arrays.asList(0L, 2L, 4L, 8L, 16L, 32L);
        final var lCounts = Arrays.asList(0L, 0L, 2L, 6L, 14L, 30L);
        for (var i = 0; i < CURVES.size(); i++) {
            var p = CURVES.get(i);
            assertThat(DragonsCurve.howMany('F', p), is(equalTo(fCounts.get(i))));
            assertThat(DragonsCurve.howMany('R', p), is(equalTo(rCounts.get(i))));
            assertThat(DragonsCurve.howMany('L', p), is(equalTo(lCounts.get(i))));
            assertThat(DragonsCurve.howMany('a', p), is(equalTo(0L)));
            assertThat(DragonsCurve.howMany('b', p), is(equalTo(0L)));
        }
    }
}
