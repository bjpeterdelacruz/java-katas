/**
 * Contains three katas related to functional programming using lambdas.
 */
package dev.bjdelacruz.katas.codewars.functionalprogramming;
