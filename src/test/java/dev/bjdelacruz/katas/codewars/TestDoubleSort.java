package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link DoubleSort}.
 */
public class TestDoubleSort {

    /**
     * Tests the {@link DoubleSort#dbSort(Object[])} method.
     */
    @Test
    public void testDbSort() {
        var objects = new Object[][]{
                {new Integer[]{6, 2, 3, 4, 5}, new Integer[]{2, 3, 4, 5, 6}},
                {new Integer[]{14, 32, 3, 5, 5}, new Integer[]{3, 5, 5, 14, 32}},
                {new Integer[]{1, 2, 3, 4, 5}, new Integer[]{1, 2, 3, 4, 5}},
                {new Object[]{"Banana", "Orange", "Apple", "Mango", 0, 2, 2},
                        new Object[]{0, 2, 2, "Apple", "Banana", "Mango", "Orange"}},
                {new Object[]{"C", "W", "W", "W", 1, 2, 0}, new Object[]{0, 1, 2, "C", "W", "W", "W"}},
                {new Object[]{"Hackathon", "Katathon", "Code", "CodeWars", "Laptop", "Macbook", "JavaScript",
                        1, 5, 2}, new Object[]{1, 2, 5, "Code", "CodeWars", "Hackathon", "JavaScript", "Katathon",
                        "Laptop", "Macbook"}},
                {new Object[]{66, "t", 101, 0, 1, 1}, new Object[]{0, 1, 1, 66, 101, "t"}},
                {new Object[]{78, 117, 110, 99, 104, 117, 107, 115, 4, 6, 5, "west"},
                        new Object[]{4, 5, 6, 78, 99, 104, 107, 110, 115, 117, 117, "west"}},
                {new Object[]{101, 45, 75, 105, 99, 107, "y", "no", "yes", 1, 2, 4},
                        new Object[]{1, 2, 4, 45, 75, 99, 101, 105, 107, "no", "y", "yes"}},
                {new Object[]{80, 117, 115, 104, 45, 85, 112, 115, 6, 7, 2},
                        new Object[]{2, 6, 7, 45, 80, 85, 104, 112, 115, 115, 117}},
                {new Object[]{1, 1, 1, 1, 1, 2, "1", "2", "three", 1, 2, 3},
                        new Object[]{1, 1, 1, 1, 1, 1, 2, 2, 3, "1", "2", "three"}},
                {new Object[]{78, 33, 22, 44, 88, 9, 6, 0, 5, 0}, new Object[]{0, 0, 5, 6, 9, 22, 33, 44, 78, 88}},
                {new Object[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3},
                        new Object[]{1, 1, 2, 2, 3, 3, 4, 5, 6, 7, 8, 9}},
                {new Object[]{82, 18, 72, 1, 11, 12, 12, 12, 12, 115, 667, 12, 2, 8, 3},
                        new Object[]{1, 2, 3, 8, 11, 12, 12, 12, 12, 12, 18, 72, 82, 115, 667}},
                {new Object[]{"t", "e", "s", "t", 3, 4, 1}, new Object[]{1, 3, 4, "e", "s", "t", "t"}},
                {new Object[]{"what", "a", "great", "kata", 1, 2, 2},
                        new Object[]{1, 2, 2, "a", "great", "kata", "what"}},
                {new Object[]{66, "codewars", 11, "alex loves pushups", 2, 3, 0},
                        new Object[]{0, 2, 3, 11, 66, "alex loves pushups", "codewars"}},
                {new Object[]{"come", "on", 110, "2500", 10, "!", 7, 15, 5, 6, 6},
                        new Object[]{5, 6, 6, 7, 10, 15, 110, "!", "2500", "come", "on"}},
                {new Object[]{"when\"s", "the", "next", "Katathon?", 9, 7, 0, 1, 2},
                        new Object[]{0, 1, 2, 7, 9, "Katathon?", "next", "the", "when\"s"}},
                {new Object[]{8, 7, 5, "bored", "of", "writing", "tests", 115, 6, 7, 0},
                        new Object[]{0, 5, 6, 7, 7, 8, 115, "bored", "of", "tests", "writing"}},
                {new Object[]{"anyone", "want", "to", "hire", "me?", 2, 4, 1},
                        new Object[]{1, 2, 4, "anyone", "hire", "me?", "to", "want"}},
                {new Object[]{}, new Object[]{}},
                {new Object[0], new Object[0]},
        };
        for (var row : objects) {
            assertThat(DoubleSort.dbSort((Object[]) row[0]), is(equalTo((Object[]) row[1])));
        }
    }
}
