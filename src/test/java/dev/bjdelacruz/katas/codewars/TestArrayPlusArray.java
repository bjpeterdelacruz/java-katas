package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link ArrayPlusArray}.
 */
public class TestArrayPlusArray {
    /**
     * Tests the arrayPlusArray method.
     */
    @Test
    public void testAreaOrPerimeter() {
        assertThat(ArrayPlusArray.arrayPlusArray(new int[] {1, 2, 3}, new int[] {3, 3, 3}), is(equalTo(15)));
        assertThat(ArrayPlusArray.arrayPlusArray(new int[] {1, 2, 3}, new int[] {}), is(equalTo(6)));
        assertThat(ArrayPlusArray.arrayPlusArray(new int[] {}, new int[] {3, 3, 3}), is(equalTo(9)));
        assertThat(ArrayPlusArray.arrayPlusArray(new int[] {}, new int[] {}), is(equalTo(0)));
    }
}
