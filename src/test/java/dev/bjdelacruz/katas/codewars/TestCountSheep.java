package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link CountSheep}.
 */
public class TestCountSheep {
    /**
     * Tests the countingSheep method.
     */
    @Test
    public void testCountingSheep() {
        assertThat(CountSheep.countingSheep(5), is(equalTo("1 sheep...2 sheep...3 sheep...4 sheep...5 sheep...")));
    }
}
