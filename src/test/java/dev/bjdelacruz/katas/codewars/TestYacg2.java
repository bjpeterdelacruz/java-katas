package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link Yacg2}.
 */
public class TestYacg2 {
    private static final String[] PLAYERS = {"Alice", "Bob", "Charlie", "Dana", "Erica", "Fred", "Greg"};

    /**
     * Tests the Yacg2 solution.
     */
    @Test
    public void sampleTests() {
        test(new String[]{"AH", "2H", "3H"}, "Alice wins");
        test(new String[]{"2H", "KH", "3H"}, "Bob wins");
        test(new String[]{"2H", "3H", "4H"}, "Charlie wins");
        test(new String[]{"2S", "KC", "2C", "3C"}, "Alice wins");
        test(new String[]{"2C", "KD", "QC", "3C"}, "Charlie wins");
        test(new String[]{"2C", "3C", "JD", "4C"}, "Dana wins");
        test(new String[]{"9D", "6D", "9C"}, "Alice wins");
        test(new String[]{"TC", "QC", "QS"}, "Bob wins");
        test(new String[]{"3S", "3H", "4S"}, "Charlie wins");
    }

    private static void test(String[] cards, String expected) {
        assertThat(Yacg2.winnerOfTrick(cards, Arrays.copyOf(PLAYERS, cards.length)), is(equalTo(expected)));
    }
}
