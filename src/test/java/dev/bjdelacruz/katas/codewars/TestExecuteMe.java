package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link ExecuteMe}.
 */
public class TestExecuteMe {

    /**
     * Tests the {@link ExecuteMe#execute(Runnable, int)} method.
     */
    @Test
    public void testExecute() {
        var executionCounter = new AtomicInteger();
        ExecuteMe.execute(executionCounter::incrementAndGet, 100);
        assertThat(executionCounter.get(), is(equalTo(100)));
    }
}
