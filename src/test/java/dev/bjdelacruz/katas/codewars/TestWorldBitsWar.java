package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link WorldBitsWar}.
 */
public class TestWorldBitsWar {
    /**
     * Tests the bitsWar method.
     */
    @Test
    public void testAreaOrPerimeter() {
        assertThat(WorldBitsWar.bitsWar(new int[] {7, -3, -2, 6}), is(equalTo("tie")));
        assertThat(WorldBitsWar.bitsWar(new int[] {7, -3, 20}), is(equalTo("evens win")));
        assertThat(WorldBitsWar.bitsWar(new int[] {1, 5, 12}), is(equalTo("odds win")));
    }
}
