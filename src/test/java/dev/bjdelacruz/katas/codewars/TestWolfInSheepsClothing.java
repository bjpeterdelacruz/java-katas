package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link WolfInSheepsClothing}.
 */
public class TestWolfInSheepsClothing {
    /**
     * Tests the bitsWar method.
     */
    @Test
    public void testAreaOrPerimeter() {
        assertThat(WolfInSheepsClothing.warnTheSheep(new String[] {"sheep", "sheep", "wolf", "sheep"}),
                is(equalTo("Oi! Sheep number 1! You are about to be eaten by a wolf!")));
        assertThat(WolfInSheepsClothing.warnTheSheep(
                new String[] {"sheep", "sheep", "wolf", "sheep", "sheep", "sheep"}),
                is(equalTo("Oi! Sheep number 3! You are about to be eaten by a wolf!")));
        assertThat(WolfInSheepsClothing.warnTheSheep(new String[] {"sheep", "sheep", "sheep", "sheep", "wolf"}),
                is(equalTo("Pls go away and stop eating my sheep")));
    }
}
