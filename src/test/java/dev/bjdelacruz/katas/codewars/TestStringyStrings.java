package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link StringyStrings}.
 */
public class TestStringyStrings {
    /**
     * Tests the stringy method with negative and positive integers.
     */
    @Test
    public void testStringy() {
        assertThat(StringyStrings.stringy(-1), is(equalTo("")));
        assertThat(StringyStrings.stringy(0), is(equalTo("")));
        assertThat(StringyStrings.stringy(1), is(equalTo("1")));
        assertThat(StringyStrings.stringy(2), is(equalTo("10")));
        assertThat(StringyStrings.stringy(5), is(equalTo("10101")));
        assertThat(StringyStrings.stringy(6), is(equalTo("101010")));
    }
}
