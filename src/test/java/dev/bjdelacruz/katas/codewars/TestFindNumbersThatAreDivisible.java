package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link FindNumbersThatAreDivisible}.
 */
public class TestFindNumbersThatAreDivisible {
    /**
     * Tests the divisibleBy method.
     */
    @Test
    public void testDivisibleBy() {
        assertThat(FindNumbersThatAreDivisible.divisibleBy(new int[] {2, 4, 3, 6, 1, 5, 9, 10}, 3),
                is(equalTo(new int[] {3, 6, 9})));
        assertThat(FindNumbersThatAreDivisible.divisibleBy(new int[] {2, 4, 3, 6, 1, 5, 9, 10}, 12),
                is(equalTo(new int[] {})));
    }
}
