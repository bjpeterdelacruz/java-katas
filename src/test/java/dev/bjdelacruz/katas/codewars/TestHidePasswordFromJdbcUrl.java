package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link HidePasswordFromJdbcUrl}.
 */
public class TestHidePasswordFromJdbcUrl {
    /**
     * Tests the hidePasswordFromConnection method.
     */
    @Test
    public void testHidePasswordFromConnection() {
        assertThat(HidePasswordFromJdbcUrl.hidePasswordFromConnection(
                "jdbc:mysql://sdasdasdasd:szdasdasd:dfsdfsdfsdf/sdfsdfsdf?user=root&password=12345"),
                is(equalTo("jdbc:mysql://sdasdasdasd:szdasdasd:dfsdfsdfsdf/sdfsdfsdf?user=root&password=*****")));
        assertThat(HidePasswordFromJdbcUrl.hidePasswordFromConnection(
                "jdbc:mysql://sdasdasdasd:szdasdasd:dfsdfsdfsdf/sdfsdfsdf?password=12345&user=root"),
                is(equalTo("jdbc:mysql://sdasdasdasd:szdasdasd:dfsdfsdfsdf/sdfsdfsdf?password=*****&user=root")));
    }
}
