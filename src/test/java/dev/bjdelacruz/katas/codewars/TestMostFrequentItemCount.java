package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link MostFrequentItemCount}.
 */
public class TestMostFrequentItemCount {

    /**
     * Tests the mostFrequentItemCount method.
     */
    @Test
    public void testMostFrequentItemCount() {
        assertThat(MostFrequentItemCount.mostFrequentItemCount(new int[] {}), is(equalTo(0)));
        assertThat(MostFrequentItemCount.mostFrequentItemCount(new int[] {1, 2, 3}), is(equalTo(1)));
        assertThat(MostFrequentItemCount.mostFrequentItemCount(new int[] {-1, 2, -1, -1, 5}), is(equalTo(3)));
    }
}
