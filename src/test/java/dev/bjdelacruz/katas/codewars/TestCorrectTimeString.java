package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link CorrectTimeString}.
 */
public class TestCorrectTimeString {
    private void test(String input) {
        assertThat(CorrectTimeString.timeCorrect(input), is(equalTo(input)));
    }

    /**
     * Tests that the output is the same as the input.
     */
    @Test
    public void testSameInputSameOutput() {
        Arrays.asList(null, "", "09:10:01", "00:00:00", "23:59:59", "12:34:56").forEach(this::test);
    }

    /**
     * Tests that the time is corrected correctly.
     */
    @Test
    public void testCorrection() {
        assertThat(CorrectTimeString.timeCorrect("11:70:10"), is(equalTo("12:10:10")));
        assertThat(CorrectTimeString.timeCorrect("19:99:09"), is(equalTo("20:39:09")));
        assertThat(CorrectTimeString.timeCorrect("19:99:99"), is(equalTo("20:40:39")));
        assertThat(CorrectTimeString.timeCorrect("24:01:01"), is(equalTo("00:01:01")));
        assertThat(CorrectTimeString.timeCorrect("52:01:01"), is(equalTo("04:01:01")));
    }

    /**
     * Tests that null is returned for invalid input.
     */
    @Test
    public void testInvalidFormat() {
        assertThat(CorrectTimeString.timeCorrect("0:00:00"), is(nullValue()));
        assertThat(CorrectTimeString.timeCorrect("00:0:00"), is(nullValue()));
        assertThat(CorrectTimeString.timeCorrect("00:00:0"), is(nullValue()));
        assertThat(CorrectTimeString.timeCorrect("-0:00:00"), is(nullValue()));
        assertThat(CorrectTimeString.timeCorrect("00:00:000"), is(nullValue()));
        assertThat(CorrectTimeString.timeCorrect("000000"), is(nullValue()));
        assertThat(CorrectTimeString.timeCorrect("00;11;22"), is(nullValue()));
        assertThat(CorrectTimeString.timeCorrect("00:00:0/"), is(nullValue()));
        assertThat(CorrectTimeString.timeCorrect(":0:00:00"), is(nullValue()));
    }

    /**
     * Tests that the time is corrected correctly (edge cases).
     */
    @Test
    public void testEdgeCases() {
        assertThat(CorrectTimeString.timeCorrect("24:00:00"), is(equalTo("00:00:00")));
        assertThat(CorrectTimeString.timeCorrect("23:59:60"), is(equalTo("00:00:00")));
        assertThat(CorrectTimeString.timeCorrect("47:59:60"), is(equalTo("00:00:00")));
        assertThat(CorrectTimeString.timeCorrect("47:60:59"), is(equalTo("00:00:59")));
        assertThat(CorrectTimeString.timeCorrect("12:00:00"), is(equalTo("12:00:00")));
        assertThat(CorrectTimeString.timeCorrect("99:99:99"), is(equalTo("04:40:39")));
        assertThat(CorrectTimeString.timeCorrect("24:60:00"), is(equalTo("01:00:00")));
        assertThat(CorrectTimeString.timeCorrect("25:00:00"), is(equalTo("01:00:00")));
    }
}
