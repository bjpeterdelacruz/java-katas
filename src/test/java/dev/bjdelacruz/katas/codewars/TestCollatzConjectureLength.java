package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link CollatzConjectureLength}.
 */
public class TestCollatzConjectureLength {

    /**
     * Tests the conjecture method.
     */
    @Test
    public void testConjecture() {
        assertThat(CollatzConjectureLength.conjecture(20), is(equalTo(8L)));
        assertThat(CollatzConjectureLength.conjecture(2), is(equalTo(2L)));
        assertThat(CollatzConjectureLength.conjecture(3), is(equalTo(8L)));
        assertThat(CollatzConjectureLength.conjecture(4), is(equalTo(3L)));
    }
}
