package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link ConferenceTraveller}.
 */
public class TestConferenceTraveller {
    /**
     * Tests the conferencePlanner method.
     */
    @Test
    public void testConferencePlanner() {
        assertThat(ConferenceTraveller.conferencePicker(
                new String[] {"Mexico City", "Johannesburg", "Stockholm", "Melbourne"},
                new String[] {"Stockholm", "Balangoda", "Melbourne"}), is(equalTo("Balangoda")));

        assertThat(ConferenceTraveller.conferencePicker(
                new String[] {"Tokyo", "Madrid", "Melbourne", "Sydney", "Houston"},
                new String[] {"Sydney", "Chicago", "Paris"}), is(equalTo("Chicago")));

        assertThat(ConferenceTraveller.conferencePicker(
                new String[] {"London", "Berlin", "Mexico City", "Melbourne"},
                new String[]{"Berlin", "London"}), is(equalTo("No worthwhile conferences this year!")));
    }
}
