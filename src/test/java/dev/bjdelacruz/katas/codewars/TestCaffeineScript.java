package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link CaffeineScript}.
 */
public class TestCaffeineScript {

    /**
     * Tests the caffeineBuzz method.
     */
    @Test
    public void basicTests() {
        assertThat(CaffeineScript.caffeineBuzz(36), is(equalTo("CoffeeScript")));
        assertThat(CaffeineScript.caffeineBuzz(9), is(equalTo("Java")));
        assertThat(CaffeineScript.caffeineBuzz(18), is(equalTo("JavaScript")));
        assertThat(CaffeineScript.caffeineBuzz(17), is(equalTo("mocha_missing!")));
    }
}
