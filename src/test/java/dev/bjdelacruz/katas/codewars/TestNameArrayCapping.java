package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link NameArrayCapping}.
 */
public class TestNameArrayCapping {
    /**
     * Tests the capMe method with lowercase strings.
     */
    @Test
    public void testCapMeDowncase() {
        var strings = new String[] {"jo", "nelson", "jurie"};
        assertThat(NameArrayCapping.capMe(strings), is(equalTo(new String[] {"Jo", "Nelson", "Jurie"})));
    }

    /**
     * Tests the capMe method with uppercase strings.
     */
    @Test
    public void testCapMeCaps() {
        var strings = new String[] {"OZZA", "ARRA", "AZZA"};
        assertThat(NameArrayCapping.capMe(strings), is(equalTo(new String[] {"Ozza", "Arra", "Azza"})));
    }

    /**
     * Tests the capMe method with strings that contain lowercase and uppercase characters.
     */
    @Test
    public void testCapMeDifferent() {
        var strings = new String[] {"Ror", "NOR", "xor", ""};
        assertThat(NameArrayCapping.capMe(strings), is(equalTo(new String[] {"Ror", "Nor", "Xor", ""})));
    }
}
