package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link OneTwoThreeFour}.
 */
public class TestOneTwoThreeFour {

    /**
     * Tests whether an English number yields the correct MyNumber enum.
     */
    @Test
    public void testEnglish() {
        assertThat(OneTwoThreeFour.getNumber("one").intValue(), is(equalTo(1)));
        assertThat(OneTwoThreeFour.getNumber("two").intValue(), is(equalTo(2)));
        assertThat(OneTwoThreeFour.getNumber("three").intValue(), is(equalTo(3)));
        assertThat(OneTwoThreeFour.getNumber("four").intValue(), is(equalTo(4)));
    }

    /**
     * Tests whether a Japanese number yields the correct MyNumber enum.
     */
    @Test
    public void testJapanese() {
        assertThat(OneTwoThreeFour.getNumber("ichi").intValue(), is(equalTo(1)));
        assertThat(OneTwoThreeFour.getNumber("ni").intValue(), is(equalTo(2)));
        assertThat(OneTwoThreeFour.getNumber("san").intValue(), is(equalTo(3)));
        assertThat(OneTwoThreeFour.getNumber("shi").intValue(), is(equalTo(4)));
    }

    /**
     * Tests whether a French number yields the correct MyNumber enum.
     */
    @Test
    public void testFrench() {
        assertThat(OneTwoThreeFour.getNumber("un").intValue(), is(equalTo(1)));
        assertThat(OneTwoThreeFour.getNumber("deux").intValue(), is(equalTo(2)));
        assertThat(OneTwoThreeFour.getNumber("trois").intValue(), is(equalTo(3)));
        assertThat(OneTwoThreeFour.getNumber("quatre").intValue(), is(equalTo(4)));
    }
}
