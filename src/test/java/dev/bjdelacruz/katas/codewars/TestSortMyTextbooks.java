package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link SortMyTextbooks}.
 */
public class TestSortMyTextbooks {
    /**
     * Tests the sort method.
     */
    @Test
    public void testSort() {
        assertThat(SortMyTextbooks.sort(List.of("HISTORY", "Algebra", "english", "Biology")),
                is(equalTo(List.of("Algebra", "Biology", "english", "HISTORY"))));
    }
}
