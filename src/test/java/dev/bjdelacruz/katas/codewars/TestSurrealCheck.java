package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Unit tests for {@link SurrealCheck}.
 */
public class TestSurrealCheck {

    /**
     * Simple tests.
     */
    @Test
    public void simpleTest() {
        var sc = new SurrealCheck();
        assertThat(sc.isSurreal("-1", "1")).isTrue();
        assertThat(sc.isSurreal("-3 -2 -1 0", "17")).isTrue();
        assertThat(sc.isSurreal("1/2", "3/4")).isTrue();
        assertThat(sc.isSurreal("18 33 -2 19 -1/8", "99 34 200/3")).isTrue();
        assertThat(sc.isSurreal("-3", "-2 65")).isTrue();

        assertThat(sc.isSurreal("1", "-1")).isFalse();
        assertThat(sc.isSurreal("18 33 -2 19 -1/8", "99 31 200/3")).isFalse();
    }

    /**
     * Tests using empty strings.
     */
    @Test
    public void emptySetTest() {
        var sc = new SurrealCheck();
        assertThat(sc.isSurreal("", "1")).isTrue();
        assertThat(sc.isSurreal("-5", "")).isTrue();
        assertThat(sc.isSurreal("-5/12", "")).isTrue();
        assertThat(sc.isSurreal("", "")).isTrue();

        assertThat(sc.isSurreal(" \t \n ", "1")).isTrue();
        assertThat(sc.isSurreal("1", " \n \t ")).isTrue();
        assertThat(sc.isSurreal(null, "10/100")).isTrue();
        assertThat(sc.isSurreal("-25/100", null)).isTrue();
    }

    /**
     * Tests using strings that contain same elements.
     */
    @Test
    public void maxMinTest() {
        var sc = new SurrealCheck();
        assertThat(sc.isSurreal("1", "1")).isFalse();
        assertThat(sc.isSurreal("-1 0 1", "1 2 3 4 5 6")).isFalse();
        assertThat(sc.isSurreal("-1/2", "-1/2")).isFalse();
    }
}
