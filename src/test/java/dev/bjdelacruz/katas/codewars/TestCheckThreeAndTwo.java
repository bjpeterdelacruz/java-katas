package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link CheckThreeAndTwo}.
 */
public class TestCheckThreeAndTwo {

    /**
     * Tests the checkThreeAndTwo method.
     */
    @Test
    public void testCheckThreeAndTwo() {
        assertThat(CheckThreeAndTwo.checkThreeAndTwo(new char[] {'a', 'a', 'b', 'b', 'b'}), is(equalTo(true)));
        assertThat(CheckThreeAndTwo.checkThreeAndTwo(new char[] {'a', 'a', 'b', 'b', 'c'}), is(equalTo(false)));
        assertThat(CheckThreeAndTwo.checkThreeAndTwo(new char[] {'a', 'a', 'a', 'b', 'b'}), is(equalTo(true)));
        assertThat(CheckThreeAndTwo.checkThreeAndTwo(new char[] {'a', 'a', 'a', 'a', 'a'}), is(equalTo(false)));
    }
}
