package dev.bjdelacruz.katas.codewars;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.awt.Point;
import java.math.BigInteger;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Assertions.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * Unit tests for {@link Counter}.
 */
public class TestCounter {

    /**
     * Tests the basic constructors (no-arg, capacity with default load factor, and capacity with load factor).
     */
    @Test
    public void testConstructorsBasics() {
        try {
            new Counter<>();
        }
        catch (Exception e) {
            fail("Failed trying to use basic constructor:\n Counter<Integer> c = new Counter()");
        }
        try {
            new Counter<>(100);
        }
        catch (Exception e) {
            fail("Failed trying to use basic constructor:\n Counter<Integer> c = new Counter(100)");
        }
        try {
            new Counter<>(100, .75f);
        }
        catch (Exception e) {
            fail("Failed trying to use basic constructor:\n Counter<Integer> c = new Counter(100, .75f)");
        }
    }


    /**
     * Tests the constructors that create a Counter object from a collection, array, stream, or map.
     */
    @Test
    public void testConstructorsCountOneShot() {

        // COLLECTIONS:

        List<Integer> lst = Arrays.asList(-8, 2, 3, 5, 6, 89, -8, -8, 3, 53, 6, 3, 5, 8);

        Map<Integer, Long> expectedLst = new HashMap<>() {{
            put(-8, 3L);
            put(2, 1L);
            put(3, 3L);
            put(5, 2L);
            put(6, 2L);
            put(89, 1L);
            put(53, 1L);
            put(8, 1L);
        }};

        assertEquals(expectedLst, new Counter<>(new ArrayList<>(lst)));
        assertEquals(expectedLst, new Counter<>(new LinkedList<>(lst)));

        Map<Integer, Long> expectedSet = new HashMap<>() {{
            expectedLst.forEach((k, v) -> put(k, 1L));
        }};
        assertEquals(expectedSet, new Counter<>(new HashSet<>(lst)));

        // ARRAYS:

        String[] arr = {"a", "a", "a", "b", "b", "c", "c", "c", "c", "c", "c"};
        Map<String, Long> expected1 = new HashMap<>() {{
            put("a", 3L);
            put("b", 2L);
            put("c", 6L);
        }};
        assertEquals(expected1, new Counter<>(arr));


        BigInteger zero = BigInteger.ZERO;
        BigInteger one = BigInteger.ONE;
        BigInteger two = new BigInteger("2");
        BigInteger[] bArr = {zero, zero, zero, one, two, zero, zero, one, two, zero, two};
        Map<BigInteger, Long> expected2 = new HashMap<>() {{
            put(zero, 6L);
            put(one, 2L);
            put(two, 3L);
        }};
        assertEquals(expected2, new Counter<>(bArr));

        // STREAMS:

        assertEquals(expectedLst, new Counter<>(lst.stream()));
        assertEquals(expected2, new Counter<>(Arrays.stream(bArr)));

        // MAP:

        Map<String, Long> map = new HashMap<>() {{
            put("a", 3L);
            put("b", 0L);
            put("c", -1L);
            put("helloww", 2L);
        }};
        assertEquals(map, new Counter<>(new HashMap<>(map)));

    }


    /**
     * Tests that Counter extends HashMap.
     */
    @Test
    public void testExtendsHashMap() {

        // inheritance assert via reflection
        assertThat(HashMap.class.isAssignableFrom(Counter.class)).isTrue();

        // inheritance assert via compilation error
        HashMap<Integer, Long> asHMap = new Counter<>();
        Map<Integer, Long> asMap = new Counter<>();
        asHMap.put(10, 5L);
        asMap.put(5, 1L);
        assertEquals(asHMap.get(10).longValue(), 5L);
        assertEquals(asMap.get(5).longValue(), 1L);

        // Check for "correct" overriding (meaning: do not override what is not asked for)
        var mC = Arrays.stream(Counter.class.getDeclaredMethods()).map(Method::getName).collect(Collectors.toSet());
        var mHM = Arrays.stream(HashMap.class.getMethods()).map(Method::getName).collect(Collectors.toSet());
        mHM.retainAll(mC);
        mHM.removeAll(Arrays.asList("get", "toString"));

        assertEquals(new HashSet<String>(), mHM);

    }


    /**
     * Test class that represents a Person.
     */
    private static class Person {

        private final String name;

        /**
         * Constructs a new Person with the given name.
         *
         * @param name The name of the Person.
         */
        Person(String name) {
            this.name = name;
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof Person person && Objects.equals(name, person.name);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(name);
        }
    }

    /**
     * Test class that represents a President that is a type of Person.
     */
    private static class President extends Person {
        /**
         * Creates a new President with the given name.
         *
         * @param name The name of the President.
         */
        President(String name) {
            super(name);
        }
    }

    /**
     * Test class that represents a Code Warrior that is a type of Person.
     */
    private static class CodeWarrior extends Person {
        /**
         * Creates a new Code Warrior with the given name.
         *
         * @param name The name of the Code Warrior.
         */
        CodeWarrior(String name) {
            super(name);
        }
    }

    /**
     * Tests instances of Counter that contain Person objects and subtypes.
     */
    @Test
    public void testWildcardsCombinePersonPresidentAndPersonCodeWarriorInCounterOfPerson() {

        Person justAPerson = new Person("John Doe");
        President current = new President("Donald Trump");
        President bush = new President("George Bush");
        CodeWarrior b4b = new CodeWarrior("B4B");


        Counter<Person> people = new Counter<>(Collections.singletonList(justAPerson));
        Counter<President> presidents = new Counter<>();
        presidents.push(bush, 2);
        presidents.push(current);

        Counter<CodeWarrior> codeWarriors = new Counter<>(Stream.of(b4b));

        Counter<Person> combined2 = new Counter<>(codeWarriors);
        Counter<Person> combined = people.add(presidents);
        combined.pushAll(codeWarriors);


        assertEquals(new HashMap<Person, Long>() {{
                    put(justAPerson, 1L);
                    put(bush, 2L);
                    put(current, 1L);
                    put(b4b, 1L);
                }},
                combined);

        combined2.mul(2);
        combined2.sub(codeWarriors);

        assertEquals(new HashMap<Person, Long>() {{
                    put(b4b, 1L);
                }}, combined2);

        people = new Counter<>(new HashMap<CodeWarrior, Long>() {{
            put(b4b, 1L);
        }});
        people.pushAll(presidents.elements());
        people.pushAll(new HashMap<CodeWarrior, Long>() {{
            put(b4b, 1L);
        }});
        people = people.subtract(codeWarriors);
        people.push(justAPerson);

        assertEquals(people, combined);

        people.union(presidents);
        people.intersect(presidents);


    }


    /**
     * Tests the constructor with capacity and load factor.
     *
     * @throws Exception If there are any problems.
     */
    @Disabled("due to java.lang.reflect.InaccessibleObjectException")
    public void testCapacityLoadFactor() throws Exception {

        int[] thresholds = {31, 32, 33};
        float[] loadFs = {.9f, .55f, .38f};

        for (int i = 0; i < loadFs.length; i++) {

            int expTh = thresholds[i];
            float expLF = loadFs[i];

            Counter<Integer> m = new Counter<>(expTh, expLF);

            Field f1 = m.getClass().getSuperclass().getDeclaredField("threshold");
            f1.setAccessible(true);
            int th = f1.getInt(m);
            int fTh = th >> 1 << 1;

            Field f2 = m.getClass().getSuperclass().getDeclaredField("loadFactor");
            f2.setAccessible(true);
            float lf = f2.getFloat(m);

            assertThat(expLF == lf).isTrue();
            assertThat(fTh == th).isTrue();

        }
    }


    /**
     * Tests for equality.
     */
    @Test
    public void testWeirdTest() {
        List<Point> lst = Arrays.asList(new Point(0, 0), new P(0, 0), new P(0, 1));
        Counter<Point> c = new Counter<>(lst);
        assertEquals(new HashMap<Point, Long>() {{
            put(new Point(0, 0), 2L);
            put(new Point(0, 1), 1L);
        }}, c);
    }

    /**
     * Test class for checking equality.
     */
    private static class P extends Point {
        public static final long serialVersionUID = 1142021;
        protected P(int x, int y) {
            super(x, y);
        }
    }


    /**
     * Tests the static builder methods that accept an array of primitives.
     */
    @Test
    public void testStaticBuildersForPrimitives() {

        assertEquals(new HashMap<Boolean, Long>() {{
                         put(true, 2L);
                         put(false, 1L);
                     }},
                Counter.of(new boolean[]{true, false, true}));

        assertEquals(new HashMap<Byte, Long>() {{
                         put((byte) 0, 2L);
                     }},
                Counter.of(new byte[]{0, 0}));

        assertEquals(new HashMap<Float, Long>() {{
                         put(0f, 1L);
                         put(2.67f, 3L);
                     }},
                Counter.of(new float[]{2.67f, 0f, 2.67f, 2.67f}));

        assertEquals(new HashMap<Short, Long>() {{
                         put((short) 0, 2L);
                     }},
                Counter.of(new short[]{0, 0}));

        assertEquals(new HashMap<Double, Long>() {{
                         put(0d, 1L);
                         put(2.67, 3L);
                     }},
                Counter.of(new double[]{2.67, 0d, 2.67, 2.67}));

        Map<Integer, Long> expectedLst = new HashMap<>() {{
            put(-8, 3L);
            put(2, 1L);
            put(3, 3L);
            put(5, 2L);
            put(6, 2L);
            put(89, 1L);
            put(53, 1L);
            put(8, 1L);
        }};
        assertEquals(expectedLst,
                Counter.of(new int[]{-8, 2, 3, 5, 6, 89, -8, -8, 3, 53, 6, 3, 5, 8}));

        assertEquals(new HashMap<Long, Long>() {{
                         put(0L, 2L);
                     }},
                Counter.of(new long[]{0L, 0L}));

        char[] arr = {'a', 'a', 'a', 'b', 'b', 'c', 'c', 'c', 'c', 'c', 'c'};
        Map<Character, Long> expected2 = new HashMap<>() {{
            put('a', 3L);
            put('b', 2L);
            put('c', 6L);
        }};
        assertEquals(expected2, Counter.of(arr));


        Map<String, Long> expected1 = new HashMap<>() {{
            put("a", 3L);
            put("b", 2L);
            put("c", 6L);
        }};
        assertEquals(expected1, Counter.of("aacabcbcccc"));

    }


    /**
     * Tests the overridden get and toString methods.
     */
    @Test
    public void testObserversGetAndToString() {
        Counter<BigInteger> cnt = new Counter<>(Arrays.asList(BigInteger.ONE, BigInteger.ONE, BigInteger.ONE));

        String msg = "Default value of 0L should be automatically returned with the 'get' method (Override)";
        BigInteger newKey = new BigInteger("102554");
        try {
            assertEquals(Long.valueOf("0"), cnt.get(newKey));
        }
        catch (Exception e) {
            fail(msg + ": " + e);
        }
        assertFalse(cnt.containsKey(newKey));


        assertEquals("Counter({a=5})", Counter.of("aaaaa").toString());
        assertEquals("Counter({1=3})", cnt.toString());
    }


    /**
     * Tests the push method using strings.
     */
    @Test
    public void testPushCountStrings() {
        Counter<String> c = new Counter<>();
        c.push("a");
        c.push("a");
        c.push("a");
        c.push("b");
        c.push("b");
        c.push("c");
        assertEquals(new HashMap<String, Long>() {{
            put("a", 3L);
            put("b", 2L);
            put("c", 1L);
        }}, c);
        c.push("helloww", -2);
        assertEquals(new HashMap<String, Long>() {{
            put("a", 3L);
            put("b", 2L);
            put("c", 1L);
            put("helloww", -2L);
        }}, c);
    }


    /**
     * Tests the push method using integers.
     */
    @Test
    public void testPushCountInts() {
        Counter<Integer> c = new Counter<>();
        c.push(5);
        c.push(6);
        c.push(7);
        c.push(5);
        assertEquals(new HashMap<Integer, Long>() {{
            put(5, 2L);
            put(6, 1L);
            put(7, 1L);
        }}, c);
        c.push(22, -4);
        assertEquals(new HashMap<Integer, Long>() {{
            put(5, 2L);
            put(6, 1L);
            put(7, 1L);
            put(22, -4L);
        }}, c);
    }


    /**
     * Tests the push method using lists.
     */
    @Test
    public void testPushCountLists() {
        List<Double> a = Arrays.asList(1.2d, 5.3d, 5.3d);
        List<Double> b = Arrays.asList(5.3d, 1.2d, 5.3d);
        List<Double> c = Arrays.asList(5.1d, 4.0d, 6.5d, 6.9d);
        Counter<List<Double>> cnt = new Counter<>();
        cnt.push(a);
        cnt.push(a);
        cnt.push(a);
        cnt.push(b);
        cnt.push(a);
        cnt.push(c);
        cnt.push(c);
        assertEquals(new HashMap<List<Double>, Long>() {{
            put(a, 4L);
            put(b, 1L);
            put(c, 2L);
        }}, cnt);
        cnt.push(c, -2);
        assertEquals(new HashMap<List<Double>, Long>() {{
            put(a, 4L);
            put(b, 1L);
            put(c, 0L);
        }}, cnt);
    }


    /**
     * Tests the equals method.
     */
    @Test
    public void testDontMessWithEquals() {
        assertThat(new Counter<>(Arrays.asList(1, 1, 1, 2)).equals(new Counter<>(Arrays.asList(1, 1, 2)))).isFalse();

        var c1 = Counter.of(IntStream.range(0, 20).map(n -> ThreadLocalRandom.current().nextInt(15)).toArray());
        var c2 = new Counter<>(c1);
        c2.push(46, 2L);
        assertNotEquals(c1, c2);
    }


    /**
     * Tests the static pushAll methods for integers.
     */
    @Test
    public void testPushPushAllStaticIntsRandomTests() {

        for (int nTests = 0; nTests < 10; nTests++) {

            Map<Integer, Long> exp = new HashMap<>();
            Counter<Integer> cnt = new Counter<>();

            int low = ThreadLocalRandom.current().nextInt(100);
            int delta = 10;
            for (int i = 0; i < 20; i++) {
                int k = low + ThreadLocalRandom.current().nextInt(delta);
                exp.put(k, exp.getOrDefault(k, 0L) + 1);
                cnt.push(k);
            }
            assertEquals(exp, cnt);


            int low2 = ThreadLocalRandom.current().nextInt(100);
            int[] arr = IntStream.range(0, 10 + ThreadLocalRandom.current().nextInt(10))
                    .map(n -> low2 + ThreadLocalRandom.current().nextInt(delta))
                    .toArray();

            Arrays.stream(arr).forEach(n -> exp.put(n, exp.getOrDefault(n, 0L) + 1));
            cnt.pushAll(Arrays.stream(arr).boxed().toArray(Integer[]::new));
            assertEquals(exp, cnt);


            Arrays.stream(arr).forEach(n -> exp.put(n, exp.getOrDefault(n, 0L) + 1));
            cnt.pushAll(Arrays.stream(arr).boxed().toArray(Integer[]::new));
            assertEquals(exp, cnt);


            Arrays.stream(arr).forEach(n -> exp.put(n, exp.getOrDefault(n, 0L) + 1));
            Counter.pushAll(cnt, arr);
            assertEquals(exp, cnt);


            Arrays.stream(arr).forEach(n -> exp.put(n, exp.getOrDefault(n, 0L) + 1));
            cnt.pushAll(Arrays.stream(arr).boxed().toList());
            assertEquals(exp, cnt);


            Arrays.stream(arr).forEach(n -> exp.put(n, exp.getOrDefault(n, 0L) + 1));
            cnt.pushAll(Arrays.stream(arr).boxed());
            assertEquals(exp, cnt);


            cnt.clear();
            exp.clear();
            for (int i = 0; i < 20; i++) {
                int n = -20 + ThreadLocalRandom.current().nextInt(40);
                int k = -5 + ThreadLocalRandom.current().nextInt(10);
                exp.put(k, exp.getOrDefault(k, 0L) + n);
                cnt.push(k, n);
                assertEquals(exp, cnt);
            }


            Map<Integer, Long> map = IntStream.range(0, 50)
                    .map(n -> low2 + ThreadLocalRandom.current().nextInt(delta))
                    .boxed()
                    .collect(Collectors.groupingBy(s -> s, HashMap<Integer, Long>::new, Collectors.counting()));
            map.forEach((k, v) -> exp.put(k, exp.getOrDefault(k, 0L) + v));
            cnt.pushAll(map);
            assertEquals(exp, cnt);
        }
    }


    /**
     * Tests the static pushAll methods for all the types except integers.
     */
    @Test
    public void testPushPushAllStaticOthers() {

        assertEquals(new HashMap<Boolean, Long>() {{
                         put(true, 2L);
                         put(false, 1L);
                     }},
                new Counter<Boolean>() {{
                    Counter.pushAll(this, new boolean[]{true, false, true});
                }});

        assertEquals(new HashMap<Byte, Long>() {{
                         put((byte) 0, 2L);
                     }},
                new Counter<Byte>() {{
                    Counter.pushAll(this, new byte[]{0, 0});
                }});

        assertEquals(new HashMap<Float, Long>() {{
                         put(0f, 1L);
                         put(2.67f, 3L);
                     }},
                new Counter<Float>() {{
                    Counter.pushAll(this, new float[]{2.67f, 0f, 2.67f, 2.67f});
                }});

        assertEquals(new HashMap<Short, Long>() {{
                         put((short) 0, 2L);
                     }},
                new Counter<Short>() {{
                    Counter.pushAll(this, new short[]{0, 0});
                }});

        assertEquals(new HashMap<Double, Long>() {{
                         put(0d, 1L);
                         put(2.67, 3L);
                     }},
                new Counter<Double>() {{
                    Counter.pushAll(this, new double[]{2.67, 0d, 2.67, 2.67});
                }});


        Map<Integer, Long> expectedLst = new HashMap<>() {{
            put(-8, 3L);
            put(2, 1L);
            put(3, 3L);
            put(5, 2L);
            put(6, 2L);
            put(89, 1L);
            put(53, 1L);
            put(8, 1L);
        }};
        assertEquals(expectedLst, new Counter<Integer>() {{
            Counter.pushAll(this, new int[]{-8, 2, 3, 5, 6, 89, -8, -8, 3, 53, 6, 3, 5, 8});
        }});


        assertEquals(new HashMap<Long, Long>() {{
                         put(0L, 2L);
                     }},
                new Counter<Long>() {{
                    Counter.pushAll(this, new long[]{0L, 0L});
                }});


        char[] arr = {'a', 'a', 'a', 'b', 'b', 'c', 'c', 'c', 'c', 'c', 'c'};
        Map<Character, Long> expected2 = new HashMap<>() {{
            put('a', 3L);
            put('b', 2L);
            put('c', 6L);
        }};
        assertEquals(expected2, new Counter<Character>() {{
            Counter.pushAll(this, arr);
        }});

    }


    /**
     * Tests the static pushAll method for strings.
     */
    @Test
    public void testPushAllStaticStringsRandomTests() {

        Map<String, Long> expected1 = new HashMap<>() {{
            put("a", 3L);
            put("b", 2L);
            put("c", 6L);
        }};
        Counter<String> c = new Counter<>() {{
            Counter.pushAll(this, "aacabcbcccc");
        }};
        assertEquals(expected1, c);

        for (int x = 0; x < 10; x++) {
            String toAdd = IntStream.range(0, 50)
                    .map(i -> 65 + ThreadLocalRandom.current().nextInt(35))
                    .mapToObj(i -> String.valueOf((char) i))
                    .peek(l -> expected1.put(l, expected1.getOrDefault(l, 0L) + 1))
                    .collect(Collectors.joining());
            Counter.pushAll(c, toAdd);
            assertEquals(expected1, c);
        }
    }


    /**
     * Tests the elements method.
     */
    @Test
    public void testObserversElements() {

        Map<String, Long> map = new HashMap<>() {{
            put("a", 3L);
            put("helloww", 2L);
            put("d", 2L);
            put("b", 0L);
            put("c", -1L);
        }};
        List<String> lst = Arrays.asList("a", "a", "a", "d", "d", "helloww", "helloww");
        Counter<String> c = new Counter<>(new ArrayList<>(lst));

        assertEquals(lst, c.elements().sorted().toList());
        assertEquals(lst, c.elementsAsList().stream().sorted().toList());

        c = new Counter<>(new HashMap<>(map));
        assertEquals(lst, c.elements().sorted().toList());
        assertEquals(lst, c.elementsAsList().stream().sorted().toList());
    }


    /**
     * Tests the mostCommon method.
     */
    @Test
    public void testObserversMostCommon() {

        Map<String, Long> map = new HashMap<>() {{
            put("a", 3L);
            put("b", 0L);
            put("c", -1L);
            put("helloww", 2L);
            put("d", 1L);
        }};
        Counter<String> c = new Counter<>(new HashMap<>(map));
        List<Map.Entry<String, Long>> mc = map.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue()).collect(Collectors.toList());
        Collections.reverse(mc);

        assertEquals(mc, c.mostCommon().toList());
        assertEquals(mc, c.mostCommonAsList());

        assertEquals(mc.subList(0, 3), c.mostCommon(3).toList());
        assertEquals(mc.subList(0, 3), c.mostCommonAsList(3));
        assertEquals(mc.subList(0, 1), c.mostCommon(1).toList());
        assertEquals(mc.subList(0, 1), c.mostCommonAsList(1));

        assertEquals(mc, c.mostCommon(31).toList());
        assertEquals(mc, c.mostCommonAsList(31));

        assertEquals(new ArrayList<Map.Entry<String, Long>>(), c.mostCommon(0).toList());
        assertEquals(new ArrayList<Map.Entry<String, Long>>(), c.mostCommonAsList(0));

        assertThatThrownBy(() -> c.mostCommon(-5)).isInstanceOf(IllegalArgumentException.class);

        assertThatThrownBy(() -> c.mostCommonAsList(-1)).isInstanceOf(IllegalArgumentException.class);
    }


    /**
     * Tests that the math methods produce a new instance.
     */
    @Test
    public void testMathNoMutations() {
        var cnt = new Counter<>(List.of(1, 1, 2, 3, -4, 3, 58, 2, 1, 58, 36, 1, -5, 1, -4, 653, 6, 11, 1, 1, 34, 2));
        Counter<Integer> cnt2 = cnt.mul(3);
        Counter<Integer> cnt3 = cnt.add(cnt2);
        Counter<Integer> cnt4 = cnt2.sub(cnt);
        Counter<Integer> cnt5 = cnt.intersect(cnt2);
        Counter<Integer> cnt6 = cnt.union(cnt2);
        Counter<Integer> cnt7 = cnt.subtract(cnt2);
        assertThat(cnt).isNotSameAs(cnt2);
        assertThat(cnt).isNotSameAs(cnt3);
        assertThat(cnt).isNotSameAs(cnt4);
        assertThat(cnt).isNotSameAs(cnt5);
        assertThat(cnt).isNotSameAs(cnt6);
        assertThat(cnt).isNotSameAs(cnt7);
    }


    /**
     * Tests the mul method.
     */
    @Test
    public void testMathMul() {
        var lst = List.of(1, 1, 2, 3, -4, 3, 58, 2, 1, 58, 36, 1, -5, 1, -4, 653, 6, 11, 1, 1, 34, 2);
        Counter<Integer> cnt = new Counter<>(new ArrayList<>(lst));

        for (int n : Arrays.asList(3, -2, 0)) {
            Counter<Integer> res = cnt.mul(n);
            cnt.keySet()
                    .forEach(k -> {
                        long exp = cnt.get(k) * n;
                        long act = res.get(k);
                        assertEquals(exp, act);
                    });
        }
    }


    private Map<Integer, Long> multiSets4Map(Map<Integer, Long> expA, Map<Integer, Long> expB, String func) {
        Map<Integer, Long> res = new HashMap<>();
        for (Integer k : Stream.concat(expA.keySet().stream(), expB.keySet().stream()).collect(Collectors.toSet())) {
            long vA = expA.getOrDefault(k, 0L);
            long vB = expB.getOrDefault(k, 0L);
            long v = switch (func) {
                case "add" -> vA + vB;
                case "intersect" -> Math.min(vA, vB);
                case "union" -> Math.max(vA, vB);
                // fall-through
                case "sub", "subtract" -> vA - vB;
                default -> throw new UnsupportedOperationException("Unsupported operation");
            };
            if (!func.equals("subtract") && v < 1) {
                continue;
            }
            res.put(k, v);
        }

        return res;
    }


    /**
     * Tests the add, sub, intersect, union, and substract methods.
     */
    @Test
    public void testMathMultisetsAndSubtract() {

        Counter<String> a = Counter.of("aaab");
        Counter<String> b = Counter.of("abb");
        Counter<String> aAddB = Counter.of("aaaabbb");
        Counter<String> aSubB = Counter.of("aa");
        Counter<String> bSubA = Counter.of("b");
        Counter<String> aInterB = Counter.of("ab");
        Counter<String> aUnionB = Counter.of("aaabb");
        Counter<String> aSubtractB = new Counter<>(new HashMap<>() {{
                    put("a", 2L);
                    put("b", -1L);
                }});
        Counter<String> bSubtractA = new Counter<>(new HashMap<>() {{
                    put("a", -2L);
                    put("b", 1L);
                }});

        assertEquals(aAddB, a.add(b));

        assertEquals(aAddB, b.add(a));

        assertEquals(aInterB, a.intersect(b));

        assertEquals(aInterB, b.intersect(a));

        assertEquals(aUnionB, a.union(b));

        assertEquals(aUnionB, b.union(a));

        assertEquals(aSubB, a.sub(b));

        assertEquals(bSubA, b.sub(a));

        assertEquals(aSubtractB, a.subtract(b));

        assertEquals(bSubtractA, b.subtract(a));
    }


    /**
     * Tests the math methods (add, sub, intersect, union, and subtract).
     */
    @Test
    public void testMathRandom() {

        Map<Integer, Long> expA;
        Map<Integer, Long> expB;
        Counter<Integer> cntA;
        Counter<Integer> cntB;

        for (int x = 0; x < 1; x++) {

            expA = new HashMap<>();
            expB = new HashMap<>();
            cntA = new Counter<>();
            cntB = new Counter<>();

            for (int i = 0; i < 10; i++) {

                int n = -10 + ThreadLocalRandom.current().nextInt(20);
                int k = -20 + ThreadLocalRandom.current().nextInt(40);
                expA.put(k, expA.getOrDefault(k, 0L) + n);
                cntA.push(k, n);

                n = -10 + ThreadLocalRandom.current().nextInt(20);
                k = -20 + ThreadLocalRandom.current().nextInt(40);
                expB.put(k, expB.getOrDefault(k, 0L) + n);
                cntB.push(k, n);
            }

            String func = "add";
            assertEquals(multiSets4Map(expA, expB, func), cntA.add(cntB));
            assertEquals(multiSets4Map(expA, expB, func), cntB.add(cntA));

            func = "intersect";
            assertEquals(multiSets4Map(expA, expB, func), cntA.intersect(cntB));
            assertEquals(multiSets4Map(expA, expB, func), cntB.intersect(cntA));

            func = "union";
            assertEquals(multiSets4Map(expA, expB, func), cntA.union(cntB));
            assertEquals(multiSets4Map(expA, expB, func), cntB.union(cntA));

            func = "sub";
            assertEquals(multiSets4Map(expA, expB, func), cntA.sub(cntB));
            assertEquals(multiSets4Map(expB, expA, func), cntB.sub(cntA));

            func = "subtract";
            assertEquals(multiSets4Map(expA, expB, func), cntA.subtract(cntB));
            assertEquals(multiSets4Map(expB, expA, func), cntB.subtract(cntA));
        }
    }
}