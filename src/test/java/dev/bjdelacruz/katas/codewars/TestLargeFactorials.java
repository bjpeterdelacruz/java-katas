package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link LargeFactorials}.
 */
public class TestLargeFactorials {

    /**
     * Tests the factorial method.
     */
    @Test
    public void testFactorial() {
        assertThat(LargeFactorials.factorial(0), is(equalTo("1")));
        assertThat(LargeFactorials.factorial(1), is(equalTo("1")));
        assertThat(LargeFactorials.factorial(5), is(equalTo("120")));
        assertThat(LargeFactorials.factorial(9), is(equalTo("362880")));
        assertThat(LargeFactorials.factorial(15), is(equalTo("1307674368000")));

        var expected = """
        3232856260909107732320814552024368470994843717673780666747942427112823747555111209488817915371028\
        1994509285073531894329267309317128089908227910302790712819216765272401892647332180411862610068329\
        2536513367893908956993571353017504051317876007724793306540233900616482555224881943657258605739922\
        2641254832982204849137721776650641276858807153128978777672951913990844377478702589172973255150283\
        241787320658188482062478582659808848825548800000000000000000000000000000000000000000000000000000000000000""";
        assertThat(LargeFactorials.factorial(250), is(equalTo(expected)));
    }

    /**
     * Tests the multiply method.
     */
    @Test
    public void testMultiply() {
        assertThat(LargeFactorials.multiply("98765", "9999"), is(equalTo("987551235")));
    }

}
