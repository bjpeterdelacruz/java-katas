package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link StringRepeat}.
 */
public class TestStringRepeat {
    /**
     * Tests the repeatStr method.
     */
    @Test
    public void testRepeatStr() {
        assertThat(StringRepeat.repeatStr(5, "abc"), is(equalTo("abcabcabcabcabc")));
    }
}
