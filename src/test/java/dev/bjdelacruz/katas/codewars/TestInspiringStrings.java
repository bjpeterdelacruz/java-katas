package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link StringyStrings}.
 */
public class TestInspiringStrings {
    /**
     * Tests the longestWord method.
     */
    @Test
    public void testLongestWord() {
        assertThat(InspiringStrings.longestWord("test"), is(equalTo("test")));
        assertThat(InspiringStrings.longestWord("This is a string that is very long"),
                is(equalTo("string")));
        assertThat(InspiringStrings.longestWord("A quick brown fox"), is(equalTo("brown")));
    }
}
