package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link CountTheMonkeys}.
 */
public class TestCountTheMonkeys {
    /**
     * Tests the monkeyCount method.
     */
    @Test
    public void testMonkeyCount() {
        var expected = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        assertThat(CountTheMonkeys.monkeyCount(10), is(equalTo(expected)));
    }
}
