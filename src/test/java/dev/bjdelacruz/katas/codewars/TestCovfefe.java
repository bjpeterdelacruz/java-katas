package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link Covfefe}.
 */
public class TestCovfefe {
    /**
     * Tests the covfefe method.
     */
    @Test
    public void testCovfefe() {
        assertThat(Covfefe.covfefe("This is good"), is(equalTo("This is good covfefe")));
        assertThat(Covfefe.covfefe("This coverage is good coverage!"),
                is(equalTo("This covfefe is good covfefe!")));
    }
}
