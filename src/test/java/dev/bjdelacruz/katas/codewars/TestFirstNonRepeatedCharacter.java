package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static dev.bjdelacruz.katas.codewars.FirstNonRepeatedCharacter.firstNonRepeated;

/**
 * Unit tests for {@link FirstNonRepeatedCharacter}.
 */
public class TestFirstNonRepeatedCharacter {
    /**
     * Tests the firstNonRepeated method.
     */
    @Test
    public void testFirstNonRepeated1() {
        assertThat(firstNonRepeated("test"), is(equalTo('e')));
    }

    /**
     * Tests the firstNonRepeated method.
     */
    @Test
    public void testFirstNonRepeated2() {
        assertThat(firstNonRepeated("teeter"), is(equalTo('r')));
    }

    /**
     * Tests the firstNonRepeated method.
     */
    @Test
    public void testFirstNonRepeated3() {
        assertThat(firstNonRepeated("1122321235121222"), is(equalTo('5')));
    }

    /**
     * Tests the firstNonRepeated method.
     */
    @Test
    public void testFirstNonRepeated4() {
        final var input = """
                1122321235121222dsfasddssdfa112232123sdfasdfasdf112232123\
                5121222dsfasddssdfa112232123sdfasdfasdf112232123112232123\
                5121222dsfasddssdfa112232123sdfasdfasdf112232123112232123\
                5121222dsfasddssdfa112232123sdfasdfasdf112232123112232123\
                5121222dsfasddssdfa112232123sdfasdfasdf112232123112232123\
                5121222dsfasddssdfa112232123sdfasdfasdf112232123asddssdfa\
                112232123sdfasdfasdf1122z321231122321235121222dsfasddssdf\
                1122321235121222dsfasddssdf1122321235121222dsfasddssdf112\
                2321235121222dsfasddssdf1122321235121222dsfasddssdf112p23\
                21235121222dsfasddssdf1122321235121222dsfasddssdf""";
        assertThat(firstNonRepeated(input), is(equalTo('z')));
    }

    /**
     * Tests the firstNonRepeated method.
     */
    @Test
    public void testFirstNonRepeated5() {
        assertThat(firstNonRepeated("11223344556677889900"), is(equalTo(null)));
    }
}
