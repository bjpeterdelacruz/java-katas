package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link APlusB}.
 */
public class TestAPlusB {
    /**
     * Tests the sum method.
     */
    @Test
    public void test() {
        assertThat(APlusB.sum((byte) 1, (byte) 3), is(equalTo((byte) 4)));
        assertThat(APlusB.sum((byte) 0, (byte) 6), is(equalTo((byte) 6)));
    }
}
