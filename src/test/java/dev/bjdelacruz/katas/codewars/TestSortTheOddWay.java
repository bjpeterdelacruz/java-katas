package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link SortTheOddWay}.
 */
public class TestSortTheOddWay {
    /**
     * Tests the sortItOut method.
     */
    @Test
    public void testSortItOut() {
        assertThat(SortTheOddWay.sortItOut(new Double[] {11d, 22d, 33d, 44d, 55d, 55d, 90.4, 4d, 78d}),
                is(equalTo(new Double[] {11d, 33d, 55d, 55d, 90.4, 78d, 44d, 22d, 4d})));
        assertThat(SortTheOddWay.sortItOut(new Double[] {26d, 243d, 52d, 2d, 432414d, 1d, 11d, 46d, 32d}),
                is(equalTo(new Double[] {1d, 11d, 243d, 432414d, 52d, 46d, 32d, 26d, 2d})));
        assertThat(SortTheOddWay.sortItOut(new Double[] {19d, 65d, 88d, 112d, 60d, 14d, 33d, 49d, 88d}),
                is(equalTo(new Double[] {19d, 33d, 49d, 65d, 112d, 88d, 88d, 60d, 14d})));
        assertThat(SortTheOddWay.sortItOut(new Double[] {68d, 25d, 99d, 50d, 10d, 67d, 2d, 5d, 8d, 34d, 67d}),
                is(equalTo(new Double[] {5d, 25d, 67d, 67d, 99d, 68d, 50d, 34d, 10d, 8d, 2d})));
        assertThat(SortTheOddWay.sortItOut(new Double[0]), is(equalTo(new Double[0])));

        assertThat(SortTheOddWay.sortItOut(new Double[] {11d, 33d, 77d, 55d}),
                is(equalTo(new Double[] {11d, 33d, 55d, 77d})));
        assertThat(SortTheOddWay.sortItOut(new Double[] {22d, 44d, 66d, 88d}),
                is(equalTo(new Double[] {88d, 66d, 44d, 22d})));
    }
}
