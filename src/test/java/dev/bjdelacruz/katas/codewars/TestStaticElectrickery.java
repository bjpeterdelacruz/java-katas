package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link StaticElectrickery}.
 */
public class TestStaticElectrickery {

    /**
     * Tests the working plus100 method.
     */
    @Test
    public void basicTests() {
        assertThat(StaticElectrickery.INST.plus100(5), is(equalTo(105)));
        assertThat(StaticElectrickery.INST.plus100(0), is(equalTo(100)));
        assertThat(StaticElectrickery.INST.plus100(100), is(equalTo(200)));
    }
}
