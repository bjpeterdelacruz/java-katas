package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link DecimalDecomposition}.
 */
public class TestDecimalDecomposition {

    /**
     * Tests the decimalDecomposition method.
     */
    @Test
    public void testDecimalDecomposition() {
        assertThat(DecimalDecomposition.decimalDecomposition(217035),
                is(equalTo("200000+10000+7000+30+5")));
        assertThat(DecimalDecomposition.decimalDecomposition(9999), is(equalTo("9000+900+90+9")));
        assertThat(DecimalDecomposition.decimalDecomposition(100), is(equalTo("100")));
    }
}
