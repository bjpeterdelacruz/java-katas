package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link RemoveDuplicates}.
 */
public class TestRemoveDuplicates {
    /**
     * Tests the {@link RemoveDuplicates#unique(int[])} method.
     */
    @Test
    public void testUniqueArray() {
        // "Empty array should return an empty array."
        var emptyArray = new int[0];
        assertThat(RemoveDuplicates.unique(emptyArray), is(equalTo(emptyArray)));

        // "One value should return that value in array."
        var singleValArr = new int[]{-1};
        assertThat(RemoveDuplicates.unique(singleValArr), is(equalTo(singleValArr)));

        // "Multiple values should return in same order as given."
        var multipleValsInOrder = new int[]{-1, 5, 10, -100, 3, 2};
        assertThat(RemoveDuplicates.unique(multipleValsInOrder), is(equalTo(multipleValsInOrder)));

        // "Duplicates should be removed and in the order first seen."
        var duplicatesInOrder1 = new int[]{1, 2, 3, 3, 2, 1, 2, 3, 1, 1, 3, 2};
        assertThat(RemoveDuplicates.unique(duplicatesInOrder1), is(equalTo(new int[]{1, 2, 3})));

        var duplicatesInOrder2 = new int[]{1, 3, 2, 3, 2, 1, 2, 3, 1, 1, 3, 2};
        assertThat(RemoveDuplicates.unique(duplicatesInOrder2), is(equalTo(new int[]{1, 3, 2})));

        var duplicatesInOrder3 = new int[]{3, 2, 3, 3, 2, 1, 2, 3, 1, 1, 3, 2};
        assertThat(RemoveDuplicates.unique(duplicatesInOrder3), is(equalTo(new int[]{3, 2, 1})));
    }
}
