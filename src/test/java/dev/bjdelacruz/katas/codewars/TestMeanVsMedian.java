package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link MeanVsMedian}.
 */
public class TestMeanVsMedian {
    /**
     * Tests the meanVsMedian method.
     */
    @Test
    public void testMeanVsMedian() {
        assertThat(MeanVsMedian.meanVsMedian(new int[] {1, 1, 1}), is(equalTo("same")));
        assertThat(MeanVsMedian.meanVsMedian(new int[] {1, 2, 37}), is(equalTo("mean")));
        assertThat(MeanVsMedian.meanVsMedian(new int[] {7, 14, -70}), is(equalTo("median")));
        assertThat(MeanVsMedian.meanVsMedian(new int[] {-10, 20, 5}), is(equalTo("same")));
    }
}
