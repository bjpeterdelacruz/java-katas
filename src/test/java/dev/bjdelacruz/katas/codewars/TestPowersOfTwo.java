package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link PowersOfTwo}.
 */
public class TestPowersOfTwo {
    /**
     * Tests the powersOfTwo method.
     */
    @Test
    public void testPowersOfTwo() {
        assertThat(PowersOfTwo.powersOfTwo(0), is(equalTo(new long[] {1})));
        assertThat(PowersOfTwo.powersOfTwo(1), is(equalTo(new long[] {1, 2})));
        assertThat(PowersOfTwo.powersOfTwo(3), is(equalTo(new long[] {1, 2, 4, 8})));
        assertThat(PowersOfTwo.powersOfTwo(5), is(equalTo(new long[] {1, 2, 4, 8, 16, 32})));
        assertThat(PowersOfTwo.powersOfTwo(8), is(equalTo(new long[] {1, 2, 4, 8, 16, 32, 64, 128, 256})));
    }
}
