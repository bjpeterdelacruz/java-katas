package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link PersistentBugger}.
 */
public class TestPersistentBugger {

    /**
     * Tests the {@link PersistentBugger#persistence(long)} method.
     */
    @Test
    public void basicTests() {
        assertThat(PersistentBugger.persistence(4), is(equalTo(0)));
        assertThat(PersistentBugger.persistence(25), is(equalTo(2)));
        assertThat(PersistentBugger.persistence(39), is(equalTo(3)));
        assertThat(PersistentBugger.persistence(444), is(equalTo(3)));
        assertThat(PersistentBugger.persistence(999), is(equalTo(4)));
    }
}
