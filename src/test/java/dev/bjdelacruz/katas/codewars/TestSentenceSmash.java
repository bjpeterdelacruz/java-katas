package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link SentenceSmash}.
 */
public class TestSentenceSmash {
    /**
     * Tests the sort method.
     */
    @Test
    public void testSmash() {
        assertThat(SentenceSmash.smash(" The ", " quick ", " brown ", " fox "), is(equalTo("The quick brown fox")));
    }
}
