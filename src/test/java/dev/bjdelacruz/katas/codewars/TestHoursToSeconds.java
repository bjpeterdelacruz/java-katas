package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link HoursToSeconds}.
 */
public class TestHoursToSeconds {

    /**
     * Tests the hoursToSeconds method.
     */
    @Test
    public void testHoursToSeconds() {
        assertThat(HoursToSeconds.hoursToSeconds(1), is(equalTo(60 * 60L)));
        assertThat(HoursToSeconds.hoursToSeconds(2), is(equalTo(2 * 60 * 60L)));
        assertThat(HoursToSeconds.hoursToSeconds(3), is(equalTo(3 * 60 * 60L)));
        assertThat(HoursToSeconds.hoursToSeconds(48), is(equalTo(48 * 60 * 60L)));
        assertThat(HoursToSeconds.hoursToSeconds(100), is(equalTo(100 * 60 * 60L)));
    }
}
