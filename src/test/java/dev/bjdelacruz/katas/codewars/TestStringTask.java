package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link StringTask}.
 */
public class TestStringTask {
    /**
     * Tests the perform method.
     */
    @Test
    public void testPerform() {
        assertThat(StringTask.perform("tour"), is(equalTo(".t.r")));
        assertThat(StringTask.perform("Codewars"), is(equalTo(".c.d.w.r.s")));
        assertThat(StringTask.perform("aBAcAba"), is(equalTo(".b.c.b")));
        assertThat(StringTask.perform("G"), is(equalTo(".g")));
        assertThat(StringTask.perform("a"), is(equalTo("")));
        assertThat(StringTask.perform("""
                femOZeCArKCpUiHYnbBPTIOFmsHmcpObtPYcLCdjFrUMIyqYzAo\
                kKUiiKZRouZiNMoiOuGVoQzaaCAOkquRjmmKKElLNqCnhGdQM"""), is(equalTo("""
                .f.m.z.c.r.k.c.p.h.n.b.b.p.t.f.m.s.h.m.c.p.b.t.p.c.l\
                .c.d.j.f.r.m.q.z.k.k.k.z.r.z.n.m.g.v.q.z.c.k.q.r.j.m\
                .m.k.k.l.l.n.q.c.n.h.g.d.q.m""")));
    }
}
