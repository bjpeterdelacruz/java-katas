package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link AreYouPlayingBanjo}.
 */
public class TestAreYouPlayingBanjo {
    /**
     * Tests the areYouPlayingBanjo method.
     */
    @Test
    public void testAreYouPlayingBanjo() {
        assertThat(AreYouPlayingBanjo.areYouPlayingBanjo("Alex"), is(equalTo("Alex does not play banjo")));
        assertThat(AreYouPlayingBanjo.areYouPlayingBanjo("romeo"), is(equalTo("romeo plays banjo")));
        assertThat(AreYouPlayingBanjo.areYouPlayingBanjo("Ron"), is(equalTo("Ron plays banjo")));
    }
}
