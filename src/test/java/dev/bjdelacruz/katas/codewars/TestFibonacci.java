package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link Fibonacci}.
 */
public class TestFibonacci {

    /**
     * Tests the {@link Fibonacci#fib(int)} method.
     */
    @Test
    public void testFib() {
        assertThat(Fibonacci.fib(1), is(equalTo(1L)));
        assertThat(Fibonacci.fib(2), is(equalTo(1L)));
        assertThat(Fibonacci.fib(3), is(equalTo(2L)));
        assertThat(Fibonacci.fib(4), is(equalTo(3L)));
        assertThat(Fibonacci.fib(5), is(equalTo(5L)));
        assertThat(Fibonacci.fib(6), is(equalTo(8L)));
        assertThat(Fibonacci.fib(7), is(equalTo(13L)));
        assertThat(Fibonacci.fib(8), is(equalTo(21L)));
    }
}
