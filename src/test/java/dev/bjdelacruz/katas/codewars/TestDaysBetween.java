package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link DaysBetween}.
 */
public class TestDaysBetween {
    /**
     * Tests the getDaysAlice method using fixed tests and random tests.
     */
    @Test
    public void getDaysAlive() {

        assertThat(DaysBetween.getDaysAlive(1987, 1, 16, 2000, 10, 15), is(equalTo(5021L)));
        assertThat(DaysBetween.getDaysAlive(2005, 10, 27, 2017, 4, 6), is(equalTo(4179L)));
        assertThat(DaysBetween.getDaysAlive(1998, 1, 10, 2008, 7, 8), is(equalTo(3832L)));

        // Random test:
        for (var count = 0; count < 500; count++) {
            var year1 = (int) (Math.random() * (2017 - 1980) + 1980);
            var year2 = (int) (Math.random() * (2017 - 1980) + 1980);

            var month1 = (int) (Math.random() * 11 + 1);
            var month2 = (int) (Math.random() * 11 + 1);

            var day1 = (int) (Math.random() * 27 + 1);
            var day2 = (int) (Math.random() * 27 + 1);

            var startDate = LocalDate.of(year1, month1, day1);
            var endDate = LocalDate.of(year2, month2, day2);

            assertThat(DaysBetween.getDaysAlive(year1, month1, day1, year2, month2, day2),
                    is(equalTo(ChronoUnit.DAYS.between(startDate, endDate))));
        }
    }
}
