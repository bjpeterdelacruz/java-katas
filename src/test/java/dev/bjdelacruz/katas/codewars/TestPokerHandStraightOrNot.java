package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link PokerHandStraightOrNot}.
 */
public class TestPokerHandStraightOrNot {
    /**
     * Tests the isStraight method.
     */
    @Test
    public void testIsStraight() {
        assertThat(PokerHandStraightOrNot.isStraight(List.of(2, 3, 4, 5, 6)), is(equalTo(true)));
        assertThat(PokerHandStraightOrNot.isStraight(List.of(14, 2, 3, 4, 5)), is(equalTo(true)));
        assertThat(PokerHandStraightOrNot.isStraight(List.of(2, 3)), is(equalTo(false)));
        assertThat(PokerHandStraightOrNot.isStraight(List.of(7, 7, 12, 11, 3, 4, 14)), is(equalTo(false)));
        assertThat(PokerHandStraightOrNot.isStraight(List.of(2, 7, 12, 11, 3, 4, 14)), is(equalTo(false)));
        assertThat(PokerHandStraightOrNot.isStraight(List.of(10, 7, 12, 11, 13, 4, 14)), is(equalTo(true)));
        assertThat(PokerHandStraightOrNot.isStraight(List.of(10, 14, 14, 13, 12, 10, 11)), is(equalTo(true)));
        assertThat(PokerHandStraightOrNot.isStraight(List.of(2, 7, 12, 11, 10, 13, 14)), is(equalTo(true)));
        assertThat(PokerHandStraightOrNot.isStraight(List.of(3, 4, 5, 7, 8, 9, 10)), is(equalTo(false)));
    }
}
