package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link ResponsibleDrinking}.
 */
public class TestResponsibleDrinking {
    /**
     * Tests the hydrate method.
     */
    @Test
    public void testHydrate() {
        assertThat(ResponsibleDrinking.hydrate("1 bottle of wine"), is(equalTo("1 glass of water")));
        assertThat(ResponsibleDrinking.hydrate("3 bottles of wine"),
                is(equalTo("3 glasses of water")));
        assertThat(ResponsibleDrinking.hydrate("3 bottles of wine, 4 glasses of whiskey"),
                is(equalTo("7 glasses of water")));
    }
}
