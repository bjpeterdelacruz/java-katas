package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link Collatz}.
 */
public class TestCollatz {

    /**
     * Tests the collatz method.
     */
    @Test
    public void testCollatz() {
        assertThat(Collatz.collatz(2), is(equalTo("2->1")));
        assertThat(Collatz.collatz(3), is(equalTo("3->10->5->16->8->4->2->1")));
        assertThat(Collatz.collatz(4), is(equalTo("4->2->1")));
    }
}
