package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link PositivesCountNegativesSum}.
 */
public class TestPositivesCountNegativesSum {
    /**
     * Tests the countPositivesSumNegatives method.
     */
    @Test
    public void testCountPositivesSumNegatives() {
        var expected = new int[] {10, -65};
        assertThat(PositivesCountNegativesSum.countPositivesSumNegatives(
                new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -11, -12, -13, -14, -15}), is(equalTo(expected)));
        expected = new int[] {8, -50};
        assertThat(PositivesCountNegativesSum.countPositivesSumNegatives(
                new int[] {0, 2, 3, 0, 5, 6, 7, 8, 9, 10, -11, -12, -13, -14}), is(equalTo(expected)));
        expected = new int[] {};
        assertThat(PositivesCountNegativesSum.countPositivesSumNegatives(null), is(equalTo(expected)));
        assertThat(PositivesCountNegativesSum.countPositivesSumNegatives(new int[] {}), is(equalTo(expected)));
    }
}
