package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static dev.bjdelacruz.katas.codewars.CompareWithinMargin.closeCompare;

/**
 * Unit tests for {@link CompareWithinMargin}.
 */
public class TestCompareWithinMargin {
    /**
     * Tests the closeCompare method with margins and no margins.
     */
    @Test
    public void testCloseCompare() {
        assertThat(closeCompare(4, 5), is(equalTo(-1)));
        assertThat(closeCompare(5, 5), is(equalTo(0)));
        assertThat(closeCompare(6, 5), is(equalTo(1)));
        assertThat(closeCompare(-6, -5), is(equalTo(-1)));

        assertThat(closeCompare(2, 5, 3), is(equalTo(0)));
        assertThat(closeCompare(8.1, 5, 3), is(equalTo(1)));
        assertThat(closeCompare(1.99, 5, 3), is(equalTo(-1)));
    }

    /**
     * Tests the closeCompare method with no margins.
     */
    @Test
    public void testCloseCompareNoMargin() {
        assertThat(closeCompare(4, 5), is(equalTo(-1)));
        assertThat(closeCompare(5, 5), is(equalTo(0)));
        assertThat(closeCompare(6, 5), is(equalTo(1)));

        assertThat(closeCompare(-4, -3), is(equalTo(-1)));
        assertThat(closeCompare(-5, -5), is(equalTo(0)));
        assertThat(closeCompare(-5, -6), is(equalTo(1)));
    }

    /**
     * Tests the closeCompare method with margin of three.
     */
    @Test
    public void testCloseCompareMarginOf3() {
        assertThat(closeCompare(2, 5, 3), is(equalTo(0)));
        assertThat(closeCompare(5, 5, 3), is(equalTo(0)));
        assertThat(closeCompare(8, 5, 3), is(equalTo(0)));
        assertThat(closeCompare(8.1, 5, 3), is(equalTo(1)));
        assertThat(closeCompare(1.99, 5, 3), is(equalTo(-1)));
    }
}
