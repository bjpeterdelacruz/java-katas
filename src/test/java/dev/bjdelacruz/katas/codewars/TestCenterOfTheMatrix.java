package dev.bjdelacruz.katas.codewars;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for {@link CenterOfTheMatrix}.
 */
public class TestCenterOfTheMatrix {

    /**
     * Tests the center method using matrices that have a center (odd dimensions).
     */
    @Test
    public void basicTestsHasCenter() {
        assertThat(CenterOfTheMatrix.center(new int[][]{
                new int[]{1, 2, 3},
                new int[]{4, 5, 6},
                new int[]{7, 8, 9}
        }), is(equalTo(5)));
        assertThat(CenterOfTheMatrix.center(new int[][]{
                new int[]{1, 2, 3, 4, 5},
                new int[]{6, 7, 8, 9, 10},
                new int[]{10, 11, 12, 13, 14}
        }), is(equalTo(8)));
    }

    /**
     * Tests the center method using matrices that have no center (empty arrays, even dimensions, rows of different
     * lengths).
     */
    @Test
    public void basicTestsNoCenter() {
        assertThat(CenterOfTheMatrix.center(null), is(equalTo(null)));
        assertThat(CenterOfTheMatrix.center(new int[][] {}), is(equalTo(null)));

        assertThat(CenterOfTheMatrix.center(new int[][] {
                new int[] {1, 2, 3, 4},
                new int[] {5, 6, 7, 8},
                new int[] {9, 10, 11, 12}
        }), is(equalTo(null)));
        assertThat(CenterOfTheMatrix.center(new int[][] {
                new int[] {1, 2},
                new int[] {3, 4}
        }), is(equalTo(null)));
        assertThat(CenterOfTheMatrix.center(new int[][] {
                new int[] {1, 2},
                new int[] {3, 4, 5},
                new int[] {6, 7, 8, 9}
        }), is(equalTo(null)));
    }
}
