/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.misc.bowling;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import org.junit.jupiter.api.Test;

/**
 * JUnit tests for the {@link Bowling} class.
 * 
 * @author BJ Peter DeLaCruz &lt;bj.peter.delacruz@gmail.com&gt;
 */
public class TestBowling {

  /**
   * Tests scores that are valid.
   */
  @Test
  public void testValidScores() {
    var score = Bowling.getScore("XXXXXXXXXXXX");
    assertThat(score).isEqualTo(300);

    score = Bowling.getScore("9-9-9-9-9-9-9-9-9-9-");
    assertThat(score).isEqualTo(90);

    score = Bowling.getScore("5/5/5/5/5/5/5/5/5/5/5");
    assertThat(score).isEqualTo(150);

    score = Bowling.getScore("11111111111111111111");
    assertThat(score).isEqualTo(20);

    score = Bowling.getScore("XXXXXXXXXX90");
    assertThat(score).isEqualTo(288);

    score = Bowling.getScore("X5/X5/X5/X5/X5/X");
    assertThat(score).isEqualTo(200);
  }

  /**
   * Tests scores that are invalid.
   */
  @Test
  public void testInvalidScores() {
    assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> Bowling.getScore("5/5/5/5/5/5/5/5/5/5/5-"))
            .withMessageContainingAll("Invalid bowling record: invalid bowling record length.");

    assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> Bowling.getScore("5/5/5/5/5/5/5/5/5/"))
            .withMessageContainingAll("Invalid bowling record: number of frames does not equal 10.");

    assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> Bowling.getScore("5/5/5/5/5/5/5/5/X/5/5"))
            .withMessageContainingAll("Invalid bowling record: X should not immediately precede /.");

    assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> Bowling.getScore("5/5/5/5/5/5/5/5/5/555"))
            .withMessageContainingAll("Invalid bowling record: invalid tenth frame.");

    var errorMessage = "Invalid bowling record: second score in frame 1 should be / or less than first score.";
    assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> Bowling.getScore("555/5/5/5/5/5/5/5/5-"))
            .withMessageContainingAll(errorMessage);

    assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> Bowling.getScore("XXXXXXXXX811"))
            .withMessageContainingAll("Invalid bowling record: invalid tenth frame.");
  }

}
