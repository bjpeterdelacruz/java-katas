/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.misc;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.Calendar;
import java.util.Locale;

import org.junit.jupiter.api.Test;

/**
 * JUnit tests for the {@link AdjacentFriday13th} class.
 * 
 * @author BJ Peter DeLaCruz &lt;bj.peter.delacruz@gmail.com&gt;
 */
public class TestAdjacentFriday13th {

  /**
   * Tests the {@link AdjacentFriday13th#find(java.util.Date)} method with a date that is close to
   * but before July 13th.
   */
  @Test
  public void testFindBeforeJuly13th() {
    var calendar = Calendar.getInstance(Locale.US);
    calendar.set(Calendar.MONTH, Calendar.JULY);
    calendar.set(Calendar.DAY_OF_MONTH, 6);
    calendar.set(Calendar.YEAR, 2012);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);

    var c = Calendar.getInstance();
    c.setTime(AdjacentFriday13th.find(calendar.getTime()));

    assertThat(c.get(Calendar.MONTH)).isEqualTo(Calendar.JULY);
    assertThat(c.get(Calendar.DAY_OF_MONTH)).isEqualTo(13);
  }

  /**
   * Tests the {@link AdjacentFriday13th#find(java.util.Date)} method with a date that is close to
   * but after July 13th.
   */
  @Test
  public void testFindAfterJuly13th() {
    var calendar = Calendar.getInstance(Locale.US);
    calendar.set(Calendar.MONTH, Calendar.JULY);
    calendar.set(Calendar.DAY_OF_MONTH, 20);
    calendar.set(Calendar.YEAR, 2012);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);

    var c = Calendar.getInstance();
    c.setTime(AdjacentFriday13th.find(calendar.getTime()));

    assertThat(c.get(Calendar.MONTH)).isEqualTo(Calendar.JULY);
    assertThat(c.get(Calendar.DAY_OF_MONTH)).isEqualTo(13);
  }

  /**
   * Tests the {@link AdjacentFriday13th#find(java.util.Date)} method with a date that is not close
   * to July 13th.
   */
  @Test
  public void testFindApril13th() {
    var calendar = Calendar.getInstance(Locale.US);
    calendar.set(Calendar.MONTH, Calendar.MAY);
    calendar.set(Calendar.DAY_OF_MONTH, 4);
    calendar.set(Calendar.YEAR, 2012);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);

    var c = Calendar.getInstance();
    c.setTime(AdjacentFriday13th.find(calendar.getTime()));

    assertThat(c.get(Calendar.MONTH)).isEqualTo(Calendar.APRIL);
    assertThat(c.get(Calendar.DAY_OF_MONTH)).isEqualTo(13);
  }

  /**
   * Tests the {@link AdjacentFriday13th#find(java.util.Date)} method with a date that is before
   * Friday the 13th of February.
   */
  @Test
  public void testFindFeb13th1() {
    var calendar = Calendar.getInstance(Locale.US);
    calendar.set(Calendar.MONTH, Calendar.FEBRUARY);
    calendar.set(Calendar.DAY_OF_MONTH, 12);
    calendar.set(Calendar.YEAR, 2015);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);

    var c = Calendar.getInstance();
    c.setTime(AdjacentFriday13th.find(calendar.getTime()));

    assertThat(c.get(Calendar.MONTH)).isEqualTo(Calendar.FEBRUARY);
    assertThat(c.get(Calendar.DAY_OF_MONTH)).isEqualTo(13);
  }

  /**
   * Tests the {@link AdjacentFriday13th#find(java.util.Date)} method with a date that is after
   * Friday the 13th of February.
   */
  @Test
  public void testFindFeb13th2() {
    var calendar = Calendar.getInstance(Locale.US);
    calendar.set(Calendar.MONTH, Calendar.FEBRUARY);
    calendar.set(Calendar.DAY_OF_MONTH, 14);
    calendar.set(Calendar.YEAR, 2015);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);

    var c = Calendar.getInstance();
    c.setTime(AdjacentFriday13th.find(calendar.getTime()));

    assertThat(c.get(Calendar.MONTH)).isEqualTo(Calendar.FEBRUARY);
    assertThat(c.get(Calendar.DAY_OF_MONTH)).isEqualTo(13);
  }

}
