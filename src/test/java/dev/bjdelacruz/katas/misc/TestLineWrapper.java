/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.misc;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

/**
 * JUnit tests for the {@link LineWrapper} class.
 * 
 * @author BJ Peter DeLaCruz &lt;bj.peter.delacruz@gmail.com&gt;
 */
public class TestLineWrapper {

  /**
   * Tests the {@link LineWrapper#getWrappedString(String, int)} method.
   */
  @Test
  public void testGetWrappedString() {
    var buffer = new StringBuilder();
    var testString = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. ";
    buffer.append(testString);
    testString = "Aenean in nulla eros. Proin eget eros vitae turpis sodales tincidunt. ";
    buffer.append(testString);
    testString = "Aliquam at orci ut diam placerat laoreet sit amet et nunc. ";
    buffer.append(testString);
    testString = "Suspendisse nisl neque, fermentum et rhoncus at, posuere eget massa.";
    buffer.append(testString);

    var chars = LineWrapper.getWrappedString(buffer.toString(), 20);
    var strings = chars.split("\n");
    for (var string : strings) {
      assertThat(string.length()).isLessThan(20);
    }

    // Assert that no words were cut off in the middle.

    var list1 = new ArrayList<String>();
    Collections.addAll(list1, buffer.toString().split("\\s+"));

    var list2 = new ArrayList<String>();
    for (var string : strings) {
      list2.addAll(Arrays.asList(string.split("\\s+")));
    }

    assertThat(list1).isEqualTo(list2);
  }

  /**
   * Tests the {@link LineWrapper#getWrappedString(String, int)} method.
   */
  @Test
  public void testGetWrappedString2() {
    var buffer = new StringBuilder();
    IntStream.range(0, 255).forEach(buffer::append);
    var result = LineWrapper.getWrappedString(buffer.toString(), 200);
    assertThat(result).isEqualTo(buffer.toString());
  }

}
