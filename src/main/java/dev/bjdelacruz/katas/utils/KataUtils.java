/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.utils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;
import java.util.StringTokenizer;

/**
 * This class contains a set of methods that will help solving future Java katas much easier.
 * 
 * @author BJ Peter DeLaCruz
 */
public final class KataUtils {

  /**
   * This class cannot be instantiated nor extended.
   */
  private KataUtils() {
    // Empty constructor.
  }

  /**
   * Given a list of N letters, returns a list of all possible combinations of strings with length N.
   * 
   * @param letters Originally a list of N letters.
   * @return List of strings with length N.
   */
  @Deprecated
  public static List<String> makeStringsList(List<Character> letters) {
    if (letters == null) {
      throw new IllegalArgumentException("letters is null");
    }
    if (letters.isEmpty()) {
      return new ArrayList<>();
    }

    var strings = new ArrayList<String>();
    makeStringsList(strings, letters);
    return strings.stream().filter(str -> str.length() == letters.size()).toList();
  }

  /**
   * A recursive function that will add all possible combinations of strings with lengths 2 to N given a list of N
   * letters to a list.
   * 
   * @param strings Originally an empty list of strings.
   * @param letters Originally a list of N letters.
   */
  @Deprecated
  private static void makeStringsList(List<String> strings, List<Character> letters) {
    if (strings == null) {
      throw new IllegalArgumentException("strings is null");
    }
    if (letters == null) {
      throw new IllegalArgumentException("letters is null");
    }
    var temp = new ArrayList<>(letters);

    if (temp.size() == 2) {
      strings.add(temp.getFirst() + "" + temp.get(1));
      strings.add(temp.get(1) + "" + temp.getFirst());
    }
    else {
      var c = temp.removeFirst();
      makeStringsList(strings, temp);
      var tempList = new ArrayList<String>();
      for (var s : strings) {
        var buffer = new StringBuilder(s);
        for (var index = 0; index < s.length() + 1; index++) {
          buffer.insert(index, c);
          tempList.add(buffer.toString());
          buffer = new StringBuilder(s);
        }
      }
      strings.addAll(tempList);
    }
  }

  /**
   * Given a set of N characters, generates a list of permutations of strings with length 0 to N.
   * 
   * @param chars The set of N characters.
   * @return The list of permutations of strings with length 0 to N.
   */
  public static List<String> getAllPermutationsOfSubsequences(Set<Character> chars) {
    if (chars == null) {
      throw new IllegalArgumentException("chars is null");
    }
    if (chars.isEmpty()) {
      return new ArrayList<>();
    }

    var permutations = new ArrayList<String>();

    generatePowerSet(chars).forEach(c -> permute(new ArrayList<>(c), 0, permutations));

    return permutations;
  }

  /**
   * Generates a set of sets of characters (i.e. a power set); each set of characters is used by
   * {@link #getAllPermutationsOfSubsequences(Set)} to create the list of permutations of strings with length 0 to N.
   * 
   * @param set The set used to create the power set.
   * @return The power set.
   */
  private static Set<Set<Character>> generatePowerSet(Set<Character> set) {
    if (set == null) {
      throw new IllegalArgumentException("set is null");
    }
    var powerSet = new HashSet<Set<Character>>();
    if (set.isEmpty()) {
      powerSet.add(new HashSet<>());
      return powerSet;
    }

    var anElement = set.iterator().next();
    set.remove(anElement);

    for (var subset : generatePowerSet(set)) {
      var setWithElement = new HashSet<>(subset);
      setWithElement.add(anElement);
      powerSet.add(setWithElement);
      powerSet.add(subset);
    }

    set.add(anElement);

    return powerSet;
  }

  /**
   * Permutes a list of characters.
   * 
   * @param characters The characters to permute.
   * @param index The starting point in the list of characters.
   * @param strings The list in which the permutations of strings are stored.
   */
  private static void permute(List<Character> characters, int index, List<String> strings) {
    if (characters == null) {
      throw new IllegalArgumentException("characters is null");
    }
    if (strings == null) {
      throw new IllegalArgumentException("strings is null");
    }
    if (characters.isEmpty()) {
      return;
    }

    if (index == characters.size()) {
      var buffer = new StringBuilder();
      for (var i = 0; i < index; i++) {
        buffer.append(characters.get(i));
      }
      strings.add(buffer.toString());
    }
    else {
      for (var i = index; i < characters.size(); i++) {
        var temp = characters.get(i);
        characters.set(i, characters.get(index));
        characters.set(index, temp);
        permute(characters, index + 1, strings);
        temp = characters.get(i);
        characters.set(i, characters.get(index));
        characters.set(index, temp);
      }
    }
  }

  /**
   * Creates a list of objects of a particular data type from a string (usually, a line read in from a file).
   * 
   * @param line The string containing a specific data type.
   * @param delim The delimiter, e.g. a whitespace character.
   * @param type The data type of the objects in the list.
   * @return A list of objects of a particular data type, or <code>null</code> if problems were
   * encountered parsing the string.
   */
  public static List<?> createList(String line, String delim, KataEnums type) {
    var tokenizer = new StringTokenizer(line, delim);
    try {
      switch (type) {
        case CHARACTER -> {
          var characters = new ArrayList<Character>();
          while (tokenizer.hasMoreTokens()) {
            characters.add(tokenizer.nextToken().toCharArray()[0]);
          }
          return characters;
        }
        case DOUBLE -> {
          var doubles = new ArrayList<Double>();
          while (tokenizer.hasMoreTokens()) {
            doubles.add(Double.parseDouble(tokenizer.nextToken()));
          }
          return doubles;
        }
        case INTEGER -> {
          var integers = new ArrayList<Integer>();
          while (tokenizer.hasMoreTokens()) {
            integers.add(Integer.parseInt(tokenizer.nextToken()));
          }
          return integers;
        }
        case STRING -> {
          var strings = new ArrayList<String>();
          while (tokenizer.hasMoreTokens()) {
            strings.add(tokenizer.nextToken());
          }
          return strings;
        }
        default -> throw new IllegalArgumentException("Unsupported data type: " + type);
      }
    }
    catch (NumberFormatException e) {
      throw new IllegalArgumentException("Non-numeric characters found: " + line);
    }
  }

  /**
   * Given the name of a file, reads in all the lines from the file.
   * 
   * @param filename Name of a file.
   * @return A list containing all the lines that were read in.
   */
  public static List<String> readLines(String filename) {
    if (filename == null) {
      throw new IllegalArgumentException("letters is null");
    }
    if (filename.isEmpty()) {
      throw new IllegalArgumentException("Filename must not be an empty string.");
    }

    var list = new ArrayList<String>();

    try (var reader = new BufferedReader(new InputStreamReader(
            new FileInputStream(filename), StandardCharsets.US_ASCII))) {
      var line = reader.readLine();
      while (line != null) {
        list.add(line);
        line = reader.readLine();
      }
    }
    catch (IOException e) {
      throw new RuntimeException(e);
    }

    return list;
  }

  /**
   * Given a list of strings, returns a concatenated string containing all the strings in the list.
   * 
   * @param strings The list of strings.
   * @return A concatenated string containing all the strings in the list, separated by spaces.
   */
  public static String stringsListToString(List<String> strings) {
    if (strings == null || strings.isEmpty()) {
      return "";
    }

    var sj = new StringJoiner(" ");
    strings.forEach(sj::add);

    return sj.toString();
  }

  /**
   * Given a string, a substring to replace, and a replacement substring, returns a list of strings
   * that contains one replacement substring per string. For example,
   * <code>replaceAllOccurences("A dog is a dog", "dog", "cat")</code>, where "dog" is the substring
   * to replace and "cat" is the replacement substring, returns "A cat is a dog" and
   * "A dog is a cat".
   * 
   * @param string The string that contains the substring to replace.
   * @param oldString The substring to replace.
   * @param newString The replacement substring.
   * @return A list of strings that contains one replacement substring per string.
   */
  public static List<String> replace(String string, String oldString, String newString) {
    if (string == null) {
      throw new IllegalArgumentException("string is null");
    }
    if (oldString == null) {
      throw new IllegalArgumentException("oldString is null");
    }
    if (newString == null) {
      throw new IllegalArgumentException("newString is null");
    }
    var tokens = new ArrayList<String>();
    var strings = new ArrayList<String>();
    var tokenizer = new StringTokenizer(string, " ");
    while (tokenizer.hasMoreTokens()) {
      tokens.add(tokenizer.nextToken());
    }
    for (var index = 0; index < tokens.size(); index++) {
      if (tokens.get(index).equals(oldString)) {
        tokens.set(index, newString);
        strings.add(stringsListToString(tokens));
        tokens.set(index, oldString);
      }
    }
    return strings;
  }

  /**
   * Creates a list of playing cards from lines read in from a file.
   * 
   * @param lines Lines containing a list of playing cards (e.g. 2C, KS).
   * @return A list of playing cards.
   */
  public static List<PlayingCard> createPlayingCards(List<String> lines) {
    if (lines == null) {
      throw new IllegalArgumentException("lines is null");
    }
    if (lines.isEmpty()) {
      return new ArrayList<>();
    }

    var temp = new ArrayList<>(lines);

    var playingCards = new ArrayList<PlayingCard>();
    do {
      var str = temp.removeFirst();

      if ("#".equals(str)) {
        break;
      }

      @SuppressWarnings("unchecked")
      var tokens = (List<String>) KataUtils.createList(str, " ", KataEnums.STRING);
      tokens.forEach(card -> playingCards.add(new PlayingCard(card, true)));
    } while (!lines.isEmpty());

    return playingCards;
  }

  /**
   * Given a number that represents currency, e.g. 40000, returns the same number as a string with a period inserted
   * behind the second to last digit, e.g. 400.00.
   * 
   * @param value The number to format, e.g. 40000.
   * @return The number formatted with a period behind the second to last digit, e.g. 400.00.
   */
  public static String getBalanceAsString(int value) {
    var balance = value + "";
    if (balance.length() == 2) {
      balance = balance.contains("-") ? balance.replace("-", "-00") : "0" + balance;
    }
    else if (balance.length() == 1) {
      balance = "00" + balance;
    }
    var length = balance.length();
    return balance.substring(0, length - 2) + "." + balance.substring(length - 2);
  }
}
