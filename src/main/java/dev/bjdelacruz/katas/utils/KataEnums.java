/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.utils;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * This enum is used especially by the <code>KataUtils</code> class to simplify the creation of
 * methods that return different types of objects.
 * 
 * @author BJ Peter DeLaCruz
 */
@SuppressFBWarnings("PI_DO_NOT_REUSE_PUBLIC_IDENTIFIERS_FIELD_NAMES")
public enum KataEnums {

  /** The different data types that an ADT can hold. */
  CHARACTER("Character"), DOUBLE("Double"), INTEGER("Integer"), STRING("String");

  KataEnums(String displayName) {
  }

}
