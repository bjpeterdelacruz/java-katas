/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.utils;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import java.util.ArrayList;
import java.util.List;

/**
 * The base class that contains fields and methods used by all katas.
 * 
 * @author BJ Peter DeLaCruz
 */
public abstract class Kata {

  /** The lines read in from a file. */
  private List<String> lines = new ArrayList<>();

  /** Processes all the lines in a file. */
  public abstract void processLines();

  /**
   * Returns a list of strings that represent lines from an input file.
   *
   * @return A list of strings
   */
  @SuppressFBWarnings("EI_EXPOSE_REP")
  public List<String> getLines() {
    return lines;
  }

  /**
   * Sets the list of strings that represent lines from an input file.
   *
   * @param lines A list of strings
   */
  public void setLines(List<String> lines) {
    this.lines = new ArrayList<>(lines);
  }
}
