/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.misc;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * This program finds the closest Friday the 13th before or after the current date.
 * 
 * @author BJ Peter DeLaCruz
 */
final class AdjacentFriday13th {

  /** Do not instantiate this class. */
  private AdjacentFriday13th() {
  }

  /**
   * Given a date, returns a Date object that contains the date of the nearest Friday the 13th.
   * 
   * @param date Date specified by the user.
   * @return Date object containing the date for the nearest Friday the 13th.
   */
  public static Date find(Date date) {
    if (date == null) {
      throw new IllegalArgumentException("date is null");
    }

    var today = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

    var friday = today;
    if (today.getDayOfWeek() == DayOfWeek.SATURDAY) {
      friday = today.minusDays(1);
    }
    else if (today.getDayOfWeek() != DayOfWeek.FRIDAY) {
      friday = today.plusDays(DayOfWeek.FRIDAY.getValue() - today.getDayOfWeek().getValue());
    }

    var before = friday;
    while (before.getDayOfMonth() != 13) {
      before = before.minusDays(7);
    }

    var after = friday;
    while (after.getDayOfMonth() != 13) {
      after = after.plusDays(7);
    }

    var numDaysBefore = ChronoUnit.DAYS.between(before, today);
    var numDaysAfter = ChronoUnit.DAYS.between(today, after);

    return numDaysBefore < numDaysAfter ? toDate(before) : toDate(after);
  }

  /**
   * Converts a {@link java.time.LocalDate} to a {@link java.util.Date}.
   * 
   * @param date The Date object to convert.
   * @return An equivalent LocalDate object.
   */
  private static Date toDate(LocalDate date) {
    return Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
  }

}
