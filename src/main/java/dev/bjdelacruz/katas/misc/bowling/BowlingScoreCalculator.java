/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.misc.bowling;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

/**
 * A front-end for the {@link Bowling} kata.
 * 
 * @author BJ Peter DeLaCruz
 */
@SuppressWarnings("PMD.ConstructorCallsOverridableMethod")
public class BowlingScoreCalculator extends JFrame {

  /**
   * Creates and displays a user interface in which the user can enter his or her 10-frame bowling record.
   */
  public BowlingScoreCalculator() {
    var panel1 = new JPanel();
    var label1 = new JLabel("Bowling Score Calculator");
    label1.setFont(new Font("Arial", Font.BOLD, 16));
    panel1.add(label1);
    panel1.setLayout(new FlowLayout(FlowLayout.CENTER));

    var panel2 = new JPanel();

    var instructions = new JLabel("""
        <html>
          <body>
            This program will calculate the final score for a bowling record.<br>
            Enter a 10-frame bowling record in the text field and then click Calculate Score.<br><br>
            Here are some examples:<br><br>
            &nbsp;&nbsp;&nbsp;XXXXXXXXXXXX (All strikes)<br><br>
            &nbsp;&nbsp;&nbsp;5/5/5/5/5/5/5/5/5/5/5 (All spares)<br><br>
            &nbsp;&nbsp;&nbsp;9-9-9-9-9-9-9-9-9-9- (Missed the last pin in all frames)<br><br>
            &nbsp;&nbsp;&nbsp;X5-5/5-5/5-5/5-5/54 (One strike, four spares, four misses)<br><br>
          </body>
        </html>""");

    var textField = new JTextField("", 21);
    var submit = new JButton("Calculate Score");

    submit.addActionListener(event -> {
        var dialog = new JDialog(BowlingScoreCalculator.this);
        JLabel results;
        try {
          results = new JLabel("Your final score is " + Bowling.getScore(textField.getText()) + ".");
        }
        catch (IllegalArgumentException e) {
          results = new JLabel("Invalid bowling record. Please try again.");
        }
        var ok = new JButton("OK");
        dialog.add(results);
        ok.addActionListener(e -> dialog.dispose());
        dialog.add(ok);
        dialog.setLayout(new FlowLayout(FlowLayout.CENTER));
        dialog.setSize(360, 75);
        dialog.setLocationRelativeTo(BowlingScoreCalculator.this);
        dialog.setVisible(true);
    });

    panel2.add(instructions);
    panel2.add(textField);
    panel2.add(submit);

    setLayout(new BorderLayout());
    add(panel1, BorderLayout.NORTH);
    add(panel2, BorderLayout.CENTER);
    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    setLocationRelativeTo(null);
    pack();
    setVisible(true);
  }

  /**
   * The main entry point of this application. Displays a window with a text box in which the user can enter a
   * 10-frame bowling record.
   * 
   * @param args None.
   */
  public static void main(String... args) {
    new BowlingScoreCalculator();
  }

}
