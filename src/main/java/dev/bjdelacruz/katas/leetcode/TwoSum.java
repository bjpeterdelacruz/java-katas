package dev.bjdelacruz.katas.leetcode;

import java.util.Arrays;
import java.util.HashMap;

/**
 * This class finds two integers in an array that sum to a target value.
 *
 * @author BJ Peter Dela Cruz
 */
public final class TwoSum {
    private TwoSum() {
    }

    /**
     * Returns the indexes of two integers in the given array that sum to the given target value. This solution assumes
     * that there is exactly one pair of integers that sum to the target value, and an integer at any index in the array
     * may not be used twice.
     *
     * @param nums The array of integers
     * @param target The target value
     * @return An array containing two indexes of the integers that sum to the target value
     */
    public static int[] twoSum(int[] nums, int target) {
        var counts = new HashMap<Integer, Integer>();
        for (var number : nums) {
            var count = counts.putIfAbsent(number, 1);
            if (count != null) {
                counts.put(number, count + 1);
            }
        }
        var number1 = 0;
        var number2 = 0;
        for (var num : nums) {
            var result = target - num;
            if (counts.containsKey(result) && (num != result || counts.get(result) >= 2)) {
                number1 = num;
                number2 = result;
                break;
            }
        }
        var indexes = new Integer[] {null, null};
        for (var idx = 0; idx < nums.length; idx++) {
            if (nums[idx] == number1 && indexes[0] == null) {
                indexes[0] = idx;
            }
            else if (nums[idx] == number2) {
                indexes[1] = idx;
            }
            if (indexes[0] != null && indexes[1] != null) {
                break;
            }
        }
        return Arrays.stream(indexes).mapToInt(Integer::intValue).toArray();
    }
}
