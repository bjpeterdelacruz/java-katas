package dev.bjdelacruz.katas.leetcode;

/**
 * This class reverses all the vowels in a string. For example, "bread-pudding" becomes
 * "briud-paddeng".
 *
 * @author BJ Peter Dela Cruz
 */
public class ReverseVowelsOfAString {
    /**
     * Returns a string in which all the vowels in it are reversed. All the consonants, symbols,
     * and whitespaces in the string are left in their place.
     *
     * @param s A string that may or may not contain vowels.
     * @return A string that contains vowels that have been reversed.
     */
    public String reverseVowels(String s) {
        var vowels = "aeiouAEIOU";
        var allVowels = new StringBuilder();

        for (var character : s.toCharArray()) {
            if (vowels.indexOf(character) != -1) {
                allVowels.append(character);
            }
        }

        allVowels.reverse();
        var newString = new StringBuilder();

        for (int idx = 0, idx2 = 0; idx < s.length(); idx++) {
            if (vowels.indexOf(s.charAt(idx)) != -1) {
                newString.append(allVowels.charAt(idx2++));
            }
            else {
                newString.append(s.charAt(idx));
            }
        }

        return newString.toString();
    }
}
