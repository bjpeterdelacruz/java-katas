package dev.bjdelacruz.katas.codewars;

import java.util.Arrays;

/**
 * This class sums two integer arrays together using the Java 8 Stream API.
 *
 * @author BJ Peter Dela Cruz
 */
public final class ArrayPlusArray {
    private ArrayPlusArray() {
    }

    /**
     * Sums the two integer arrays together using the Java 8 Stream API and returns the result.
     *
     * @param arr1 The first integer array
     * @param arr2 The second integer array
     * @return The sum of all the integers in both arrays
     */
    public static int arrayPlusArray(int[] arr1, int[] arr2) {
        return Arrays.stream(arr1).sum() + Arrays.stream(arr2).sum();
    }
}
