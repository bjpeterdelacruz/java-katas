package dev.bjdelacruz.katas.codewars;

import java.util.HashMap;
import java.util.Map;

/**
 * This class provides an API that returns an enum for a number from one to four in the English, Japanese, and French
 * languages.
 *
 * @author BJ Peter Dela Cruz
 */
public final class OneTwoThreeFour {
    private OneTwoThreeFour() {
    }

    private static final Map<String, MyNumber> CACHE = new HashMap<>();
    static {
        for (MyNumber myNumber : MyNumber.values()) {
            for (String name : myNumber.names) {
                CACHE.put(name, myNumber);
            }
        }
    }

    enum MyNumber {
        ONE(1, "one", "ichi", "un"),
        TWO(2, "two", "ni", "deux"),
        THREE(3, "three", "san", "trois"),
        FOUR(4, "four", "shi", "quatre");

        private final int val;
        private final String[] names;

        MyNumber(int val, String... names) {
            this.val = val;
            this.names = names;
        }

        public int intValue() {
            return this.val;
        }
    }

    /**
     * Returns an enum representing a number given the name of that number in English, Japanese, or French
     *
     * @param name The name of a number from one to four in English, Japanese, or French
     * @return An enum representing the number
     */
    public static MyNumber getNumber(final String name) {
        return CACHE.get(name);
    }
}
