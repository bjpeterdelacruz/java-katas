package dev.bjdelacruz.katas.codewars;

/**
 * This class sums two bytes together and returns a byte.
 *
 * @author BJ Peter Dela Cruz
 */
public final class APlusB {
    private APlusB() {
    }

    /**
     * Sums two bytes together and returns the result as a byte. It is assumed that the sum fits into a byte.
     *
     * @param a The first byte
     * @param b The second byte
     * @return The sum of adding the two bytes
     */
    public static byte sum(byte a, byte b) {
        return (byte) (a + b);
    }
}
