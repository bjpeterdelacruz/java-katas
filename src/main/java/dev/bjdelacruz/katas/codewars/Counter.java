package dev.bjdelacruz.katas.codewars;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.stream.Stream;

/**
 * This class is equivalent to the Counter class found in Python. Basically, a Counter will keep track of the count of
 * each object it stores.
 *
 * @author BJ Peter Dela Cruz
 * @param <T> The type of the objects that a Counter instance will store
 */
@SuppressFBWarnings("PI_DO_NOT_REUSE_PUBLIC_IDENTIFIERS_CLASS_NAMES")
public class Counter<T> extends HashMap<T, Long> {

    public static final long serialVersionUID = 1212021;

    /**
     * Creates an empty Counter object.
     */
    public Counter() {
        super();
    }

    /**
     * Creates a new Counter object and populates it with the values in the given Counter object.
     *
     * @param other The Counter object from which to populate values.
     * @param <V> The type of the objects stored in the other Counter object.
     */
    public <V extends T> Counter(Counter<V> other) {
        super(other);
    }

    /**
     * Creates a new Counter object with the given initial capacity and default load factor.
     *
     * @param capacity The initial capacity.
     */
    public Counter(int capacity) {
        super(capacity);
    }

    /**
     * Creates a new Counter object with the given initial capacity and given load factor.
     *
     * @param capacity The initial capacity.
     * @param loadFactor The load factor.
     */
    public Counter(int capacity, float loadFactor) {
        super(capacity, loadFactor);
    }

    /**
     * Creates a new Counter object and populates it with the values in the given map.
     *
     * @param map The map from which to populate values.
     * @param <V> The type of the objects stored in the other Counter object.
     */
    public <V extends T> Counter(Map<V, Long> map) {
        super(map);
    }

    /**
     * Creates a new Counter object and populates it with the values in the given array.
     *
     * @param arr The array from which to populate values.
     */
    public Counter(T[] arr) {
        Arrays.stream(arr).forEach(this::push);
    }

    /**
     * Creates a new Counter object and populates it with the values in the given collection.
     *
     * @param coll The collection from which to populate values.
     */
    public Counter(Collection<T> coll) {
        coll.stream().forEach(this::push);
    }

    /**
     * Creates a new Counter object and populates it with the values in the given stream.
     *
     * @param stream The stream from which to populate values.
     */
    public Counter(Stream<T> stream) {
        stream.forEach(this::push);
    }

    /**
     * Creates a new Counter object and populates it with the values in the given boolean array.
     *
     * @param arr A boolean array.
     * @return A Counter object that has the counts for each boolean in the array.
     */
    public static Counter<Boolean> of(boolean[] arr) {
        List<Boolean> temp = new ArrayList<>();
        for (var value : arr) {
            temp.add(value);
        }
        return new Counter<>(temp);
    }

    /**
     * Creates a new Counter object and populates it with the values in the given byte array.
     *
     * @param arr A byte array.
     * @return A Counter object that has the counts for each byte in the array.
     */
    public static Counter<Byte> of(byte[] arr) {
        List<Byte> temp = new ArrayList<>();
        for (var value : arr) {
            temp.add(value);
        }
        return new Counter<>(temp);
    }

    /**
     * Creates a new Counter object and populates it with the values in the given char array.
     *
     * @param arr A char array.
     * @return A Counter object that has the counts for each char in the array.
     */
    public static Counter<Character> of(char[] arr) {
        List<Character> temp = new ArrayList<>();
        for (var value : arr) {
            temp.add(value);
        }
        return new Counter<>(temp);
    }

    /**
     * Creates a new Counter object and populates it with the values in the given double array.
     *
     * @param arr A double array.
     * @return A Counter object that has the counts for each double in the array.
     */
    public static Counter<Double> of(double[] arr) {
        List<Double> temp = new ArrayList<>();
        for (var value : arr) {
            temp.add(value);
        }
        return new Counter<>(temp);
    }

    /**
     * Creates a new Counter object and populates it with the values in the given float array.
     *
     * @param arr A float array.
     * @return A Counter object that has the counts for each float in the array.
     */
    public static Counter<Float> of(float[] arr) {
        List<Float> temp = new ArrayList<>();
        for (var value : arr) {
            temp.add(value);
        }
        return new Counter<>(temp);
    }

    /**
     * Creates a new Counter object and populates it with the values in the given int array.
     *
     * @param arr An int array.
     * @return A Counter object that has the counts for each int in the array.
     */
    public static Counter<Integer> of(int[] arr) {
        List<Integer> temp = new ArrayList<>();
        for (var value : arr) {
            temp.add(value);
        }
        return new Counter<>(temp);
    }

    /**
     * Creates a new Counter object and populates it with the values in the given long array.
     *
     * @param arr A long array.
     * @return A Counter object that has the counts for each long in the array.
     */
    public static Counter<Long> of(long[] arr) {
        List<Long> temp = new ArrayList<>();
        for (var value : arr) {
            temp.add(value);
        }
        return new Counter<>(temp);
    }

    /**
     * Creates a new Counter object and populates it with the values in the given short array.
     *
     * @param arr A short array.
     * @return A Counter object that has the counts for each short in the array.
     */
    public static Counter<Short> of(short[] arr) {
        List<Short> temp = new ArrayList<>();
        for (var value : arr) {
            temp.add(value);
        }
        return new Counter<>(temp);
    }

    /**
     * Creates a new Counter object and counts each letter in the given string. Each letter is stored as a string in
     * the Counter object.
     *
     * @param string A string.
     * @return A Counter object that has the counts for each letter in the string.
     */
    public static Counter<String> of(String string) {
        List<String> temp = new ArrayList<>();
        for (var character : string.toCharArray()) {
            temp.add(Character.toString(character));
        }
        return new Counter<>(temp);
    }

    /**
     * Increments the count for the given element by one.
     *
     * @param element The element to add to this object.
     */
    public void push(T element) {
        put(element, get(element) + 1);
    }

    /**
     * Increments the count for the given element by the given number.
     *
     * @param element The element to add to this object.
     * @param n The number by which to increment the count.
     */
    public void push(T element, long n) {
        put(element, get(element) + n);
    }

    /**
     * Increments the count for each element from the given collection by one.
     *
     * @param coll The collection that contains the elements to add to this object.
     * @param <V> The type of the elements in the collection.
     */
    public <V extends T> void pushAll(Collection<V> coll) {
        for (var element : coll) {
            push(element);
        }
    }

    /**
     * Increments the count for each element from the given array by one.
     *
     * @param arr The array that contains the elements to add to this object.
     * @param <V> The type of the elements in the array.
     */
    public <V extends T> void pushAll(V[] arr) {
        for (var element : arr) {
            push(element);
        }
    }

    /**
     * Increments the count for each element from the given stream by one.
     *
     * @param stream The stream that contains the elements to add to this object.
     * @param <V> The type of the elements in the stream.
     */
    public <V extends T> void pushAll(Stream<V> stream) {
        stream.forEach(this::push);
    }

    /**
     * Increments the count for each element from the given map by the count stored in the map.
     *
     * @param map The map that contains the elements to add to this object.
     * @param <V> The type of the elements in the map.
     */
    public <V extends T> void pushAll(Map<V, Long> map) {
        for (var entry : map.entrySet()) {
            put(entry.getKey(), get(entry.getKey()) + entry.getValue());
        }
    }

    /**
     * Adds the boolean values in the given array to the given Counter of Boolean objects.
     *
     * @param counter The Counter object to which to add the boolean values.
     * @param arr The boolean array.
     */
    public static void pushAll(Counter<Boolean> counter, boolean[] arr) {
        for (var value : arr) {
            counter.push(value);
        }
    }

    /**
     * Adds the byte values in the given array to the given Counter of Byte objects.
     *
     * @param counter The Counter object to which to add the byte values.
     * @param arr The byte array.
     */
    public static void pushAll(Counter<Byte> counter, byte[] arr) {
        for (var value : arr) {
            counter.push(value);
        }
    }

    /**
     * Adds the char values in the given array to the given Counter of Character objects.
     *
     * @param counter The Counter object to which to add the char values.
     * @param arr The char array.
     */
    public static void pushAll(Counter<Character> counter, char[] arr) {
        for (var value : arr) {
            counter.push(value);
        }
    }

    /**
     * Adds the double values in the given array to the given Counter of Double objects.
     *
     * @param counter The Counter object to which to add the double values.
     * @param arr The double array.
     */
    public static void pushAll(Counter<Double> counter, double[] arr) {
        for (var value : arr) {
            counter.push(value);
        }
    }

    /**
     * Adds the float values in the given array to the given Counter of Float objects.
     *
     * @param counter The Counter object to which to add the float values.
     * @param arr The float array.
     */
    public static void pushAll(Counter<Float> counter, float[] arr) {
        for (var value : arr) {
            counter.push(value);
        }
    }

    /**
     * Adds the int values in the given array to the given Counter of Integer objects.
     *
     * @param counter The Counter object to which to add the int values.
     * @param arr The int array.
     */
    public static void pushAll(Counter<Integer> counter, int[] arr) {
        for (var value : arr) {
            counter.push(value);
        }
    }

    /**
     * Adds the long values in the given array to the given Counter of Long objects.
     *
     * @param counter The Counter object to which to add the long values.
     * @param arr The long array.
     */
    public static void pushAll(Counter<Long> counter, long[] arr) {
        for (var value : arr) {
            counter.push(value);
        }
    }

    /**
     * Adds the short values in the given array to the given Counter of Short objects.
     *
     * @param counter The Counter object to which to add the short values.
     * @param arr The short array.
     */
    public static void pushAll(Counter<Short> counter, short[] arr) {
        for (var value : arr) {
            counter.push(value);
        }
    }

    /**
     * Adds each character in the given string as a string to the given Counter of String objects.
     *
     * @param other The Counter object to which to add each letter as a string.
     * @param string The string whose characters are added as strings.
     */
    public static void pushAll(Counter<String> other, String string) {
        for (var character : string.toCharArray()) {
            other.push(Character.toString(character));
        }
    }

    @Override
    public String toString() {
        return "Counter(" + super.toString() + ")";
    }

    @Override
    public Long get(Object key) {
        return getOrDefault(key, 0L);
    }

    /**
     * Returns a stream of elements, each element repeating as many times as its count.
     *
     * @return A stream of elements.
     */
    public Stream<T> elements() {
        return elementsAsList().stream();
    }

    /**
     * Returns a list of elements, each element repeating as many times as its count.
     *
     * @return A list of elements.
     */
    public List<T> elementsAsList() {
        List<T> elements = new ArrayList<>();
        for (Map.Entry<T, Long> entry : entrySet()) {
            for (int count = 0; count < entry.getValue(); count++) {
                elements.add(entry.getKey());
            }
        }
        return elements;
    }

    /**
     * Returns all elements with their counts as a stream, sorted in descending order from most common to least common.
     *
     * @param <V> The type of the elements in this Counter object.
     * @return A stream of all elements with their counts, sorted in descending order.
     */
    public <V extends T> Stream<Map.Entry<T, Long>> mostCommon() {
        return mostCommonAsList(size()).stream();
    }

    /**
     * Returns all elements with their counts as a list, sorted in descending order from most common to least common.
     *
     * @param <V> The type of the elements in this Counter object.
     * @return A list of all elements with their counts, sorted in descending order.
     */
    public <V extends T> List<Map.Entry<T, Long>> mostCommonAsList() {
        return mostCommonAsList(size());
    }

    /**
     * Returns N most common elements with their counts as a stream, sorted in descending order.
     *
     * @param <V> The type of the elements in this Counter object.
     * @param n The number of elements to return.
     * @return A stream of the N most common elements with their counts, sorted in descending order.
     */
    public <V extends T> Stream<Map.Entry<T, Long>> mostCommon(int n) {
        return mostCommonAsList(n).stream();
    }

    /**
     * Returns N most common elements with their counts as a list, sorted in descending order.
     *
     * @param <V> The type of the elements in this Counter object.
     * @param n The number of elements to return.
     * @return A list of the N most common elements with their counts, sorted in descending order.
     */
    public <V extends T> List<Map.Entry<T, Long>> mostCommonAsList(int n) {
        if (n < 0) {
            throw new IllegalArgumentException("n must be greater than or equal to 0.");
        }
        List<Map.Entry<T, Long>> elements = new ArrayList<>(entrySet());
        elements.sort(Map.Entry.comparingByValue());
        Collections.reverse(elements);
        if (n >= elements.size()) {
            return elements;
        }
        return new ArrayList<>(elements.subList(0, n));
    }

    /**
     * Sums the counts of the elements in the given Counter object with the counts of the elements in this Counter
     * object. If the sum is zero or negative, the element will not be included in the new instance that is returned.
     *
     * @param other The other Counter object.
     * @param <V> The type of the elements in the other Counter object.
     * @return A new Counter instance with the counts summed together.
     */
    public <V extends T> Counter<T> add(Counter<V> other) {
        return performFunction(other, Long::sum);
    }

    /**
     * Subtracts the counts of the elements in the given Counter object with the counts of the elements in this Counter
     * object. If the difference is zero or negative, the element will not be included in the new instance that is
     * returned.
     *
     * @param other The other Counter object.
     * @param <V> The type of the elements in the other Counter object.
     * @return A new Counter instance with the new counts.
     */
    public <V extends T> Counter<T> sub(Counter<V> other) {
        return performFunction(other, (value1, value2) -> value1 - value2);
    }

    /**
     * Finds the maximum between the counts of the elements in the given Counter object and the counts of the elements
     * in this Counter object. If the maximum is zero or negative, the element will not be included in the new instance
     * that is returned.
     *
     * @param other The other Counter object.
     * @param <V> The type of the elements in the other Counter object.
     * @return A new Counter instance with the new counts.
     */
    public <V extends T> Counter<T> intersect(Counter<V> other) {
        return performFunction(other, Math::min);
    }

    /**
     * Finds the minimum between the counts of the elements in the given Counter object and the counts of the elements
     * in this Counter object. If the minimum is zero or negative, the element will not be included in the new instance
     * that is returned.
     *
     * @param other The other Counter object.
     * @param <V> The type of the elements in the other Counter object.
     * @return A new Counter instance with the new counts.
     */
    public <V extends T> Counter<T> union(Counter<V> other) {
        return performFunction(other, Math::max);
    }

    /**
     * Applies the given function (e.g. addition, subtraction, maximum, minimum) to an element in this Counter object
     * and the same element in the other Counter object.
     *
     * @param other The other Counter object.
     * @param function The function to apply to the counts (addition, subtraction, maximum, minimum).
     * @param <V> The type of the elements in the other Counter object.
     * @return A new Counter instance with the new counts.
     */
    private <V extends T> Counter<T> performFunction(Counter<V> other, BiFunction<Long, Long, Long> function) {
        Counter<T> counter = new Counter<>(this);
        Set<T> keys = new HashSet<>(counter.keySet());
        keys.addAll(other.keySet());
        for (T key : keys) {
            long result = function.apply(counter.get(key), other.get(key));
            if (result > 0) {
                counter.put(key, result);
            }
            else {
                counter.remove(key);
            }
        }
        return counter;
    }

    /**
     * Subtracts the counts of the elements in the given Counter object with the counts of the elements in this Counter
     * object. A new instance of Counter is returned.
     *
     * @param other The other Counter object.
     * @param <V> The type of the elements in the other Counter object.
     * @return A new Counter instance with the new counts.
     */
    public <V extends T> Counter<T> subtract(Counter<V> other) {
        Counter<T> counter = new Counter<>(this);
        for (var entry : other.entrySet()) {
            counter.put(entry.getKey(), counter.get(entry.getKey()) - entry.getValue());
        }
        return counter;
    }

    /**
     * Multiplies the counts in this Counter object by the given number.
     *
     * @param n The number by which each count is multiplied.
     * @return A new Counter instance with each count multiplied by n.
     */
    public Counter<T> mul(int n) {
        Counter<T> counter = new Counter<>(this);
        for (var entry : entrySet()) {
            counter.put(entry.getKey(), counter.get(entry.getKey()) * n);
        }
        return counter;
    }
}
