package dev.bjdelacruz.katas.codewars;

import java.util.Arrays;
import java.util.Locale;

/**
 * This class turns all the letters of each string in an array of strings into lowercase, except for the first letter,
 * which is turned into uppercase.
 *
 * @author BJ Peter Dela Cruz
 */
public final class NameArrayCapping {

    private NameArrayCapping() {
    }

    /**
     * Returns an array of strings. The first letter of each string in the array is capitalized, and the rest are turned
     * into lowercase. The original input array is not modified.
     *
     * @param strings An array of strings
     * @return An array of strings, each string starts with a capitalized character and the rest of the characters are
     * turned into lowercase
     */
    public static String[] capMe(String[] strings) {
        return Arrays.stream(strings)
                .map(name -> {
                    if (name.isEmpty()) {
                        return "";
                    }
                    var firstLetter = name.substring(0, 1).toUpperCase(Locale.getDefault());
                    var rest = name.substring(1).toLowerCase(Locale.getDefault());
                    return firstLetter + rest;
                }).toList().toArray(new String[strings.length]);
    }
}
