package dev.bjdelacruz.katas.codewars;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * This class removes duplicate integers from an array while keeping the order of the elements in the array.
 *
 * @author BJ Peter Dela Cruz
 */
public final class RemoveDuplicates {
    /**
     * Do not instantiate this class.
     */
    private RemoveDuplicates() {
    }

    /**
     * Removes duplicate integers from the given array while keeping the order of the unique elements. A set
     * is used to remove the duplicates.
     *
     * @param integers The array of integers that contains duplicates
     * @return An array of integers with duplicates removed
     */
    public static int[] unique(int[] integers) {
        var unique = new ArrayList<Integer>();
        var set = new HashSet<Integer>();
        for (var number : integers) {
            if (!set.contains(number)) {
                unique.add(number);
                set.add(number);
            }
        }
        return unique.stream().mapToInt(i -> i).toArray();
    }
}
