package dev.bjdelacruz.katas.codewars;

import java.util.Arrays;

/**
 * This class sorts the given space-delimited list of students first by their name length in descending order and then
 * by alphabetical order from Z to A if two or more strings names the same length.
 *
 * @author BJ Peter Dela Cruz
 */
public final class LineUpStudents {
    private LineUpStudents() {
    }

    /**
     * Returns an array of student names in sorted order as described above.
     *
     * @param students A space-delimited list of student names
     * @return A sorted array of student names
     */
    public static String[] lineupStudents(String students) {
        return Arrays.stream(students.split("\\s+")).sorted((first, second) -> {
            var result = second.length() - first.length();
            return result == 0 ? second.compareTo(first) : result;
        }).toArray(String[]::new);
    }
}
