package dev.bjdelacruz.katas.codewars;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * This class generates a comma-separated list of all the elements in an array of objects.
 *
 * @author BJ Peter Dela Cruz
 */
public final class PrintingArrayElements {
    private PrintingArrayElements() {
    }

    /**
     * Returns a comma-separated list of all the elements in the given array as a string.
     *
     * @param array A list of objects of type T
     * @param <T> The type of the elements in the array
     * @return A comma-separated list of all the elements in the array
     */
    public static <T> String printArray(T[] array) {
        return Arrays.stream(array).map(Object::toString).collect(Collectors.joining(","));
    }
}
