package dev.bjdelacruz.katas.codewars;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * This class counts the number of days between two dates.
 *
 * @author BJ Peter Dela Cruz
 */
public final class DaysBetween {
    /**
     * Do not instantiate this class.
     */
    private DaysBetween() {
    }

    /**
     * Returns the number of days between two dates.
     *
     * @param year The year of the first date
     * @param month The month of the first date
     * @param day The day of the first date
     * @param year2 The year of the second date
     * @param month2 The month of the second date
     * @param day2 The day of the second date
     * @return The number of days between two dates
     */
    public static long getDaysAlive(int year, int month, int day, int year2, int month2, int day2) {
        return ChronoUnit.DAYS.between(LocalDate.of(year, month, day), LocalDate.of(year2, month2, day2));
    }
}
