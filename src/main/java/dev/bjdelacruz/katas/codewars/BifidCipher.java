package dev.bjdelacruz.katas.codewars;

import java.util.HashSet;
import java.util.Locale;

/**
 * This class uses the Bifid Cipher to either encode or decode a message. The cipher uses a 2-dimensional array of
 * characters for the encoding/decoding process. It is important to note that the letter J is stripped off the key used
 * to generate cipher key. The letter is also stripped off any messages that are to be encoded/decoded. All the letters
 * in the encoded ciphertext or decoded plaintext will be in uppercase.
 *
 * @author BJ Peter Dela Cruz
 */
public final class BifidCipher {

    /**
     * Do not instantiate this class.
     */
    private BifidCipher() {
    }

    private static final class PolybiusSquare {
        private static final String[][] MATRIX = new String[6][6];
        private static final String ALPHABET = "ABCDEFGHIKLMNOPQRSTUVWXYZ";

        /**
         * This class contains a 2-dimensional matrix that is used to encode and decode a message using the Bifid
         * Cipher. Note that the 2-dimensional matrix does not use the first row indexed at zero and the first column
         * of each row (i.e. the first element in each array is not used). The matrix does not contain the letter J.
         *
         * @param key The key used to generate the complete cipher key, which is 25 letters long and stored in a
         *            2-dimensional array
         */
        private PolybiusSquare(String key) {
            MATRIX[0] = new String[] {};
            MATRIX[1] = new String[] {"", "A", "B", "C", "D", "E"};
            MATRIX[2] = new String[] {"", "F", "G", "H", "I", "K"};
            MATRIX[3] = new String[] {"", "L", "M", "N", "O", "P"};
            MATRIX[4] = new String[] {"", "Q", "R", "S", "T", "U"};
            MATRIX[5] = new String[] {"", "V", "W", "X", "Y", "Z"};

            var row = 1;
            var column = 1;
            var newKey = key.toUpperCase(Locale.ENGLISH).replace('J', 'I') + ALPHABET;
            var uniqueChars = new HashSet<String>();
            for (Character character : newKey.toCharArray()) {
                var str = character.toString();
                if (uniqueChars.contains(str)) {
                    continue;
                }
                uniqueChars.add(str);
                MATRIX[row][column] = str;
                column++;
                if (column == 6) {
                    column = 1;
                    row++;
                }
            }
        }

        /**
         * Gets the character in the 2-dimensional matrix at the given row and column.
         *
         * @param row The row in the 2-dimensional matrix, starting at index 1
         * @param column The column in the 2-dimensional matrix, starting at index 1
         * @return The character
         */
        String getCharacterAt(int row, int column) {
            return MATRIX[row][column];
        }

        /**
         * Gets the row and column in the 2-dimensional matrix for the given character. An IllegalArgumentException is
         * thrown if the character is not found in the matrix. Note that the exception will be thrown for the letter J.
         *
         * @param character The character that is in the matrix
         * @return An array of size 2: the row and the column
         */
        int[] getRowColumnFor(char character) {
            var str = Character.toString(character).toUpperCase(Locale.ENGLISH);
            for (int row = 1; row < MATRIX.length; row++) {
                for (int column = 1; column < MATRIX[row].length; column++) {
                    if (MATRIX[row][column].equals(str)) {
                        return new int[] {row, column};
                    }
                }
            }
            throw new IllegalArgumentException("character " + character + " not found in matrix.");
        }

        /**
         * Returns a string representation of the 2-dimensional matrix of characters.
         *
         * @return The 2-dimensional matrix of characters.
         */
        String printMatrix() {
            var builder = new StringBuilder();
            for (var row : MATRIX) {
                for (var col : row) {
                    builder.append(col).append(" ");
                }
                builder.append("\n");
            }
            return builder.toString();
        }
    }

    /**
     * Generates an array of character codes that represent the given message. If encode is true, the row and column of
     * each character code are laid out from top to bottom in a 2-dimensional matrix and then written to an array from
     * left to right. For example, the character codes [1 2] and [5 2] are written to the 2-dimensional matrix as
     * [[1 5] [2 2]], and then the resulting array will be [1 5 2 2]. If encode is false, the character codes are
     * simply written directly to an array, first the row followed by the column.
     *
     * @param square The Polybius Square that contains the 2-dimensional matrix of character codes
     * @param message The message to either encode or decode
     * @param encode True if the message should be encoded, false otherwise
     * @return An array of character codes, i.e. the rows and columns of each character in the message that is found in
     * the Polybius Square
     */
    private static int[] generateCharacterCodes(PolybiusSquare square, String message, boolean encode) {
        var str = String.join("", message.replace('J', 'I').split("\\s+"));
        var indices = new int[2][];
        indices[0] = new int[str.length()];
        indices[1] = new int[str.length()];
        var codes = new int[str.length() * 2];
        if (!encode) {
            for (int idx = 0, codesIdx = 0; idx < str.length(); idx++, codesIdx += 2) {
                var rowCol = square.getRowColumnFor(str.charAt(idx));
                codes[codesIdx] = rowCol[0];
                codes[codesIdx + 1] = rowCol[1];
            }
            return codes;
        }
        for (var idx = 0; idx < str.length(); idx++) {
            var rowCol = square.getRowColumnFor(str.charAt(idx));
            indices[0][idx] = rowCol[0];
            indices[1][idx] = rowCol[1];
        }
        var idx = 0;
        for (var row = 0; row < 2; row++) {
            for (var col = 0; col < str.length(); col++, idx++) {
                codes[idx] = indices[row][col];
            }
        }
        return codes;
    }

    /**
     * Given an array of character codes, i.e. rows and indices of characters found in the given Polybius Square,
     * returns either an encoded text (ciphertext) or decoded text (plaintext).
     *
     * @param square The Polybius Square that contains the 2-dimensional matrix from which to retrieve characters using
     *               the rows and columns in codes
     * @param codes The array of character codes
     * @param encode True if the message is to be encoded, false to be decoded
     * @return A ciphertext or plaintext
     */
    private static String getStringFromCodes(PolybiusSquare square, int[] codes, boolean encode) {
        var builder = new StringBuilder();
        if (encode) {
            for (var idx = 1; idx < codes.length; idx += 2) {
                builder.append(square.getCharacterAt(codes[idx - 1], codes[idx]));
            }
        }
        else {
            for (var idx = 0; idx < codes.length / 2; idx++) {
                builder.append(square.getCharacterAt(codes[idx], codes[idx + (codes.length / 2)]));
            }
        }
        return builder.toString();
    }

    /**
     * Encodes the given message with the given key using a Bifid Cipher.
     *
     * @param key The key used to generate the complete cipher key
     * @param message The message to encode
     * @return An encoded message (ciphertext)
     */
    public static String encodeBifid(String key, String message) {
        var square = new PolybiusSquare(key);
        return getStringFromCodes(square, generateCharacterCodes(square, message, true), true);
    }

    /**
     * Decodes the given message with the given key that was encoded with a Bifid Cipher.
     *
     * @param key The key used to generate the complete cipher key
     * @param message The message to decode
     * @return A decoded message (plaintext)
     */
    public static String decodeBifid(String key, String message) {
        var square = new PolybiusSquare(key);
        return getStringFromCodes(square, generateCharacterCodes(square, message, false), false);
    }

    /**
     * Prints the contents of the Polybius Square used to encode/decode a message.
     *
     * @param key The key used to generate the complete cipher key
     * @return The contents of the Polybius Square
     */
    public static String printMatrix(String key) {
        return new PolybiusSquare(key).printMatrix();
    }
}
