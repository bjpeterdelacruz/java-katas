package dev.bjdelacruz.katas.codewars;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * This class contains methods that return sorted arrays that are copies of the original arrays.
 *
 * @author BJ Peter Dela Cruz
 */
public final class SortArrays1 {
    private SortArrays1() {
    }

    /**
     * Sorts the given array of strings. A new array is returned, and the input array is not modified.
     *
     * @param values An array of strings
     * @return A sorted array of strings
     */
    public static String[] sortArray(String[] values) {
        return Arrays.stream(values).sorted().toArray(String[]::new);
    }

    /**
     * Sorts the given array of integers. A new array is returned, and the input array is not modified.
     *
     * @param values An array of integers
     * @return A sorted array of integers
     */
    public static int[] sortArray(int[] values) {
        return Arrays.stream(values).sorted().toArray();
    }

    /**
     * Sorts the given array of longs. A new array is returned, and the input array is not modified.
     *
     * @param values An array of longs
     * @return A sorted array of longs
     */
    public static long[] sortArray(long[] values) {
        return Arrays.stream(values).sorted().toArray();
    }

    /**
     * Sorts the given array of doubles. A new array is returned, and the input array is not modified.
     *
     * @param values An array of doubles
     * @return A sorted array of doubles
     */
    public static double[] sortArray(double[] values) {
        return Arrays.stream(values).sorted().toArray();
    }

    /**
     * Sorts the given array of floats. A new array is returned, and the input array is not modified.
     *
     * @param values An array of floats
     * @return A sorted array of floats
     */
    public static float[] sortArray(float[] values) {
        var sortedValues = IntStream.range(0, values.length).mapToObj(idx -> values[idx]).sorted().toList();
        var floats = new float[values.length];
        for (var idx = 0; idx < values.length; idx++) {
            floats[idx] = sortedValues.get(idx);
        }
        return floats;
    }
}
