package dev.bjdelacruz.katas.codewars;

import java.util.ArrayList;
import java.util.List;

/**
 * This class finds all the numbers that are multiples of two numbers.
 *
 * @author BJ Peter Dela Cruz
 */
public final class MultiplesOfTwo {
    private MultiplesOfTwo() {
    }

    /**
     * Returns a list of integers that are multiples of both firstNumber and secondNumber. The list will contain
     * multiples that are less than maximum.
     *
     * @param firstNumber The first integer
     * @param secondNumber The second integer
     * @param maximum The last number in the range, exclusive
     * @return A list of integers that are multiples of both firstNumber and secondNumber
     */
    public static List<Integer> findMultiples(int firstNumber, int secondNumber, int maximum) {
        var arr = new ArrayList<Integer>();
        for (var idx = Math.max(firstNumber, secondNumber); idx < maximum; idx++) {
            if (idx % firstNumber == 0 && idx % secondNumber == 0) {
                arr.add(idx);
            }
        }
        return arr;
    }
}
