package dev.bjdelacruz.katas.codewars;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

/**
 * This class returns the name of a random color for a ghost.
 *
 * @author BJ Peter Dela Cruz
 */
public class ColorGhost {
    private static final String[] COLORS = {"white", "yellow", "purple", "red"};

    /**
     * Returns a copy of the array of four colors.
     *
     * @return An array that contains four colors.
     */
    public static String[] getColors() {
        return Arrays.stream(COLORS).toArray(String[]::new);
    }

    /**
     * Returns a random color from the array of four colors.
     *
     * @return A random color.
     */
    public String getColor() {
        return COLORS[ThreadLocalRandom.current().nextInt(0, COLORS.length)];
    }
}
