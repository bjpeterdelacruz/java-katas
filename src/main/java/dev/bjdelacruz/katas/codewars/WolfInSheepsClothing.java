package dev.bjdelacruz.katas.codewars;

import java.util.stream.IntStream;

/**
 * This class displays a message depending on the location of a wolf in an array of strings representing sheep.
 *
 * @author BJ Peter Dela Cruz
 */
public final class WolfInSheepsClothing {
    private WolfInSheepsClothing() {
    }

    /**
     * Given an array of strings representing sheep, this method finds the location of a wolf in the array. If the wolf
     * is at the back of the array, which is closest to the sheep farmer, the message "Pls go away and stop eating my
     * sheep" is returned. Otherwise, the message "Oi! Sheep number N! You are about to be eaten by a wolf!" is
     * returned, where N is the N-th sheep starting from the back of the array. It is the sheep that is to the immediate
     * right of the wolf. It is assumed that the array will always have only one wolf.
     *
     * @param array An array of strings containing sheep and one wolf
     * @return A message that the sheep farmer says depending on the location of the wolf in the array
     */
    public static String warnTheSheep(String[] array) {
        var list = IntStream.rangeClosed(1, array.length).mapToObj(idx -> array[array.length - idx]).toList();
        var wolfIndex = IntStream.range(0, array.length).filter(index -> list.get(index).equals("wolf")).findFirst();
        if (wolfIndex.getAsInt() == 0) {
            return "Pls go away and stop eating my sheep";
        }
        return "Oi! Sheep number " + wolfIndex.getAsInt() + "! You are about to be eaten by a wolf!";
    }
}
