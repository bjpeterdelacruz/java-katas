package dev.bjdelacruz.katas.codewars;

import java.util.Arrays;

/**
 * This class uses the Java 8 Stream API to count the number of alcoholic drinks that were consumed.
 *
 * @author BJ Peter Dela Cruz
 */
public final class ResponsibleDrinking {
    private ResponsibleDrinking() {
    }

    /**
     * Counts the number of alcoholic drinks that were consumed from a string such as "3 glasses of wine, 4 shots of
     * whiskey". The number of alcoholic drinks consumed is equal to the number of glasses of water that is recommended
     * so as to not experience a hangover.
     *
     * @param drinkString A string containing the number and types of alcoholic drinks consumed
     * @return The number of glasses of water that is recommended, which is equal to the number of alcoholic drinks
     */
    public static String hydrate(String drinkString) {
        var sum = Arrays.stream(drinkString.split("\\s+")).filter(str -> str.matches("[0-9]+"))
                .mapToInt(Integer::parseInt).sum();
        return sum == 1 ? "1 glass of water" : sum + " glasses of water";
    }
}
