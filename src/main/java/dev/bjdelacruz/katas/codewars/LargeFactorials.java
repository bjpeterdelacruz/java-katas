package dev.bjdelacruz.katas.codewars;

import java.util.ArrayList;

/**
 * This class calculates the factorial of a number without using {@link java.math.BigInteger}.
 *
 * @author BJ Peter Dela Cruz
 */
public final class LargeFactorials {

    /**
     * Do not instantiate this class.
     */
    private LargeFactorials() {
    }

    /**
     * This method calculates the factorial of the given number without using {@link java.math.BigInteger}.
     *
     * @param number The number for which to calculate the factorial
     * @return The result of N!
     */
    public static String factorial(int number) {
        if (number == 0) {
            return "1";
        }
        if (number < 3) {
            return String.valueOf(number);
        }
        var product = multiply(String.valueOf(number), String.valueOf(number - 1));
        for (var fact = number - 2; fact > 0; fact--) {
            product = multiply(product, String.valueOf(fact));
        }
        return product;
    }

    /**
     * This helper method either multiplies or adds two digits in the given string representation of two numbers. The
     * carry digit is also added to the result of the mathematical operation.
     *
     * @param num1 The first number
     * @param num2 The second number
     * @param idx1 The index of the digit in the first number to multiply or add
     * @param idx2 The index of the digit in the second number to multiply or add
     * @param carry The carry digit to add after the multiplication or addition is performed on the two numbers
     * @param multiply True to multiply the two numbers together, false to add
     * @return The result of the multiplication or addition of both numbers and the carry
     */
    private static int performOperation(String num1, String num2, int idx1, int idx2, int carry, boolean multiply) {
        var first = Integer.parseInt(String.valueOf(num1.charAt(idx1)));
        var second = Integer.parseInt(String.valueOf(num2.charAt(idx2)));
        return (multiply ? (first * second) : (first + second)) + carry;
    }

    /**
     * This helper method gets the carry digit from the given value, which is in the leftmost position of the number.
     *
     * @param value The number from which to get the carry digit, e.g. 23 + 8 = 31 and 1 is the carry digit
     * @return The carry digit
     */
    private static int getCarry(int value) {
        return value > 9 ? Integer.parseInt(String.valueOf(String.valueOf(value).charAt(0))) : 0;
    }

    /**
     * This helper method multiplies two numbers together without using BigInteger.
     *
     * @param firstNumber The first number
     * @param secondNumber The second number
     * @return The result of the multiplication operation
     */
    static String multiply(String firstNumber, String secondNumber) {
        var results = new ArrayList<StringBuilder>();
        var maxLength = 0;
        for (int firstIdx = firstNumber.length() - 1, carry = 0, numZeroes = 0;
             firstIdx >= 0;
             firstIdx--, numZeroes++, carry = 0) {
            var product = new StringBuilder();
            for (var secondIdx = secondNumber.length() - 1; secondIdx >= 0; secondIdx--) {
                var prod = performOperation(firstNumber, secondNumber, firstIdx, secondIdx, carry, true);
                carry = getCarry(prod);
                var resultAsString = String.valueOf(prod);
                product.insert(0, resultAsString.charAt(resultAsString.length() - 1));
            }
            product.insert(0, carry);
            product.append("0".repeat(Math.max(0, numZeroes)));
            maxLength = Math.max(maxLength, product.length());
            results.add(product);
        }
        for (var result : results) {
            for (var length = result.toString().length(); length < maxLength; length++) {
                result.insert(0, "0");
            }
        }
        var finalSum = new StringBuilder("0".repeat(maxLength));
        for (var result : results) {
            var first = result.toString();
            var carry = 0;
            var temp = new StringBuilder();
            for (var idx = first.length() - 1; idx >= 0; idx--) {
                var sum = performOperation(first, finalSum.toString(), idx, idx, carry, false);
                carry = getCarry(sum);
                var sumAsString = String.valueOf(sum);
                temp.insert(0, sumAsString.charAt(sumAsString.length() - 1));
            }
            finalSum = new StringBuilder(temp);
        }
        while (finalSum.charAt(0) == '0') {
            finalSum = new StringBuilder(finalSum.substring(1));
        }
        return finalSum.toString();
    }

}
