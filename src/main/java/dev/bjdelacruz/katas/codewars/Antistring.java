package dev.bjdelacruz.katas.codewars;

import java.util.Locale;
import java.util.stream.Collectors;

/**
 * This class manipulates a string in such a way that its characters are replaced and the string itself is reversed.
 *
 * @author BJ Peter Dela Cruz
 */
public final class Antistring {
    private Antistring() {
    }

    private static final String ALPHABET_LOWER = "abcdefghijklmnopqrstuvwxyz";
    private static final String ALPHABET_UPPER = ALPHABET_LOWER.toUpperCase(Locale.getDefault());
    private static final String ALL_LETTERS = ALPHABET_LOWER + ALPHABET_UPPER;
    private static final String REVERSED_LETTERS = new StringBuilder(ALL_LETTERS).reverse().toString();

    private static final String NUMBERS = "0123456789";
    private static final String REVERSED_NUMBERS = "9876543210";

    /**
     * Reverses the output of the string that is returned from {@link #switchAlphanumeric(String)}.
     *
     * @param str The input string
     * @return A reversed string
     */
    public static String antiString(String str) {
        return new StringBuilder(switchAlphanumeric(str)).reverse().toString();
    }

    /**
     * This helper method switches letters (a -> Z, b -> Y, c -> X ... A -> z, B -> y, C -> x, etc.) and numbers
     * (0 -> 9, 1 -> 8, 2 -> 7, etc.) in the given string.
     *
     * @param str The input string
     * @return A string whose characters had been replaced as described above
     */
    private static String switchAlphanumeric(String str) {
        return str.chars().mapToObj(i -> (char) i).map(
                c -> Character.isDigit(c) ? REVERSED_NUMBERS.charAt(NUMBERS.indexOf(c))
                        : REVERSED_LETTERS.charAt(ALL_LETTERS.indexOf(c)))
                .map(Object::toString).collect(Collectors.joining());
    }
}
