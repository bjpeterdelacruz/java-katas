package dev.bjdelacruz.katas.codewars;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * This class returns true if an array of characters contains three of one character and two of another character.
 *
 * @author BJ Peter Dela Cruz
 */
public final class CheckThreeAndTwo {
    private CheckThreeAndTwo() {
    }

    /**
     * This method returns true if the given array of characters contains three of one character and two of another
     * character. The characters themselves are irrelevant. Otherwise, false is returned.
     *
     * @param chars The input array of characters
     * @return True if the array contains three of one character and two of another character, false otherwise
     */
    public static boolean checkThreeAndTwo(char[] chars) {
        var counts = new HashMap<Character, Integer>();
        for (var c : chars) {
            var count = counts.putIfAbsent(c, 1);
            if (count != null) {
                counts.put(c, count + 1);
            }
        }
        return new HashSet<>(counts.values()).equals(new HashSet<>(List.of(2, 3)));
    }
}
