package dev.bjdelacruz.katas.codewars;

import java.util.Arrays;

/**
 * This class returns "mean" if the mean (average) of all the numbers in an array is greater than the median (the number
 * in the middle of the array after the array is sorted). If the median is greater than the mean, "median" is returned.
 * If both are equal, then "same" is returned.
 *
 * @author BJ Peter Dela Cruz
 */
public final class MeanVsMedian {

    private MeanVsMedian() {
    }

    /**
     * Returns "mean" if the mean is greater than the median, "median" if the median is greater than the mean, or "same"
     * if both are equal. Note that the array is sorted first before the median is taken. The original array is not
     * modified.
     *
     * @param numbers A sorted or unsorted array of integers
     * @return The strings "mean", "median", or "same"
     */
    public static String meanVsMedian(int[] numbers) {
        var mean = Arrays.stream(numbers).average().getAsDouble();
        var sortedArray = Arrays.stream(numbers).sorted().toArray();
        int position = numbers.length / 2;
        var median = sortedArray[position];
        return mean > median ? "mean" : (median > mean ? "median" : "same");
    }
}
