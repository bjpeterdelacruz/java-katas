package dev.bjdelacruz.katas.codewars;

import java.util.Arrays;

/**
 * This class generates a two-element array: the first element represents the count of all the positive numbers in the
 * input array, and the second element represents the sum of all the negative numbers in the same input array.
 *
 * @author BJ Peter Dela Cruz
 */
public final class PositivesCountNegativesSum {

    private PositivesCountNegativesSum() {
    }

    /**
     * Returns a two-element integer array. The first element is the count of all the positive numbers in the input
     * array. The second element is the sum of all the negative numbers in the same input array. If the input array is
     * null or empty, then an empty array is returned.
     *
     * @param input An array of integers
     * @return A two-element array that contains a count and a sum
     */
    public static int[] countPositivesSumNegatives(int[] input) {
        return input == null || input.length == 0 ? new int[] {}
                : new int[] {(int) Arrays.stream(input).filter(x -> x > 0).count(),
                        Arrays.stream(input).filter(x -> x < 0).sum()};
    }
}
