package dev.bjdelacruz.katas.codewars;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * This class returns the name of the city that the conference traveller should visit if she has never visited it
 * before.
 *
 * @author BJ Peter Dela Cruz
 */
public final class ConferenceTraveller {

    private ConferenceTraveller() {
    }

    /**
     * Given an array of cities that the conference traveller has visited before and an array of cities in which a
     * conference is going to be held, returns the name of the city that the conference traveller should visit if she
     * has not visited it before. If she has visited all the cities in which a conference is going to be held, the
     * string "No worthwhile conferences this year!" is returned.
     *
     * @param citiesVisited The array of cities that the conference traveller has visited before
     * @param citiesOffered The array of cities in which a conference is going to be held
     * @return The name of a city or "No worthwhile conferences this year!"
     */
    public static String conferencePicker(String[] citiesVisited, String[] citiesOffered) {
        var citiesVisitedSet = Arrays.stream(citiesVisited).collect(Collectors.toSet());
        var cityToVisit = Arrays.stream(citiesOffered).filter(city -> !citiesVisitedSet.contains(city)).findFirst();
        return cityToVisit.orElse("No worthwhile conferences this year!");
    }
}
