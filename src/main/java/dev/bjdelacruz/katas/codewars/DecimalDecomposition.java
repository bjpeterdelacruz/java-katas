package dev.bjdelacruz.katas.codewars;

import java.util.ArrayList;

/**
 * This class decomposes a number into a string that represents the sum of each digit's value.
 */
public final class DecimalDecomposition {
    private DecimalDecomposition() {
    }

    /**
     * This method decomposes the given number into a string representing the sum of each digit's value. For example,
     * given 213057, <code>"200000+10000+3000+50+7"</code> is returned.
     *
     * @param number The number to decompose
     * @return A string representing the sum of the value of each digit in the given number
     */
    public static String decimalDecomposition(int number) {
        var numString = String.valueOf(number);
        var strings = new ArrayList<String>();
        for (int idx = 0, numZeros = numString.length() - 1; idx < numString.length(); idx++, numZeros--) {
            if (numString.charAt(idx) != '0') {
                strings.add(numString.charAt(idx) + "0".repeat(numZeros));
            }
        }
        return String.join("+", strings);
    }
}
