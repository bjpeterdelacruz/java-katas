package dev.bjdelacruz.katas.codewars;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

/**
 * This class is an example of how to create and start threads, and how to add integers to a collection in a thread-safe
 * manner.
 *
 * @author BJ Peter Dela Cruz
 */
public final class RunRunner {
    private RunRunner() {
    }

    private static final Set<String> THREAD_NAMES = new HashSet<>();
    private static CountDownLatch latch;
    private static final RunRunner KATA = new RunRunner();

    private static final class Logan5 implements Runnable {
        public void run() {
            System.out.println("Hello from Logan5");
            KATA.method1();
            latch.countDown();
        }
    }

    private static final class Jessica6 implements Runnable {
        public void run() {
            System.out.println("Hello from Jessica6");
            KATA.method2();
            latch.countDown();
        }
    }

    private void method1() {
        synchronized (THREAD_NAMES) {
            THREAD_NAMES.add(Thread.currentThread().getName());
        }
    }

    private void method2() {
        synchronized (THREAD_NAMES) {
            THREAD_NAMES.add(Thread.currentThread().getName());
        }
    }

    /**
     * Returns a copy of the set that contains thread IDs.
     *
     * @return A set containing thread IDs
     */
    public static Set<String> getThreadNames() {
        return new HashSet<>(THREAD_NAMES);
    }

    /**
     * Creates and starts two threads, each printing a message to standard output and then adding its ID to a set.
     */
    public static void runRunners() {
        THREAD_NAMES.clear();
        latch = new CountDownLatch(2);
        new Thread(new Logan5(), "logan5-1").start();
        new Thread(new Jessica6(), "jessica6-2").start();
        try {
            latch.await();
        }
        catch (InterruptedException ie) {
            System.err.format("Current thread was interrupted while waiting.%n");
        }
    }
}
