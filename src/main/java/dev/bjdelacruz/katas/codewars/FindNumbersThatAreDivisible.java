package dev.bjdelacruz.katas.codewars;

import java.util.Arrays;

/**
 * This class finds all the numbers in an array that are divisible by a given number.
 *
 * @author BJ Peter Dela Cruz
 */
public final class FindNumbersThatAreDivisible {

    private FindNumbersThatAreDivisible() {
    }

    /**
     * Returns all numbers from the given array that are divisible by the given number.
     *
     * @param numbers The array of numbers
     * @param divider The number by which to divide each number in the array
     * @return All numbers from the array that are divisible by <code>divider</code>
     */
    public static int[] divisibleBy(int[] numbers, int divider) {
        return Arrays.stream(numbers).filter(x -> x % divider == 0).toArray();
    }
}
