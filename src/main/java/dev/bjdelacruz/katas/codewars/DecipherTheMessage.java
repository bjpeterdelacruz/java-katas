package dev.bjdelacruz.katas.codewars;

import java.util.stream.Collectors;

/**
 * This class decodes a string that was encoded using a Caesar Cipher with an alphabetic shift of seven.
 *
 * @author BJ Peter Dela Cruz
 */
public final class DecipherTheMessage {
    private static final int CAPITAL_A = 65;
    private static final int CAPITAL_Z = 90;
    private static final int LOWERCASE_A = CAPITAL_A + 32;
    private static final int LOWERCASE_Z = CAPITAL_Z + 32;
    private static final int NUMBER_OF_CHARACTERS_TO_SHIFT = 7;
    private static final int LETTERS_COUNT = 26;

    private DecipherTheMessage() {
    }

    /**
     * This method decodes the given coded message and returns the decoded message.
     *
     * @param codedMessage The encoded message to decode
     * @return The decoded message
     */
    public static String decipher(String codedMessage) {
        return codedMessage.chars().mapToObj(DecipherTheMessage::decode).collect(Collectors.joining());
    }

    /**
     * This helper method returns an alphabetic character that is seven characters to the right of the given character.
     * The method will wrap around to the beginning of the alphabet if the number of characters to the right is less
     * than seven. If the given character is not a letter from A to Z, it is simply returned.
     *
     * @param character The character
     * @return A character that is seven characters to the right of the given character, or the character itself if it
     * is not a letter from A to Z
     */
    private static String decode(int character) {
        if (character >= CAPITAL_A && character <= CAPITAL_Z) {
            return Character.toString(
                    (character - CAPITAL_A + NUMBER_OF_CHARACTERS_TO_SHIFT) % LETTERS_COUNT + CAPITAL_A);
        }
        else if (character >= LOWERCASE_A && character <= LOWERCASE_Z) {
            return Character.toString(
                    (character - LOWERCASE_A + NUMBER_OF_CHARACTERS_TO_SHIFT) % LETTERS_COUNT + LOWERCASE_A);
        }
        return Character.toString(character);
    }
}
