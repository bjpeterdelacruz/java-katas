package dev.bjdelacruz.katas.codewars;

/**
 * This class returns a message depending on the first letter of a person's name.
 *
 * @author BJ Peter Dela Cruz
 */
public final class AreYouPlayingBanjo {
    private AreYouPlayingBanjo() {
    }

    /**
     * Returns the name of the person who plays banjo if his or her name starts with a lowercase or uppercase R.
     *
     * @param name A string representing the name of the person who plays or does not play banjo.
     * @return A string that states whether the person plays banjo.
     */
    public static String areYouPlayingBanjo(String name) {
        return switch (name.substring(0, 1)) {
            case "R", "r" -> name + " plays banjo";
            default -> name + " does not play banjo";
        };
    }
}
