package dev.bjdelacruz.katas.codewars;

/**
 * This class returns the longest word from a space-separated list of words.
 *
 * @author BJ Peter Dela Cruz
 */
public final class InspiringStrings {
    private InspiringStrings() {
    }

    /**
     * Returns the longest word found in the string. If there are multiple words with the same length, the last word in
     * the string with the longest length is returned.
     *
     * @param wordString A string containing words separated by one or more spaces
     * @return The longest word in the string
     */
    public static String longestWord(String wordString) {
        var length = 0;
        var word = "";
        for (var string : wordString.split("\\s+")) {
            if (string.length() >= length) {
                length = string.length();
                word = string;
            }
        }
        return word;
    }
}
