package dev.bjdelacruz.katas.codewars;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * This class returns a string of alternating ones and zeroes.
 *
 * @author BJ Peter Dela Cruz
 */
public final class StringyStrings {

    private StringyStrings() {
    }

    /**
     * Returns a string of length <code>size</code> that begins with a one and alternates between one and zero.
     *
     * @param size The length of the string
     * @return A string that starts with a one and alternates between one and zero, e.g. "10101" and "10101010"
     */
    public static String stringy(int size) {
        return IntStream.range(0, size).mapToObj(x -> x % 2 == 0 ? "1" : "0").collect(Collectors.joining());
    }
}
