package dev.bjdelacruz.katas.codewars;

import java.util.Comparator;
import java.util.List;

/**
 * This class performs a case-insensitive sort of a list of textbook names.
 *
 * @author BJ Peter Dela Cruz
 */
public final class SortMyTextbooks {

    private SortMyTextbooks() {
    }

    /**
     * Returns a sorted list of textbook names. The original list is not modified. The sort is case-insensitive.
     *
     * @param textbooks The list of textbook names.
     * @return A sorted list of textbook names.
     */
    public static List<String> sort(List<String> textbooks) {
        return textbooks.stream().sorted(Comparator.comparing(String::toLowerCase)).toList();
    }
}
