package dev.bjdelacruz.katas.codewars;

/**
 * This class converts seconds to days, hours, minutes, and seconds.
 *
 * @author BJ Peter Dela Cruz
 */
public final class ConvertTimeToString {
    /**
     * Do not instantiate this class.
     */
    private ConvertTimeToString() {
    }

    /**
     * Converts the given number of seconds to days, hours, minutes, and seconds. For example,
     * given 90061, the string "1 1 1 1" will be returned.
     *
     * @param timeDiff The number of seconds
     * @return A string containing the number of days, hours, minutes, and seconds
     */
    public static String convertTime(int timeDiff) {
        var days = timeDiff / (60 * 60 * 24);
        var hours = timeDiff / (60 * 60) % 24;
        var minutes = timeDiff / 60 % 60;
        var seconds = timeDiff % 60;
        return days + " " + hours + " " + minutes + " " + seconds;
    }
}
