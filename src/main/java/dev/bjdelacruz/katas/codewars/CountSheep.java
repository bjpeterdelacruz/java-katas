package dev.bjdelacruz.katas.codewars;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * This class generates a string that someone would say when trying to sleep, that is, counting sheep.
 *
 * @author BJ Peter Dela Cruz
 */
public final class CountSheep {
    private CountSheep() {
    }

    /**
     * Counts sheep from 1 to <code>num</code> by generating a string with the following pattern:
     * "1 sheep...2 sheep...3 sheep...<code>num</code> sheep".
     *
     * @param num The number of sheep
     * @return A string with the number of sheep from 1 to <code>num</code>
     */
    public static String countingSheep(int num) {
        return IntStream.rangeClosed(1, num).mapToObj(number -> number + " sheep...").collect(Collectors.joining());
    }
}
