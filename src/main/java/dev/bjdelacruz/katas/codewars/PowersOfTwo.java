package dev.bjdelacruz.katas.codewars;

/**
 * This class generates an array that contains all the powers of two, e.g. [1, 2, 4, 8, 16].
 *
 * @author BJ Peter Dela Cruz
 */
public final class PowersOfTwo {
    private PowersOfTwo() {
    }

    /**
     * Returns an array that contains all the powers of two. The range of the exponent is from 0 to
     * <code>exponent</code>, inclusive.
     *
     * @param exponent The exponent for the last power of two in the array that is returned
     * @return An array containing all the powers of two from 0 to <code>exponent</code>, inclusive
     */
    public static long[] powersOfTwo(int exponent) {
        var results = new long[exponent + 1];
        results[0] = 1;
        for (var i = 1; i < exponent + 1; i++) {
            results[i] = 2 * results[i - 1];
        }
        return results;
    }
}
