package dev.bjdelacruz.katas.codewars;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class determines who the winner is after one trick (or round) in a game of cards. When Player 1 plays a card,
 * all the other players must follow suit. If all the other plays do not follow suit, then Player 1 wins. If other
 * players follow suit, then the player with the highest rank in the suit wins. For example, Player 1 plays a QC
 * (Queen of Clubs), and Player 2 plays an AC (Ace of Clubs), so Player 2 wins the trick.
 *
 * @author BJ Peter Dela Cruz
 */
public final class Yacg2 {

    /**
     * Do not instantiate this class.
     */
    private Yacg2() {
    }

    private static final Map<Character, Integer> RANKS = new HashMap<>();
    static {
        RANKS.put('A', 14);
        RANKS.put('K', 13);
        RANKS.put('Q', 12);
        RANKS.put('J', 11);
        RANKS.put('T', 10);
        RANKS.put('9', 9);
        RANKS.put('8', 8);
        RANKS.put('7', 7);
        RANKS.put('6', 6);
        RANKS.put('5', 5);
        RANKS.put('4', 4);
        RANKS.put('3', 3);
        RANKS.put('2', 2);
    }

    /**
     * Returns the highest card between two cards. The cards are assumed to be in the same suit.
     *
     * @param card1 The first card
     * @param card2 The second card
     * @return The highest card
     */
    private static String getHighestCard(String card1, String card2) {
        var first = RANKS.get(card1.charAt(0));
        var second = RANKS.get(card2.charAt(0));
        return first > second ? card1 : card2;
    }

    /**
     * Determines the player who won a trick. First, all cards that do not follow the first player's suit are filtered
     * out. Then the highest card between the remaining cards is determined. The index of the highest card is also the
     * index of the player, so it is assumed that the cards and players arrays are the same length.
     *
     * @param cards The array of cards
     * @param players The array of players
     * @return The name of the player who won the trick
     */
    public static String winnerOfTrick(String[] cards, String[] players) {
        var card = cards[0];
        var suit = String.valueOf(card.charAt(1));
        List<String> remaining = Arrays.stream(cards).filter(c -> c.endsWith(suit)).toList();
        for (String currentCard : remaining) {
            card = getHighestCard(currentCard, card);
        }
        var idx = Arrays.asList(cards).indexOf(card);
        return players[idx] + " wins";
    }
}
