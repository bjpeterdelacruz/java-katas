package dev.bjdelacruz.katas.codewars;

import java.time.Duration;
import java.time.LocalDateTime;

/**
 * This class converts hours to seconds using the LocalDateTime and Duration classes.
 *
 * @author BJ Peter Dela Cruz
 */
public final class HoursToSeconds {
    private HoursToSeconds() {
    }

    /**
     * Converts hours to seconds using methods from the LocalDateTime and Duration classes.
     *
     * @param hours The number of hours
     * @return The number of second
     */
    public static long hoursToSeconds(final int hours) {
        var now = LocalDateTime.now();
        var hoursFromNow = now.plusHours(hours);
        return Duration.between(now, hoursFromNow).getSeconds();
    }
}
