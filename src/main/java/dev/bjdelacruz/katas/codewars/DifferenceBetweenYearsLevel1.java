package dev.bjdelacruz.katas.codewars;

/**
 * This class calculates the difference in years between two dates.
 *
 * @author BJ Peter Dela Cruz
 */
public final class DifferenceBetweenYearsLevel1 {
    private DifferenceBetweenYearsLevel1() {
    }

    /**
     * This method receives two date strings as input and returns the difference in years. The format of the two dates
     * is YYYY/MM/DD.
     *
     * @param date1 The first date string
     * @param date2 The second date string
     * @return The difference in years
     */
    public static int howManyYears(String date1, String date2) {
        var year1 = Integer.parseInt(date1.substring(0, date1.indexOf('/')));
        var year2 = Integer.parseInt(date2.substring(0, date2.indexOf('/')));
        return Math.abs(year1 - year2);
    }
}
