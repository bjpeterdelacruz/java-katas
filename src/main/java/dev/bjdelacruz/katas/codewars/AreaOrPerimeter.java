package dev.bjdelacruz.katas.codewars;

/**
 * This class calculates the area or perimeter depending on whether the sides are equal.
 *
 * @author BJ Peter Dela Cruz
 */
public final class AreaOrPerimeter {
    private AreaOrPerimeter() {
    }

    /**
     * Calculates the area if both the length and width are the same, otherwise calculates the perimeter.
     *
     * @param length The length
     * @param width The width
     * @return The area if both the length and width are equal, otherwise the perimeter
     */
    public static int areaOrPerimeter(int length, int width) {
        return length == width ? length * width : length + width + length + width;
    }
}
