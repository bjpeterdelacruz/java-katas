package dev.bjdelacruz.katas.codewars;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * This class uses the Java 8 Stream API to combine all the strings in an array into one string.
 *
 * @author BJ Peter Dela Cruz
 */
public final class SentenceSmash {
    private SentenceSmash() {
    }

    /**
     * Combines all the strings in the given array into one string, each separated by a space character.
     *
     * @param words An array of strings
     * @return A string that contains all the strings from <code>words</code>, each separated by a space
     */
    public static String smash(String... words) {
        return Arrays.stream(words).map(String::trim).collect(Collectors.joining(" "));
    }
}
