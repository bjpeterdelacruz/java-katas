package dev.bjdelacruz.katas.codewars;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * This class sorts the column names in a string that represents a CSV file. The sort is alphabetical and
 * case-insensitive.
 *
 * @author BJ Peter Dela Cruz
 */
public final class SortCsvFileColumns {

    /**
     * Do not instantiate this class.
     */
    private SortCsvFileColumns() {
    }

    /**
     * Helper class that contains data that belongs under a particular column.
     */
    private static final class ColumnData {
        private final String columnName;
        private final String data;

        private ColumnData(String columnName, String data) {
            this.columnName = columnName;
            this.data = data;
        }

        public String getColumnName() {
            return columnName;
        }

        public String getData() {
            return data;
        }
    }

    /**
     * Sorts the column headers in the given string that represents a CSV file. The data below the column headers are
     * guaranteed to be in the correct columns. The first line in the string contains the column headers.
     *
     * @param csvFileContent The input string that represents a CSV file.
     * @return The same CSV file but with the column headers sorted alphabetically
     */
    public static String sortCsvColumns(String csvFileContent) {
        var lines = csvFileContent.split("\n");
        var columnNames = lines[0].split(";");
        var sortedColumnNames = Arrays.stream(columnNames)
                .sorted(Comparator.comparing(str -> str.toLowerCase(Locale.ENGLISH)))
                .collect(Collectors.joining(";"));
        var sortedData = new ArrayList<String>();
        for (var lineIdx = 1; lineIdx < lines.length; lineIdx++) {
            var data = lines[lineIdx].split(";");
            var row = new ArrayList<ColumnData>();
            for (var colIdx = 0; colIdx < data.length; colIdx++) {
                row.add(new ColumnData(columnNames[colIdx], data[colIdx]));
            }
            sortedData.add(row.stream()
                    .sorted(Comparator.comparing(columnData -> columnData.getColumnName().toLowerCase(Locale.ENGLISH)))
                    .map(ColumnData::getData).collect(Collectors.joining(";")));
        }
        return String.format("%s%n%s", sortedColumnNames, String.join("\n", sortedData));
    }

}
