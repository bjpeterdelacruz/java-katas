package dev.bjdelacruz.katas.codewars;

import java.util.HashMap;

/**
 * This class counts each integer in an array and returns the highest count.
 *
 * @author BJ Peter Dela Cruz
 */
public final class MostFrequentItemCount {
    private MostFrequentItemCount() {
    }

    /**
     * Counts the number of times an integer appears in the given array and returns the highest count.
     *
     * @param collection An array of integers
     * @return The highest count
     */
    public static int mostFrequentItemCount(int[] collection) {
        var counts = new HashMap<Integer, Integer>();
        for (var number : collection) {
            var count = counts.putIfAbsent(number, 1);
            if (count != null) {
                counts.put(number, count + 1);
            }
        }
        return counts.values().stream().max(Integer::compare).orElse(0);
    }
}
