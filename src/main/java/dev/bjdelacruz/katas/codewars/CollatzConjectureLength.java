package dev.bjdelacruz.katas.codewars;

/**
 * This class calculates a collatz sequence by repeatedly applying the following formula to a positive integer until the
 * result is equal to 1: if the number is even, then divide it by 2; if the number is odd, then multiply the number by 3
 * and then add 1.
 *
 * @author BJ Peter Dela Cruz
 */
public final class CollatzConjectureLength {

    /**
     * Do not instantiate this class.
     */
    private CollatzConjectureLength() {
    }

    /**
     * Returns the length of a collatz sequence. For example, given 20, 8 is returned because the collatz sequence is
     * [20, 10, 5, 16, 8, 4, 2, 1].
     *
     * @param number A positive integer greater than 0
     * @return A collatz sequence
     */
    public static long conjecture(long number) {
        return Collatz.calculateCollatzSequence(number).size();
    }
}
