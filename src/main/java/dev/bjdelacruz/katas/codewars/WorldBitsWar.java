package dev.bjdelacruz.katas.codewars;

import java.util.Arrays;

/**
 * This class counts the total number of bits that are turned on in a group of even numbers and a group of odd numbers.
 *
 * @author BJ Peter Dela Cruz
 */
public final class WorldBitsWar {
    private WorldBitsWar() {
    }

    /**
     * This method separates all the integers in the given array into two groups: one containing even numbers, and
     * another containing odd numbers. Each integer in both groups is converted to a binary string, and all the ones in
     * the binary string are counted. The group with the greatest number of bits that are turned on (i.e., the greatest
     * number of ones) is considered the winner. Otherwise, it is a tie.
     *
     * @param numbers An array of even and odd integers
     * @return One of three strings: "evens win", "odds win", or "tie"
     */
    public static String bitsWar(int[] numbers) {
        var evens = Arrays.stream(numbers).filter(number -> Math.abs(number) % 2 == 0)
                .map(WorldBitsWar::countOnes).sum();
        var odds = Arrays.stream(numbers).filter(number -> Math.abs(number) % 2 == 1)
                .map(WorldBitsWar::countOnes).sum();
        return evens > odds ? "evens win" : (odds > evens ? "odds win" : "tie");
    }

    /**
     * A helper function that counts the number of ones in the binary representation of the given integer.
     *
     * @param number An integer
     * @return The number of ones in the given integer
     */
    private static int countOnes(int number) {
        var binaryString = Integer.toBinaryString(Math.abs(number));
        var count = (int) binaryString.chars().filter(digit -> "1".equals(Character.toString((char) digit))).count();
        return number < 0 ? count * -1 : count;
    }
}
