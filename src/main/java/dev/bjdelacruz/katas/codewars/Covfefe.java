package dev.bjdelacruz.katas.codewars;

/**
 * This class replaces all instnaces of the word "coverage" with "covfefe".
 *
 * @author BJ Peter Dela Cruz
 */
public final class Covfefe {
    private Covfefe() {
    }

    /**
     * Replaces all instances of the word "coverage" with "covfefe". If "coverage" is not found in the string, a space
     * and "covfefe" are appended to the end of the string.
     *
     * @param tweet The input string, may contain one or more instances of "coverage"
     * @return A string in which all instances of "coverage" are replaced with "covfefe"
     */
    public static String covfefe(String tweet) {
        if (!tweet.contains("coverage")) {
            return tweet + " covfefe";
        }
        while (tweet.contains("coverage")) {
            tweet = tweet.replace("coverage", "covfefe");
        }
        return tweet;
    }
}
