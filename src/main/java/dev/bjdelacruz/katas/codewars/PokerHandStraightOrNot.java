package dev.bjdelacruz.katas.codewars;

import java.util.List;
import java.util.stream.IntStream;

/**
 * This class determines whether a given list of cards represents a straight in the game of poker.
 *
 * @author BJ Peter Dela Cruz
 */
public final class PokerHandStraightOrNot {
    private PokerHandStraightOrNot() {
    }

    /**
     * Returns true if the given list of cards represents a straight in the game of poker or false otherwise. An ace has
     * the value of 1 or 14, so the following hands are considered straights: [14, 2, 3, 4, 5] and [10, 11, 12, 13, 14].
     *
     * @param cards A list of cards from 2 to 14
     * @return True if the hand represents a straight or false otherwise
     */
    public static boolean isStraight(List<Integer> cards) {
        if (cards.containsAll(List.of(14, 2, 3, 4, 5))) {
            return true;
        }
        for (var firstCard = 2; firstCard <= 10; firstCard++) {
            var hand = IntStream.range(firstCard, firstCard + 5).boxed().toList();
            if (cards.containsAll(hand)) {
                return true;
            }
        }
        return false;
    }
}
