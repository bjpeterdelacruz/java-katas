package dev.bjdelacruz.katas.codewars;

import java.util.concurrent.CountDownLatch;

/**
 * This class executes a Runnable object in separate threads.
 *
 * @author BJ Peter Dela Cruz
 */
public final class ExecuteMe {
    private ExecuteMe() {
    }

    /**
     * This method creates and starts a number of threads, each executing the given Runnable object. The number of
     * threads is equal to the integer passed to the method.
     *
     * @param action The Runnable to execute in each thread
     * @param nTimes The number of threads to create and start
     */
    public static void execute(Runnable action, int nTimes) {
        var latch = new CountDownLatch(nTimes);
        for (var i = 0; i < nTimes; i++) {
            new Thread(action) {
                @Override
                public void run() {
                    action.run();
                    latch.countDown();
                }
            }.start();
        }
        try {
            latch.await();
        }
        catch (InterruptedException ie) {
            System.err.format("Current thread was interrupted while waiting.%n");
        }
    }
}
