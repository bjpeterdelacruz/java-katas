package dev.bjdelacruz.katas.codewars.student;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This class groups student names by department using the Java 8 stream API.
 */
public final class StudentNamesByDept {
    /**
     * Do not instantiate this class.
     */
    private StudentNamesByDept() {
    }

    /**
     * Groups student names by department.
     *
     * @param students A stream of Student objects
     * @return A map of student names grouped by department
     */
    public static Map<String, List<String>> getStudentNamesByDepartment(Stream<Student> students) {
        return students.collect(Collectors.groupingBy(Student::department,
                Collectors.mapping(Student::name, Collectors.toList())));
    }
}
