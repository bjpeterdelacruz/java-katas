/**
 * Contains four katas related to using the Java 8 stream API.
 */
package dev.bjdelacruz.katas.codewars.student;
