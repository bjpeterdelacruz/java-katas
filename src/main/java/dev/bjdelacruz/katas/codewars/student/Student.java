package dev.bjdelacruz.katas.codewars.student;

/**
 * This record represents a student with a name, his or her final grade, the department to which he or she belongs, and
 * his or her gender.
 *
 * @param name The name of a student
 * @param grade The student's final grade
 * @param department The department to which the student belongs
 * @param gender The student's gender
 */
public record Student(String name, double grade, String department, Gender gender) {

    /**
     * The gender of a student, either male or female.
     */
    public enum Gender {
        /**
         * Represents a male student.
         */
        MALE,
        /**
         * Represents a female student.
         */
        FEMALE
    }
}
