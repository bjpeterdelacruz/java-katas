package dev.bjdelacruz.katas.codewars.student;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This class groups the number of students by gender for each department using the Java 8 stream API.
 */
public final class NumStudentsByGenderByDept {
    /**
     * Do not instantiate this class.
     */
    private NumStudentsByGenderByDept() {
    }

    /**
     * Groups the number of students by gender for each department.
     *
     * @param students A stream of Student objects
     * @return A map containing the number of students by gender for each department
     */
    public static Map<String, Map<Student.Gender, Long>> getNumStudentsByGenderByDepartment(Stream<Student> students) {
        return students.collect(Collectors.groupingBy(Student::department,
                Collectors.groupingBy(Student::gender, Collectors.counting())));
    }
}
