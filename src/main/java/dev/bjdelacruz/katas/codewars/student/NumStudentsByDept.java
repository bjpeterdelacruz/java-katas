package dev.bjdelacruz.katas.codewars.student;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This class groups the number of students by department using the Java 8 stream API.
 */
public final class NumStudentsByDept {
    /**
     * Do not instantiate this class.
     */
    private NumStudentsByDept() {
    }

    /**
     * Groups the number of students by department.
     *
     * @param students A stream of Student objects
     * @return A map containing the number of students by department
     */
    public static Map<String, Long> getNumberOfStudentsByDepartment(Stream<Student> students) {
        return students.collect(Collectors.groupingBy(Student::department, Collectors.counting()));
    }
}
