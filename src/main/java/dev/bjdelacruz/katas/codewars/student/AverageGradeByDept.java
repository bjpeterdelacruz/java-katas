package dev.bjdelacruz.katas.codewars.student;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This class groups the average grade by department using the Java 8 stream API.
 */
public final class AverageGradeByDept {
    /**
     * Do not instantiate this class.
     */
    private AverageGradeByDept() {
    }

    /**
     * Groups the average grade by department.
     *
     * @param students A stream of Student objects
     * @return A map containing the average grade for all students in each department
     */
    public static Map<String, Double> getAverageGradeByDepartment(Stream<Student> students) {
        return students.collect(Collectors.groupingBy(Student::department, Collectors.averagingDouble(Student::grade)));
    }
}
