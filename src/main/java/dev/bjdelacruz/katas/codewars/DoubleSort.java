package dev.bjdelacruz.katas.codewars;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * This class sorts an array of objects that may contain only numbers, only strings, or a combination of both.
 *
 * @author BJ Peter Dela Cruz
 */
public final class DoubleSort {
    private DoubleSort() {
    }

    /**
     * Sorts the given list of objects first by numbers in ascending order and then by strings in alphabetical order.
     * Numbers that are strings are treated as strings.
     *
     * @param array The array of objects to sort, may contain only numbers, only strings, or a combination of both
     * @return The array in sorted order as described above
     */
    public static Object[] dbSort(Object[] array) {
        return Arrays.stream(array).sorted((Object first, Object second) -> {
            if (first instanceof String && second instanceof Number) {
                return 1;
            }
            if (first instanceof Number && second instanceof String) {
                return -1;
            }
            if (first instanceof Number) {
                return new BigDecimal(String.valueOf(first)).compareTo(new BigDecimal(String.valueOf(second)));
            }
            return first.toString().compareTo(second.toString());
        }).toArray();
    }
}
