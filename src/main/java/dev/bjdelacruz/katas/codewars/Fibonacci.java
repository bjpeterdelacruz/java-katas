package dev.bjdelacruz.katas.codewars;

/**
 * This class contains a method called {@link #fib(int)} that will return the n-th Fibonacci number without using any
 * data structures.
 *
 * @author BJ Peter Dela Cruz
 */
public final class Fibonacci {
    private Fibonacci() {
    }

    /**
     * This method returns the n-th Fibonacci number without using recursion or any data structures.
     *
     * @param nth The n-th Fibonacci number to return, starting from 1
     * @return The n-th Fibonacci number in the sequence
     */
    public static long fib(int nth) {
        int sum = 1;
        for (int current = 1, next = 1, index = 3; index <= nth; current = next, next = sum, index++) {
            sum = current + next;
        }
        return sum;
    }
}
