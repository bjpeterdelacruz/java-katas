package dev.bjdelacruz.katas.codewars;

/**
 * This class returns a number without using any if statements, switch statements, or ternary operators.
 *
 * @author BJ Peter Dela Cruz
 */
@SuppressWarnings("PMD.AvoidBranchingStatementAsLastInLoop")
public final class FourSeven {
    private FourSeven() {
    }

    /**
     * Returns 7 if number is 4, 4 if number is 4, or 0 if number is anything else. No if statements, switch statements,
     * or ternary operators are used.
     *
     * @param number The input number
     * @return 7 if number is 4, 4 if number is 7, 0 if number is anything else
     */
    public static int fourSeven(int number) {
        while (number == 4) {
            return 7;
        }
        while (number == 7) {
            return 4;
        }
        return 0;
    }
}
