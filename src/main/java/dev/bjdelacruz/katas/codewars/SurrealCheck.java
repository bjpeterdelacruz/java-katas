package dev.bjdelacruz.katas.codewars;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class determines if two lists of integers fit the definition of a surreal number in terms of a form.
 *
 * @author BJ Peter Dela Cruz
 */
public class SurrealCheck {

    /**
     * Returns true if the form is a surreal number. A form consists of two lists, left and right, and a form is a
     * surreal number if the intersection of left and right is empty and all the numbers in left are less than each
     * number in right. The numbers in left and right may be whole numbers (e.g. 5, 7, 11) or ratios (e.g. 200/3, 1/8).
     * If either left or right is an empty list, true will be returned.
     *
     * @param left The first space-delimited list of numbers
     * @param right The second space-delimited list of numbers
     * @return True if the form is a surreal number, or false otherwise
     */
    public boolean isSurreal(String left, String right) {
        if (left == null || left.isEmpty() || right == null || right.isEmpty()) {
            return true;
        }
        var leftNumbers = convertStringToList(left);
        var rightNumbers = convertStringToList(right);
        if (leftNumbers.isEmpty() || rightNumbers.isEmpty()) {
            return true;
        }
        var intersection = leftNumbers.stream().distinct().filter(rightNumbers::contains)
                .collect(Collectors.toSet());
        return intersection.isEmpty() && leftNumbers.get(leftNumbers.size() - 1) < rightNumbers.getFirst();
    }

    /**
     * Helper method that will split the string into a list of numbers and return a sorted list. If ratios are present
     * in the string (e.g. 200/3, 1/8), they will be converted to decimal format (e.g. 66.666666666666667, 0.125).
     *
     * @param string A space-delimited list of numbers
     * @return A sorted list of numbers with each ratio converted to a decimal
     */
    private static List<Double> convertStringToList(String string) {
        var strings = Arrays.stream(string.split("\\s+")).toList();
        var doubles = new ArrayList<Double>();
        for (var num : strings) {
            if (num.contains("/")) {
                var fraction = num.split("/");
                var numerator = Double.parseDouble(fraction[0]);
                var denominator = Double.parseDouble(fraction[1]);
                doubles.add(numerator / denominator);
            }
            else {
                doubles.add(Double.parseDouble(num));
            }
        }
        return doubles.stream().sorted().toList();
    }

}
