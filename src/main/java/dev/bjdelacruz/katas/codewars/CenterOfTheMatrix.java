package dev.bjdelacruz.katas.codewars;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * This class finds the integer that is at the center of a matrix.
 *
 * @author BJ Peter Dela Cruz
 */
public final class CenterOfTheMatrix {
    private CenterOfTheMatrix() {
    }

    /**
     * This method returns the integer that is at the center of a square matrix, i.e. 2-dimensional array. A matrix will
     * have a center if the length of array is odd, and the length of each subarray is also odd. If the matrix does not
     * have an integer that is at the center, null is returned.
     *
     * @param matrix A 2-dimensional array of integers
     * @return An integer, or null if there is no integer at the center of the matrix
     */
    public static Integer center(int[][] matrix) {
        if (matrix == null || matrix.length % 2 == 0) {
            return null;
        }
        Set<Integer> lengths = Arrays.stream(matrix).map(row -> row.length).collect(Collectors.toSet());
        if (lengths.size() > 1 || matrix[0].length % 2 == 0) {
            return null;
        }
        int middleRow = (matrix.length - 1) / 2;
        int middleColumn = (matrix[0].length - 1) / 2;
        return matrix[middleRow][middleColumn];
    }
}
