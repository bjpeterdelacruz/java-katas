package dev.bjdelacruz.katas.codewars;

/**
 * This class implements a simple interpreter for the esoteric language HQ9+.
 */
public final class HQ9Plus {
    private HQ9Plus() {
    }

    /**
     * This method returns "Hello World!" if the character is 'H'. If the character is 'Q', the character is returned as
     * a string. If the character is '9', the full lyrics of <u>99 Bottles of Beer</u> is returned. If the character is
     * none of the three, null is returned.
     *
     * @param code A character
     * @return A string or null as described above
     */
    public static String hq9Plus(char code) {
        switch (code) {
            case 'H':
                return "Hello World!";
            case 'Q':
                return Character.toString(code);
            case '9':
                var buffer = new StringBuilder();
                for (var beer = 99; beer > 0; beer--) {
                    if (beer > 1) {
                        var line = String.format("%d bottles of beer on the wall, %d bottles of beer.%n", beer, beer);
                        buffer.append(line);
                    }
                    else {
                        buffer.append("1 bottle of beer on the wall, 1 bottle of beer.\n");
                    }
                    buffer.append("Take one down and pass it around, ");
                    switch (beer) {
                        case 2 -> buffer.append("1 bottle of beer on the wall.\n");
                        case 1 -> buffer.append("no more bottles of beer on the wall.\n");
                        default -> buffer.append(String.format("%d bottles of beer on the wall.%n", beer - 1));
                    }
                }
                buffer.append("No more bottles of beer on the wall, no more bottles of beer.\n");
                buffer.append("Go to the store and buy some more, 99 bottles of beer on the wall.");
                return buffer.toString();
            default:
                return null;
        }
    }
}
