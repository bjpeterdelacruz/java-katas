package dev.bjdelacruz.katas.codewars.functionalprogramming;

import java.util.function.IntFunction;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;

/**
 * This class demonstrates the use of Java 8 lambda functions to create a dragon's curve, which is a fractal in
 * mathematics.
 *
 * @author BJ Peter Dela Cruz
 */
public final class DragonsCurve {
    private DragonsCurve() {
    }

    private static final IntFunction<String> MAP_FUNCTION = character -> switch (character) {
        case 'a' -> "aRbFR";
        case 'b' -> "LFaLb";
        default -> Character.toString(character);
    };

    /**
     * This method returns a string that represents a dragon's curve, which is a fractal that is created by first
     * starting with the string "Fa" and then replacing all the characters "a" with the string "aRbFR" and all the
     * characters "b" with the string "LFaLb". This process is repeated N times.
     *
     * @param n The number of times to repeat the process of replacing the characters "a" and "b"
     * @return A string representation of a dragon's curve, which is a fractal
     */
    public static String createCurve(int n) {
        var curve = "Fa";
        for (var times = 0; times < n; times++) {
            curve = curve.chars().mapToObj(MAP_FUNCTION).collect(Collectors.joining());
        }
        return curve.chars().filter(createFilter('a', false).and(createFilter('b', false)))
                .mapToObj(Character::toString).collect(Collectors.joining());
    }

    /**
     * This method returns the number of times the given character appears in a curve.
     *
     * @param c The character to count in the curve
     * @param curve A curve
     * @return The number of times the given character appears in the curve
     */
    public static long howMany(char c, String curve) {
        return curve.chars().filter(createFilter(c, true)).count();
    }

    /**
     * This method creates a predicate that will either filter out or keep all instances of the given character.
     *
     * @param filterWhat The character to either filter out or keep
     * @param keep True to keep the character, false to filter it out
     * @return An {@link java.util.function.IntPredicate} that will either filter out or keep characters
     */
    public static IntPredicate createFilter(char filterWhat, boolean keep) {
        return keep ? character -> character == filterWhat : character -> character != filterWhat;
    }
}
