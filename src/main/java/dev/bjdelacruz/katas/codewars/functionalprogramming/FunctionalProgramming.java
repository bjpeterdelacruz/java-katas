package dev.bjdelacruz.katas.codewars.functionalprogramming;

import java.util.function.Function;

/**
 * This class demonstrates the use of the Function class, which was introduced in Java 8.
 *
 * @author BJ Peter Dela Cruz
 */
public final class FunctionalProgramming {
    private FunctionalProgramming() {
    }

    /**
     * This function returns true if a student's name is equal to "John Smith" and the student's number is equal to
     * "js123".
     */
    public static final Function<Student, Boolean> FUNCTION =
            student -> "John Smith".equals(student.getFullName()) && "js123".equals(student.studentNumber());
}
