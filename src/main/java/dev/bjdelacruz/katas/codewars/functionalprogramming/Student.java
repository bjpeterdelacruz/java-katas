package dev.bjdelacruz.katas.codewars.functionalprogramming;

/**
 * This class represents a student with a first name, last name, and ID number.
 *
 * @author BJ Peter Dela Cruz
 * @param firstName A student's first name
 * @param lastName A student's last name
 * @param studentNumber A student's ID number
 */
public record Student(String firstName, String lastName, String studentNumber) {
    /**
     * Returns a student's full name, which is the concatenation of the student's first and last names.
     *
     * @return A student's full name
     */
    public String getFullName() {
        return firstName + " " + lastName;
    }
}
