package dev.bjdelacruz.katas.codewars.functionalprogramming;

import java.util.function.IntUnaryOperator;

/**
 * This class demonstrates a closure that sums two numbers together.
 *
 * @author BJ Peter Dela Cruz
 */
public final class AdderFactory {
    private AdderFactory() {
    }

    /**
     * Returns a function that will add the given number to a number that will be passed to the function.
     *
     * @param addTo The number to add to the number that is passed to the function that is returned by this method
     * @return A function that will return the result of adding two numbers together
     */
    public static IntUnaryOperator create(int addTo) {
        return x -> x + addTo;
    }
}
