package dev.bjdelacruz.katas.codewars;

import java.util.stream.Collectors;

/**
 * This class returns the first character that is not repeated (i.e. appears more than once) in a string.
 *
 * @author BJ Peter Dela Cruz
 */
public final class FirstNonRepeatedCharacter {

    private FirstNonRepeatedCharacter() {
    }

    /**
     * Returns the first character in the given string that is not repeated. First, a map of all the times that a
     * character appears in a string is generated. Then each character is iterated over the string from the beginning,
     * and if the count for a character is one, then that character is returned. If all the characters in the string
     * appear more than once, then <code>null</code> is returned.
     *
     * @param source The input string, may or may not contain a character that appears only once
     * @return The character that appears only once, or <code>null</code> if all characters in the string appear more
     * than once
     */
    public static Character firstNonRepeated(String source) {
        var result = source.chars().mapToObj(Character::toString)
                .collect(Collectors.groupingBy(String::toString, Collectors.counting()));
        var found = source.chars().mapToObj(Character::toString)
                .filter(x -> result.get(x) == 1).findFirst();
        return found.isPresent() ? found.get().charAt(0) : null;
    }
}
