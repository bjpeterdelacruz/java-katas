package dev.bjdelacruz.katas.codewars;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * This class provides a working method that adds one hundred to a number.
 *
 * @author BJ Peter Dela Cruz
 */
public final class StaticElectrickery {
    /** One create one instance of this class. */
    public static final StaticElectrickery INST = new StaticElectrickery();

    private static final int ONE_HUNDRED = 100;

    private static int value = ONE_HUNDRED;

    @SuppressWarnings("PMD.AssignmentToNonFinalStatic")
    @SuppressFBWarnings("ST_WRITE_TO_STATIC_FROM_INSTANCE_METHOD")
    private StaticElectrickery() {
        value = ONE_HUNDRED;
    }

    /**
     * Adds one hundred to the given integer and returns the result.
     *
     * @param n The number to add
     * @return The sum
     */
    public int plus100(int n) {
        return value + n;
    }
}
