package dev.bjdelacruz.katas.codewars;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * This class masks out a password if it is found in a JDBC connection string.
 *
 * @author BJ Peter Dela Cruz
 */
public final class HidePasswordFromJdbcUrl {
    private HidePasswordFromJdbcUrl() {
    }

    /**
     * Replaces the value of the "password" query parameter in the given JDBC connection string with asterisks.
     *
     * @param urlString A JDBC connection string
     * @return The same JDBC connection string
     */
    public static String hidePasswordFromConnection(String urlString) {
        var strings = urlString.split("\\?");
        var parameters = new ArrayList<String>();
        Arrays.stream(strings).forEach(string -> {
            if (string.contains("&")) {
                var parameter = string.split("&");
                for (var param : parameter) {
                    if (param.contains("password")) {
                        var password = param.split("=");
                        var result = "password=" + "*".repeat(password[1].length());
                        parameters.add(result);
                    }
                    else {
                        parameters.add(param);
                    }
                }
            }
        });
        return strings[0] + "?" + String.join("&", parameters);
    }
}
