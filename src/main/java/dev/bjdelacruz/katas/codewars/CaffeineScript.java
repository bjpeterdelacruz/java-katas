package dev.bjdelacruz.katas.codewars;

/**
 * This class returns a string based on some characteristics of an integer.
 *
 * @author BJ Peter Dela Cruz
 */
public final class CaffeineScript {
    private CaffeineScript() {
    }

    /**
     * This method returns "CoffeeScript" if the given number is a multiple of both 3 and 4. If the number is a multiple
     * of 3 and is even, "JavaScript" is returned. If the number is a multiple of 3 and is odd, "Java" is returned.
     * Otherwise, "mocha_missing!" is returned.
     *
     * @param n The input integer
     * @return A string: "CoffeeScript", "JavaScript", "Java", or "mocha_missing!"
     */
    public static String caffeineBuzz(int n) {
        if (n % 12 == 0) {
            return "CoffeeScript";
        }
        if (n % 3 == 0) {
            if (n % 2 == 0) {
                return "JavaScript";
            }
            return "Java";
        }
        return "mocha_missing!";
    }
}
