package dev.bjdelacruz.katas.codewars;

/**
 * This class repeats a string one or more times.
 *
 * @author BJ Peter Dela Cruz
 */
public final class StringRepeat {

    private StringRepeat() {
    }

    /**
     * Repeats the given string a certain number of times using the {@link java.lang.String#repeat(int)} method that
     * was introduced in Java 11.
     *
     * @param count The number of times to repeat <code>string</code>
     * @param string The string to repeat
     * @return A string that is repeated <code>count</code> times
     */
    public static String repeatStr(final int count, final String string) {
        return string.repeat(count);
    }
}
