package dev.bjdelacruz.katas.codewars;

import java.util.ArrayList;
import java.util.List;

/**
 * This class calculates a collatz sequence by repeatedly applying the following formula to a positive integer until the
 * result is equal to 1: if the number is even, then divide it by 2; if the number is odd, then multiply the number by 3
 * and then add 1.
 *
 * @author BJ Peter Dela Cruz
 */
public final class Collatz {

    /**
     * Do not instantiate this class.
     */
    private Collatz() {
    }

    /**
     * Generates a collatz sequence starting from the given number.
     *
     * @param number The number to start with
     * @return A collatz sequence as an array list
     */
    public static List<Long> calculateCollatzSequence(long number) {
        var numbers = new ArrayList<Long>();
        var temp = number;
        while (temp != 1) {
            numbers.add(temp);
            if (temp % 2 == 0) {
                temp = temp / 2;
            }
            else {
                temp = 3 * temp + 1;
            }
        }
        numbers.add(1L);
        return numbers;
    }

    /**
     * Returns a collatz sequence as a string. For example, given 3, <code>"3->10->5->16->8->4->2->1"</code> is
     * returned.
     *
     * @param number A positive integer greater than 0
     * @return A collatz sequence
     */
    public static String collatz(int number) {
        var numbers = calculateCollatzSequence(number).stream().map(Object::toString).toList();
        return String.join("->", numbers);
    }
}
