package dev.bjdelacruz.katas.codewars;

/**
 * This class compares two doubles and returns whether one is lower than, close to, or higher than the other.
 *
 * @author BJ Peter Dela Cruz
 */
public final class CompareWithinMargin {
    private CompareWithinMargin() {
    }

    /**
     * Compares two doubles. If the first double is greater than the second one, returns 1. If the second double is
     * greater than the first one, returns -1. If both are equal, returns 0.
     *
     * @param a The first double
     * @param b The second double
     * @return -1, 0, or -1
     */
    public static int closeCompare(double a, double b) {
        return Double.compare(a, b);
    }

    /**
     * Compares two doubles. If the distance between the first double and the second double is less than or equal to the
     * margin, they are considered close to each other, so 0 is returned. Otherwise, calls
     * {@link #closeCompare(double, double)} to compare the two values.
     *
     * @param a The first double
     * @param b The second double
     * @param margin The maximum distance between the two values
     * @return -1, 0, or -1
     */
    public static int closeCompare(double a, double b, double margin) {
        return Math.abs(a - b) <= margin ? 0 : closeCompare(a, b);
    }
}
