package dev.bjdelacruz.katas.codewars;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * This class calculates a person's age in days.
 *
 * @author BJ Peter Dela Cruz
 */
public final class AgeInDays {
    private AgeInDays() {
    }

    /**
     * Calculates a person's age in days given the year, month, and date on which he or she was born.
     *
     * @param year The year of a person's birth
     * @param month The month in which the person was born
     * @param date The day on which the person was born
     * @param endDate The date used to calculate the number of days since the person's birth date
     * @return A string that contains the person's age in days
     */
    public static String ageInDays(int year, int month, int date, LocalDate endDate) {
        var days = ChronoUnit.DAYS.between(LocalDate.of(year, month, date), endDate);
        return "You are " + days + " days old";
    }
}
