package dev.bjdelacruz.katas.codewars;

import java.util.Locale;
import java.util.stream.Collectors;

/**
 * This class converts all vowels in a string to uppercase letters.
 *
 * @author BJ Peter Dela Cruz
 */
public final class ChangingLetters {
    private ChangingLetters() {
    }

    /**
     * This method converts all vowels in the given string to uppercase letters. All other characters are left as is in
     * the string.
     *
     * @param string The input string that may or may not contain vowels
     * @return The same string but with all vowels converted to uppercase letters
     */
    public static String swap(String string) {
        return string.chars().mapToObj(i -> {
            if ("aeiou".indexOf(i) == -1) {
                return Character.toString(i);
            }
            return Character.toString(i).toUpperCase(Locale.getDefault());
        }).collect(Collectors.joining());
    }
}
