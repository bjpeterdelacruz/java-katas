package dev.bjdelacruz.katas.codewars;

import java.util.stream.IntStream;

/**
 * This class returns an array of numbers representing the number of monkeys that a child has counted.
 *
 * @author BJ Peter Dela Cruz
 */
public final class CountTheMonkeys {

    private CountTheMonkeys() {
    }

    /**
     * Returns an array of numbers from 1 to N that represents the number of monkeys that a child has counted.
     *
     * @param n The number of monkeys
     * @return An array from 1 to N
     */
    public static int[] monkeyCount(final int n) {
        return IntStream.range(1, n + 1).toArray();
    }
}
