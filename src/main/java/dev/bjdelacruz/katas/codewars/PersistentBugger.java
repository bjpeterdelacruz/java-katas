package dev.bjdelacruz.katas.codewars;

/**
 * This class counts the number of times the digits in a number is multiplied with each other until the product contains
 * only a single digit.
 *
 * @author BJ Peter Dela Cruz
 */
public final class PersistentBugger {
    private PersistentBugger() {
    }

    /**
     * This method multiplies each digit in the given number with each other. The process is repeated until the product
     * contains a single digit. The number of steps to reach a single digit is then returned.
     *
     * @param number A positive integer
     * @return The number of steps until the product contains a single digit, or zero if the number is less than 10
     */
    public static int persistence(long number) {
        if (number < 10) {
            return 0;
        }
        var count = 0;
        var num = number;
        do {
            num = Long.toString(num).chars().mapToObj(Character::toString).map(Integer::parseInt)
                    .reduce(1, (subtotal, element) -> subtotal * element);
            count++;
        } while (num > 9);
        return count;
    }
}
