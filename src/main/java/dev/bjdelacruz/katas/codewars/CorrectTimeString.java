package dev.bjdelacruz.katas.codewars;

/**
 * This class corrects a timestamp by performing addition to the number of seconds, minutes,
 * and hours. For example, given "19:99:99", the correct timestamp should be "20:40:39".
 *
 * @author BJ Peter Dela Cruz
 */
public final class CorrectTimeString {
    /**
     * Do not instantiate this class.
     */
    private CorrectTimeString() {
    }

    /**
     * Corrects the given timestamp. A valid timestamp is between 00:00:00 and 23:59:59, inclusive.
     *
     * @param timestring The timestamp to correct
     * @return A correct, valid timestamp
     */
    public static String timeCorrect(String timestring) {
        if (timestring == null || timestring.isEmpty()) {
            return timestring;
        }
        if (!timestring.matches("^\\d{2}:\\d{2}:\\d{2}$")) {
            return null;
        }
        var numbers = timestring.split(":");
        var hours = Integer.parseInt(numbers[0]);
        var minutes = Integer.parseInt(numbers[1]);
        var seconds = Integer.parseInt(numbers[2]);
        while (seconds >= 60) {
            seconds -= 60;
            minutes++;
        }
        while (minutes >= 60) {
            minutes -= 60;
            hours++;
        }
        while (hours >= 24) {
            hours -= 24;
        }
        var hoursString = (hours < 10 ? "0" : "") + hours;
        var minutesString = (minutes < 10 ? "0" : "") + minutes;
        var secondsString = (seconds < 10 ? "0" : "") + seconds;
        return hoursString + ":" + minutesString + ":" + secondsString;
    }
}
