package dev.bjdelacruz.katas.codewars;

import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * This class removes characters from a string, inserts characters into the same string, and converts the remaining
 * characters in the string.
 *
 * The kata was removed from Codewars due to copyright, but the link to the solutions is
 * <a href="https://www.codewars.com/kata/598ab63c7367483c890000f4/solutions/java" target="_blank">here</a>.
 *
 * @author BJ Peter Dela Cruz
 */
public final class StringTask {
    private StringTask() {
    }

    /**
     * Removes all vowels in the given string, inserts a period before each consonant, and converts all consonants to
     * lowercase.
     *
     * @param word The input string
     * @return A string that is the result of the aforementioned operations
     */
    public static String perform(String word) {
        var vowels = Set.of("a", "e", "i", "o", "u", "y");
        return word.chars().mapToObj(character -> {
            var charString = Character.toString(character).toLowerCase(Locale.getDefault());
            return vowels.contains(charString) ? "" : "." + charString;
        }).collect(Collectors.joining());
    }
}
