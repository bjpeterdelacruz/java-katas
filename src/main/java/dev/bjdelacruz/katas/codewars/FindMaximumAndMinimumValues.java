package dev.bjdelacruz.katas.codewars;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * This class returns the minimum and maximum values in an array of numbers.
 *
 * @author BJ Peter Dela Cruz
 */
public final class FindMaximumAndMinimumValues {

    private FindMaximumAndMinimumValues() {
    }

    /**
     * Returns the minimum value in the array using {@link IntStream#min()}.
     *
     * @param list The array of integers
     * @return The minimum value in the array
     */
    public static int min(int[] list) {
        return Arrays.stream(list).min().getAsInt();
    }

    /**
     * Returns the maximum value in the array using {@link IntStream#max()}.
     *
     * @param list The array of integers
     * @return The maximum value in the array
     */
    public static int max(int[] list) {
        return Arrays.stream(list).max().getAsInt();
    }
}
