package dev.bjdelacruz.katas.codewars;

import java.util.ArrayList;

/**
 * This class encodes and decodes text using the Hamming Code.
 *
 * @author BJ Peter Dela Cruz
 */
public class HammingCode {

    /**
     * Converts the given number to a binary string and pads the string with zeros until it is eight bits long.
     *
     * @param number The number to convert to a binary string
     * @return An 8-bit long binary string
     */
    private static String padBinaryString(int number) {
        return String.format("%8s", Integer.toBinaryString(number)).replace(" ", "0");
    }

    /**
     * This method encodes the given string using the following algorithm: convert every letter to its ASCII value,
     * convert ASCII values to 8-bit binary strings, triple every bit in each binary string, and then concatenate the
     * results.
     *
     * @param text The string to encode
     * @return The string encoded as a binary string
     */
    public String encode(String text) {
        var binaryStrings = text.chars().mapToObj(HammingCode::padBinaryString).toList();
        var bits = new ArrayList<String>();
        for (var s : binaryStrings) {
            for (Character c : s.toCharArray()) {
                bits.add(c.toString().repeat(3));
            }
        }
        return String.join("", bits);
    }

    /**
     * This method decodes the given binary string using the following algorithm: split the binary string into groups of
     * three, check if an error occurred by replacing each group with the character that occurs most often (e.g.
     * "010" becomes "0" and "110" becomes "1"), split into groups of 8-bit binary strings, convert each binary string
     * to an ASCII value and then to a character, and finally concatenate all the characters together.
     *
     * @param bits The binary string to decode
     * @return A decoded text
     */
    public String decode(String bits) {
        var bitGroups = new ArrayList<String>();
        for (var idx = 0; idx < bits.length(); idx += 3) {
            bitGroups.add(bits.substring(idx, idx + 3));
        }
        var binaryStrings = new ArrayList<String>();
        for (var b : bitGroups) {
            var countZeros = b.chars().filter(ch -> ch == '0').count();
            var countOnes = b.chars().filter(ch -> ch == '1').count();
            binaryStrings.add(countZeros > countOnes ? "0" : "1");
        }
        var binaryString = String.join("", binaryStrings);
        var letters = new ArrayList<String>();
        for (var idx = 0; idx < binaryString.length(); idx += 8) {
            letters.add(Character.toString((char) Integer.parseInt(binaryString.substring(idx, idx + 8), 2)));
        }
        return String.join("", letters);
    }

}