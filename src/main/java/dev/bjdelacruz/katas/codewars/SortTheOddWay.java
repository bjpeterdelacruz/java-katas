package dev.bjdelacruz.katas.codewars;

import java.util.Arrays;

/**
 * This class sorts an array of numbers based on whether they are even or odd.
 *
 * @author BJ Peter Dela Cruz
 */
public final class SortTheOddWay {
    private SortTheOddWay() {
    }

    /**
     * Sorts the odd numbers in the given array in ascending order and then the even numbers in descending order. The
     * odd numbers will come first in the sorted array. All numbers are rounded down before they are checked whether
     * they are even or odd.
     *
     * @param array A list of numbers to sort
     * @return A sorted array of numbers
     */
    public static Double[] sortItOut(Double[] array) {
        return Arrays.stream(array).sorted((first, second) -> {
            var a = Math.floor(first);
            var b = Math.floor(second);
            if (a % 2 == 1 && b % 2 == 0) {
                return -1;
            }
            if (a % 2 == 0 && b % 2 == 1) {
                return 1;
            }
            if (a % 2 == 0) {
                return Double.compare(second, first);
            }
            return Double.compare(first, second);
        }).toArray(Double[]::new);
    }
}
