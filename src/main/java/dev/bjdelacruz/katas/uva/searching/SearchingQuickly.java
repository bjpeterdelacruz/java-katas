/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva.searching;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;

import dev.bjdelacruz.katas.utils.Kata;
import dev.bjdelacruz.katas.utils.KataEnums;
import dev.bjdelacruz.katas.utils.KataUtils;

/**
 * This program sorts a list of titles using KWIC (Keywords In Context).
 * 
 * @author BJ Peter DeLaCruz
 * @see <a href="https://bjdelacruz.dev/files/katas/123_Searching_Quickly.pdf">Searching
 * Quickly</a>
 */
public class SearchingQuickly extends Kata {

  private final List<String> ignoreWordsList = new ArrayList<>();
  private final List<String> keywordsList = new ArrayList<>();
  private static final String SEPARATOR = "::";

  /**
   * Given a list of words to ignore and a list of keywords, which are words that are found in a
   * title and are not in the former list, sorts a list of titles using the latter and prints the
   * results; a title may be printed more than once.
   */
  @Override
  public void processLines() {
    if (getLines().isEmpty()) {
      throw new IllegalArgumentException("List is empty.");
    }

    processIgnoreWords();

    if (getLines().isEmpty()) {
      throw new IllegalArgumentException("At least one title must be specified.");
    }

    processKeywords();

    Collections.sort(keywordsList);
    var titles = new LinkedHashSet<String>(); // No repeats, keeps insertion order.

    var us = Locale.US;

    keywordsList.forEach(w -> getLines().stream().filter(t -> t.toLowerCase(us).contains(w.toLowerCase(us)))
      .forEach(t -> titles.addAll(KataUtils.replace(t.toLowerCase(us), w.toLowerCase(us),
          w.toUpperCase(us)))));

    titles.forEach(System.out::println);
  }

  /**
   * Processes each line until a separator is reached; each line before the separator represents a
   * word that is to be ignored when sorting the list of titles.
   */
  private void processIgnoreWords() {
    var line = getLines().removeFirst().toLowerCase(Locale.getDefault());
    while (!line.equals(SEPARATOR)) {
      ignoreWordsList.add(line);
      line = getLines().removeFirst().toLowerCase(Locale.getDefault());
    }
  }

  /**
   * Processes each line and puts all the words from each that are not in the ignore list in the
   * list of keywords, which is used for sorting the titles.
   */
  private void processKeywords() {
    for (var line : getLines()) {
      @SuppressWarnings("unchecked")
      List<String> keywords = (List<String>) KataUtils.createList(line, " ", KataEnums.STRING);
      var keywordsLowerCase = new ArrayList<String>();
      keywords.forEach(k -> keywordsLowerCase.add(k.toLowerCase(Locale.getDefault())));
      keywordsLowerCase.removeAll(ignoreWordsList);
      keywordsList.addAll(keywordsLowerCase);
    }
  }

  /**
   * Returns the list of ignore words.
   *
   * @return The list of ignore words.
   */
  public List<String> getIgnoreWordsList() {
    return new ArrayList<>(ignoreWordsList);
  }

  /**
   * Returns the list of keywords.
   *
   * @return The list of keywords.
   */
  public List<String> getKeywordsList() {
    return new ArrayList<>(keywordsList);
  }
}
