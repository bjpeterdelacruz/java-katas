/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva.searching;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import dev.bjdelacruz.katas.utils.Kata;
import dev.bjdelacruz.katas.utils.KataEnums;
import dev.bjdelacruz.katas.utils.KataUtils;

/**
 * Given a file containing profiles and titles, this program determines which titles satisfy each
 * profile.
 * 
 * @author BJ Peter DeLaCruz
 * @see <a href="https://bjdelacruz.dev/files/katas/175_Keywords.pdf">Searching
 * Quickly</a>
 */
public class KeyWord extends Kata {

  // Regular expression for all non-alphabetic characters, excluding whitespace characters.
  private static final Pattern PATTERN = Pattern.compile("[^a-zA-Z\\s]+");

  private final List<String> profiles = new ArrayList<>();
  private final List<String> titles = new ArrayList<>();
  private final List<String> results = new ArrayList<>();

  /**
   * Reads in a file that contains profiles and titles, and determines which titles satisfy each
   * profile.
   */
  @SuppressWarnings("unchecked")
  @Override
  public void processLines() {
    addProfilesAndTitles();

    for (String profile : profiles) {
      List<String> profileTokens =
          (List<String>) KataUtils.createList(profile, " ", KataEnums.STRING);

      String token = profileTokens.removeFirst();

      int threshold = -1;
      try {
        threshold = Integer.parseInt(token) + 1;
      }
      catch (NumberFormatException e) {
        throw new IllegalArgumentException("Expected number for threshold, was " + token + ".");
      }

      StringBuilder builder = new StringBuilder((profiles.indexOf(profile) + 1) + ": ");
      for (String title : titles) {
        // Replace all non-alphabetic characters, excluding whitespace characters, with an empty
        // string.
        String newTitle = PATTERN.matcher(title).replaceAll("");

        List<String> titleTokens =
            (List<String>) KataUtils.createList(newTitle, " ", KataEnums.STRING);

        if (processTitle(threshold, profileTokens, titleTokens)) {
          builder.append(titles.indexOf(title) + 1).append(",");
        }
      }

      results.add(builder.substring(0, builder.toString().length() - 1));
    }

    for (String result : results) {
      System.out.print(result + "\n");
    }
  }

  /**
   * Gets all the profiles and titles found in a file.
   */
  private void addProfilesAndTitles() {
    while (!getLines().isEmpty()) {
      String line = getLines().removeFirst();
      // This line is a profile.
      if (line.contains("P:")) {
        line = line.replace("P:", "").toLowerCase(Locale.getDefault());
        profiles.add(line);
      }
      // This line is a title.
      else if (line.contains("T:")) {
        line = line.replace("T:", "").replace("|", "").toLowerCase(Locale.getDefault());
        titles.add(line);
      }
      else {
        line = line.replace("|", "").toLowerCase(Locale.getDefault()); // The | marks the end of a title.
        titles.set(titles.size() - 1, titles.get(titles.size() - 1) + line);
      }
    }
  }

  /**
   * Given a threshold value, a profile, and a title, returns true if two keywords appear in the
   * title and also if the number of words between the two is not greater than the threshold value,
   * false otherwise.
   * 
   * @param threshold The maximum number of words that can appear between two keywords.
   * @param profileTokens The list of keywords.
   * @param titleTokens The title.
   * @return True if two keywords appear in the title and also if the number of words between the
   * two is not greater than the threshold value, false otherwise.
   */
  private boolean processTitle(int threshold, List<String> profileTokens,
      List<String> titleTokens) {
    for (int index = 0; index < profileTokens.size(); index += 2) {
      String firstProfileToken = profileTokens.get(index);
      String secondProfileToken = profileTokens.get(index + 1);
      int firstIndex = titleTokens.indexOf(firstProfileToken);
      int secondIndex = titleTokens.indexOf(secondProfileToken);
      int distance = Math.abs(firstIndex - secondIndex);
      if (firstIndex != -1 && secondIndex != -1 && distance <= threshold) {
        return true;
      }
    }
    return false;
  }

}
