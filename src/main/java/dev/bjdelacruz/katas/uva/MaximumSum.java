/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import java.util.ArrayList;
import java.util.List;

import dev.bjdelacruz.katas.utils.Kata;
import org.apache.commons.lang3.tuple.Pair;

/**
 * @see <a href="https://bjdelacruz.dev/files/katas/108_max_sum.pdf" target="_blank">Problem
 * Description</a>
 * 
 * @author BJ Peter DeLaCruz
 */
public class MaximumSum extends Kata {

  /**
   * Gets the dimension of a rectangle, its values, and prints the sum of the maximal sub-rectangle.
   */
  @Override
  public void processLines() {
    int size;
    try {
      size = Integer.parseInt(getLines().removeFirst());
    }
    catch (Exception e) {
      throw new IllegalArgumentException("Expected integer for size of rectangle.", e);
    }

    var rectangle = new int[size][size];

    var row = 0;
    var column = 0;

    // Create main rectangle
    while (!getLines().isEmpty()) {
      var array = getLines().removeFirst().split("\\s+");
      for (var s : array) {
        rectangle[row][column] = Integer.parseInt(s);
        if (column == size - 1) {
          row++;
          column = 0;
        }
        else {
          column++;
        }
      }
    }

    var pairs = getSubrectangleDimensions(size);

    var subrectangles = new ArrayList<List<Integer>>();

    for (var pair : pairs) {
      for (row = 0; row < size; row++) {
        for (column = 0; column < size; column++) {
          var subrectangle = getSubrectangle(rectangle, row, column, pair);
          if (!subrectangle.isEmpty()) {
            subrectangles.add(subrectangle);
          }
        }
      }
    }

    var maxSum = Integer.MIN_VALUE;

    for (var subrectangle : subrectangles) {
      var sum = 0;
      for (var value : subrectangle) {
        sum += value;
      }
      if (sum > maxSum) {
        maxSum = sum;
      }
    }

    System.out.print(maxSum);
  }

  /**
   * Gets all the possible sub-rectangle dimensions that are less than or equal to the given maximum
   * length and width.
   * 
   * @param maxLengthWidth The maximum length and width.
   * @return A list of sub-rectangle dimensions.
   */
  private static List<Pair<Integer, Integer>> getSubrectangleDimensions(int maxLengthWidth) {
    var sizes = new ArrayList<Pair<Integer, Integer>>();

    for (var x = 1; x <= maxLengthWidth; x++) {
      for (var y = 1; y <= maxLengthWidth; y++) {
        sizes.add(Pair.of(x, y));
      }
    }

    return sizes;
  }

  /**
   * Gets a sub-rectangle with the given dimension from the given rectangle starting from the given
   * row index (<code>rowStart</code>) and column index (<code>columnStart</code>).</p>
   * 
   * If a sub-rectangle is not valid, i.e. if the product of its dimensions does not equal the total
   * number of elements in the list, an empty list is returned. Otherwise, a list that represents a
   * sub-rectangle is returned.
   * 
   * @param rectangle The rectangle from which to get a sub-rectangle.
   * @param rowStart The starting row index.
   * @param columnStart The starting column index.
   * @param dimension The dimension of the sub-rectangle.
   * @return A list with N*M elements that represents a sub-rectangle of length N and width M, or an
   * empty list.
   */
  private static List<Integer> getSubrectangle(int[][] rectangle, int rowStart, int columnStart,
      Pair<Integer, Integer> dimension) {
    var subrectangle = new ArrayList<Integer>();

    var row = rowStart;
    var rowIdx = 0;

    while (row < rectangle.length && rowIdx < dimension.getLeft()) {

      var column = columnStart;
      var columnIdx = 0;

      while (column < rectangle[row].length && columnIdx < dimension.getRight()) {

        subrectangle.add(rectangle[row][column]);

        column++;
        columnIdx++;
      }

      row++;
      rowIdx++;
    }

    return dimension.getLeft() * dimension.getRight() == subrectangle.size() ? subrectangle : new ArrayList<>();
  }

}
