/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import java.util.HashMap;
import java.util.Map;

import dev.bjdelacruz.katas.utils.Kata;

/**
 * @see <a href="https://bjdelacruz.dev/files/katas/100_3n_plus_1.pdf"
 * target="_blank">Problem Description</a>
 * 
 * @author BJ Peter DeLaCruz
 */
public class CycleLength extends Kata {

  /** Used to store intermediate values to speed up the algorithm. */
  private final Map<Integer, Integer> values = new HashMap<>();

  /**
   * Processes all the input values (i and j) on each line and then prints i, j, and the max cycle
   * length between i and j, inclusive. Note that for i and j, i &lt; j or j &lt; i.
   */
  @Override
  public void processLines() {
    while (!getLines().isEmpty()) {
      var line = getLines().removeFirst();
      var array = line.split("\\s+");
      int i;
      int j;
      try {
        i = Integer.parseInt(array[0]);
      }
      catch (Exception e) {
        throw new IllegalArgumentException("Value for i is not an integer.");
      }
      try {
        j = Integer.parseInt(array[1]);
      }
      catch (Exception e) {
        throw new IllegalArgumentException("Value for j is not an integer.");
      }
      if (i > j) {
        var temp = i;
        i = j;
        j = temp;
      }
      var maxCycle = 0;
      for (var counter = i; counter <= j; counter++) {
        var temp = calculateCycleLength(counter);
        if (temp > maxCycle) {
          maxCycle = temp;
        }
      }
      System.out.printf("%s %s %d%n", array[0], array[1], maxCycle);
    }
  }

  /**
   * Calculates the cycle length for the given number. The cycle length is the number of values
   * printed to the screen, including <code>input</code> and 1.
   * 
   * @param input The first number in the cycle.
   * @return The cycle length.
   */
  private int calculateCycleLength(int input) {
    var n = input;
    // System.out.println(n);
    var length = 1;
    while (n != 1) {
      if (values.containsKey(n)) {
        n = values.get(n);
      }
      else {
        int temp = (n % 2 == 0) ? n >> 1 : 3 * n + 1;
        values.put(n, temp);
        n = temp;
      }
      // System.out.println(n);
      length++;
    }
    return length;
  }

}
