/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva.consecutive;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * This class contains methods that find the number of representations for a given positive integer.
 * 
 * @author BJ Peter DeLaCruz
 * @see <a href="https://bjdelacruz.dev/files/katas/1210_Consecutive_Primes.pdf">Consecutive
 * Primes</a>
 */
final class ConsecutivePrimes {

  private final Set<Integer> primes = new LinkedHashSet<>();
  private final Set<Integer> composites = new HashSet<>();
  private final Map<Integer, List<List<Integer>>> resultsMap = new HashMap<>();

  /**
   * Finds all prime numbers that are less than the given number.
   * 
   * @param number The number for which to find the number of representations.
   */
  private void sieveEratosthenes(int number) {
    if (number < 0) {
      throw new IllegalArgumentException("number must not be negative");
    }

    IntStream.rangeClosed(2, number).filter(n -> !composites.contains(n)).forEach(n -> {
      primes.add(n);
      for (var sum = n + n; sum <= number; sum += n) {
        composites.add(sum);
      }
    });
  }

  /**
   * Counts the number of representations for the given number and stores the results in a map.
   * 
   * @param number The number for which to find the number of representations.
   */
  private void countSum(int number) {
    if (number < 0) {
      throw new IllegalArgumentException("number must not be negative");
    }

    if (resultsMap.get(number) != null) {
      return;
    }

    // Find all prime numbers first.
    sieveEratosthenes(number);

    var primes = new ArrayList<>(this.primes);
    var resultsList = new ArrayList<List<Integer>>();
    for (var i = 0; i < primes.size() && primes.get(i) <= number; i++) {
      var results = new ArrayList<Integer>();
      var sum = primes.get(i);
      results.add(sum);
      for (var j = i + 1; j < primes.size() && sum < number; j++) {
        sum += primes.get(j);
        results.add(primes.get(j));
      }
      if (sum == number) {
        resultsList.add(results);
      }
    }
    resultsMap.put(number, resultsList);
  }

  /**
   * Returns the number of representations for the given number.
   * 
   * @param number The number for which to find the number of representations.
   * @return The number of representations.
   */
  Integer getCount(int number) {
    if (number < 0) {
      throw new IllegalArgumentException("number must not be negative");
    }

    countSum(number);
    return resultsMap.get(number).size();
  }

  /**
   * Returns a list of representations for the given number.
   * 
   * @param number The number for which to find the number of representations.
   * @return A list of representations for the given number.
   */
  String getResults(int number) {
    if (resultsMap.get(number) == null && getCount(number) == 0) {
      return "[]";
    }
    return resultsMap.get(number).stream().map(Object::toString).collect(Collectors.joining(" "));
  }

}
