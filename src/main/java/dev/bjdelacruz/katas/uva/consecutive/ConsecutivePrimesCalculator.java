/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva.consecutive;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import dev.bjdelacruz.commons.utils.UiUtils;

/**
 * The user interface for the <a href="http://www.bjdelacruz.com/files/katas/1210_Consecutive_Primes.pdf"
 * target="_blank">Sum of Consecutive Prime Numbers kata</a>.
 * 
 * @author BJ Peter DeLaCruz
 */
public final class ConsecutivePrimesCalculator extends JFrame {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  static {
    UiUtils.useSystemLookAndFeel(LOGGER);
  }

  private final JTextField textField = new JTextField() {

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("PMD.EmptyCatchBlock")
    public void processKeyEvent(KeyEvent event) {
      try {
        var c = event.getKeyChar();
        // Ignore all non-printable characters. Just check the printable ones.
        if (c > 31 && c < 127) {
          Integer.parseInt(Character.toString(c));
        }
        super.processKeyEvent(event);
      }
      catch (NumberFormatException nfe) {
        // Do nothing. Character entered is not a number, so ignore it.
      }
    }

  };
  private final JLabel resultsLabel = new JLabel(" ");

  private final ConsecutivePrimes engine = new ConsecutivePrimes();

  /**
   * Creates and displays the user interface, which looks like a normal calculator.
   */
  @SuppressWarnings("PMD.ConstructorCallsOverridableMethod")
  public ConsecutivePrimesCalculator() {
    super("Consecutive Primes");
    setLayout(new BorderLayout());

    var numberButtons = new JButton[10];
    var panel = new JPanel(new GridLayout(4, 3));
    for (var index = 0; index <= 9; index++) {
      numberButtons[index] = new JButton(index + "");
      numberButtons[index].addActionListener(event -> {
          textField.setText(textField.getText() + ((JButton) event.getSource()).getText());
          resultsLabel.setText(" ");
      });
      if (index > 0) {
        panel.add(numberButtons[index]);
      }
    }
    var emptyPanel = new JPanel();
    panel.add(emptyPanel);
    panel.add(numberButtons[0]);
    var enterButton = new JButton("Enter");
    enterButton.addActionListener(_ -> findSums());
    panel.add(enterButton);

    textField.addKeyListener(new EnterKeyListener());
    add(textField, BorderLayout.NORTH);
    add(panel, BorderLayout.CENTER);
    var southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 10, 10));
    southPanel.add(resultsLabel);
    add(southPanel, BorderLayout.SOUTH);

    setResizable(false);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    pack();
    setLocationRelativeTo(null);
    setVisible(true);
  }

  /**
   * A listener that will find the number of representations for the number that the user entered
   * when the Enter key is entered.
   * 
   * @author BJ Peter DeLaCruz
   */
  private final class EnterKeyListener extends KeyAdapter {

    /** {@inheritDoc} */
    @Override
    public void keyReleased(KeyEvent event) {
      if (event.getKeyCode() == KeyEvent.VK_ENTER) {
        findSums();
      }
    }

  }

  /**
   * Finds the number of representations for the number that was entered.
   */
  private void findSums() {
    if (textField.getText().isEmpty()) {
      return;
    }
    var number = Integer.parseInt(textField.getText());
    if (number <= 1) {
      textField.setText("");
      resultsLabel.setText("<html>Found <b>0</b> representations.</html>");
      return;
    }
    var count = engine.getCount(number);
    textField.setText("");
    var text = "<html>Found <b>" + count + "</b> representations.</html>";
    if (count == 1) {
      text = text.replace("representations", "representation");
    }
    resultsLabel.setText(text);
    LOGGER.log(Level.INFO, number + ": " + engine.getResults(number));
  }

  /**
   * The main entry point of this application. Displays a numeric pad in which the user can enter a
   * number.
   * 
   * @param args None.
   */
  public static void main(String... args) {
    new ConsecutivePrimesCalculator();
  }

}
