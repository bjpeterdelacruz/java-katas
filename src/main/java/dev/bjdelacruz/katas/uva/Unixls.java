/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import dev.bjdelacruz.katas.utils.Kata;

/**
 * Prints a list of filenames to a table in sorted alphabetical order to the screen.
 * Each row contains no more than sixty characters.
 * 
 * @see <a href="https://bjdelacruz.dev/files/katas/400_Unix_Ls.pdf">Unix Ls</a>
 * 
 * @author BJ Peter DeLaCruz &lt;bj.peter.delacruz@gmail.com&gt;
 */
public class Unixls extends Kata {

  private String table;
  private static final int ROW_MAX_WIDTH = 60;

  /** {@inheritDoc} */
  @Override
  public void processLines() {
    var copy = new ArrayList<>(getLines());
    var numberOfLines = Integer.parseInt(copy.removeFirst());
    var files = new ArrayList<String>();
    IntStream.rangeClosed(0, numberOfLines - 1).forEach(i -> files.add(copy.removeFirst()));
    table = getFilesTable(files);
  }

  String getTable() {
    return table;
  }

  /**
   * Sorts the given list of filenames and then returns a string with the filenames listed in
   * columns in a table. The filenames in the table are sorted in alphabetical order from left to
   * right starting from the first column in the top row.
   * 
   * @param filenames The list of filenames to put in a table.
   * @return A table with all the filenames listed in it.
   */
  static String getFilesTable(List<String> filenames) {
    if (filenames == null || filenames.isEmpty()) {
      return "";
    }
    if (filenames.size() == 1) {
      return filenames.getFirst();
    }

    var max = filenames.stream().mapToInt(String::length).max().getAsInt();

    filenames.sort(String::compareToIgnoreCase);

    var numberOfColumns = getNumberOfColumns(max);
    var table = new StringBuilder();
    for (var idx = 0; idx < filenames.size(); idx++) {
      if ((idx + 1) % numberOfColumns == 0) {
        String column = getColumn(filenames.get(idx), max) + "\n";
        table.append(column);
      }
      else {
        table.append(getColumn(filenames.get(idx), max + 2));
      }
      if ((idx + 1) % numberOfColumns != 0 && idx + 1 == filenames.size()) {
        table.append("\n");
      }
    }
    return getHyphens() + table + getHyphens();
  }

  /**
   * Returns a string containing n hyphens.
   * 
   * @return A string containing n hyphens.
   */
  private static String getHyphens() {
    var buffer = new StringBuffer();
    IntStream.rangeClosed(0, 59).forEach(i -> buffer.append("-"));
    return buffer + "\n";
  }

  /**
   * Returns the number of columns that should be printed to the screen.
   * 
   * @param maxFilenameLength The maximum length of a filename in a column.
   * @return The number of columns.
   */
  static int getNumberOfColumns(int maxFilenameLength) {
    if (maxFilenameLength <= 0) {
      throw new IllegalArgumentException("Max filename length must be greater than 0.");
    }

    var numColumns = 0;
    while ((maxFilenameLength + 2) * numColumns + maxFilenameLength < ROW_MAX_WIDTH) {
      numColumns++;
    }
    return numColumns;
  }

  /**
   * Returns a string containing the given filename with zero, one, or more spaces appended to the
   * end of it.
   * 
   * @param filename The filename to be printed.
   * @param maxNumChars The maximum number of characters for this column. If the number of
   * characters in the filename is less than this number, spaces will be appended to end of the
   * filename until the length of the new string is equal to this number. Otherwise, the filename is
   * returned with no spaces appended.
   * @return A string containing the filename and spaces.
   */
  static String getColumn(String filename, int maxNumChars) {
    if (filename == null) {
      throw new IllegalArgumentException("filename is null");
    }
    if (maxNumChars <= 0) {
      throw new IllegalArgumentException("Max number of characters must be greater than 0.");
    }

    var buffer = new StringBuffer(filename);
    IntStream.rangeClosed(filename.length(), maxNumChars - 1).forEach(i -> buffer.append(" "));
    return buffer.toString();
  }

}
