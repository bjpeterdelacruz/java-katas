/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva.treelevel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.StringTokenizer;

import dev.bjdelacruz.katas.utils.Kata;

/**
 * This program builds one or more trees and then prints the contents of each in level order.
 * 
 * @author BJ Peter DeLaCruz
 * @see <a href="https://bjdelacruz.dev/files/katas/122_Tree_Level.pdf">Tree Level</a>
 */
final class TreeLevel extends Kata {

  /**
   * Processes each line in a file until a "()" is reached, at which point a tree is constructed and
   * whose contents are printed in level order; repeats until the end of the file is reached.
   */
  @Override
  public void processLines() {
    while (!getLines().isEmpty()) {
      var builder = new StringBuilder();
      var line = getLines().removeFirst() + " ";
      builder.append(line);
      while (!line.contains("()") && !getLines().isEmpty()) {
        line = getLines().removeFirst() + " ";
        builder.append(line);
      }

      var result = getLevelOrder(builder.toString());
      System.out.print((result == null ? "not complete" : result) + "\n");
    }
  }

  /**
   * Returns a string containing the tree printed in level order, or <code>null</code> if the tree
   * couldn't be built.
   * 
   * @param line Represents the tree to build and then print in level order.
   * @return A string containing the tree printed in level order, or <code>nulL</code> if the tree
   * couldn't be built.
   */
  public static String getLevelOrder(String line) {
    if (line == null || line.isEmpty()) {
      throw new IllegalArgumentException("line is null or an empty string");
    }

    var root = buildTree(createNodesList(line));
    return root == null ? null : getLevelOrder(root);
  }

  /**
   * Given a line containing pairs of values and paths, builds a list of nodes, each with a value
   * and path.
   * 
   * @param line The line read in from a file containing pairs of values and paths.
   * @return The list of nodes.
   */
  private static List<TreeNode> createNodesList(String line) {
    if (line == null || line.isEmpty()) {
      throw new IllegalArgumentException("line is null or an empty string");
    }

    var tokenizer = new StringTokenizer(line, "(), ");
    var strings = new ArrayList<String>();
    while (tokenizer.hasMoreTokens()) {
      strings.add(tokenizer.nextToken());
    }

    for (var index = 0; index < strings.size(); index += 2) {
      try {
        Integer.parseInt(strings.get(index));
      }
      catch (NumberFormatException e) {
        strings.add(index - 1, "-");
        break;
      }
    }

    var nodes = new ArrayList<TreeNode>();
    for (var index = 0; index < strings.size() - 1; index += 2) {
      nodes.add(new TreeNode(Integer.parseInt(strings.get(index)), strings.get(index + 1)));
    }

    Collections.sort(nodes);
    return nodes;
  }

  /**
   * Given a list of nodes, builds a tree using the paths found in the nodes.
   * 
   * @param nodes The list of nodes with which to build a tree.
   * @return The root of the tree.
   */
  private static TreeNode buildTree(List<TreeNode> nodes) {
    Objects.requireNonNull(nodes);
    var tree = new ArrayList<TreeNode>();
    tree.add(nodes.removeFirst());
    if (!"-".equals(tree.getFirst().getPosition())) {
      return null;
    }

    while (!nodes.isEmpty()) {
      var node = nodes.removeFirst();
      var parent = tree.getFirst();
      var path = node.getPosition().toUpperCase(Locale.getDefault()).toCharArray();
      var isInserted = false;
      for (var direction : path) {
        if (direction == 'L' && parent.getLeft() == null) {
          parent.setLeft(node);
          isInserted = true;
        }
        else if (direction == 'R' && parent.getRight() == null) {
          parent.setRight(node);
          isInserted = true;
        }
        else if (direction == 'L') {
          parent = parent.getLeft();
        }
        else if (direction == 'R') {
          parent = parent.getRight();
        }
        else {
          throw new IllegalArgumentException("Invalid character found: " + direction);
        }
      }
      if (!isInserted) {
        throw new IllegalArgumentException("Node not inserted into tree: " + node);
      }
    }

    return tree.getFirst();
  }

  /**
   * Returns a string containing the tree printed in level order.
   * 
   * @param treeNode The root of the tree.
   * @return A string containing the tree printed in level order.
   */
  private static String getLevelOrder(TreeNode treeNode) {
    Objects.requireNonNull(treeNode);
    var queue = new ArrayList<TreeNode>();
    queue.add(treeNode);

    var builder = new StringBuilder();
    while (true) {
      var currentNode = queue.getFirst();
      if (currentNode.getLeft() != null) {
        queue.add(currentNode.getLeft());
      }
      if (currentNode.getRight() != null) {
        queue.add(currentNode.getRight());
      }
      if (queue.size() > 1) {
        builder.append(queue.removeFirst().getValue());
        builder.append(" ");
      }
      else {
        break;
      }
    }
    return builder.toString() + queue.removeFirst().getValue();
  }

}
