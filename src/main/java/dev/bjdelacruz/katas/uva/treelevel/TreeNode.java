/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva.treelevel;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import java.util.Locale;
import java.util.Objects;

/**
 * This class is used to create a Node object that will be put into a tree.
 * 
 * @author BJ Peter DeLaCruz
 */
@SuppressFBWarnings("PI_DO_NOT_REUSE_PUBLIC_IDENTIFIERS_CLASS_NAMES")
final class TreeNode implements Comparable<TreeNode> {

  private final int value;
  private final String position;
  private TreeNode left;
  private TreeNode right;

  /**
   * Creates a Node object.
   * 
   * @param value The value for the node.
   * @param position The position in the tree at which this node is located.
   */
  TreeNode(int value, String position) {
    this.value = value;
    this.position = position;
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    return "Node=[value=" + this.value + ",position=" + this.position + ",left=" + this.left + ",right=" + this.right
            + "]";
  }

  /** {@inheritDoc} */
  @Override
  public boolean equals(Object object) {
    return object instanceof TreeNode treeNode && value == treeNode.value && Objects.equals(position, treeNode.position)
        && Objects.equals(left, treeNode.left) && Objects.equals(right, treeNode.right);
  }

  /** {@inheritDoc} */
  @Override
  public int hashCode() {
    return Objects.hash(value, position, left, right);
  }

  /** {@inheritDoc} */
  @Override
  public int compareTo(TreeNode treeNode) {
    return position.toUpperCase(Locale.US).compareTo(treeNode.position.toUpperCase(Locale.US));
  }

  public int getValue() {
    return value;
  }

  public String getPosition() {
    return position;
  }

  public TreeNode getLeft() {
    return left;
  }

  public void setLeft(TreeNode left) {
    this.left = left;
  }

  public TreeNode getRight() {
    return right;
  }

  public void setRight(TreeNode right) {
    this.right = right;
  }
}
