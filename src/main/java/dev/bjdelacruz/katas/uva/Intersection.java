/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import java.awt.Point;
import java.util.Objects;

import dev.bjdelacruz.katas.utils.Kata;
import dev.bjdelacruz.katas.utils.KataEnums;
import dev.bjdelacruz.katas.utils.KataUtils;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * This program determines whether a line intersects a rectangle by checking if a point on the line
 * is inside the rectangle.
 * 
 * @author BJ Peter DeLaCruz
 * @see <a href="https://bjdelacruz.dev/files/katas/191_Intersection.pdf">Intersection</a>
 */
public class Intersection extends Kata {

  /**
   * Creates a rectangle given two points: one will be in the top left corner and the other will be in the bottom right
   * corner.
   *
   * @author BJ Peter DeLaCruz
   * @param topLeft     The point representing the top left corner.
   * @param bottomRight The point representing the bottom right corner.
   */
  @SuppressFBWarnings("PI_DO_NOT_REUSE_PUBLIC_IDENTIFIERS_CLASS_NAMES")
  record Rectangle(Point topLeft, Point bottomRight) {
    /**
     * Creates a new Rectangle object given the coordinates for the top left and bottom right
     * corners.
     */
    Rectangle {
      Objects.requireNonNull(topLeft);
      Objects.requireNonNull(bottomRight);
    }
  }

  /**
   * For each line and rectangle, determines whether the line intersects the rectangle by checking
   * if a point on the line is inside the rectangle.
   */
  @Override
  public void processLines() {
    int numLines;
    try {
      numLines = Integer.parseInt(this.getLines().getFirst());
      this.getLines().removeFirst();
    }
    catch (NumberFormatException e) {
      throw new IllegalArgumentException("Non-numeric character(s) found: " + getLines().getFirst());
    }

    if (numLines != this.getLines().size()) {
      var msg = numLines == 1 ? "1 line" : numLines + " lines";
      throw new IllegalArgumentException("Expected " + msg + ". Found " + getLines().size() + ".");
    }

    while (!this.getLines().isEmpty()) {
      var coordinates = KataUtils.createList(this.getLines().removeFirst(), " ", KataEnums.INTEGER);

      if (coordinates.isEmpty()) {
        continue;
      }

      var size = coordinates.size();
      if (size != 8) {
        throw new IllegalArgumentException("Expected 8 integers. Found " + size + ".");
      }

      // Get the start and end points.
      var start = new Point((Integer) coordinates.getFirst(), (Integer) coordinates.get(1));
      var end = new Point((Integer) coordinates.get(2), (Integer) coordinates.get(3));
      if (start.equals(end)) {
        throw new IllegalArgumentException("Length of line cannot be 0.");
      }

      var slope = calculateSlope(start, end);

      // Create the rectangle.
      var topLeft = new Point((Integer) coordinates.get(4), (Integer) coordinates.get(5));
      var bottomRight = new Point((Integer) coordinates.get(6), (Integer) coordinates.get(7));
      var rectangle = new Rectangle(topLeft, bottomRight);

      // Check if the start point is inside the rectangle.
      var p = start; // createNewPoint(start, slope);
      var isInside = isInsideRectangle(p, rectangle);

      // If the start point is not inside the rectangle, check every point on the line until one is
      // found inside the rectangle or until the end point is reached.
      while (!isInside && !p.equals(end)) {
        p = createNewPoint(p, slope);
        isInside = isInsideRectangle(p, rectangle);
      }

      System.out.print(isInside ? "T\n" : "F\n");
    }
  }

  /**
   * Given a point on a line and the slope of the line, returns the next point on the line.
   * 
   * @param current The current point (x, y).
   * @param slope The slope of the line (run, rise).
   * @return The new point at (x + run, y + rise).
   */
  private Point createNewPoint(Point current, Point slope) {
    return new Point(current.x + slope.x, current.y + slope.y);
  }

  /**
   * Finds the greatest common divisor for two numbers, e.g. 7 is the greatest common divisor for
   * both 14 and 21.
   * 
   * @param number1 The first number.
   * @param number2 The second number.
   * @return The common divisor for both numbers.
   */
  private int getGreatestCommonDivisor(int number1, int number2) {
    var num1 = number1;
    var num2 = number2;

    if (num1 > num2) {
      var temp = num1;
      num1 = num2;
      num2 = temp;
    }
    var divisor = -1;
    for (var i = 2; i <= num1; i++) {
      if (num1 % i == 0 && num2 % i == 0) {
        divisor = i;
      }
    }
    return divisor;
  }

  /**
   * Calculates the slope of a line given the following formula:
   * <code>slope = (y1 - y2) / (x1 - x2)</code>. Returns the slope in simplified form, e.g. if the
   * slope is (21, 14), then (3, 2) is returned.
   * 
   * @param start The start point (x1, y1).
   * @param end The end point (x2, y2).
   * @return The slope stored in a <code>Point</code> ((x1 - x2), (y1 - y2)).
   */
  private Point calculateSlope(Point start, Point end) {
    var slope = new int[2];
    slope[0] = Math.abs(start.y - end.y);
    slope[1] = Math.abs(start.x - end.x);
    var divisor = getGreatestCommonDivisor(slope[0], slope[1]);
    if (divisor != -1) {
      slope[1] /= divisor;
      slope[0] /= divisor;
    }
    if (start.x > end.x) {
      slope[1] *= -1;
    }
    if (start.y > end.y) {
      slope[0] *= -1;
    }
    return new Point(slope[1], slope[0]);
  }

  /**
   * Checks if a point on a line is inside a rectangle.
   * 
   * @param point The point to check.
   * @param rectangle The rectangle.
   * @return True if the point is inside the rectangle, false otherwise.
   */
  private boolean isInsideRectangle(Point point, Rectangle rectangle) {
    return point.x >= rectangle.topLeft().x && point.y <= rectangle.topLeft().y
        && point.x <= rectangle.bottomRight().x && point.y >= rectangle.bottomRight().y;
  }

}
