/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import dev.bjdelacruz.katas.utils.Kata;
import dev.bjdelacruz.katas.utils.KataEnums;
import dev.bjdelacruz.katas.utils.KataUtils;

/**
 * Given N letters and a list of constraints, a list of strings that satisfy
 * <span style="text-decoration:underline">all</span> of the constraints will be returned.
 * 
 * @see <a href="https://bjdelacruz.dev/files/katas/124_Following_Orders.pdf">Following
 * Orders</a>
 * 
 * @author BJ Peter DeLaCruz
 */
public class FollowingOrders extends Kata {

  private String solution = "No ordering exists.";

  /**
   * Creates a new FollowingOrders object.
   */
  public FollowingOrders() {
    // Empty constructor.
  }

  /**
   * Processes each pair of lines (one is the letters to order, the other is the list of
   * constraints), and prints out a list of strings whose characters satisfy <span
   * style="text-decoration:underline">all</span> of the constraints.
   */
  @Override
  public void processLines() {
    while (!this.getLines().isEmpty()) {

      if (getLines().size() != 2) {
        throw new IllegalArgumentException("Size does not equal 2.");
      }

      var line = this.getLines().removeFirst();
      var letters = (List<Character>) KataUtils.createList(line, " ", KataEnums.CHARACTER);
      var tempSet = new HashSet<>(letters);
      if (tempSet.size() < letters.size()) {
        throw new IllegalArgumentException("Duplicate letters exist: " + line);
      }

      var constraints = this.getLines().removeFirst();
      var constraintsList = (List<Character>) KataUtils.createList(constraints, " ", KataEnums.CHARACTER);
      if (constraintsList.size() % 2 != 0) {
        throw new IllegalArgumentException("Expected even number of constraints but was odd.");
      }

      var strings = KataUtils.makeStringsList(new ArrayList<>(letters));

      var results = new ArrayList<String>();
      for (var s : strings) {
        letters = new ArrayList<>();
        for (var c : s.toCharArray()) {
          letters.add(c);
        }
        if (this.checkConstraints(letters, constraintsList)) {
          results.add(s);
        }
      }

      if (!results.isEmpty()) {
        Collections.sort(results);
        solution = String.join("\n", results);
      }
    }
  }

  /**
   * Returns the solution to this kata.
   *
   * @return The solution to this kata.
   */
  public String getSolution() {
    return solution;
  }

  /**
   * Checks if all constraints are satisfied.
   * 
   * @param letters The letters to check.
   * @param constraints The constraints to meet.
   * @return True if all constraints are satisfied, false otherwise.
   */
  private boolean checkConstraints(List<Character> letters, List<Character> constraints) {
    for (var index = 0; index < constraints.size(); index += 2) {
      if (letters.indexOf(constraints.get(index)) > letters.indexOf(constraints.get(index + 1))) {
        return false;
      }
    }
    return true;
  }

}
