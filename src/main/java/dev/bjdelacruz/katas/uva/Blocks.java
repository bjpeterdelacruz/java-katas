/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import dev.bjdelacruz.katas.utils.Kata;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * @see <a href="http://www.bjdelacruz.com/files/katas/101_blocks.pdf" target="_blank">Problem
 * Description</a>
 * 
 * @author BJ Peter DeLaCruz
 */
public class Blocks extends Kata {

  private static final Map<Integer, List<Block>> BLOCKS_WORD = new HashMap<>();

  /**
   * Processes a series of commands that instruct a robot arm in how to manipulate blocks that lie
   * on a flat table.
   */
  @Override
  public void processLines() {
    int numberOfBlocks;
    try {
      numberOfBlocks = Integer.parseInt(getLines().removeFirst());
    }
    catch (Exception e) {
      throw new IllegalArgumentException("Expected integer for number of blocks.", e);
    }

    IntStream.range(0, numberOfBlocks).forEach(
        i -> BLOCKS_WORD.put(i, new ArrayList<>(Collections.singletonList(new Block(i)))));

    while (!getLines().isEmpty()) {
      var line = getLines().removeFirst().split("\\s+");
      if ("quit".equals(line[0])) {
        break;
      }

      int from;
      try {
        from = Integer.parseInt(line[1]);
      }
      catch (Exception e) {
        throw new IllegalArgumentException("Expected integer for first block.");
      }

      int to;
      try {
        to = Integer.parseInt(line[3]);
      }
      catch (Exception e) {
        throw new IllegalArgumentException("Expected integer for second block.");
      }

      if (from == to) {
        continue;
      }

      switch (line[0]) {
        case "move" -> move(from, to, "onto".equals(line[2]));
        case "pile" -> pile(from, to, "onto".equals(line[2]));
        default -> throw new IllegalArgumentException("Invalid command: " + line[0]);
      }
    }

    IntStream.range(0, numberOfBlocks).forEach(i -> {
      System.out.print(i + ": ");
      BLOCKS_WORD.get(i).forEach(b -> System.out.print(b.position() + " "));
      System.out.print("\n");
    });
  }

  /**
   * Puts block <code>from</code> onto the top of the stack containing block <code>to</code>, after
   * returning any blocks that are stacked on top of block <code>from</code> to their initial
   * positions.</p>
   * 
   * If <code>moveOnto</code> is true, puts block <code>from</code> onto block <code>to</code> after
   * returning any blocks that are stacked on top of blocks <code>from</code> and <code>to</code> to
   * their initial positions.
   * 
   * @param from Block to move.
   * @param to Block on which to move first block onto/over.
   * @param moveOnto If true, processes the "move onto" command; if false, processes the "move over"
   * command.
   */
  private void move(int from, int to, boolean moveOnto) {
    for (var entry : BLOCKS_WORD.entrySet()) {
      var fromStack = entry.getValue();
      if (!fromStack.contains(new Block(from))) {
        continue;
      }
      if (fromStack.contains(new Block(to))) {
        return;
      }

      var idx = fromStack.indexOf(new Block(from));
      for (var currentIdx = fromStack.size() - 1; currentIdx != idx
          && currentIdx < fromStack.size();) {
        var otherBlock = fromStack.remove(currentIdx);
        BLOCKS_WORD.get(otherBlock.position()).add(otherBlock);
      }

      moveBlocks(to, moveOnto, List.of(fromStack.remove(idx)));
    }
  }

  /**
   * Moves the pile of blocks consisting of block <code>from</code>, and any blocks that are stacked
   * above block <code>from</code>, onto block <code>to</code>. All blocks on top of block
   * <code>to</code> are moved to their initial positions prior to the pile taking place. The blocks
   * stacked above block <code>from</code> retain their order when moved.</p>
   * 
   * If <code>moveOnto</code> is true, puts the pile of blocks consisting of block <code>from</code>
   * , and any blocks that are stacked above block <code>from</code>, onto the top of the stack
   * containing block <code>to</code>. The blocks stacked above block <code>from</code> retain their
   * original order when moved.
   * 
   * @param from Block to move.
   * @param to Block on which to pile first block onto/over.
   * @param moveOnto If true, processes the "pile onto" command; if false, processes the "pile over"
   * command.
   */
  private void pile(int from, int to, boolean moveOnto) {
    for (var entry : BLOCKS_WORD.entrySet()) {
      var fromStack = entry.getValue();
      if (!fromStack.contains(new Block(from))) {
        continue;
      }
      if (fromStack.contains(new Block(to))) {
        return;
      }

      var tempStack = new ArrayList<Block>();

      var idx = fromStack.indexOf(new Block(from));

      while (idx < fromStack.size()) {
        tempStack.add(fromStack.remove(idx));
      }

      moveBlocks(to, moveOnto, tempStack);
    }
  }

  private void moveBlocks(int to, boolean moveOnto, List<Block> blocksToAdd) {
    for (var entry : BLOCKS_WORD.entrySet()) {
      var toStack = entry.getValue();
      if (!toStack.contains(new Block(to))) {
        continue;
      }

      if (moveOnto) {
        var idx = toStack.indexOf(new Block(to));
        for (var currentIdx = idx + 1; currentIdx < toStack.size();) {
          var otherBlock = toStack.remove(currentIdx);
          BLOCKS_WORD.get(otherBlock.position()).add(otherBlock);
        }
      }
      toStack.addAll(blocksToAdd);
    }
  }

  /**
   * Represents a block.
   * 
   * @author BJ Peter DeLaCruz
   * @param position The position of this block
   */
  @SuppressFBWarnings("PI_DO_NOT_REUSE_PUBLIC_IDENTIFIERS_CLASS_NAMES")
  record Block(int position) {
  }

}
