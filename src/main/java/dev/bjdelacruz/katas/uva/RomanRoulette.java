/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import java.util.ArrayList;
import java.util.List;

import dev.bjdelacruz.katas.utils.Kata;
import dev.bjdelacruz.katas.utils.KataEnums;
import dev.bjdelacruz.katas.utils.KataUtils;
import dev.bjdelacruz.commons.utils.ListUtils;

/**
 * A solution to the Josephus problem (also known as Roman Roulette).
 * 
 * @author BJ Peter DeLaCruz
 * @see <a href="https://bjdelacruz.dev/files/katas/130_Roman_Roulette.pdf">Roman Roulette</a>
 */
public class RomanRoulette extends Kata {

  /** The ID of the last person standing in a circle of people. */
  private final int survivor;

  /**
   * Creates a new instance with the ID of the last person standing.
   *
   * @param survivor The ID of the last person standing
   */
  public RomanRoulette(int survivor) {
    this.survivor = survivor;
  }

  /**
   * For each line, finds the position that a person should stand at in a circle of N people, where
   * every K-th person dies, in order to survive.
   */
  @Override
  public void processLines() {
    while (true) {
      var line = getLines().removeFirst();
      @SuppressWarnings("unchecked")
      var params = (List<Integer>) KataUtils.createList(line, " ", KataEnums.INTEGER);
      if (params.size() != 2) {
        throw new IllegalArgumentException("Only need 2 parameters: N and K. Found " + params.size() + ".");
      }

      var n = params.getFirst(); // Number of people in the circle
      var k = params.get(1); // Kill every k-th person

      if (n == 0 && k == 0) { // EOF
        break;
      }
      // If there is only one person in the circle from the very beginning, then that person is the
      // survivor.
      if (n == 1) {
        System.out.print("N: " + n + "\tK: " + k + "\tPosition: " + n + "\n");
      }
      else {
        findPosition(n, k, params);
      }
    }
  }

  /**
   * Finds the position that a person should stand at in a circle of N people, where every K-th
   * person dies, in order to survive.
   * 
   * @param n The number of people in a circle.
   * @param k Every K-th person dies.
   * @param params Contains N and K.
   */
  private void findPosition(int n, int k, List<Integer> params) {
    var ids = new ArrayList<>(ListUtils.createIntegersList(1, n));
    var isFirstVictim = true;
    var isPositionFound = false;
    var numPeople = n;

    for (int index = 0, indexOfVictim, indexOfPerson; index < numPeople; index++) {
      // Start counting from the person at position index.
      indexOfVictim = (index + k - 1) % numPeople;
      while (!ids.isEmpty()) {
        if (isFirstVictim) {
          // Start counting from 1 to k beginning with the person at position index.
          indexOfPerson = (indexOfVictim + k) % numPeople;
          isFirstVictim = false;
        }
        else {
          // Start counting from 1 to k beginning with the person to the left of the victim.
          indexOfPerson = (indexOfVictim + k + 1) % numPeople;
        }
        var person = ids.get(indexOfPerson);
        // Don't remove the element at position indexOfVictim, i.e. the victim. Just set its
        // value to that of person. Then remove the element at position indexOfPerson, i.e. the
        // person who buries the victim.
        ids.set(indexOfVictim, person);
        ids.remove(indexOfPerson);
        numPeople--;
        if (ids.size() == 1) {
          // We found the position in the circle that a person should stand at in order to
          // survive.
          if (ids.getFirst() == survivor) {
            System.out.print("N: " + params.getFirst() + "\tK: " + params.get(1));
            System.out.print("\tPosition: " + (index + 1) + "\n");
            isPositionFound = true;
            break;
          }
          ids.clear();
        }
        indexOfVictim = (indexOfVictim + k) % numPeople;
      }
      if (isPositionFound) {
        break;
      }
      // Try the next position, but first, reset all values.
      ids = new ArrayList<>(ListUtils.createIntegersList(1, n));
      numPeople = params.getFirst();
      isFirstVictim = true;
    }
  }

}
