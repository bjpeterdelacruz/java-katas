/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

import dev.bjdelacruz.katas.utils.Kata;
import org.apache.commons.lang3.StringUtils;

import static dev.bjdelacruz.commons.utils.StringUtils.replaceLast;

/**
 * Implements a calculator that uses custom operators, and precedence and associativity rules.
 * 
 * @author BJ Peter DeLaCruz &lt;bj.peter.delacruz@gmail.com&gt;
 * @see <a href="https://bjdelacruz.dev/files/katas/354_Crazy_Calculator.pdf">Crazy
 * Calculator</a>
 */
final class CrazyCalculator extends Kata {

  private static final char LEFT_ASSOC = 'L';
  private static final char RIGHT_ASSOC = 'R';

  private static final String OPERATORS = "~@#$%^&*()_+=-{}[]:;|<>,.?/";

  private final List<String> results = new ArrayList<>();

  /**
   * Represents a rule for this calculator, which contains an operator, a crazy operator, and
   * precedence and associativity for the operator.
   * 
   * @author BJ Peter DeLaCruz &lt;bj.peter.delacruz@gmail.com&gt;
   */
  static final class Rule implements Comparable<Rule> {

    private final char op;
    private final char crazyOp;
    private final int precedence;
    private final char associativity;
    private final int lastPosition;

    /**
     * Creates a new Rule.
     * 
     * @param op The operator.
     * @param crazyOp The crazy operator.
     * @param precedence The precedence of the operator.
     * @param associativity The associativity of the operator.
     * @param lastPosition The last position that the given crazy operator appears in a string.
     */
    Rule(char op, char crazyOp, int precedence, char associativity, int lastPosition) {
      this.op = op;
      if (!OPERATORS.contains(crazyOp + "")) {
        throw new IllegalArgumentException("Not a valid crazy operator: " + crazyOp);
      }
      this.crazyOp = crazyOp;
      this.precedence = precedence;
      if (associativity != LEFT_ASSOC && associativity != RIGHT_ASSOC) {
        throw new IllegalArgumentException("Not a valid associativity: " + associativity);
      }
      this.associativity = associativity;
      this.lastPosition = lastPosition;
    }

    /**
     * Sets the last position that the given crazy operator appears in a string.
     * 
     * @param lastPosition The last position that the given crazy operator appears in a string.
     * @return A new Rule with a new value for the last position.
     */
    public Rule setLastPosition(int lastPosition) {
      return new Rule(op, crazyOp, precedence, associativity, lastPosition);
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object object) {
      return object instanceof Rule rule && op == rule.op && crazyOp == rule.crazyOp && precedence == rule.precedence
          && associativity == rule.associativity && lastPosition == rule.lastPosition;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
      return Objects.hash(op, crazyOp, precedence, associativity, lastPosition);
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
      return op + "" + crazyOp + (precedence + "") + associativity
          + ((lastPosition == -1) ? "" : " " + lastPosition);
    }

    /** {@inheritDoc} */
    @Override
    public int compareTo(Rule rule) {
      var result = Integer.compare(precedence, rule.precedence);
      return result == 0 ? Integer.compare(lastPosition, rule.lastPosition) : result;
    }

    public char getOp() {
      return op;
    }

    public char getCrazyOp() {
      return crazyOp;
    }

    public char getAssociativity() {
      return associativity;
    }
  }

  /** {@inheritDoc} */
  @Override
  public void processLines() {
    var rulesMap = new TreeMap<Character, Rule>();
    results.clear();
    for (var idx = 0; idx < 4 && !getLines().isEmpty(); idx++) {
      var line = getLines().removeFirst();
      if (line.length() != 4) {
        throw new IllegalArgumentException("Line doesn't contain just four characters.");
      }
      var precedence = Integer.parseInt(line.charAt(2) + "");
      var rule = new Rule(line.charAt(0), line.charAt(1), precedence, line.charAt(3), -1);
      if (rulesMap.put(rule.getCrazyOp(), rule) != null) {
        throw new IllegalArgumentException("Rule already defined: " + line);
      }
    }
    while (!getLines().isEmpty()) {
      var oldLine = getLines().removeFirst();
      if (oldLine.isEmpty()) {
        continue;
      }
      var line = replaceCrazyOps(rulesMap, oldLine);
      var rulesList = new ArrayList<Rule>();
      rulesMap.forEach((k, v) -> rulesList.add(v.setLastPosition(oldLine.lastIndexOf(v.getCrazyOp()))));
      Collections.sort(rulesList);
      Collections.reverse(rulesList);
      System.out.print(line + " = ");
      for (var rule : rulesList) {
        line = calculate(line, rule);
      }
      System.out.println(line + "\n");
      results.add(line);
    }
  }

  /**
   * Replaces all crazy operators with regular operators (i.e. '+', '-', '*', and '/').
   * 
   * @param rules The map of arithmetic rules.
   * @param line The line that contains crazy operators that need to be replaced.
   * @return A line with regular operators.
   */
  private static String replaceCrazyOps(Map<Character, Rule> rules, String line) {
    if (rules == null) {
      throw new IllegalArgumentException("rules is null");
    }
    if (line == null || line.length() == 0) {
      throw new IllegalArgumentException("line is null or an empty string");
    }

    var newLine = line.toCharArray();
    for (var index = 0; index < newLine.length; index++) {
      if (rules.get(newLine[index]) == null) {
        continue;
      }
      if (index == 0 && newLine[index] == '-') {
        continue;
      }
      try {
        if (newLine[index] == '-') {
          Integer.parseInt(newLine[index - 1] + "");
        }
      }
      catch (NumberFormatException nfe) {
        // For example: 1--2
        continue;
      }
      newLine[index] = rules.get(newLine[index]).getOp();
    }
    return new String(newLine);
  }

  /**
   * Calculates the given equation.
   * 
   * @param line The line that represents an equation.
   * @param rule An arithmetic rule.
   * @return The original line if the given operator is not found in the line, or a partially or
   * fully solved equation.
   */
  @SuppressWarnings("PMD.EmptyCatchBlock")
  private static String calculate(String line, Rule rule) {
    if (line == null || line.length() == 0) {
      throw new IllegalArgumentException("line is null or an empty string");
    }
    if (rule == null) {
      throw new IllegalArgumentException("rule is null");
    }

    var newLine = line;
    int position;

    while (true) {
      position = switch (rule.getAssociativity()) {
        case LEFT_ASSOC -> newLine.indexOf(rule.getOp());
        case RIGHT_ASSOC -> newLine.lastIndexOf(rule.getOp());
        default -> throw new IllegalArgumentException("Invalid argument: " + rule.getAssociativity());
      };

      if (position < 0) {
        return newLine;
      }

      var chars = newLine.toCharArray();

      // Get value for left operand
      var leftmostPosition = position;
      if (!isNumeric(chars, leftmostPosition - 1)) {
        return newLine;
      }
      var end = leftmostPosition - 1;
      do {
        leftmostPosition--;
      } while (isNumeric(chars, leftmostPosition));
      var start = leftmostPosition + 1;
      var buffer = new StringBuilder();
      for (var index = start; index < end + 1; index++) {
        buffer.append(chars[index]);
      }
      var left = Integer.parseInt(buffer.toString());

      // Get value for right operand
      var rightmostPosition = position;
      start = rightmostPosition + 1;
      end = rightmostPosition + 1;
      while (true) {
        if (isNumeric(chars, end)) {
          end++;
        }
        else {
          break;
        }
      }
      end--;
      buffer = new StringBuilder();
      for (var index = start; index < end + 1; index++) {
        buffer.append(chars[index]);
      }
      var right = Integer.parseInt(buffer.toString());

      var oldSubstring = left + "" + rule.getOp() + right;
      if (rule.getOp() == '/' && right == 0) {
        throw new IllegalArgumentException("Attempted to divide by 0.");
      }
      var result = switch (rule.getOp()) {
        case '+' -> left + right;
        case '-' -> left - right;
        case '*' -> left * right;
        case '/' -> left / right;
        default -> throw new IllegalArgumentException("Invalid operator: " + rule.getOp());
      };

      newLine = switch (rule.getAssociativity()) {
        case LEFT_ASSOC -> StringUtils.replace(newLine, oldSubstring, result + "");
        case RIGHT_ASSOC -> replaceLast(newLine, oldSubstring, result + "");
        default -> throw new IllegalArgumentException("Invalid argument: " + rule.getAssociativity());
      };

      try {
        Integer.parseInt(newLine);
        return newLine;
      }
      catch (NumberFormatException nfe) {
        // Do nothing
      }

    }
  }

  /**
   * Returns true if the given array of characters represents a number, or false otherwise.
   * 
   * @param chars The array of characters.
   * @param position The position in the array.
   * @return True if the given array of characters represents a number; or false if position < 0,
   * position >= chars.length, or the given array of characters does not represent a number.
   */
  private static boolean isNumeric(char[] chars, int position) {
    if (chars == null) {
      throw new IllegalArgumentException("chars is null");
    }
    if (position < 0 || position >= chars.length) {
      return false;
    }
    if (position == 0 && chars[position] == '-') {
      return true;
    }
    try {
      if (position - 1 >= 0) {
        Integer.parseInt(chars[position - 1] + "");
      }
    }
    catch (NumberFormatException nfe) {
      if (chars[position] == '-') {
        return true;
      }
    }
    return chars[position] != '+' && chars[position] != '-' && chars[position] != '*'
        && chars[position] != '/';
  }

  /** @return A copy of the results for one set of calculations. */
  public List<String> getResults() {
    return new ArrayList<>(results);
  }

}
