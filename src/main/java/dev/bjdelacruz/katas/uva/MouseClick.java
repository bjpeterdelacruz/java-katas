/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

import dev.bjdelacruz.katas.utils.Kata;

/**
 * Processes a file containing coordinates for icons, regions, and mouse clicks, and outputs
 * all regions that were clicked on and all icons that were nearest to a mouse click.
 * 
 * @see <a href="https://bjdelacruz.dev/files/katas/142_Mouse_Clicks.pdf">Mouse Clicks</a>
 * 
 * @author BJ Peter DeLaCruz
 */
public class MouseClick extends Kata {

  private final List<Integer[][]> icons = new ArrayList<>();
  private final List<Integer[][]> regions = new ArrayList<>();
  private final List<Integer[][]> mouseClicks = new ArrayList<>();

  /**
   * Puts all icons, regions, and mouse clicks into three different lists and then processes each
   * mouse click.
   */
  @Override
  public void processLines() {
    for (int index = 1, letter = 65; !this.getLines().isEmpty();) {
      if ("#".equals(this.getLines().getFirst())) {
        break;
      }

      // Read in all icons, regions, and mouse clicks first.
      var tokenizer = new StringTokenizer(this.getLines().removeFirst(), " ");
      while (tokenizer.hasMoreTokens()) {
        var type = tokenizer.nextToken();
        if ("I".equals(type)) {
          this.addIcon(tokenizer, index);
          index++;
        }
        else if ("R".equals(type)) {
          this.addRegion(tokenizer, letter);
          letter++;
        }
        else {
          this.addMouseClick(tokenizer);
        }
      }
    }

    // Don't process icons that are covered by a region.
    for (var index = 0; index < this.icons.size(); index++) {
      if (this.isInsideRegion(this.icons.get(index)) != null) {
        this.icons.remove(index);
      }
    }

    processMouseClicks();
  }

  /**
   * Adds an icon to a list of icons.
   * 
   * @param tokenizer The tokenizer that contains the coordinates for the icon.
   * @param index The index associated with an icon; starting from 1.
   */
  private void addIcon(StringTokenizer tokenizer, int index) {
    var icon = new Integer[1][3];
    icon[0][0] = Integer.parseInt(tokenizer.nextToken());
    icon[0][1] = Integer.parseInt(tokenizer.nextToken());
    icon[0][2] = index;
    this.icons.add(icon);
  }

  /**
   * Adds a region to the list of regions.
   * 
   * @param tokenizer The tokenizer that contains the coordinates for the region.
   * @param letter The letter associated with a region; starting from A.
   */
  private void addRegion(StringTokenizer tokenizer, int letter) {
    var region = new Integer[1][5];
    for (var idx = 0; idx < 4; idx++) {
      region[0][idx] = Integer.parseInt(tokenizer.nextToken());
    }
    region[0][4] = letter;
    this.regions.add(region);
  }

  /**
   * Adds a mouse click to the list of mouse clicks.
   * 
   * @param tokenizer The tokenizer that contains the coordinates for the mouse click.
   */
  private void addMouseClick(StringTokenizer tokenizer) {
    var mouseClick = new Integer[1][2];
    mouseClick[0][0] = Integer.parseInt(tokenizer.nextToken());
    mouseClick[0][1] = Integer.parseInt(tokenizer.nextToken());
    this.mouseClicks.add(mouseClick);
  }

  /**
   * Processes all mouse clicks and prints the regions that were clicked on and the icons that were
   * nearest to a mouse click.
   */
  private void processMouseClicks() {
    for (var click : this.mouseClicks) {
      var region = this.isInsideRegion(click);
      if (region == null) {
        var distances = new ArrayList<Double>();
        var positions = new ArrayList<Integer>();
        for (var icon : this.icons) {
          distances.add(this.distanceFormula(click, icon));
          positions.add(icon[0][2]);
        }

        var minimums = new ArrayList<Double>();
        var minPositions = new ArrayList<Integer>();
        minimums.add(distances.removeFirst());
        minPositions.add(positions.removeFirst());
        while (!distances.isEmpty()) {
          if (minimums.getFirst() > distances.getFirst()) {
            minimums.clear();
            minPositions.clear();
            minimums.add(distances.removeFirst());
            minPositions.add(positions.removeFirst());
          }
          else if (minimums.getFirst().equals(distances.getFirst())) {
            minimums.add(distances.removeFirst());
            minPositions.add(positions.removeFirst());
          }
          else {
            distances.removeFirst();
            positions.removeFirst();
          }
        }

        for (var index = 0; index < minPositions.size() - 1; index++) {
          System.out.print("   " + minPositions.get(index));
        }
        System.out.print("   " + minPositions.get(minPositions.size() - 1) + "\n");
      }
      else {
        System.out.print(new String(Character.toChars(region[0][4])) + "\n");
      }
    }
  }

  /**
   * Calculates the distance formula:
   * 
   * <pre>
   * <code>
   *    distance = sqrt((x1 - x2)^2 + (y1 - y2)^2).
   * </code>
   * </pre>
   * 
   * @param coordinates1 (x1, y1).
   * @param coordinates2 (x2, y2).
   * @return The distance between two points.
   */
  private double distanceFormula(Integer[][] coordinates1, Integer[][] coordinates2) {
    var x1 = coordinates1[0][0];
    var x2 = coordinates2[0][0];
    var y1 = coordinates1[0][1];
    var y2 = coordinates2[0][1];

    x2 -= x1;
    x2 *= x2;
    y2 -= y1;
    y2 *= y2;

    return Math.sqrt(x2 + y2);
  }

  /**
   * Gets a region if a pair of coordinates is located within it.
   * 
   * @param coordinates The x and y coordinates of an icon or a mouse click.
   * @return The region if the given pair of coordinates is located within it, or <code>null</code>
   * otherwise.
   */
  private Integer[][] isInsideRegion(Integer[][] coordinates) {
    var x = coordinates[0][0];
    var y = coordinates[0][1];
    Integer[][] result = null;

    Collections.reverse(this.regions);
    for (var region : this.regions) {
      var topLeftX = region[0][0];
      var topLeftY = region[0][1];
      var bottomRightX = region[0][2];
      var bottomRightY = region[0][3];
      if (x <= bottomRightX && x >= topLeftX && y >= topLeftY && y <= bottomRightY) {
        result = region;
        break;
      }
    }

    Collections.reverse(this.regions);
    return result;
  }

}
