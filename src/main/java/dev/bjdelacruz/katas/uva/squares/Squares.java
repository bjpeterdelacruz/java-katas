/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva.squares;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import dev.bjdelacruz.katas.utils.Kata;

/**
 * This program reads in lines representing start or end points for horizontal or vertical lines and
 * counts the number of squares that the lines form.
 * 
 * @author BJ Peter DeLaCruz &lt;bj.peter.delacruz@gmail.com&gt;
 * @see <a href="https://bjdelacruz.dev/files/katas/201_Squares.pdf">Squares</a>
 */
public class Squares extends Kata {

  /** {@inheritDoc} */
  @Override
  public void processLines() {
    var problemNum = 1;
    while (!this.getLines().isEmpty()) {

      int dimension;
      try {
        dimension = Integer.parseInt(this.getLines().removeFirst()) + 1;
      }
      catch (NumberFormatException nfe) {
        throw new IllegalArgumentException("Dimension is not an integer.");
      }

      int numLines;
      try {
        numLines = Integer.parseInt(this.getLines().removeFirst());
      }
      catch (NumberFormatException nfe) {
        throw new IllegalArgumentException("Number of lines is not an integer.");
      }

      if (dimension - 1 < 2) {
        throw new IllegalArgumentException("Dimension must be greater than 1.");
      }
      if (numLines < 1) {
        throw new IllegalArgumentException("Number of lines must be greater than 0.");
      }

      var linesSet = new HashSet<Line>();
      while (!this.getLines().isEmpty() && this.getLines().getFirst().length() > 1) {
        processLine(linesSet, dimension);
      }

      if (linesSet.size() != numLines) {
        throw new IllegalArgumentException("Expected " + numLines + " lines. Found " + linesSet.size() + ".");
      }

      var lines = new ArrayList<>(linesSet);
      Collections.sort(lines);

      System.out.println("Problem #" + problemNum++ + "\n");
      var rowsColumns = new HashMap<Integer, List<Line>>();
      organizeLines(lines, rowsColumns);
      findSquares(rowsColumns);
    }
  }

  /**
   * Processes a line that contains either a start or end point for a horizontal or vertical line.
   * 
   * @param lines The set of lines to which a line will be added.
   * @param dimension The number of points in a row or column in the grid.
   */
  private void processLine(Set<Line> lines, int dimension) {
    var line = this.getLines().removeFirst();
    var tokenizer = new StringTokenizer(line);
    if (tokenizer.countTokens() == 3) {
      var direction = tokenizer.nextToken();
      var i = Integer.parseInt(tokenizer.nextToken());
      var j = Integer.parseInt(tokenizer.nextToken());
      if ("H".equals(direction)) {
        if (i > 0 && j > 0 && i <= dimension && j < dimension && j + 1 <= dimension) {
          lines.add(new Line(Line.Direction.HORIZONTAL, i, j, i, j + 1));
        }
        else {
          throw new IllegalArgumentException("Invalid point for line: " + i + " " + j);
        }
      }
      else if ("V".equals(direction)) {
        if (i > 0 && j > 0 && i < dimension && i + 1 <= dimension && j < dimension) {
          lines.add(new Line(Line.Direction.VERTICAL, j, i, j + 1, i));
        }
        else {
          throw new IllegalArgumentException("Invalid point for line: " + i + " " + j);
        }
      }
      else {
        throw new IllegalArgumentException("Invalid value for direction: " + direction);
      }
    }
    else {
      throw new IllegalArgumentException("Expected direction, row index, and column index. Found [" + line + "].");
    }
  }

  /**
   * Finds squares by following line paths from a start point on one line to an end point on a
   * different line.
   * 
   * @param rowsColumns The map containing array lists of lines. A key represents a row or column in
   * which a line is found. A value is an array list containing lines found in that row or column.
   */
  private void findSquares(Map<Integer, List<Line>> rowsColumns) {
    var rowsColumnsList = new ArrayList<>(rowsColumns.entrySet());
    var linesSet = new LinkedHashSet<Line>();
    var counts = new HashMap<Integer, Integer>();
    for (var i = 0; i < rowsColumnsList.size() - 1; i++) {
      for (var j = i + 1; j < rowsColumnsList.size(); j++) {
        for (var line : rowsColumnsList.get(i).getValue()) {
          addHorizLineToSet(linesSet, line, rowsColumnsList.get(j).getValue());
          addVertLineToSet(linesSet, line, rowsColumnsList.get(j).getValue());
        }

        var lines = new ArrayList<>(linesSet);

        var horizontalLines = followLinePath(Line.Direction.HORIZONTAL, lines);
        var verticalLines = followLinePath(Line.Direction.VERTICAL, lines);
        if (!horizontalLines.isEmpty() && !verticalLines.isEmpty()
            && horizontalLines.get(1).getEndPoint().equals(verticalLines.get(1).getEndPoint())) {
          var start = horizontalLines.getFirst().getStartPoint().x;
          var end = horizontalLines.get(1).getEndPoint().x;
          var size = start > end ? start - end : end - start;
          if (!counts.containsKey(size)) {
            counts.put(size, 0);
          }
          counts.put(size, counts.get(size) + 1);
        }
        linesSet.clear();
      }
    }
    printResults(counts);
  }

  /**
   * Prints the number of squares of a particular size found.
   * 
   * @param counts The map containing the number of squares of a particular size found.
   */
  private void printResults(Map<Integer, Integer> counts) {
    if (counts.isEmpty()) {
      System.out.println("No completed squares can be found.");
    }
    else {
      counts.forEach((k, v) -> System.out.println(v + " square" + (v > 1 ? "s" : "")
          + " of size " + k));
    }
    System.out.println();
    System.out.println("**********************************\n");
  }

  /**
   * Adds a horizontal line to a set of lines.
   * 
   * @param lines The set of lines to which a horizontal line will be added.
   * @param lineToAdd The horizontal line to add.
   * @param list The list of lines containing horizontal and vertical lines from which all
   * horizontal lines in a specific row will be added to the given set.
   */
  private void addHorizLineToSet(Set<Line> lines, Line lineToAdd, List<Line> list) {
    if (lineToAdd.getDirection().equals(Line.Direction.HORIZONTAL)) {
      for (var line : list) {
        if (line.getDirection().equals(Line.Direction.HORIZONTAL)
            && lineToAdd.getStartPoint().y == line.getStartPoint().y
            && lineToAdd.getEndPoint().y == line.getEndPoint().y) {
          lines.add(lineToAdd);
          lines.add(line);
        }
      }
    }
  }

  /**
   * Adds a vertical line to a set of lines.
   * 
   * @param lines The set of lines to which a vertical line will be added.
   * @param lineToAdd The vertical line to add.
   * @param list The list of lines containing horizontal and vertical lines from which all vertical
   * lines in a specific column will be added to the given set.
   */
  private void addVertLineToSet(Set<Line> lines, Line lineToAdd, List<Line> list) {
    if (lineToAdd.getDirection().equals(Line.Direction.VERTICAL)) {
      for (var line : list) {
        if (line.getDirection().equals(Line.Direction.VERTICAL)
            && lineToAdd.getStartPoint().x == line.getStartPoint().x
            && lineToAdd.getEndPoint().x == line.getEndPoint().x) {
          lines.add(lineToAdd);
          lines.add(line);
        }
      }
    }
  }

  /**
   * Follows a path from a start point on one line to an end point on a different line. The start
   * point should have the same x- and y-values. Otherwise, the value for <code>lastLine</code> will
   * be <code>null</code>, and the method will return. If both x- and y-values are the same, the
   * method will iterate through the given list of lines until a line with the same x- and y-values
   * for the end point is found or the end of the list is reached (in which case a square was not
   * found).
   * 
   * @param direction The direction of the line from which to start.
   * @param lines The list of lines containing both horizontal and vertical lines.
   * @return A list containing the first and last lines, or <code>null</code> if the line from which
   * to start did not have the same x- and y-values for the start point.
   */
  private List<Line> followLinePath(Line.Direction direction, List<Line> lines) {
    Line firstLine = null;
    Line lastLine = null;
    Point startPoint;
    Point endPoint;

    for (int index = 0; index < lines.size(); index++) {
      firstLine = lines.get(index);
      startPoint = firstLine.getStartPoint();
      boolean isSameValues = startPoint.x == startPoint.y;
      if (firstLine.getDirection().equals(direction) && isSameValues) {
        lastLine = getLineWithSameStartPoint(firstLine.getEndPoint(), lines);
        break;
      }
    }
    if (lastLine == null) {
      return new ArrayList<>();
    }

    for (var index = 0; index < lines.size(); index++) {
      startPoint = lines.get(index).getStartPoint();
      endPoint = lastLine.getEndPoint();
      if (startPoint.equals(endPoint) || endPoint.x == endPoint.y) {
        if (endPoint.x == endPoint.y) {
          // System.out.println(firstLine + " * " + lastLine);
          return new ArrayList<>(List.of(firstLine, lastLine));
        }
        lastLine = getLineWithSameStartPoint(lines.get(index).getEndPoint(), lines);
        index = 0;
      }
    }

    return new ArrayList<>();
  }

  /**
   * Returns a line that has the same x- and y-values for the start point as <code>point</code>.
   * 
   * @param startPoint The point to check against.
   * @param lines The list of horizontal and vertical lines.
   * @return A line that has the same x- and y-values for the start point as <code>point</code>, or
   * <code>null</code> if no line is found.
   */
  private Line getLineWithSameStartPoint(Point startPoint, List<Line> lines) {
    for (var line : lines) {
      if (line.getStartPoint().equals(startPoint)) {
        return line;
      }
    }
    return null;
  }

  /**
   * Puts all horizontal and vertical lines into a hash map where the keys represent either rows or
   * columns in which the lines in the array lists are found.
   * 
   * @param lines The list containing both horizontal and vertical lines.
   * @param rowsColumns An initially empty hash map that will contain array lists of horizontal and
   * vertical lines.
   */
  private void organizeLines(List<Line> lines, Map<Integer, List<Line>> rowsColumns) {
    // Gather all horizontal lines.
    var rows = new HashMap<Integer, List<Line>>();
    for (var line : lines) {
      if (line.getDirection().equals(Line.Direction.HORIZONTAL)) {
        var row = line.getStartPoint().x;
        rows.putIfAbsent(row, new ArrayList<>());
        rows.get(row).add(line);
      }
    }

    // Gather all vertical lines.
    var columns = new HashMap<Integer, List<Line>>();
    for (var line : lines) {
      if (line.getDirection().equals(Line.Direction.VERTICAL)) {
        var row = line.getStartPoint().y;
        columns.putIfAbsent(row, new ArrayList<>());
        columns.get(row).add(line);
      }
    }

    // Gather all horizontal lines in row N and all vertical lines in column N, put them together in
    // an array list, and store the array list at key N.
    addLines(rowsColumns, rows);
    addLines(rowsColumns, columns);
  }

  private void addLines(Map<Integer, List<Line>> rowsColumns, Map<Integer, List<Line>> columns) {
    for (var entry : columns.entrySet()) {
      rowsColumns.putIfAbsent(entry.getKey(), new ArrayList<>());
      rowsColumns.get(entry.getKey()).addAll(entry.getValue());
    }
  }

}
