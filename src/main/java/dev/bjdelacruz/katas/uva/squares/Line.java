/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva.squares;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import java.awt.Point;
import java.util.Objects;

/**
 * A class representing a line for the {@link Squares} kata.
 * 
 * @author BJ Peter DeLaCruz &lt;bj.peter.delacruz@gmail.com&gt;
 */
@SuppressFBWarnings("PI_DO_NOT_REUSE_PUBLIC_IDENTIFIERS_CLASS_NAMES")
public class Line implements Comparable<Line> {

  /**
   * An enum representing a direction for a line.
   * 
   * @author BJ Peter DeLaCruz &lt;bj.peter.delacruz@gmail.com&gt;
   */
  enum Direction {
    HORIZONTAL, VERTICAL
  }

  private final Direction direction;
  private final Point startPoint;
  private final Point endPoint;

  /**
   * Creates a new line.
   * 
   * @param direction The direction of the line.
   * @param startX The X-coordinate for the start point.
   * @param startY The Y-coordinate for the start point.
   * @param endX The X-coordinate for the end point.
   * @param endY The Y-coordinate for the end point.
   */
  Line(Direction direction, int startX, int startY, int endX, int endY) {
    this.direction = direction;
    startPoint = new Point(startX, startY);
    endPoint = new Point(endX, endY);
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    var msg = switch (this.direction) {
      case HORIZONTAL -> "H";
      case VERTICAL -> "V";
    };
    msg += " (" + this.startPoint.getX() + ", " + this.startPoint.getY() + ") ";
    return msg + "(" + this.endPoint.getX() + ", " + this.endPoint.getY() + ")";
  }

  /** {@inheritDoc} */
  @Override
  public int compareTo(Line line) {
    if (this.startPoint.x == line.startPoint.x) {
      return this.startPoint.y - line.startPoint.y;
    }
    return this.startPoint.x - line.startPoint.x;
  }

  /** {@inheritDoc} */
  @Override
  public boolean equals(Object obj) {
    return obj instanceof Line line && direction == line.direction && startPoint.equals(line.startPoint)
            && endPoint.equals(line.endPoint);
  }

  /** {@inheritDoc} */
  @Override
  public int hashCode() {
    return Objects.hash(direction, startPoint, endPoint);
  }

  /**
   * Returns the direction of the line.
   *
   * @return The direction of the line.
   */
  Direction getDirection() {
    return direction;
  }

  /**
   * Returns the start point of the line.
   *
   * @return The start point of the line.
   */
  public Point getStartPoint() {
    return new Point(startPoint);
  }

  /**
   * Returns the end point of the line.
   *
   * @return The end point of the line.
   */
  public Point getEndPoint() {
    return new Point(endPoint);
  }
}
