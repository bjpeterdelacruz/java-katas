/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import dev.bjdelacruz.katas.utils.Kata;
import dev.bjdelacruz.katas.utils.KataEnums;
import dev.bjdelacruz.katas.utils.KataUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Finds the number of digit primes between two numbers, inclusive. A digit prime
 * is a prime number whose sum of all digits is also prime.
 * 
 * @author BJ Peter DeLaCruz
 * @see <a href="https://bjdelacruz.dev/files/katas/10533_digit_primes.pdf">Digit Primes</a>
 */
public class DigitPrimes extends Kata {

  private final List<Integer> results = new ArrayList<>();

  /**
   * Each line in the input consists of two integers, <code>t_1</code> and <code>t_2</code>. This
   * method will, for each line, print the number of digit primes between <code>t_1</code> and
   * <code>t_2</code>, inclusive.
   */
  @Override
  public void processLines() {
    int numberOfLines;
    try {
      numberOfLines = Integer.parseInt(getLines().removeFirst());
    }
    catch (NumberFormatException nfe) {
      throw new IllegalArgumentException(
          "Expected an integer that indicates the number of inputs to process.", nfe);
    }
    if (numberOfLines != getLines().size()) {
      var msg = "Integer [%d] does not match actual number of inputs [%d].";
      throw new IllegalArgumentException(String.format(msg, numberOfLines, getLines().size()));
    }
    for (var idx = 0; idx < numberOfLines; idx++) {
      var params = KataUtils.createList(getLines().get(idx), " ", KataEnums.INTEGER);
      var current = (Integer) params.getFirst();
      var end = (Integer) params.get(1);
      var count = 0;
      while (current <= end) {
        if (isPrime(current) && isPrime(sumDigits(current))) {
          count++;
        }
        current++;
      }
      results.add(count);
    }
  }

  /**
   * Returns the results of processing the input file for this kata.
   *
   * @return The list of results
   */
  public List<Integer> getResults() {
    return new ArrayList<>(results);
  }

  /**
   * Sums all the digits in the given number.</p>
   * 
   * For example, <code>41 -> 4 + 1 = 5</code>.</p>
   * 
   * @param number The number whose digits are summed together.
   * @return The sum of all the digits.
   */
  private static int sumDigits(int number) {
    return (int) Integer.toString(number).chars()
            .mapToObj(i -> Integer.parseInt(Character.toString(i)))
            .collect(Collectors.summarizingInt(Integer::intValue)).getSum();
  }

  /**
   * Tests whether the given number is prime.
   * 
   * @param number The number to test for primality.
   * @return True if the number is prime, false otherwise.
   * @see <a href="https://en.wikipedia.org/wiki/Primality_test#Pseudocode">Pseudocode</a>
   */
  private static boolean isPrime(int number) {
    if (number <= 1) {
      return false;
    }
    if (number <= 3) {
      return true;
    }
    if (number % 2 == 0 || number % 3 == 0) {
      return false;
    }

    for (var i = 5; i * i <= number; i += 6) {
      if (number % i == 0 || number % (i + 2) == 0) {
        return false;
      }
    }
    return true;
  }

}
