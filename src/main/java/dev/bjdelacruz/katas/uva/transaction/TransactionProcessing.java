/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva.transaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import dev.bjdelacruz.katas.utils.Kata;
import dev.bjdelacruz.katas.utils.KataUtils;

/**
 * Processes a file containing a list of transactions and finds those that are not balanced.
 * 
 * @author BJ Peter DeLaCruz
 * @see <a href="https://bjdelacruz.dev/files/katas/187_Transaction_Processing.pdf">Transaction
 * Processing</a>
 */
final class TransactionProcessing extends Kata {

  private static final int NUM_SPACES = 50;
  private final Map<Integer, List<Transaction>> transactions = new HashMap<>();
  private final Map<Integer, Account> accounts = new HashMap<>();

  /**
   * Inputs all accounts and transactions, and processes all transactions by summing all the
   * items in each transaction. If the sum is not equal to zero, i.e. a transaction is not balanced,
   * information about that transaction will be printed to the screen.
   */
  @Override
  public void processLines() {
    System.out.println(process());
  }

  /**
   * Inputs all accounts and transactions, and processes all transactions by summing all the
   * items in each transaction. If the sum is not equal to zero, i.e. a transaction is not balanced,
   * information about that transaction will be in the string that is returned.
   * 
   * @return The string that contains all transactions that are not balanced.
   */
  public String process() {
    inputAccounts();
    inputTransactions();
    return processTransactions();
  }

  /**
   * Inputs all account information read in from a file into a HashMap.
   */
  private void inputAccounts() {
    while (!getLines().getFirst().contains("000")) {
      var info = getLines().removeFirst();
      var accountNo = Integer.parseInt(info.substring(0, 3));
      accounts.put(accountNo, new Account(accountNo, info.substring(3)));
    }
    getLines().removeFirst();
  }

  /**
   * Inserts all transactions read in from a file into a HashMap.
   */
  private void inputTransactions() {
    while (!getLines().isEmpty()) {
      var line = getLines().removeFirst();
      var tokenizer = new StringTokenizer(line, " ");
      if (tokenizer.countTokens() != 2) {
        throw new IllegalArgumentException("Transaction is in incorrect format: " + line);
      }
      var info = tokenizer.nextToken();
      if ("000000".equals(info)) {
        break;
      }
      var accountNo = Integer.parseInt(info.substring(3, 6));
      var transactionNo = Integer.parseInt(info.substring(0, 3));
      var balance = Integer.parseInt(tokenizer.nextToken());
      var transaction = new Transaction(accountNo, transactionNo, balance);
      transactions.computeIfAbsent(Integer.parseInt(info.substring(0, 3)), k -> new ArrayList<>());
      transactions.get(Integer.parseInt(info.substring(0, 3))).add(transaction);
    }
  }

  /**
   * Sums all the transactions and returns information about those that are out of balance.
   * 
   * @return A string that contains information about transactions that are not balanced.
   */
  private String processTransactions() {
    var builder = new StringBuilder();
    for (var entry : transactions.entrySet()) {
      var sum = entry.getValue().stream().mapToInt(Transaction::getBalance).sum();
      builder.append(getResults(entry, Math.abs(sum)));
    }
    return builder.toString();
  }

  /**
   * Prints information about the transactions that are out of balance.
   * 
   * @param entry Contains the transaction ID and a list of transactions.
   * @param sum A value greater or less than, but not equal to, zero.
   * @return A string displaying a list of transactions that are out of balance.
   */
  private String getResults(Entry<Integer, List<Transaction>> entry, int sum) {
    if (sum == 0) {
      return "Transaction " + entry.getKey() + " is not out of balance.";
    }

    var builder = new StringBuilder();

    builder.append("*** Transaction ").append(entry.getKey()).append(" is out of balance ***\n");

    for (var t : entry.getValue()) {
      var temp = t.getAccountNo() + " " + accounts.get(t.getAccountNo()).getAccountDescr();
      builder.append(temp);
      var index = temp.length() + KataUtils.getBalanceAsString(t.getBalance()).length();
      while (index++ < NUM_SPACES) {
        builder.append(" ");
      }
      builder.append(KataUtils.getBalanceAsString(t.getBalance())).append("\n");
    }

    var temp = "999 Out of Balance";
    builder.append(temp);
    var balance = KataUtils.getBalanceAsString(sum);
    builder.append(" ".repeat(Math.max(0, NUM_SPACES - (temp.length() + balance.length()))));
    builder.append(balance).append("\n");

    return builder.toString();
  }

}
