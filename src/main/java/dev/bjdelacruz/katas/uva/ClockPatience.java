/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dev.bjdelacruz.katas.utils.Kata;
import dev.bjdelacruz.katas.utils.PlayingCard;
import dev.bjdelacruz.katas.utils.KataUtils;

/**
 * This class creates a card game in which the computer takes a card from the top of a pile, flips
 * it over, and then puts it under another pile. See {@link #playGame()} for rules on how this game
 * is played.
 * 
 * @see <a href="https://bjdelacruz.dev/files/katas/170_Clock_Patience.pdf">Clock
 * Patience</a>
 * 
 * @author BJ Peter DeLaCruz
 */
public class ClockPatience extends Kata {

  private List<PlayingCard> playingCards;
  private final Map<Integer, List<PlayingCard>> clock = new HashMap<>();
  private static final int NUM_DECKS = 13;

  /**
   * This constructor will set up the piles of cards.
   */
  public ClockPatience() {
    for (var index = 1; index <= NUM_DECKS; index++) {
      clock.put(index, new ArrayList<PlayingCard>());
    }
  }

  /**
   * Creates a set of 52 playing cards, deals them, and then plays the game. See {@link #playGame()}
   * .
   */
  @Override
  public void processLines() {
    playingCards = KataUtils.createPlayingCards(getLines());

    dealCards();

    playGame();

    if (areAllCardsFlippedOver()) {
      System.out.println("* You win! *");
    }
  }

  /**
   * Deals the cards by placing each card in a pile until there are thirteen piles and four cards
   * per pile.
   */
  private void dealCards() {
    Collections.reverse(playingCards);

    var index = 1;
    for (var card : playingCards) {
      clock.get(index++).add(card);
      if (index > NUM_DECKS) {
        index = 1;
      }
    }

    for (index = 1; index <= NUM_DECKS; index++) {
      Collections.reverse(clock.get(index));
    }
  }

  /**
   * Takes the top card from a pile, flips it over (i.e. the card is now facing up), and places it
   * at the bottom of the pile corresponding to the value of the card. Then the top card from the
   * pile under which the previous card was placed is taken, flipped over, and placed at the bottom
   * of another pile corresponding to the value of that card. This process continues until all cards
   * in the pile under which the card that was just played are flipped over.
   */
  private void playGame() {
    var count = 1;
    var currentCard = clock.get(NUM_DECKS).removeFirst();
    currentCard.flipCardOver();
    var position = getClockPosition(currentCard);
    clock.get(position).add(currentCard);

    while (clock.get(position).getFirst().isCardFacingDown()) {
      currentCard = clock.get(position).removeFirst();
      currentCard.flipCardOver();
      position = getClockPosition(currentCard);
      clock.get(position).add(currentCard);
      count++;
    }

    printClock();

    System.out.println("\n" + count + "," + currentCard.getCard());
  }

  /**
   * Prints all the playing cards in each of the thirteen piles.
   */
  private void printClock() {
    for (var index = 1; index <= NUM_DECKS; index++) {
      var cards = clock.get(index);
      System.out.print(index + ":\t");
      for (var pos = 0; pos < cards.size() - 1; pos++) {
        System.out.print(cards.get(pos) + " ");
      }
      System.out.println(cards.get(cards.size() - 1));
    }
  }

  /**
   * Returns a number from 1 to 13 given a playing card (Ace = 1, T = 10, Jack = 11, Queen = 12, King = 13).
   * 
   * @param card The playing card.
   * @return A number from 1 to 13.
   */
  private int getClockPosition(PlayingCard card) {
    var value = card.getCardValue();
    if (value >= '1' && value <= '9') {
      return Integer.parseInt(value + "");
    }
    return switch (value) {
      case 'A' -> 1;
      case 'T' -> 10;
      case 'J' -> 11;
      case 'Q' -> 12;
      case 'K' -> 13;
      default -> throw new IllegalArgumentException("Invalid character: " + value);
    };
  }

  /**
   * Returns true if all cards are flipped over (i.e. all cards on the top of each pile are facing up), false otherwise.
   * 
   * @return True if all cards are flipped over, false otherwise.
   */
  private boolean areAllCardsFlippedOver() {
    for (var index = 1; index <= NUM_DECKS; index++) {
      if (clock.get(index).getFirst().isCardFacingDown()) {
        return false;
      }
    }
    return true;
  }

}
