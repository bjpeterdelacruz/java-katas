/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva.minesweeper;

import dev.bjdelacruz.commons.utils.math.Point;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * A class representing a square in the Minesweeper game.
 * 
 * @author BJ Peter DeLaCruz &lt;bj.peter.delacruz@gmail.com&gt;
 * 
 * @param <X> Row number.
 * @param <Y> Column number.
 */
@SuppressFBWarnings("SE_NO_SERIALVERSIONID")
final class MinesweeperSquare<X extends Number, Y extends Number> extends Point<X, Y> {

  private final boolean mineLocatedHere;
  private int numSurroundingMines;

  /**
   * Constructs a new MinesweeperSquare.
   * 
   * @param x The row number.
   * @param y The column number.
   * @param mineLocatedHere True if a mine is located in this square, false otherwise.
   */
  MinesweeperSquare(X x, Y y, boolean mineLocatedHere) {
    super(x, y);
    this.mineLocatedHere = mineLocatedHere;
    numSurroundingMines = mineLocatedHere ? Integer.MIN_VALUE : 0;
  }

  /**
   * Increments the number of squares that the current square is next to that contain mines.
   */
  public void incrementNumSurroundingMines() {
    this.numSurroundingMines++;
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    var msg = "MinesweeperSquare[Row: " + this.getX() + " - Column: " + this.getY() + " " + this.mineLocatedHere;
    return msg + (this.mineLocatedHere ? "]" : " - " + this.numSurroundingMines + "]");
  }

  /** {@inheritDoc} */
  @Override
  public boolean equals(Object o) {
    if (o == null || this.getClass() != o.getClass()) {
      return false;
    }
    @SuppressWarnings("unchecked")
    var square = (MinesweeperSquare<Number, Number>) o;
    return this.getX().equals(square.getX()) && this.getY().equals(square.getY())
            && this.mineLocatedHere == square.mineLocatedHere
            && this.numSurroundingMines == square.numSurroundingMines;
  }

  /** {@inheritDoc} */
  @Override
  public int hashCode() {
    return this.getX().hashCode() + this.getY().hashCode() + (this.mineLocatedHere ? 1 : 0) + this.numSurroundingMines;
  }

  public boolean isMineLocatedHere() {
    return mineLocatedHere;
  }

  public int getNumSurroundingMines() {
    return numSurroundingMines;
  }
}
