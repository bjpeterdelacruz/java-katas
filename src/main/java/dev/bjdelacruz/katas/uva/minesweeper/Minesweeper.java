/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva.minesweeper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import dev.bjdelacruz.katas.utils.Kata;

/**
 * A program that will display hint numbers for each grid in the Minesweeper game.
 * 
 * @author BJ Peter DeLaCruz &lt;bj.peter.delacruz@gmail.com&gt;
 * @see <a href="https://bjdelacruz.dev/files/katas/10189_Minesweeper.pdf">Minesweeper</a>
 */
final class Minesweeper extends Kata {

  private final Map<Integer, List<MinesweeperSquare<Number, Number>>> minesweeperGrid = new HashMap<>();
  private int numRows;
  private int numColumns;

  /** {@inheritDoc} */
  @Override
  public void processLines() {
    var numGrids = 1;
    while (!this.getLines().isEmpty()) {
      var info = this.getLines().removeFirst();
      var tokenizer = new StringTokenizer(info);
      if (tokenizer.countTokens() != 2) {
        var msg = "Expected 2 arguments: number of rows and number of columns.";
        throw new IllegalArgumentException(msg);
      }

      this.numRows = Integer.parseInt(tokenizer.nextToken());
      this.numColumns = Integer.parseInt(tokenizer.nextToken());
      var rowCount = 1;
      if (numRows != getLines().size()) {
        throw new IllegalArgumentException("Expected " + this.numRows + " rows. Found " + getLines().size() + ".");
      }
      for (var index = 0; index < this.numRows && !this.getLines().isEmpty(); index++, rowCount++) {
        var line = this.getLines().removeFirst();
        var columnCount = addRow(line, rowCount);
        if (columnCount != this.numColumns) {
          throw new IllegalArgumentException("Expected " + this.numColumns + " columns. Found " + columnCount + ".");
        }
      }

      minesweeperGrid.forEach((k, v) -> v.forEach(this::processMine));

      // printGrid();
      // System.out.println();
      System.out.println("Field #" + numGrids++);
      printGridHints();
      System.out.println();
      // this.minesweeperGrid.clear();
    }
  }

  /**
   * Prints the grid with hint numbers.
   */
  private void printGridHints() {
    minesweeperGrid.forEach((k, v) -> {
      v.forEach(
          square -> System.out.print(square.isMineLocatedHere() ? "*" : square
              .getNumSurroundingMines()));
      System.out.println();
    });
  }

  /**
   * Adds the given line to the grid.
   * 
   * @param line The line that contains the row to add.
   * @param rowCount The current row number.
   * @return The number of columns.
   */
  int addRow(String line, int rowCount) {
    var tokenizer = new StringTokenizer(line);
    var counter = 1;
    while (tokenizer.hasMoreTokens()) {
      var token = tokenizer.nextToken();
      MinesweeperSquare<Number, Number> square;
      if ("*".equals(token)) {
        square = new MinesweeperSquare<>(rowCount, counter, true);
      }
      else if (".".equals(token)) {
        square = new MinesweeperSquare<>(rowCount, counter, false);
      }
      else {
        throw new IllegalArgumentException("Invalid character: " + token);
      }
      if (!this.minesweeperGrid.containsKey(rowCount)) {
        this.minesweeperGrid.put(rowCount, new ArrayList<>());
      }
      this.minesweeperGrid.get(rowCount).add(square);
      counter++;
    }
    return counter - 1;
  }

  /**
   * Given a square that contains a mine, updates the number of mines in the surrounding squares.
   * 
   * @param square The square that contains a mine.
   */
  void processMine(MinesweeperSquare<Number, Number> square) {
    if (!square.isMineLocatedHere()) {
      return;
    }

    /** Corners **/
    if (square.getX().intValue() == 1 && square.getY().intValue() == 1) {
      // Right neighbor
      incrementCount(square.getX().intValue(), square.getY().intValue());
      // Right-bottom neighbor
      incrementCount(square.getX().intValue() + 1, square.getY().intValue());
      // Bottom neighbor
      incrementCount(square.getX().intValue() + 1, square.getY().intValue() - 1);
    }
    else if (square.getX().intValue() == 1 && square.getY().intValue() == this.numColumns) {
      // Left neighbor
      incrementCount(square.getX().intValue(), square.getY().intValue() - 2);
      // Left-bottom neighbor
      incrementCount(square.getX().intValue() + 1, square.getY().intValue() - 2);
      // Bottom neighbor
      incrementCount(square.getX().intValue() + 1, square.getY().intValue() - 1);
    }
    else if (square.getX().intValue() == this.numRows
        && square.getY().intValue() == this.numColumns) {
      // Left neighbor
      incrementCount(square.getX().intValue(), square.getY().intValue() - 2);
      // Left-top neighbor
      incrementCount(square.getX().intValue() - 1, square.getY().intValue() - 2);
      // Top neighbor
      incrementCount(square.getX().intValue() - 1, square.getY().intValue() - 1);
    }
    else if (square.getX().intValue() == this.numRows && square.getY().intValue() == 1) {
      // Right neighbor
      incrementCount(square.getX().intValue(), square.getY().intValue());
      // Right-top neighbor
      incrementCount(square.getX().intValue() - 1, square.getY().intValue());
      // Top neighbor
      incrementCount(square.getX().intValue() - 1, square.getY().intValue() - 1);
    }
    /** Sides **/
    else if (square.getX().intValue() == 1) {
      // Left neighbor
      incrementCount(square.getX().intValue(), square.getY().intValue() - 2);
      // Left-bottom neighbor
      incrementCount(square.getX().intValue() + 1, square.getY().intValue() - 2);
      // Bottom neighbor
      incrementCount(square.getX().intValue() + 1, square.getY().intValue() - 1);
      // Right-bottom neighbor
      incrementCount(square.getX().intValue() + 1, square.getY().intValue());
      // Right neighbor
      incrementCount(square.getX().intValue(), square.getY().intValue());
    }
    else if (square.getX().intValue() == this.numRows) {
      // Left neighbor
      incrementCount(square.getX().intValue(), square.getY().intValue() - 2);
      // Left-top neighbor
      incrementCount(square.getX().intValue() - 1, square.getY().intValue() - 2);
      // Top neighbor
      incrementCount(square.getX().intValue() - 1, square.getY().intValue() - 1);
      // Right-top neighbor
      incrementCount(square.getX().intValue() - 1, square.getY().intValue());
      // Right neighbor
      incrementCount(square.getX().intValue(), square.getY().intValue());
    }
    else if (square.getY().intValue() == 1) {
      // Top neighbor
      incrementCount(square.getX().intValue() - 1, square.getY().intValue() - 1);
      // Right-top neighbor
      incrementCount(square.getX().intValue() - 1, square.getY().intValue());
      // Right neighbor
      incrementCount(square.getX().intValue(), square.getY().intValue());
      // Right-bottom neighbor
      incrementCount(square.getX().intValue() + 1, square.getY().intValue());
      // Bottom neighbor
      incrementCount(square.getX().intValue() + 1, square.getY().intValue() - 1);
    }
    else if (square.getY().intValue() == this.numColumns) {
      // Bottom neighbor
      incrementCount(square.getX().intValue() + 1, square.getY().intValue() - 1);
      // Left-bottom neighbor
      incrementCount(square.getX().intValue() + 1, square.getY().intValue() - 2);
      // Left neighbor
      incrementCount(square.getX().intValue(), square.getY().intValue() - 2);
      // Left-top neighbor
      incrementCount(square.getX().intValue() - 1, square.getY().intValue() - 2);
      // Top neighbor
      incrementCount(square.getX().intValue() - 1, square.getY().intValue() - 1);
    }
    /** Anywhere else inside the grid. */
    else {
      // Left neighbor
      incrementCount(square.getX().intValue(), square.getY().intValue() - 2);
      // Left-top neighbor
      incrementCount(square.getX().intValue() - 1, square.getY().intValue() - 2);
      // Top neighbor
      incrementCount(square.getX().intValue() - 1, square.getY().intValue() - 1);
      // Right-top neighbor
      incrementCount(square.getX().intValue() - 1, square.getY().intValue());
      // Right neighbor
      incrementCount(square.getX().intValue(), square.getY().intValue());
      // Right-bottom neighbor
      incrementCount(square.getX().intValue() + 1, square.getY().intValue());
      // Bottom neighbor
      incrementCount(square.getX().intValue() + 1, square.getY().intValue() - 1);
      // Left-bottom neighbor
      incrementCount(square.getX().intValue() + 1, square.getY().intValue() - 2);
    }
  }

  /**
   * Increments the number of mines that the square at (row, column) is next to.
   * 
   * @param row The row number.
   * @param column The column number.
   */
  private void incrementCount(int row, int column) {
    if (this.minesweeperGrid.containsKey(row) && column > -1
        && column < this.minesweeperGrid.get(row).size()
        && !this.minesweeperGrid.get(row).get(column).isMineLocatedHere()) {
      this.minesweeperGrid.get(row).get(column).incrementNumSurroundingMines();
    }
  }

  /**
   * Sets the number of rows.
   * 
   * @param numRows The number of rows.
   */
  void setNumRows(int numRows) {
    this.numRows = numRows;
  }

  /**
   * Sets the number of columns.
   * 
   * @param numColumns The number of columns.
   */
  void setNumColumns(int numColumns) {
    this.numColumns = numColumns;
  }

  /**
   * Returns the square at (row, column) in the grid.
   * 
   * @param row The row number.
   * @param col The column number.
   * @return The square at (row, column) in the grid.
   */
  MinesweeperSquare<Number, Number> getSquareAt(int row, int col) {
    if (minesweeperGrid.containsKey(row) && col >= 1 && col <= minesweeperGrid.get(row).size()) {
      return minesweeperGrid.get(row).get(col - 1);
    }

    return null;
  }

}
