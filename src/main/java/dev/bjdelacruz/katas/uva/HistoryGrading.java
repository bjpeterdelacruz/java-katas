/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Contains methods that will read in students' responses and grade them.
 * 
 * @see <a href="https://bjdelacruz.dev/files/katas/111_History_Grading.pdf">History
 * Grading</a>
 * 
 * @author BJ Peter DeLaCruz &lt;bj.peter.delacruz@gmail.com&gt;
 */
public class HistoryGrading {

  /** Line containing a student's responses. */
  private String line;
  /** Total number of historical events. */
  private int length;
  /** List of historical events. */
  private final List<String> historicalEvents = new ArrayList<>();
  /** List of one student's responses. */
  private final List<String> studentResponses = new ArrayList<>();
  /** List of all students' responses. */
  private final List<String> allResponses = new ArrayList<>();
  /** List of all students' grades. */
  private final List<Integer> grades = new ArrayList<>();

  /**
   * Given a filename, gets the total number of historical events, the historical events in the
   * correct order, and the students' responses from the file. Grade a student's responses before
   * reading in the next student's responses. The number of responses by the student must match the
   * total number of historical events. If a problem is encountered, return immediately.
   * 
   * @param filename Name of the file containing the students' responses to grade.
   * @return True if no problems are encountered, false otherwise.
   */
  public boolean readFile(String filename) {
    if ("".equals(filename) || filename == null) {
      throw new IllegalArgumentException("Filename must not be null or empty.");
    }

    var file = new File(filename);

    try (var reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8))) {

      processTotalHistoricalEvents(reader);

      if (!processHistoricalEvents(reader) || !processStudentResponses(reader)) {
        return false;
      }
    }
    catch (IOException ioe) {
      throw new RuntimeException("A problem occurred while trying to process " + filename + ".");
    }

    return true;
  }

  /**
   * Gets the line containing the total number of historical events.
   * 
   * @param reader Used to read in a line.
   * @throws IOException If there are problems reading in the file.
   */
  private void processTotalHistoricalEvents(BufferedReader reader) throws IOException {
    line = reader.readLine();
    if (line == null) {
      throw new RuntimeException("Empty file.");
    }

    try {
      length = Integer.parseInt(line);
      if (length < 2 || length > 20) {
        throw new IllegalArgumentException("Length must be between 2 and 20, inclusive.");
      }
    }
    catch (NumberFormatException e) {
      throw new RuntimeException("First line is not a number.");
    }
  }

  /**
   * Gets the line containing the historical events in the correct order.
   * 
   * @param reader Used to read in a line.
   * @return True on success, false otherwise.
   * @throws IOException If there are problems reading in the file.
   */
  private boolean processHistoricalEvents(BufferedReader reader) throws IOException {
    line = reader.readLine();
    if (line == null) {
      throw new RuntimeException("Missing line containing correct order of historical events.");
    }

    var tokenizer = new StringTokenizer(line, " ");
    if (tokenizer.countTokens() != length) {
      var msg = "Expected " + length + " historical events. Found " + tokenizer.countTokens();
      throw new RuntimeException(msg + ".");
    }
    while (tokenizer.hasMoreTokens()) {
      historicalEvents.add(tokenizer.nextToken());
    }

    return !duplicatesExist(historicalEvents);
  }

  /**
   * Gets the line containing a student's responses.
   * 
   * @param reader Used to read in a line.
   * @return True on success, false otherwise.
   * @throws IOException If there are problems reading in the file.
   */
  private boolean processStudentResponses(BufferedReader reader) throws IOException {
    line = reader.readLine();
    if (line == null) {
      throw new RuntimeException("Need at least one student's responses.");
    }

    while (line != null) {
      var tokenizer = new StringTokenizer(line, " ");
      if (tokenizer.countTokens() != length) {
        var msg = "Expected " + length + " responses from student. Found " + tokenizer.countTokens() + ".";
        throw new RuntimeException(msg);
      }
      while (tokenizer.countTokens() > 0) {
        studentResponses.add(tokenizer.nextToken());
      }
      if (duplicatesExist(studentResponses)) {
        return false;
      }
      grades.add(gradeResponses());
      allResponses.addAll(studentResponses);
      allResponses.add("\n");
      studentResponses.clear();
      // System.out.println(line + "\n");
      line = reader.readLine();
    }

    return true;
  }

  /**
   * Checks whether duplicates exist in a list.
   * 
   * @param list List in which to check whether duplicates exist.
   * @return True if duplicates exist or list is null or empty, false otherwise.
   */
  private boolean duplicatesExist(List<String> list) {
    if (list == null || list.isEmpty()) {
      throw new RuntimeException("List is null or empty.");
    }

    return new HashSet<>(list).size() != list.size();
  }

  /**
   * Iterates through the <code>historicalEvents</code> and <code>studentResponses</code> lists
   * twice to find the length of the longest sequence of events that are in the correct order; the
   * length that is returned is the student's grade.
   * 
   * @return The length of the longest sequence of events that are in the correct order, i.e. the
   * student's grade.
   */
  private Integer gradeResponses() {
    var gradeFirstPass = checkResponses(false);
    var gradeSecondPass = checkResponses(true);
    return Math.max(gradeFirstPass, gradeSecondPass);
  }

  /**
   * Iterates through the <code>historicalEvents</code> and <code>studentResponses</code> lists to
   * find the length of the sequence of events that are in the correct order; the length that is
   * returned is the student's grade.
   * 
   * @param useHistoricalEvents True to remove an event from the <code>historicalEvents</code> list
   * first or false to remove the event from the <code>studentResponses</code> list first; the event
   * that is removed is then removed from the other list as well.
   * @return Student's grade.
   */
  private int checkResponses(boolean useHistoricalEvents) {
    var tempHistoricalEvents = new ArrayList<>(historicalEvents);
    var tempStudentResponses = new ArrayList<>(studentResponses);
    var grade = 0;

    var firstStudentResponse = tempStudentResponses.getFirst();
    while (!firstStudentResponse.equals(tempHistoricalEvents.getFirst())) {
      var temp = tempHistoricalEvents.getFirst();
      tempStudentResponses.remove(temp);
      tempHistoricalEvents.remove(temp);
      // System.out.println("** Removed " + temp + ". **");
    }

    for (var index = 0; index < tempStudentResponses.size(); index++) {
      if (tempStudentResponses.get(index).equals(tempHistoricalEvents.get(index))) {
        grade++;
      }
      else {
        var historicalEvent = tempHistoricalEvents.get(index);
        var studentResponse = tempStudentResponses.get(index);
        var temp = useHistoricalEvents ? historicalEvent : studentResponse;
        tempHistoricalEvents.remove(temp);
        tempStudentResponses.remove(temp);
        // System.out.println("Removed " + temp + ".");
        index--;
      }
    }

    return grade;
  }

  /**
   * Prints the results of grading all students' responses to the screen.
   */
  public void printResults() {
    System.out.println("Historical Events:");
    for (var event : historicalEvents) {
      System.out.print(event + " ");
    }
    System.out.println("\n\nStudents' Responses:");
    for (var response : allResponses) {
      System.out.print(response);
      if (!"\n".equals(response)) {
        System.out.print(" ");
      }
    }
    System.out.println("\nGrades:");
    for (var grade : grades) {
      System.out.println(grade);
    }
  }

}
