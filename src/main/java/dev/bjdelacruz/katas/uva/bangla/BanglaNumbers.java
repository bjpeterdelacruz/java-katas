/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva.bangla;

import java.util.ArrayList;
import java.util.List;

import dev.bjdelacruz.katas.utils.Kata;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * This program converts a number into a Bangla number:<br>
 * <br>
 * 10^2 = shata<br>
 * 10^3 = hajar<br>
 * 10^5 = lakh<br>
 * 10^7 = kuti<br>
 * <br>
 * The number to convert cannot be greater than 999,999,999,999,999.
 * 
 * @see <a href="https://bjdelacruz.dev/files/katas/10101_Bangla_Numbers.pdf">Bangla Numbers</a>
 * 
 * @author BJ Peter DeLaCruz &lt;bj.peter.delacruz@gmail.com&gt;
 */
final class BanglaNumbers extends Kata {

  private static final long MAX = 999_999_999_999_999L;
  private static final String SHATA = " shata ";
  private static final String HAJAR = " hajar ";
  private static final String LAKH = " lakh ";
  private static final String KUTI = " kuti ";

  /** {@inheritDoc} */
  @SuppressWarnings({"checkstyle:missingswitchdefault"})
  @SuppressFBWarnings("SF_SWITCH_NO_DEFAULT")
  @Override
  public void processLines() {
    while (!getLines().isEmpty()) {
      var number = Long.parseLong(getLines().removeFirst());
      if (number > MAX) {
        throw new IllegalArgumentException(number + " is greater than " + MAX);
      }
      var numbers = Long.toString(number).toCharArray();

      var characters = new ArrayList<Character>();
      for (int index = numbers.length - 1, counter = 1; index > -1; index--, counter++) {
        characters.addFirst(numbers[index]);
        if (index == 0) {
          break;
        }
        switch (counter) {
          case 2, 9 -> characters = insertWord(SHATA, characters);
          case 3, 10 -> characters = insertWord(HAJAR, characters);
          case 5, 12 -> characters = insertWord(LAKH, characters);
          case 7, 14 -> characters = insertWord(KUTI, characters);
        }
      }
      characters.forEach(System.out::print);
      System.out.println();
    }
  }

  /**
   * Returns a new list with all the characters in the given word plus all the characters in the
   * given list.
   * 
   * @param word The word whose characters are to be inserted into a new list.
   * @param characters A list whose characters are to be inserted into the new list after the
   * characters in the given word.
   * @return A list with all the characters in the given word plus all the characters in the
   * given list.
   */
  private static ArrayList<Character> insertWord(String word, List<Character> characters) {
    var builder = new StringBuilder();
    characters.forEach(builder::append);

    var temp = new ArrayList<Character>();
    (word + builder).chars().mapToObj(i -> (char) i).forEach(temp::add);
    return temp;
  }

}
