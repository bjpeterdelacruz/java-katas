/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import dev.bjdelacruz.katas.utils.Kata;

/**
 * Solves linear equations with only one variable.
 * 
 * @author BJ Peter DeLaCruz &lt;bj.peter.delacruz@gmail.com&gt;
 * @see <a href="https://bjdelacruz.dev/files/katas/1200_Linear_Equation.pdf">Linear
 * Equation</a>
 */
public class LinearEquationSolver extends Kata {

  private final List<String> results = new ArrayList<>();

  /** {@inheritDoc} */
  @Override
  public void processLines() {
    int numLines;
    try {
      numLines = Integer.parseInt(getLines().removeFirst());
    }
    catch (NumberFormatException e) {
      throw new IllegalArgumentException("Expected an integer for number of equations to solve.");
    }

    if (numLines != getLines().size()) {
      throw new IllegalArgumentException("Expected " + numLines + " equations. Found " + getLines().size() + ".");
    }

    while (!getLines().isEmpty()) {
      var equation = getLines().removeFirst();

      if (!equation.contains("=")) {
        throw new IllegalArgumentException("Invalid equation: " + equation + " (missing right-hand side)");
      }

      var variablesL = new ArrayList<String>();
      var constantsL = new ArrayList<String>();
      var variablesR = new ArrayList<String>();
      var constantsR = new ArrayList<String>();

      Arrays.stream(equation.split("=")).forEach(s -> validateSide(s.trim()));

      // Process left-hand side first.
      var tokenizer = new StringTokenizer(equation);
      while (tokenizer.hasMoreTokens()) {
        if (processSide(tokenizer, variablesL, constantsL)) {
          break;
        }
      }

      // Then process right-hand side.
      while (tokenizer.hasMoreTokens()) {
        processSide(tokenizer, variablesR, constantsR);
      }

      solve(variablesL, constantsL, variablesR, constantsR);
    }
  }

  /**
   * Solves the current equation by processing the lists of variables and constants.
   * 
   * @param variablesL The list of variables that appear on the left-hand side.
   * @param constantsL The list of constants that appear on the right-hand side.
   * @param variablesR The list of variables that appear on the left-hand side.
   * @param constantsR The list of constants that appear on the right-hand side.
   * @return <ul>
   * <li><b>NaN</b> if the equation cannot be solved.</li>
   * <li><b>Infinity</b> if there are an infinite number of solutions.</li>
   * <li>The solution, if one exists.</li>
   * </ul>
   */
  double solve(List<String> variablesL, List<String> constantsL,
      List<String> variablesR, List<String> constantsR) {
    // Sum variables and constants on left-hand side first and then right-hand side.
    var variableL = sumList(variablesL);
    var constantL = sumList(constantsL);

    var variableR = sumList(variablesR);
    var constantR = sumList(constantsR);

    // Solve for each side.
    var variableSum = variableL - variableR;
    var constantSum = constantR - constantL;
    if (variableSum < 0 && constantSum < 0) {
      variableSum *= -1;
      constantSum *= -1;
    }
    else if (constantR == 0 && constantL < 0) {
      constantSum *= -1;
    }

    // Print results.
    if (variableSum == 0 && constantSum == 0) {
      results.add("IDENTITY");
      return Double.POSITIVE_INFINITY;
    }
    else if (variableSum == 0) {
      results.add("IMPOSSIBLE");
      return Double.NaN;
    }
    else if (constantSum == 0) {
      results.add("0");
      return 0.0;
    }
    else {
      var result = variableSum > 1 ? (double) constantSum / (double) variableSum : constantSum;
      results.add(String.valueOf((int) result));
      return result;
    }
  }

  /**
   * Returns a list of results.
   *
   * @return A list of results.
   */
  public List<String> getResults() {
    return new ArrayList<>(results);
  }

  /**
   * Returns the sum of the contents of a given list.
   * 
   * @param strings The list of variables or constants whose contents will be summed together.
   * @return The sum of the contents of the given list.
   */
  private static int sumList(List<String> strings) {
    return strings.stream().mapToInt(LinearEquationSolver::extractConstant).sum();
  }

  /**
   * Validates a side of an equation. Throws an IllegalArgumentException if it is not valid, i.e.
   * missing operators between operands.
   * 
   * @param s The side to validate.
   */
  private static void validateSide(String s) {
    var array = s.split("\\s+");
    for (var idx = 1; idx < array.length; idx += 2) {
      if (!array[idx].equals("+") && !array[idx].equals("-")) {
        throw new IllegalArgumentException("Expected + or - between operands.");
      }
    }
  }

  /**
   * Processes a side of the equation.
   * 
   * @param tokenizer The StringTokenizer.
   * @param variables The list of variables to which a token will be added.
   * @param constants The list of constants to which a token will be added.
   * @return True if an '=' was reached (in which case the left-hand side of the equation is
   * finished processing), false otherwise (in which case the right-hand side is finished
   * processing).
   */
  static boolean processSide(StringTokenizer tokenizer, List<String> variables,
      List<String> constants) {
    var token = tokenizer.nextToken();

    if (token.indexOf('x') != -1) {
      variables.add(token);
      return false;
    }

    if ((token.contains("-") || token.contains("+")) && token.length() == 1) {
      if (tokenizer.hasMoreTokens()) {
        token += tokenizer.nextToken();
        if (token.contains("x")) {
          variables.add(token);
        }
        else {
          constants.add(token);
        }
      }
      else {
        throw new IllegalArgumentException("Invalid equation: missing operand.");
      }
    }
    else if ((token.contains("-") || token.contains("+")) && token.length() > 1) {
      constants.add(token);
    }
    else if (!token.contains("=") && token.length() >= 1) {
      constants.add(token);
    }
    else {
      return token.contains("=");
    }

    return false;
  }

  /**
   * Returns the constant that is in front of a variable.
   * 
   * @param variable A variable such as x.
   * @return The constant that is in front of the variable.
   */
  private static int extractConstant(String variable) {
    if (variable.contains("+x")) {
      return 1;
    }
    else if (variable.contains("-x")) {
      return -1;
    }
    else if ((variable.contains("-") || variable.contains("+")) && variable.contains("x")) {
      var idx = variable.contains("+") ? 1 : 0;
      return Integer.parseInt(variable.substring(idx, variable.indexOf('x')));
    }
    else if (variable.contains("x")) {
      if (variable.length() == 1) {
        return 1;
      }
      return Integer.parseInt(variable.substring(0, variable.indexOf('x')));
    }
    else {
      try {
        return Integer.parseInt(variable);
      }
      catch (NumberFormatException nfe) {
        throw new IllegalArgumentException("Invalid token: " + variable);
      }
    }
  }

}
