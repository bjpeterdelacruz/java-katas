/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import java.util.List;

import dev.bjdelacruz.katas.utils.Kata;
import dev.bjdelacruz.katas.utils.KataEnums;
import dev.bjdelacruz.katas.utils.KataUtils;
import dev.bjdelacruz.commons.utils.StringUtils;

/**
 * This program will form double leading crosses if common letters are found in each pair of
 * vertical and horizontal words.
 * 
 * @see <a href="https://bjdelacruz.dev/files/katas/159_Word_Crosses.pdf">Word Crosses</a>
 * 
 * @author BJ Peter DeLaCruz
 */
public class WordCrosses extends Kata {

  /** Number of spaces between each horizontal word. */
  private static final int NUM_SPACES = 3;

  /**
   * Constructs a new WordCross object.
   */
  public WordCrosses() {
    // Empty constructor.
  }

  /**
   * Makes double leading crosses with the words found on each line if each line contains four and
   * only four words, and also if there are common letters in each pair of vertical and horizontal
   * words.
   */
  @Override
  public void processLines() {
    while (!this.getLines().isEmpty()) {
      String line = this.getLines().removeFirst();
      if ("#".equals(line)) { // EOF
        return;
      }

      @SuppressWarnings("unchecked")
      List<String> strings = (List<String>) KataUtils.createList(line, " ", KataEnums.STRING);
      if (strings.size() != 4) {
        String msg = "Line does not contain four and only four words: " + line;
        throw new IllegalArgumentException(msg);
      }

      // positionNumbers is used to keep track of indices at which common letters are found.
      // positionNumbers[0] contains the index for the first horizontal word.
      // positionNumbers[1] contains the index for the second horizontal word.
      // positionNumbers[2] contains the index for the first vertical word.
      // positionNumbers[3] contains the index for the second vertical word.
      var positionNumbers = new int[4];

      // Process the next line since no double leading crosses could be made.
      if (!areCommonCharsFound(strings, positionNumbers)) {
        continue;
      }

      printDoubleLeadingCrosses(strings, positionNumbers);
    }
  }

  /**
   * For each pair of vertical and horizontal words, finds the first common letter in each pair. The
   * letter that is found is located as near as possible towards the beginning of the horizontal
   * word.
   * 
   * @param strings The list of four words.
   * @param positionNumbers The integer array containing indices of common letters found.
   * @return True if common letters are found in <span style="text-decoration:underline">both</span>
   * pairs, false otherwise.
   */
  private boolean areCommonCharsFound(List<String> strings, int[] positionNumbers) {
    var hPos = 0;
    var vPos = 2;

    var isCommonCharFound = false;
    for (var index = 0; index < strings.size(); index += 2) {
      var horizontalLetters = StringUtils.toCharacterList(strings.get(index));
      var verticalLetters = StringUtils.toCharacterList(strings.get(index + 1));

      isCommonCharFound = false;
      for (Character c : horizontalLetters) {
        if (verticalLetters.contains(c)) {
          positionNumbers[hPos++] = horizontalLetters.indexOf(c) + 1;
          positionNumbers[vPos++] = verticalLetters.indexOf(c) + 1;
          isCommonCharFound = true;
          break;
        }
      }

      if (!isCommonCharFound) {
        System.out.println("Unable to make two crosses.\n");
        break;
      }
    }

    return isCommonCharFound;
  }

  /**
   * Prints double leading crosses if common letters are found in each pair of vertical and
   * horizontal words.
   * 
   * @param strings The list of four words.
   * @param positionNumbers The integer array containing indices of common letters found.
   */
  private void printDoubleLeadingCrosses(List<String> strings, int[] positionNumbers) {
    var numSpacesBetweenVerticalWords =
        strings.getFirst().length() - positionNumbers[0] + NUM_SPACES + positionNumbers[1] - 1;
    var verticalDistanceBetweenFirstLetters = positionNumbers[2] - positionNumbers[3];

    var firstVerticalWord = StringUtils.toCharacterList(strings.get(1));
    var secondVerticalWord = StringUtils.toCharacterList(strings.get(3));

    for (int index = 0, secondVerticalWordIndex = 0;; index++) {
      var isFirstVerticalWordPrinted = index >= firstVerticalWord.size();
      var isSecondVerticalWordPrinted = secondVerticalWordIndex >= secondVerticalWord.size();
      if (isFirstVerticalWordPrinted && isSecondVerticalWordPrinted) {
        System.out.println();
        break;
      }
      // Print the horizontal words once the index containing the common letter in both first
      // vertical and first horizontal words is reached.
      if (index == positionNumbers[2] - 1) {
        System.out.print(strings.getFirst());
        // Print spaces between the two horizontal words.
        for (int pos = 0; pos < NUM_SPACES; pos++) {
          System.out.print(" ");
        }
        System.out.println(strings.get(2));
        secondVerticalWordIndex++; // Skip to the next letter in the second vertical word.
        continue;
      }
      // Print spaces before printing a letter in the first vertical word.
      for (var pos = 0; pos < positionNumbers[0] - 1; pos++) {
        System.out.print(" ");
      }
      if (index < firstVerticalWord.size()) {
        System.out.print(firstVerticalWord.get(index));
      }
      // There are no more letters in the first vertical word to print, so print spaces
      // instead if there are more letters in the second vertical word to print.
      else {
        System.out.print(" ");
      }
      // Print spaces between the two vertical words.
      for (var pos = 0; pos < numSpacesBetweenVerticalWords; pos++) {
        System.out.print(" ");
      }
      // Once the vertical distance between the current letter in the first vertical word and the
      // first letter in the second vertical word is zero, i.e. index is now equal to the vertical
      // distance between the two first letters, start printing the letters in the second vertical
      // word. Otherwise, just print a newline character.
      if (index >= verticalDistanceBetweenFirstLetters) {
        if (secondVerticalWordIndex < secondVerticalWord.size()) {
          System.out.println(secondVerticalWord.get(secondVerticalWordIndex++));
        }
        else {
          System.out.println();
        }
      }
      else {
        System.out.println();
      }
    }
  }

}
