/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import dev.bjdelacruz.katas.utils.Kata;
import dev.bjdelacruz.katas.utils.KataEnums;
import dev.bjdelacruz.katas.utils.KataUtils;
import dev.bjdelacruz.commons.utils.StringUtils;

/**
 * Reads in a file containing one or more spreadsheets that contain formulas and outputs one
 * or more new spreadsheets that contain the results of calculating those formulas.
 * 
 * @see <a href="https://bjdelacruz.dev/files/katas/196_Spreadsheet.pdf">Spreadsheet</a>
 * 
 * @author BJ Peter DeLaCruz
 */
public class Spreadsheet extends Kata {

  /** Number of spreadsheets in a file. */
  private int numSpreadsheets;
  /** List of all spreadsheets in a file. */
  private final List<Object[][]> spreadsheets = new ArrayList<>();

  /**
   * Processes all spreadsheets found in a file, calculates all formulas found, and outputs new
   * spreadsheets to the screen.
   */
  @Override
  public void processLines() {
    for (var count = 0; count < this.numSpreadsheets; count++) {
      var line = this.getLines().removeFirst();
      @SuppressWarnings("unchecked")
      var dimensions = (List<Integer>) KataUtils.createList(line, " ", KataEnums.INTEGER);
      if (dimensions.size() != 2) {
        throw new IllegalArgumentException("Expected only 2 values: number of rows and columns.");
      }
      if (dimensions.getFirst() < 0 || dimensions.get(1) < 0) {
        throw new IllegalArgumentException("Negative values are not allowed for rows and columns.");
      }

      this.spreadsheets.add(new Object[dimensions.get(1)][dimensions.getFirst()]);

      populateSpreadsheet(dimensions);

      calculateFormulas();
    }

    System.out.println(StringUtils.get2dArrayContents(this.spreadsheets));
  }

  /**
   * Populates a spreadsheet after reading in all the lines from a file.
   * 
   * @param dimensions Dimensions of the spreadsheet (number of rows and columns).
   */
  private void populateSpreadsheet(List<Integer> dimensions) {
    for (var rowIndex = 0; rowIndex < dimensions.get(1); rowIndex++) {
      var tokenizer = new StringTokenizer(this.getLines().removeFirst(), " ");
      // Get recently added spreadsheet.
      var tempSpreadsheet = this.spreadsheets.get(this.spreadsheets.size() - 1);
      // Add values to each cell in row.
      var colIndex = 0;
      while (tokenizer.hasMoreTokens()) {
        var token = tokenizer.nextToken();
        try {
          tempSpreadsheet[rowIndex][colIndex] = Integer.parseInt(token);
        }
        catch (NumberFormatException e) {
          if (token.toCharArray()[0] == '=') {
            tempSpreadsheet[rowIndex][colIndex] = token;
          }
          else {
            throw new IllegalArgumentException("Illegal token found: " + token);
          }
        }
        colIndex++;
      }
    }
  }

  /**
   * Calculates all the formulas found in a spreadsheet.
   */
  private void calculateFormulas() {
    var rowIndex = 0;
    var colIndex = 0;
    for (var row : this.spreadsheets.get(this.spreadsheets.size() - 1)) {
      for (var element : row) {
        if (element instanceof String formula) {
          this.spreadsheets.get(this.spreadsheets.size() - 1)[rowIndex][colIndex] =
              this.doCalculation(formula, rowIndex, colIndex);
        }
        colIndex++;
      }
      colIndex = 0;
      rowIndex++;
    }
  }

  /**
   * Returns a value based on the formula found in the cell at the specified row and column.
   * 
   * @param formula Formula to calculate.
   * @param rowIndex Row number; used to detect cyclic dependencies.
   * @param colIndex Column number; used to detect cyclic dependencies.
   * @return Calculated value.
   */
  private int doCalculation(String formula, Integer rowIndex, Integer colIndex) {
    var coordinates = new ArrayList<List<Integer>>();
    var tokenizer = new StringTokenizer(formula.substring(1), "+"); // skip '='
    var sum = 0;
    while (tokenizer.hasMoreTokens()) {
      var token = tokenizer.nextToken();
      var coords = this.getCoordinates(token);
      if (rowIndex == coords.getFirst().intValue() && colIndex == coords.get(1).intValue()) {
        var msg = "Cyclic dependency detected in formula " + formula + ". ";
        msg += token + " refers to row " + (rowIndex + 1) + ", column " + (colIndex + 1) + ".";
        throw new IllegalArgumentException(msg);
      }
      coordinates.add(coords);
    }
    var spreadsheet = this.spreadsheets.get(this.spreadsheets.size() - 1);
    var maxRows = spreadsheet.length;
    var maxCols = spreadsheet[0].length;
    for (var coord : coordinates) {
      var row = coord.getFirst();
      var col = coord.get(1);
      if (row >= maxRows || col >= maxCols) {
        throw new IllegalArgumentException("row " + (row + 1) + ", column " + (col + 1) + " does not exist.");
      }
      // If a cell also contains a formula, calculate that formula first before calculating the
      // current one.
      if (spreadsheet[row][col] instanceof String value) {
        sum += this.doCalculation(value, row, col);
        continue;
      }
      sum += (Integer) spreadsheet[row][col];
    }
    return sum;
  }

  /**
   * Gets the numerical coordinates for a two-dimensional array given one or more letters (A-Z)
   * representing column numbers and one or more digits (0-9) representing row numbers.
   * 
   * @param coord The coordinate of the cell for which to get the row and column numbers.
   * @return A list containing the row and column numbers.
   */
  private List<Integer> getCoordinates(String coord) {
    var coords = new ArrayList<Integer>();
    var rowNum = 0;
    var rowMultiplier = 0;
    var colNum = new StringBuilder();
    for (var c : coord.toCharArray()) {
      if (c > 64 && c < 91) {
        rowNum += c - 65 + rowMultiplier * 26;
        rowMultiplier += 1;
      }
      else if (c > 96 && c < 123) {
        rowNum += c - 97 + rowMultiplier * 26;
        rowMultiplier += 1;
      }
      else if (c > 47 && c < 58) {
        colNum.append(c);
      }
      else {
        throw new IllegalArgumentException("Illegal character found: " + c);
      }
    }
    coords.add(Integer.parseInt(colNum.toString()) - 1);
    coords.add(rowNum);
    return coords;
  }

  /**
   * Returns the number of spreadsheets.
   *
   * @return The number of spreadsheets.
   */
  public int getNumSpreadsheets() {
    return numSpreadsheets;
  }

  /**
   * Sets the number of spreadsheets.
   *
   * @param numSpreadsheets The number of spreadsheets.
   */
  public void setNumSpreadsheets(int numSpreadsheets) {
    this.numSpreadsheets = numSpreadsheets;
  }

}
