/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import dev.bjdelacruz.katas.utils.Kata;
import dev.bjdelacruz.katas.utils.KataUtils;
import dev.bjdelacruz.commons.utils.StringUtils;

/**
 * Reads in strings from a file and determines their positions in the ordered sequence
 * of permutations of their constituent characters.
 * 
 * @see <a href="https://bjdelacruz.dev/files/katas/153_Permalex.pdf">Permalex</a>
 * 
 * @author BJ Peter DeLaCruz
 */
public class Permalex extends Kata {

  /**
   * Creates a new Permalex object.
   */
  public Permalex() {
    // Empty constructor.
  }

  /**
   * For each string in the input file, prints out the position number at which the string is
   * located in the list of sorted permutations of the characters in the string.
   */
  @SuppressWarnings("deprecation")
  @Override
  public void processLines() {
    while (true) {

      var line = this.getLines().removeFirst();
      if ("#".equals(line)) { // EOF
        break;
      }

      var letters = StringUtils.toCharacterList(line);
      var strings = KataUtils.makeStringsList(new ArrayList<>(letters));
      strings = new ArrayList<>(new HashSet<>(strings));
      Collections.sort(strings);

      System.out.print((strings.indexOf(StringUtils.toString(letters)) + 1) + "\n");

    }
  }

}
