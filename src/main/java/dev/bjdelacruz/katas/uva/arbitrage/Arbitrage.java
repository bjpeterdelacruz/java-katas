/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva.arbitrage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import dev.bjdelacruz.katas.utils.Kata;
import dev.bjdelacruz.katas.utils.KataEnums;
import dev.bjdelacruz.katas.utils.KataUtils;

/**
 * This program creates one or more currency exchange tables and finds the exchange sequence that
 * results in the highest profit for each currency exchange table.
 * 
 * @see <a href="https://bjdelacruz.dev/files/katas/104_Arbitrage.pdf">Arbitrage</a>
 * 
 * @author BJ Peter DeLaCruz
 */
public class Arbitrage extends Kata {

  private List<CurrencyExchange> exchanges = new ArrayList<>();

  /**
   * Processes a currency exchange table and finds the exchange sequence that results in the highest
   * profit.
   */
  @Override
  public void processLines() {
    while (!this.getLines().isEmpty()) {
      // Get the dimension of the currency exchange table.
      var dim = getLines().removeFirst();
      int dimension;
      try {
        dimension = Integer.parseInt(dim);
        if (dimension < 2) {
          throw new NumberFormatException("Dimension must be at least 2.");
        }
      }
      catch (NumberFormatException e) {
        throw new RuntimeException("Invalid dimension found: " + dim);
      }

      var table = createTable(dimension);

      // Create list of sequences from 1 to dimension.
      var characters = new ArrayList<Character>();
      for (char index = 49; index < (dimension + 49); index++) {
        characters.add(index);
      }

      var sequences = getAllExchangeSequences(characters);

      exchanges = calculateProfits(table, sequences);

      updateList(exchanges);

      // Print the exchange sequence that results in the highest profit.
      if (exchanges.isEmpty()) {
        System.out.println("\nNo arbitrage sequence exists.\n");
      }
      else {
        System.out.println("\n" + exchanges.get(exchanges.size() - 1) + "\n");
      }
    }
  }

  /**
   * Creates a table with N rows and N columns.
   * 
   * @param dimension Dimension of the table (number of rows and columns).
   * @return The table used to store the exchange rates.
   */
  private List<Double[]> createTable(int dimension) {
    var table = new ArrayList<Double[]>();

    System.out.println("Currency Exchange Table:");
    for (var index = 0; index < dimension; index++) {
      var row = new Double[dimension];
      @SuppressWarnings("unchecked")
      var data = (List<Double>) KataUtils.createList(this.getLines().removeFirst(), " ", KataEnums.DOUBLE);
      for (var pos = 0; pos < row.length; pos++) {
        if (pos == index) {
          row[pos] = 1.0;
        }
        else {
          row[pos] = data.removeFirst();
        }
      }
      table.add(row);

      for (var d : row) {
        System.out.print(d + "\t");
      }
      System.out.println();
    }

    return table;
  }

  /**
   * Gets all exchange sequences.
   * 
   * @param characters The characters used to generate all exchange sequences.
   * @return The list of strings representing exchange sequences whose lengths are from 2 to N.
   */
  private List<String> getAllExchangeSequences(List<Character> characters) {
    var permutations = KataUtils.getAllPermutationsOfSubsequences(new HashSet<>(characters));
    Collections.sort(permutations);

    // Delete all strings with lengths less than 2.
    var deletedStrings = permutations.stream().filter(perm -> perm.length() <= 1).toList();
    permutations.removeAll(deletedStrings);

    for (var index = 0; index < permutations.size(); index++) {
      var combo = permutations.get(index);
      permutations.set(index, combo + combo.charAt(0));
    }
    Collections.sort(permutations);

    return permutations;
  }

  /**
   * Calculates the profits made by all exchange sequence.
   * 
   * @param table The table containing the exchange rates.
   * @param sequences The list of exchange sequences.
   * @return A list of <code>CurrencyExchange</code> objects that contain exchange sequences and
   * profits.
   */
  private List<CurrencyExchange> calculateProfits(List<Double[]> table, List<String> sequences) {
    var exchanges = new ArrayList<CurrencyExchange>();
    for (var str : sequences) {
      var positions = str.toCharArray();
      var row = Integer.parseInt((positions[0] - 49) + "");
      var result = table.get(row)[row];
      for (var position : positions) {
        var col = Integer.parseInt((position - 49) + "");
        result *= table.get(row)[col];
        row = col;
      }
      if (result > 1.00) {
        exchanges.add(new CurrencyExchange(str, result));
      }
    }
    return exchanges;
  }

  /**
   * Removes all exchange sequences that are longer than the shortest exchange sequences and removes
   * all but one exchange sequence that result in the same profit.
   * 
   * @param exchanges The list of exchange sequences to update.
   */
  private void updateList(List<CurrencyExchange> exchanges) {
    Collections.sort(exchanges);
    // Remove the longest exchanges.
    for (var index = 0; index < exchanges.size() - 1; index++) {
      if (exchanges.get(index).exchangeSequence().length() < exchanges.get(index + 1).exchangeSequence().length()) {
        exchanges.remove(index + 1);
        index--;
      }
    }

    // Remove all but one exchange that result in the same profit.
    for (var index = 0; index < exchanges.size() - 1; index++) {
      var profit1 = exchanges.get(index).profit();
      var profit2 = exchanges.get(index + 1).profit();
      if (profit1.compareTo(profit2) == 0) {
        exchanges.remove(index + 1);
        index--;
      }
    }
  }

  /**
   * Returns the list of currency exchanges.
   *
   * @return The list of currency exchanges.
   */
  public List<CurrencyExchange> getExchanges() {
    return new ArrayList<>(exchanges);
  }
}
