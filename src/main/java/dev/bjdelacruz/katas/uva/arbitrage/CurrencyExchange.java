/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva.arbitrage;

/**
 * This class is used by the Arbitrage class to store a currency exchange sequence and the profit
 * one makes by using it.
 * 
 * @author BJ Peter DeLaCruz
 * @param exchangeSequence The sequence of currency exchanges
 * @param profit The profit amount
 */
public record CurrencyExchange(String exchangeSequence, Double profit) implements Comparable<CurrencyExchange> {

  /** {@inheritDoc} */
  @Override
  public int compareTo(CurrencyExchange exchange) {
    return profit.compareTo(exchange.profit);
  }

}
