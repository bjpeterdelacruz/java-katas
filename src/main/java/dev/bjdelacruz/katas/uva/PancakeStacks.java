/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.katas.uva;

import java.util.ArrayList;
import java.util.List;

import dev.bjdelacruz.katas.utils.Kata;
import dev.bjdelacruz.katas.utils.KataEnums;
import dev.bjdelacruz.katas.utils.KataUtils;
import dev.bjdelacruz.commons.utils.StringUtils;

/**
 * Sorts one or more stacks of pancakes that were read in from a file from smallest to biggest.
 * The biggest pancake will be at the bottom of the stack, and the smallest one will be at the top.
 * 
 * @see <a href="https://bjdelacruz.dev/files/katas/120_Pancake_Stacks.pdf">Pancake
 * Stacks</a>
 * 
 * @author BJ Peter DeLaCruz
 */
public class PancakeStacks extends Kata {

  private final List<List<Integer>> stacks = new ArrayList<>();
  private final List<Integer> positions = new ArrayList<>();
  private int headIndex;

  /**
   * Reads in one or more stacks of pancakes and then sorts each stack.
   */
  @SuppressWarnings("unchecked")
  @Override
  public void processLines() {
    for (var s : this.getLines()) {
      var list = (List<Integer>) KataUtils.createList(s, " ", KataEnums.INTEGER);
      this.stacks.add(list);
    }

    flipPancakes();
  }

  /**
   * Sorts each of the stacks of pancakes found in the input file, and then displays each original
   * stack of pancakes and also the number of flips it took to sort the pancakes from smallest to
   * biggest.
   */
  private void flipPancakes() {
    for (var stack : this.stacks) {
      System.out.println("Stack:     " + StringUtils.getListContents(stack));
      var size = stack.size();
      while (!this.isSorted(stack)) {
        flip(stack.subList(0, size--));
      }
      this.positions.add(0);
      System.out.println("Positions: " + StringUtils.getListContents(this.positions) + "\n");
      this.positions.clear();
      this.headIndex = 1;
    }
  }

  /**
   * Given the entire stack or a subset of pancakes, finds the biggest pancake, and then flips that
   * pancake along with the other ones on top of it.
   * 
   * @param stack The stack of pancakes to flip.
   */
  private void flip(List<Integer> stack) {
    if (stack == null || stack.isEmpty()) {
      throw new IllegalArgumentException("Stack is null or empty.");
    }

    var maxPos = 0;
    for (var index = 0; index < stack.size() - 1; index++) {
      if (stack.get(maxPos) < stack.get(index + 1)) {
        maxPos = index + 1;
      }
    }

    // The pancake with the biggest diameter is at the beginning of the list, so just flip the
    // pancakes from the bottom so that it will be at the bottom. Otherwise, flip the pancakes at
    // the position found, and then flip them again from the bottom so that the biggest pancake will
    // be at the bottom.
    if (maxPos == 0) {
      flip(stack, stack.size() - 1);
      this.positions.add(this.headIndex);
    }
    else if (maxPos != stack.size() - 1) {
      flip(stack, maxPos);
      this.positions.add(stack.size() - maxPos);
      flip(stack, stack.size() - 1);
      this.positions.add(this.headIndex);
    }
    this.headIndex++;
  }

  /**
   * Flips a subset of pancakes from under the pancake at position <code>length</code>.
   * 
   * @param stack The stack of pancakes.
   * @param length The number of pancakes to flip.
   */
  private void flip(List<Integer> stack, int length) {
    if (stack == null || stack.isEmpty()) {
      throw new IllegalArgumentException("Stack is null or empty.");
    }
    if (length < 0) {
      throw new IllegalArgumentException("Length is less than 0.");
    }
    else if (length > stack.size()) {
      throw new IllegalArgumentException("Length cannot be greater than " + stack.size() + ".");
    }
    var numSwaps = Math.ceil(length / 2.0);
    for (int head = 0, tail = length; head < numSwaps; head++, tail--) {
      var temp = stack.get(tail);
      stack.set(tail, stack.get(head));
      stack.set(head, temp);
    }
  }

  /**
   * Returns <code>true</code> if a stack of pancakes is sorted from smallest to largest, beginning
   * from the top of the stack; <code>false</code> otherwise.
   * 
   * @param stack The stack of pancakes.
   * @return True if the pancakes are sorted from smallest to biggest, false otherwise.
   */
  private boolean isSorted(List<Integer> stack) {
    if (stack == null || stack.isEmpty()) {
      throw new IllegalArgumentException("Stack is null or empty.");
    }
    for (var index = 0; index < stack.size() - 1; index++) {
      if (stack.get(index) > stack.get(index + 1)) {
        return false;
      }
    }
    return true;
  }

}
