package dev.bjdelacruz.examples;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Gatherer;

/**
 * This class demonstrates a new preview feature in Java 22 called a stream gatherer.
 */
@SuppressWarnings("preview")
public class GathererImpl implements Gatherer<String, Set<Integer>, String> {

    /**
     * Returns a {@link Supplier} that generates a new {@link HashSet} for storing the lengths of
     * strings.
     *
     * @return A {@link Supplier} that generates a new {@link HashSet}.
     */
    @Override
    public Supplier<Set<Integer>> initializer() {
        return HashSet::new;
    }

    /**
     * Returns an {@link java.util.stream.Gatherer.Integrator} that will calculate the length of
     * a string, store its length into a {@link HashSet}, and finally pass the string to a
     * downstream operation. Each string in downstream operations will be unique with respect to
     * its length, not its content.
     *
     * @return An integrator.
     */
    @Override
    public Integrator<Set<Integer>, String, String> integrator() {
        return Integrator.ofGreedy((state, item, downstream) -> {
            // If the current string has the same length as one that has already been processed,
            // skip the current string.
            if (!state.contains(item.length())) {
                state.add(item.length());
                // Push the current string to the next downstream operation.
                downstream.push(item);
            }
            return true;
        });
    }

    /**
     * Demonstrates the use of {@link GathererImpl}.
     */
    void main() {
        var words = List.of("foo", "bar", "baz", "foobar", "abcxyz", "helloworld");
        words.stream().gather(new GathererImpl()).forEach(System.out::println);
    }
}
