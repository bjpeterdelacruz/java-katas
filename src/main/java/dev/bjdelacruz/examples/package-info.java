/**
 * A package that contains code examples for use in live demonstrations and training.
 */
package dev.bjdelacruz.examples;
