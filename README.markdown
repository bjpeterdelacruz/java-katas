# Java Katas

Katas are programming exercises designed to help programmers improve their skills through practice and repetition.

### Codewars

1. [8kyu Interpreters: HQ9+](https://www.codewars.com/kata/591588d49f4056e13f000001/java)
2. [A + B](https://www.codewars.com/kata/5512a0b0509063e57d0003f5/java)
3. [A Wolf in Sheep's Clothing](https://www.codewars.com/kata/5c8bfa44b9d1192e1ebd3d15/java)
4. [Age in Days](https://www.codewars.com/kata/5803753aab6c2099e600000e/java)
5. [Antistring](https://www.codewars.com/kata/5ab349e01aaf060cd0000069/java)
6. [Are You Playing Banjo?](https://www.codewars.com/kata/53af2b8861023f1d88000832/java)
7. [Area or Perimeter](https://www.codewars.com/kata/5ab6538b379d20ad880000ab/java)
8. [Array Plus Array](https://www.codewars.com/kata/5a2be17aee1aaefe2a000151/java)
9. [Bifid Cipher](https://www.codewars.com/kata/5c4a0fe93c3cc13c8b23d802/java)
10. [Caffeine Script](https://www.codewars.com/kata/5434283682b0fdb0420000e6/java)
11. [Changing Letters](https://www.codewars.com/kata/5831c204a31721e2ae000294/java)
12. [Center of the Matrix](https://www.codewars.com/kata/54c91b5228ec4c3b5900036e/java)
13. [Check Three and Two](https://www.codewars.com/kata/5a9e86705ee396d6be000091/java)
14. [Collatz](https://www.codewars.com/kata/5286b2e162056fd0cb000c20/java)
15. [Collatz Conjecture Length](https://www.codewars.com/kata/54fb963d3fe32351f2000102/java)
16. [Color Ghost](https://www.codewars.com/kata/53f1015fa9fe02cbda00111a/java)
17. [Compare with Margin](https://www.codewars.com/kata/56453a12fcee9a6c4700009c/java)
18. [Conference Traveller](https://www.codewars.com/kata/56f5594a575d7d3c0e000ea0/java)
19. [Convert Time to String](https://www.codewars.com/kata/5502ddd734137e90af000f62/java)
20. [Correct the Time String](https://www.codewars.com/kata/57873ab5e55533a2890000c7/java)
21. [Count of Positives / Sum of Negatives](https://www.codewars.com/kata/576bb71bbbcf0951d5000044/java)
22. [Count the Monkeys!](https://www.codewars.com/kata/56f69d9f9400f508fb000ba7/java)
23. [Count the Number of Days Between Two Dates](https://www.codewars.com/kata/59569ba543e2a8ebda00012a/java)
24. [Counter](https://www.codewars.com/kata/5a3832c18882f32aca0000d2/java)
25. [Covfefe](https://www.codewars.com/kata/592fd8f752ee71ac7e00008a/java)
26. [Decimal Decomposition](https://www.codewars.com/kata/581f5cf739dc42ac2400007b/java)
27. [Decipher the Message](https://www.codewars.com/kata/57452748976d65946d000599/java)
28. [Difference Between Years (Level 1)](https://www.codewars.com/kata/588f5a38ec641b411200005b/java)
29. [Double Sort](https://www.codewars.com/kata/57cc79ec484cf991c900018d/java)
30. [Error Correction #1 - Hamming Code](https://www.codewars.com/kata/5ef9ca8b76be6d001d5e1c3e/java)
31. [Execute Me nTimes](https://www.codewars.com/kata/5b2b4836b6989d207700005b/java)
32. [Fibonacci](https://www.codewars.com/kata/57a1d5ef7cb1f3db590002af/java)
33. [Find Count of Most Frequent Item in an Array](https://www.codewars.com/kata/56582133c932d8239900002e/java)
34. [Find Maximum and Minimum Values of a List](https://www.codewars.com/kata/577a98a6ae28071780000989/java)
35. [Find Numbers Which Are Divisible by Given Number](https://www.codewars.com/kata/55edaba99da3a9c84000003b/java)
36. [The First Non Repeated Character In A String](https://www.codewars.com/kata/570f6436b29c708a32000826/solutions/java)
37. [Four/Seven](https://www.codewars.com/kata/5ff50f64c0afc50008861bf0/java)
38. [Hide Password from JDBC URL](https://www.codewars.com/kata/5a726f16373c2ee6c60000db/java)
39. [Hours to Seconds](https://www.codewars.com/kata/596b041e224071ece200002e/java)
40. [If You Can't Sleep, Just Count Sheep!!](https://www.codewars.com/kata/5b077ebdaf15be5c7f000077/java)
41. [Inspiring Strings](https://www.codewars.com/kata/5939ab6eed348a945f0007b2/java)
42. [Java Functional Programming (Part 1: The Beginning)](https://www.codewars.com/kata/54a6b43e478d8ee14c000a5d/java)
43. [Java Functional Programming (Part 3: Closured for Business)](https://www.codewars.com/kata/54a6d39a478d8e07e4000b69/java)
44. [Java Functional Programming (Part 4: Row Row Row Your Boat, Gently Down the...)](https://www.codewars.com/kata/54ac045d35adf4e6e2000e43/java)
45. [Large Factorials](https://www.codewars.com/kata/557f6437bf8dcdd135000010/java)
46. [Mean vs. Median](https://www.codewars.com/kata/5806445c3f1f9c2f72000031/java)
47. [Name Array Capping](https://www.codewars.com/kata/5356ad2cbb858025d800111d/java)
48. [One, Two, Three, Four](https://www.codewars.com/kata/5b13c699af73864aa7000031/java)
49. [Persistent Bugger](https://www.codewars.com/kata/55bf01e5a717a0d57e0000ec/java)
50. [Poker Hand, Straight or Not?](https://www.codewars.com/kata/582afcadac2d9baa0900054c/java)
51. [Powers of Two](https://www.codewars.com/kata/534d0a229345375d520006a0/java)
52. [Printing Array Elements with Comma Delimiters](https://www.codewars.com/kata/56e2f59fb2ed128081001328/java)
53. [Remove Duplicates](https://www.codewars.com/kata/53e30ec0116393fe1a00060b/java)
54. [Responsible Drinking](https://www.codewars.com/kata/5aee86c5783bb432cd000018/java)
55. [Run, Runner!](https://www.codewars.com/kata/596b2b9d9e2fbd7811000046/java)
56. [Sentence Smash](https://www.codewars.com/kata/53dc23c68a0c93699800041d/java)
57. [Show Multiples of Two Numbers Within a Range](https://www.codewars.com/kata/583989556754d6f4c700018e/java)
58. [Some Fun with Aggregate Operations (Part 1): Get Average Grade for Every Department](https://www.codewars.com/kata/595fa01cde9d341e8c000045/java)
59. [Some Fun with Aggregate Operations (Part 2): Get Number of Students by Department](https://www.codewars.com/kata/596092e42e8c8b5382000026/java)
60. [Some Fun with Aggregate Operations (Part 3): Get Names of Students by Department](https://www.codewars.com/kata/5960e6cf09868d7f2f0000bc/java)
61. [Some Fun with Aggregate Operations (Part 4): Get Number of Students by Gender for Each Department](https://www.codewars.com/kata/59623e9450091000150000d2/java)
62. [Sort Arrays - 1](https://www.codewars.com/kata/51f41b98e8f176e70d0002a8/java)
63. [Sort My Textbooks](https://www.codewars.com/kata/5a07e5b7ffe75fd049000051/java)
64. [Sort the Columns of a CSV File](https://www.codewars.com/kata/57f7f71a7b992e699400013f/java)
65. [Sort the Odd Way!](https://www.codewars.com/kata/57fb79784e2d0639c9000066/java)
66. [Static Electrickery](https://www.codewars.com/kata/596c55fc7bd5476bf60000d5/java)
67. [String Repeat](https://www.codewars.com/kata/57a0e5c372292dd76d000d7e/java)
68. [String Task](https://www.codewars.com/kata/598ab63c7367483c890000f4/java)
69. [Stringy Strings](https://www.codewars.com/kata/563b74ddd19a3ad462000054/java)
70. [Surreal Numbers Construction Rule](https://www.codewars.com/kata/5a24e2258f27f2bcb50000c7/java)
71. [Suzuki Needs Help Lining Up His Students!](https://www.codewars.com/kata/5701800886306a876a001031/java)
72. [World Bits War](https://www.codewars.com/kata/58865bfb41e04464240000b0/java)
73. [YaCG: #2 One Trick, One Taker](https://www.codewars.com/kata/5977ae37fd72c64008000081/java)

### LeetCode

1. [Two Sum](https://leetcode.com/problems/two-sum/)

### University of Valladolid (UVa) Katas (pre-2021)
1. [3n + 1](https://bjdelacruz.dev/files/katas/100_3n_plus_1.pdf)
2. [Arbitrage](https://bjdelacruz.dev/files/katas/104_Arbitrage.pdf)
3. [Bangla Numbers](https://bjdelacruz.dev/files/katas/10101_Bangla_Numbers.pdf)
4. [Blocks](https://bjdelacruz.dev/files/katas/101_blocks.pdf)
5. [Consecutive Primes](https://bjdelacruz.dev/files/katas/1210_Consecutive_Primes.pdf)
6. [Clock Patience](https://bjdelacruz.dev/files/katas/170_Clock_Patience.pdf)
7. [Crazy Calculator](https://bjdelacruz.dev/files/katas/354_Crazy_Calculator.pdf)
8. [Digit Primes](https://bjdelacruz.dev/files/katas/10533_digit_primes.pdf)
9. [Following Orders](https://bjdelacruz.dev/files/katas/124_Following_Orders.pdf)
10. [History Grading](https://bjdelacruz.dev/files/katas/111_History_Grading.pdf)
11. [Intersection](https://bjdelacruz.dev/files/katas/191_Intersection.pdf)
12. [Keywords](https://bjdelacruz.dev/files/katas/175_Keywords.pdf)
13. [Linear Equation Solver](https://bjdelacruz.dev/files/katas/1200_Linear_Equation.pdf)
14. [Maximum Sum](https://bjdelacruz.dev/files/katas/108_max_sum.pdf)
15. [Minesweeper](https://bjdelacruz.dev/files/katas/10189_Minesweeper.pdf)
16. [Mouse Clicks](https://bjdelacruz.dev/files/katas/142_Mouse_Clicks.pdf)
17. [Pancake Stacks](https://bjdelacruz.dev/files/katas/120_Pancake_Stacks.pdf)
18. [Permalex](https://bjdelacruz.dev/files/katas/153_Permalex.pdf)
19. [Roman Roulette](https://bjdelacruz.dev/files/katas/130_Roman_Roulette.pdf)
20. [Searching Quickly](https://bjdelacruz.dev/files/katas/123_Searching_Quickly.pdf)
21. [Spreadsheet](https://bjdelacruz.dev/files/katas/196_Spreadsheet.pdf)
22. [Squares](https://bjdelacruz.dev/files/katas/201_Squares.pdf)
23. [Transaction Processing](https://bjdelacruz.dev/files/katas/187_Transaction_Processing.pdf)
24. [Tree Level](https://bjdelacruz.dev/files/katas/122_Tree_Level.pdf)
25. [Unix ls](https://bjdelacruz.dev/files/katas/400_Unix_Ls.pdf)
26. [Word Crosses](https://bjdelacruz.dev/files/katas/159_Word_Crosses.pdf)

### Other Katas

1. **Adjacent Friday the 13th**. Calculates the nearest date that falls on Friday the 13th.
2. **Bowling**. Calculates a bowler's final score given a string of scores.
3. **Line Wrapper**. Returns a string with line breaks such that no line exceeds the max length.

## Developer Guide
1. Install the following:
    * Java 23 JDK or higher

2. Run `./gradlew clean build` to compile all the classes and execute Checkstyle, PMD, SpotBugs, and JUnit against the 
   source code.

## Links

- [Developer's Website](https://bjdelacruz.dev)
- [Programming Blog](https://javaprogramminglog.blogspot.com)
- [Codewars](https://www.codewars.com/users/bjpeterdelacruz/stats)
- [LeetCode](https://leetcode.com/bjpeterdelacruz)
